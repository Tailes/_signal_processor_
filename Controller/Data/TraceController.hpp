#pragma once
#include "..\..\Model\Data\Batch.hpp"

#include <cstddef>
#include <string>
#include <array>


class Trace;



class TraceController
{
public:
	TraceController(Trace &model);
	
	void clear();
	void save(const std::array<std::wstring, std::tuple_size<Batch>::value> &names);
	
	void on_ethernet_tick_length(std::float_t length);
	void on_ethernet_steps(std::uint8_t steps);
	void on_ethernet_scans(std::uint8_t scans);
	
	void time_frame(std::float_t value);
	void distance(std::float_t value);
	void comments(const std::string &value);
	void epsilon(std::float_t value);
	void tagc(std::float_t value);
	
	std::float_t survey_time() const;
	std::float_t time_frame() const;
	std::float_t distance() const;
	std::float_t epsilon() const;
	std::float_t tagc() const;
	std::string comments() const;
	std::size_t count() const;
	
	bool is_dirty() const;
	
private:
	Trace &_model;
};

