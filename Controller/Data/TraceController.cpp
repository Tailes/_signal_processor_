#include "..\..\stdafx.hpp"
#include "..\..\Model\Data\Trace.hpp"
#include "..\..\Controller\Data\TraceController.hpp"



TraceController::TraceController(Trace &model):
	_model(model)
{
}

void TraceController::clear()
{
	_model.clear();
}

void TraceController::save(const std::array<std::wstring, std::tuple_size<Batch>::value> &names)
{
	_model.save(names);
}

void TraceController::on_ethernet_tick_length(std::float_t length)
{
	_model.step_length(length);
}

void TraceController::on_ethernet_steps(std::uint8_t steps)
{
	_model.step_count(std::max<std::uint8_t>(steps, 1));
}

void TraceController::on_ethernet_scans(std::uint8_t scans)
{
	_model.scan_count(scans);
}

std::string TraceController::comments() const
{
	return _model.comments();
}

std::size_t TraceController::count() const
{
	return _model.count();
}

std::float_t TraceController::survey_time() const
{
	return static_cast<std::float_t>(_model.survey_time());
}

std::float_t TraceController::time_frame() const
{
	return static_cast<std::float_t>(_model.time_frame());
}

std::float_t TraceController::distance() const
{
	return static_cast<std::float_t>(_model.distance());
}

std::float_t TraceController::epsilon() const
{
	return static_cast<std::float_t>(_model.epsilon());
}

std::float_t TraceController::tagc() const
{
	return static_cast<std::float_t>(_model.tagc());
}



void TraceController::time_frame(std::float_t frame)
{
	_model.time_frame(static_cast<float>(frame));
}

void TraceController::distance(std::float_t distance)
{
	_model.distance(static_cast<float>(distance));
}

void TraceController::comments(const std::string &comment)
{
	_model.comments(comment);
}

void TraceController::epsilon(std::float_t epsilon)
{
	_model.epsilon(static_cast<float>(epsilon));
}

void TraceController::tagc(std::float_t tagc)
{
	_model.tagc(static_cast<float>(tagc));
}

bool TraceController::is_dirty() const
{
	return _model.is_dirty();
}