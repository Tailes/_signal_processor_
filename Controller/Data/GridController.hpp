#pragma once




namespace WTL { template<bool> class CDCT; }



class GridController
{
public:
	GridController(const WTL::CDCT<false> &dc, const float begin, const float end, const int screen_width);
	
	float first_line() const;
	float last_line() const;
	float value(const float x) const;
	float label(const float line) const;
	float part(const float line) const;
	
private:
	const float _begin;
	const float _step;
	const float _end;
	
	float step(const float span, const float labels_per_width) const;
};