#include "..\..\stdafx.hpp"
#include "..\..\Controller\Data\GridController.hpp"

#include <array>
#include <cmath>


static const float Grids[] = {{0.1f}, {0.2f}, {0.25f}, {0.5f}, {1.0f}, {2.f}, {2.5f}, {5.0f}, {10.0f}, {20.0f}, {25.0f}, {50.0f}};
static const float PrecisionCoeff = 100.f;
static const float LabelWidth = 80.0f; //pix
static const float PsToNs = 1000.f;



GridController::GridController(const WTL::CDCT<false> &dc, const float begin, const float end, const int screen_width):
	_begin(begin), _end(end), _step(this->step(end - begin, static_cast<float>(screen_width) / LabelWidth))
{
}

float GridController::first_line() const
{
	return std::ceil(_begin / _step);
}

float GridController::last_line() const
{
	return std::ceil(_end / _step);
}

float GridController::value(const float x) const
{
	return std::nearbyint((_begin + x*(_end - _begin)) * PrecisionCoeff / PsToNs) / PrecisionCoeff;
}

float GridController::part(const float line) const
{
	return (line * _step - _begin) / (_end - _begin);
}

float GridController::label(const float line) const
{
	return  std::nearbyint(line * _step * PrecisionCoeff / PsToNs) / PrecisionCoeff;
}

float GridController::step(const float span, const float labels_per_width) const
{
	const float * cit = std::cbegin(Grids);
	while ( cit != std::cend(Grids) && labels_per_width < std::nearbyint(span / (PsToNs * *cit)) ) ++cit;
	return *cit * PsToNs;
}