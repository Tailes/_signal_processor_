#pragma once
#include <functional>



template<class ...Args>
std::function<void(Args...)> operator+(const std::function<void(Args...)> &left, const std::function<void(Args...)> &right)
{
	return left ? [=](Args... args){left(args...); right(args...); } : right;
}