#include "..\..\stdafx.hpp"
#include "..\..\Model\Hardware\HardwareDataSource.hpp"
#include "..\..\Controller\Hardware\HardwareDataSourceController.hpp"



HardwareDataSourceController::HardwareDataSourceController(HardwareDataSource &model):
	_model(model),
	_ethernet_data_source_controller(model.ethernet_data_source()),
	_serial_data_source_controller(model.serial_data_source())
{
}

void HardwareDataSourceController::load(const std::wstring &root)
{
	_serial_data_source_controller.load(root);
	_ethernet_data_source_controller.load(root);
}

void HardwareDataSourceController::save(const std::wstring &root) const
{
	_ethernet_data_source_controller.save(root);
	_serial_data_source_controller.save(root);
}

void HardwareDataSourceController::mode(HardwareDataSourceMode value)
{
	_model.mode(value);
}

HardwareDataSourceMode HardwareDataSourceController::mode() const
{
	return _model.mode();
}

const Ethernet::DataSourceController& HardwareDataSourceController::ethernet_data_source() const
{
	return _ethernet_data_source_controller;
}

const Serial::DataSourceController& HardwareDataSourceController::serial_data_source() const
{
	return _serial_data_source_controller;
}

Ethernet::DataSourceController& HardwareDataSourceController::ethernet_data_source()
{
	return _ethernet_data_source_controller;
}

Serial::DataSourceController& HardwareDataSourceController::serial_data_source()
{
	return _serial_data_source_controller;
}
