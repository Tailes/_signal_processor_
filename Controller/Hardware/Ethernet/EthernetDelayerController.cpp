#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetDelayer.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTypeRanges.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetDelayerController.hpp"


namespace Ethernet
{
	DelayerController::DelayerController(Delayer &model):
		_model(model), _timer(model.timer()), _counter(model.counter())
	{
		_model.limit(DelayMin, DelayMax);
	}

	const DelayerModeCounterController & DelayerController::counter() const
	{
		return _counter;
	}

	const DelayerModeTimerController & DelayerController::timer() const
	{
		return _timer;
	}

	const Delayer& DelayerController::delayer() const
	{
		return _model;
	}

	DelayerMode DelayerController::mode() const
	{
		return _model.mode();
	}

	Delay DelayerController::delta() const
	{
		return _model.delta();
	}

	Delay DelayerController::upper_limit() const
	{
		return _model.upper_limit();
	}

	Delay DelayerController::lower_limit() const
	{
		return _model.lower_limit();
	}

	bool DelayerController::enabled() const
	{
		return _model.enabled();
	}

	DelayerModeCounterController & DelayerController::counter()
	{
		return _counter;
	}

	DelayerModeTimerController & DelayerController::timer()
	{
		return _timer;
	}

	Delayer& DelayerController::delayer()
	{
		return _model;
	}

	void DelayerController::direction(bool forward)
	{
		_model.direction(forward);
	}

	void DelayerController::enable(bool enabled)
	{
		_model.enable(enabled);
	}

	void DelayerController::delta(Delay delay)
	{
		_model.delta(delay);
	}

	void DelayerController::limit(Delay lower, Delay upper)
	{
		_model.limit(lower, upper);
	}

	void DelayerController::mode(DelayerMode mode)
	{
		_model.mode(mode);
	}
}