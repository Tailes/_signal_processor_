#pragma once
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <limits>

namespace Ethernet
{
	static const std::uint16_t GeneratorSyncFrequencyMin = 0;
	static const std::uint16_t GeneratorSyncFrequencyMax = 1000;
	static const std::uint16_t GeneratorSyncFrequencyStep = 5;
	static const std::uint16_t GeneratorSyncFrequencyDefault = 100;

	static const std::uint16_t ProbingPeriodMin = 0;
	static const std::uint16_t ProbingPeriodMax = 100;

	static const std::int32_t DelayMin = 0;
	static const std::int32_t DelayMax = 255*1024;
	static const std::int32_t DelayStep = 1;

	static const std::uint8_t ChargesCountMin = 1;
	static const std::uint8_t ChargesCountMax = 32;

	static const std::uint8_t StrobePulseLengthMin = 1;
	static const std::uint8_t StrobePulseLengthStep = 1;
	static const std::uint8_t StrobePulseLengthMax = 255;

	static const std::uint8_t ScansPerTriggerMin = 1;
	static const std::uint8_t ScansPerTriggerStep = 1;
	static const std::uint8_t ScansPerTriggerMax = 255;

	static const std::uint8_t StepsPerTriggerMin = 1;
	static const std::uint8_t StepsPerTriggerStep = 1;
	static const std::uint8_t StepsPerTriggerMax = 255;

	static const std::uint8_t HomogenousAccumulationMin = 1;
	static const std::uint8_t HomogenousAccumulationStep = 1;
	static const std::uint8_t HomogenousAccumulationMax = 16;
}