#include "..\..\..\..\Model\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmSlice.hpp"
#include "..\..\..\..\Controller\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmSliceController.hpp"


namespace Ethernet
{

	ScanningAlgorithmSliceController::ScanningAlgorithmSliceController(ScanningAlgorithmSlice & model):
		_model{model}
	{
		_channels = _model.channels();
	}

	void ScanningAlgorithmSliceController::enable(Channel channel)
	{
		_channels.insert(channel);
		_model.channels(_channels);
	}

	void ScanningAlgorithmSliceController::disable(Channel channel)
	{
		_channels.erase(channel);
		if (_channels.empty())
		{
			throw std::runtime_error("At least one channel must be selected");
		}
		_model.channels(_channels);
	}

	bool ScanningAlgorithmSliceController::enabled(Channel channel) const
	{
		return _channels.find(channel) != _channels.end();
	}
}