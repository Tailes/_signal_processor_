#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <set>


namespace Ethernet
{
	class ScanningAlgorithmSlice;



	class ScanningAlgorithmSliceController
	{
	public:
		ScanningAlgorithmSliceController(ScanningAlgorithmSlice &model);
		
		void enable(Channel channel);
		void disable(Channel channel);
		
		bool enabled(Channel channel) const;
		
	private:
		ScanningAlgorithmSlice &_model;
		std::set<Channel> _channels;
	};
}