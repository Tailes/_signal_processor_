#pragma once
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetDelayerModes.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\DelayerModes\EthernetDelayerModeTimerController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\DelayerModes\EthernetDelayerModeCounterController.hpp"

#include <mutex>

namespace Ethernet
{
	class Delayer;


	class DelayerController
	{
	public:
		DelayerController(Delayer &model);
		
		const DelayerModeCounterController &counter() const;
		const DelayerModeTimerController &timer() const;
		const Delayer &delayer() const;
		DelayerMode mode() const;
		Delay upper_limit() const;
		Delay lower_limit() const;
		Delay delta() const;
		
		bool enabled() const;
		
		DelayerModeCounterController& counter();
		DelayerModeTimerController& timer();
		Delayer& delayer();
		
		void direction(bool forward);
		void enable(bool enabled);
		void limit(Delay lower, Delay upper);
		void delta(Delay delay);
		void mode(DelayerMode mode);
		
	private:
		std::recursive_mutex mutable _guard;
		DelayerModeCounterController _counter;
		DelayerModeTimerController _timer;
		Delayer& _model;
		Delay _delta;
	};
}