#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSignalSource.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetSignalSourceController.hpp"



namespace Ethernet
{
	SignalSourceController::SignalSourceController(SignalSource &model):
		_model(model), _socket_settings(model.settings()), _input(model.input()), _output(_model.output()), _tracker(_model.tracker()), _positioner(_model.positioner())
	{
	}

	void SignalSourceController::load(const std::wstring &root)
	{
		_output.load(root);
	}

	void SignalSourceController::save(const std::wstring &root) const
	{
		_output.save(root);
	}

	InputController& SignalSourceController::input()
	{
		return _input;
	}

	OutputController& SignalSourceController::output()
	{
		return _output;
	}

	TrackerController& SignalSourceController::tracker()
	{
		return _tracker;
	}

	PositionerController& SignalSourceController::positioner()
	{
		return _positioner;
	}

	SocketSettingsController& SignalSourceController::settings()
	{
		return _socket_settings;
	}

	const InputController& SignalSourceController::input() const
	{
		return _input;
	}

	const OutputController& SignalSourceController::output() const
	{
		return _output;
	}

	const TrackerController& SignalSourceController::tracker() const
	{
		return _tracker;
	}

	const PositionerController& SignalSourceController::positioner() const
	{
		return _positioner;
	}

	const SocketSettingsController& SignalSourceController::settings() const
	{
		return _socket_settings;
	}
}