#pragma once
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <cmath>



namespace Ethernet
{
	class Timer;
	class Timing;
	
	
	
	class TimerController
	{
	public:
		TimerController(Timer &model, const Timing &timing);
		
		void time_frame_sedecuple(TimeFrameSedecuple frame);
		void time_frame_quadruple(TimeFrameQuadruple frame);
		void time_frame_octuple(TimeFrameOctuple frame);
		void time_frame_single(TimeFrameSingle frame);
		void time_frame_double(TimeFrameDouble frame);
		void scanning_delay(Delay delay);
		void signal_length(SignalLength length);
		
		//TimeFrameSingle time_frame_single() const;
		//TimeFrameDouble time_frame_double() const;
		//std::uint16_t delay() const;
		
	private:
		const Timing &_timing;
		Timer &_model;
	};
}