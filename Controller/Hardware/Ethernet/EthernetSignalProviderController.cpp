#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalProvider.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetSignalProviderController.hpp"



namespace Ethernet
{
	SignalProviderController::SignalProviderController(SignalProvider &model):
		_model(model),
		_signal_recorder(model.signal_recorder()),
		_signal_source(model.signal_source())
	{
	}

	void SignalProviderController::load(const std::wstring & root)
	{
		_signal_source.load(root);
	}

	void SignalProviderController::save(const std::wstring & root) const
	{
		_signal_source.save(root);
	}

	const SignalRecorderController& SignalProviderController::signal_recorder() const
	{
		return _signal_recorder;
	}

	const SignalSourceController& SignalProviderController::signal_source() const
	{
		return _signal_source;
	}

	const SignalProvider & SignalProviderController::model() const
	{
		return _model;
	}



	SignalRecorderController& SignalProviderController::signal_recorder()
	{
		return _signal_recorder;
	}

	SignalSourceController& SignalProviderController::signal_source()
	{
		return _signal_source;
	}
}