#pragma once
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetDelayLines.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTimerController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetDelayerController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetChannelerController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetInterpolatorController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetSettingsController.hpp"



namespace Ethernet
{
	class DataSource;



	class DataSourceController
	{
		typedef std::array<std::uint16_t, DelayTotalLinesCount> Basis;
		
	public:
		DataSourceController(DataSource &model);
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		void advanced_settings_change_handler(std::function<void(bool)> callback);
		
		void delays_basis(const Basis &basis);
		void signal_length(SignalLength length);
		void scanning_delay(Delay delay);
		void time_frame_single(TimeFrameSingle frame);
		void time_frame_double(TimeFrameDouble frame);
		void time_frame_octuple(TimeFrameOctuple frame);
		void time_frame_quadruple(TimeFrameQuadruple frame);
		void time_frame_sedecuple(TimeFrameSedecuple frame);
		
		void advanced_settings(bool enable);
		
		Basis delays_basis() const;
		Delay scanning_delay() const;
		SignalLength signal_length() const;
		TimeFrameSingle time_frame_single() const;
		TimeFrameDouble time_frame_double() const;
		TimeFrameOctuple time_frame_octuple() const;
		TimeFrameQuadruple time_frame_quadruple() const;
		TimeFrameSedecuple time_frame_sedecuple() const;
		
		const InterpolatorController& interpolator() const;
		const ChannelerController& channeler() const;
		const SettingsController& settings() const;
		const DelayerController& delayer() const;
		const TimerController& timer() const;
		
		bool advanced_settings() const;
		
		InterpolatorController& interpolator();
		ChannelerController& channeler();
		SettingsController& settings();
		DelayerController& delayer();
		TimerController& timer();
		
	private:
		DataSource &_model;
		InterpolatorController _interpolator;
		ChannelerController _channeler;
		SettingsController _settings;
		DelayerController _delayer;
		TimerController _timer;
		
		std::function<void(bool)> _advanced_settings_change_handler;
		
		bool _enable_advanced_settings;
	};
}