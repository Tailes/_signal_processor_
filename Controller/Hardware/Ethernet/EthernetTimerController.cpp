#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetTimer.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTimerController.hpp"



namespace Ethernet
{
	TimerController::TimerController(Timer &model, const Timing &timing):
		_model(model), _timing(timing)
	{
	}




	void TimerController::signal_length(SignalLength length)
	{
		_model.signal_length(length, _timing);
	}

	void TimerController::time_frame_single(TimeFrameSingle frame)
	{
		_model.time_frame_single(frame, _timing);
	}

	void TimerController::time_frame_double(TimeFrameDouble frame)
	{
		_model.time_frame_double(frame, _timing);
	}

	void TimerController::time_frame_quadruple(TimeFrameQuadruple frame)
	{
		_model.time_frame_quadruple(frame, _timing);
	}

	void TimerController::time_frame_octuple(TimeFrameOctuple frame)
	{
		_model.time_frame_octuple(frame, _timing);
	}

	void TimerController::time_frame_sedecuple(TimeFrameSedecuple frame)
	{
		_model.time_frame_sedecuple(frame, _timing);
	}




	void TimerController::scanning_delay(Delay delay)
	{
		_model.scanning_delay(delay, _timing);
	}



	//TimeFrameSingle TimerController::time_frame_single() const
	//{
	//	return _model.time_frame_single();
	//}
	//
	//TimeFrameDouble TimerController::time_frame_double() const
	//{
	//	return _model.time_frame_double();
	//}

	//std::uint16_t TimerController::delay() const
	//{
	//	return _model.scanning_delay();
	//}
}