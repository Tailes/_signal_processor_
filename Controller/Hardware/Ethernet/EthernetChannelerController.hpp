#pragma once
#include "..\..\..\Controller\Hardware\Ethernet\EthernetSignalProviderController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetSettingsController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmSliceController.hpp"

#include <map>



namespace Ethernet
{
	class Channeler;
	
	
	
	class ChannelerController
	{
		using Selection = std::map<Mode, Channel>;
		
	public:
		ChannelerController(Channeler &model);
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		const ScanningAlgorithmSliceController& scanning_algorithm_slice() const;
		const SignalProviderController& signal_provider() const;
		
		ScanningAlgorithmSliceController& scanning_algorithm_slice();
		SignalProviderController& signal_provider();
		
		void channel(Channel channel);
		void mode(Mode mode);
		
		Channel channel() const;
		Mode mode() const;
		
	private:
		ScanningAlgorithmSliceController _scanning_aglorithm_slice;
		SignalProviderController _signal_provider;
		Selection _selection;
		Channeler &_model;
	};
}