#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <functional>
#include <mutex>



namespace Ethernet
{
	class Positioner;



	class PositionerController
	{
	public:
		PositionerController(Positioner &model);
		
		void trigger_queue_length_callback(std::function<void(std::uint16_t)> &&callback);
		void anchor_tick_count();
		
		std::uint32_t tick_count_difference() const;
		std::uint16_t trigger_state() const;
		std::uint16_t queue_length() const;
		std::uint32_t tick_count() const;
		std::float_t velocity() const;
		
	private:
		std::recursive_mutex mutable _guard;
		Positioner &_model;
		std::function<void(std::uint16_t)> _trigger_queue_length_callback;
		std::uint32_t _tick_count_anchor;
		std::uint16_t _trigger_state;
		std::uint16_t _queue_length;
		std::uint32_t _tick_count;
		std::float_t _velocity;
		
		void trigger_state_callback(std::uint16_t state);
		void queue_length_callback(std::uint16_t length);
		void tick_count_callback(std::uint32_t state);
		void velocity_callback(std::uint16_t velocity);
	};
}
