#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Controller\Utility\Callbacks.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\..\..\Controller\Hardware\Ethernet\EthernetTypeRanges.hpp"
#include "..\..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetOutputController.hpp"



namespace Ethernet
{
	OutputController::OutputController(Output &model):
		_model(model)
	{
		_triggers.insert_or_assign(TriggerMode::None, std::make_pair<std::uint8_t, std::uint8_t>(1, 1));
		_triggers.insert_or_assign(TriggerMode::Velocity, std::make_pair<std::uint8_t, std::uint8_t>(1, 1));
		_triggers.insert_or_assign(TriggerMode::WheelDirect, std::make_pair<std::uint8_t, std::uint8_t>(1, 1));
		_triggers.insert_or_assign(TriggerMode::WheelReversed, std::make_pair<std::uint8_t, std::uint8_t>(1, 1));
	}

	void OutputController::load(const std::wstring & root)
	{
		_triggers.at(_model.trigger_mode()).second = _model.scans_per_trigger();
		_triggers.at(_model.trigger_mode()).first = _model.steps_per_trigger();
	}

	void OutputController::save(const std::wstring & root) const
	{
	}

	TriggerMode OutputController::trigger_mode() const
	{
		return _model.trigger_mode();
	}

	std::uint8_t OutputController::charges_count() const
	{
		return _model.charges_count();
	}

	std::uint16_t OutputController::probing_period() const
	{
		return _model.probing_period();
	}

	std::uint8_t OutputController::scans_per_trigger() const
	{
		return _model.scans_per_trigger();
	}

	std::uint8_t OutputController::steps_per_trigger() const
	{
		return _model.steps_per_trigger();
	}

	std::uint8_t OutputController::strobe_pulse_length() const
	{
		return _model.strobe_pulse_length();
	}

	std::uint8_t OutputController::homogenous_accumulation() const
	{
		return _model.homogenous_accumulation();
	}






	void OutputController::trigger_scans_callback(std::function<void(std::uint8_t)> &&callback)
	{
		_trigger_scans_callback = _trigger_scans_callback + std::move(callback);
	}

	void OutputController::trigger_steps_callback(std::function<void(std::uint8_t)> &&callback)
	{
		_trigger_steps_callback = _trigger_steps_callback + std::move(callback);
	}

	void OutputController::trigger_mode_callback(std::function<void(TriggerMode)> &&callback)
	{
		_trigger_mode_callback = _trigger_mode_callback + std::move(callback);
	}







	void OutputController::trigger_mode(TriggerMode mode)
	{
		_model.trigger_mode(mode);
		_trigger_mode_callback(mode);
		_model.steps_per_trigger(mode == TriggerMode::None ? 1 : _triggers.at(mode).first);
		_trigger_steps_callback(_triggers.at(mode).first);
		_model.scans_per_trigger(mode == TriggerMode::None ? 1 : _triggers.at(mode).second);
		_trigger_scans_callback(_triggers.at(mode).second);
	}

	void OutputController::charges_count(std::uint8_t count)
	{
		_model.charges_count(std::max<std::uint8_t>(count, 1));
	}

	void OutputController::probing_period(std::uint16_t period)
	{
		_model.probing_period(std::max<std::uint16_t>(std::min<std::uint16_t>(period, ProbingPeriodMax), ProbingPeriodMin));
	}

	void OutputController::steps_per_trigger(std::uint8_t steps)
	{
		_triggers.at(_model.trigger_mode()).first = steps;
		_model.steps_per_trigger(_model.trigger_mode() == TriggerMode::None ? 1 : steps);
		_trigger_steps_callback(steps);
	}

	void OutputController::strobe_pulse_length(std::uint8_t level)
	{
		_model.strobe_pulse_length(level);
	}

	void OutputController::scans_per_trigger(std::uint8_t scans)
	{
		_triggers.at(_model.trigger_mode()).second = scans;
		_model.scans_per_trigger(_model.trigger_mode() == TriggerMode::None ? 1 : scans);
		_trigger_scans_callback(scans);
	}

	void OutputController::power_on(bool status)
	{
		_model.power_on(status);
	}
}