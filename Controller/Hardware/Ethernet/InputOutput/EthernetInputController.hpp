#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <mutex>



namespace Ethernet
{
	class Input;
	
	
	
	class InputController
	{
	public:
		InputController(Input &model);
		
	private:
		std::recursive_mutex mutable _guard;
		Input &_model;
	};
}
