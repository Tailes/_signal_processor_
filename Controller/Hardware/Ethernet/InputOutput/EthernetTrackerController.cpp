#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetTracker.hpp"
#include "..\..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetTrackerController.hpp"



namespace Ethernet
{
	TrackerController::TrackerController(Tracker &model):
		_model(model), _current(0.0f), _voltage(0.0f), _adc_state(0)
	{
		model.adc_state_callback([this](std::uint16_t state) {this->adc_state_callback(state); });
		model.current_callback([this](std::uint8_t current) {this->current_callback(current); });
		model.voltage_callback([this](std::uint8_t voltage) {this->voltage_callback(voltage); });
	}

	std::float_t TrackerController::current() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _current;
	}

	std::float_t TrackerController::voltage() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _voltage;
	}

	std::uint16_t TrackerController::adc_state() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _adc_state;
	}



	void TrackerController::adc_state_callback(std::uint16_t state)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_adc_state = state;
	}

	void TrackerController::current_callback(std::uint8_t current)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_current = static_cast<std::float_t>(current) / static_cast<std::float_t>(std::numeric_limits<std::uint8_t>::max());
	}

	void TrackerController::voltage_callback(std::uint8_t voltage)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_voltage = static_cast<std::float_t>(voltage) / static_cast<std::float_t>(std::numeric_limits<std::uint8_t>::max());
	}
}