#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSocketSettings.hpp"
#include "..\..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetSocketSettingsController.hpp"



namespace Ethernet
{
	SocketSettingsController::SocketSettingsController(SocketSettings &model):
		_model(model)
	{
	}

	void SocketSettingsController::linger(bool enable)
	{
		_model.linger(enable);
	}

	void SocketSettingsController::keep_alive(bool enable)
	{
		_model.keep_alive(enable);
	}

	void SocketSettingsController::address_reuse(bool enable)
	{
		_model.address_reuse(enable);
	}

	void SocketSettingsController::nagle_algorithm(bool enable)
	{
		_model.nagle_algorithm(enable);
	}

	void SocketSettingsController::exclusive_address_use(bool enable)
	{
		_model.exclusive_address_use(enable);
	}


	int SocketSettingsController::address_family() const
	{
		return _model.address_family();
	}

	int SocketSettingsController::type() const
	{
		return _model.type();
	}

	int SocketSettingsController::protocol() const
	{
		return _model.protocol();
	}

	int SocketSettingsController::source_port() const
	{
		return _model.source_port();
	}

	int SocketSettingsController::target_port() const
	{
		return _model.target_port();
	}

	int SocketSettingsController::receive_timeout() const
	{
		return _model.receive_timeout();
	}

	int SocketSettingsController::send_timeout() const
	{
		return _model.send_timeout();
	}

	int SocketSettingsController::input_buffer_size() const
	{
		return _model.input_buffer_size();
	}

	int SocketSettingsController::output_buffer_size() const
	{
		return _model.output_buffer_size();
	}

	unsigned short SocketSettingsController::linger_delay() const
	{
		return _model.linger_delay();
	}

	unsigned long SocketSettingsController::source_ip() const
	{
		return _model.source_ip();
	}

	unsigned long SocketSettingsController::target_ip() const
	{
		return _model.target_ip();
	}


	void SocketSettingsController::address_family(int family)
	{
		_model.address_family(family);
	}

	void SocketSettingsController::type(int type)
	{
		_model.type(type);
	}

	void SocketSettingsController::protocol(int protocol)
	{
		_model.protocol(protocol);
	}

	void SocketSettingsController::source_ip(unsigned long ip)
	{
		_model.source_ip(ip);
	}

	void SocketSettingsController::target_ip(unsigned long ip)
	{
		_model.target_ip(ip);
	}

	void SocketSettingsController::source_port(int port)
	{
		_model.source_port(port);
	}

	void SocketSettingsController::target_port(int port)
	{
		_model.target_port(port);
	}

	void SocketSettingsController::receive_timeout(int milliseconds)
	{
		_model.receive_timeout(milliseconds);
	}

	void SocketSettingsController::input_buffer_size(int size)
	{
		_model.input_buffer_size(size);
	}

	void SocketSettingsController::output_buffer_size(int size)
	{
		_model.output_buffer_size(size);
	}

	void SocketSettingsController::send_timeout(int milliseconds)
	{
		_model.send_timeout(milliseconds);
	}

	void SocketSettingsController::linger_delay(unsigned short delay)
	{
		_model.linger_delay(delay);
	}


	bool SocketSettingsController::lingering() const
	{
		return _model.lingering();
	}

	bool SocketSettingsController::keep_alive() const
	{
		return _model.keep_alive();
	}

	bool SocketSettingsController::address_reuse() const
	{
		return _model.address_reuse();
	}

	bool SocketSettingsController::nagle_algorithm() const
	{
		return _model.nagle_algorithm();
	}

	bool SocketSettingsController::exclusive_address_use() const
	{
		return _model.exclusive_address_use();
	}
}