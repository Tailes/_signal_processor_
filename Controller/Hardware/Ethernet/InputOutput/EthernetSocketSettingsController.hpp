#pragma once


namespace Ethernet
{
	class SocketSettings;



	class SocketSettingsController
	{
	public:
		SocketSettingsController(SocketSettings &model);
		
		void linger(bool enable);
		void keep_alive(bool enable);
		void address_reuse(bool enable);
		void nagle_algorithm(bool enable);
		void exclusive_address_use(bool enable);
		
		int address_family() const;
		int type() const;
		int protocol() const;
		int source_port() const;
		int target_port() const;
		int receive_timeout() const;
		int send_timeout() const;
		int input_buffer_size() const;
		int output_buffer_size() const;
		unsigned short linger_delay() const;
		unsigned long source_ip() const;
		unsigned long target_ip() const;
		
		void address_family(int family);
		void type(int type);
		void protocol(int protocol);
		void source_ip(unsigned long ip);
		void target_ip(unsigned long ip);
		void source_port(int port);
		void target_port(int port);
		void receive_timeout(int milliseconds);
		void input_buffer_size(int size);
		void output_buffer_size(int size);
		void send_timeout(int milliseconds);
		void linger_delay(unsigned short delay);
		
		bool lingering() const;
		bool keep_alive() const;
		bool address_reuse() const;
		bool nagle_algorithm() const;
		bool exclusive_address_use() const;
		
	private:
		SocketSettings &_model;
	};
}