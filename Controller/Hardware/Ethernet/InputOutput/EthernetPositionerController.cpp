#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetPositioner.hpp"
#include "..\..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetPositionerController.hpp"



namespace Ethernet
{
	PositionerController::PositionerController(Positioner &model):
		_model(model), _velocity(0.0f), _queue_length(0), _trigger_state(0), _tick_count(0)
	{
		model.trigger_state_callback([this](std::uint16_t state) {this->trigger_state_callback(state); });
		model.queue_length_callback([this](std::uint16_t state) {this->queue_length_callback(state); });
		model.tick_count_callback([this](std::uint32_t state) {this->tick_count_callback(state); });
		model.velocity_callback([this](std::uint16_t state) {this->velocity_callback(state); });
	}

	void PositionerController::trigger_queue_length_callback(std::function<void(std::uint16_t)> &&callback)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_trigger_queue_length_callback = std::move(callback);
	}

	void PositionerController::anchor_tick_count()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_tick_count_anchor = _tick_count;
	}

	std::float_t PositionerController::velocity() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _velocity;
	}

	std::uint32_t PositionerController::tick_count() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _tick_count;
	}

	std::uint32_t PositionerController::tick_count_difference() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _tick_count - _tick_count_anchor;
	}

	std::uint16_t PositionerController::trigger_state() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _trigger_state;
	}

	std::uint16_t PositionerController::queue_length() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _queue_length;
	}





	void PositionerController::trigger_state_callback(std::uint16_t state)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_trigger_state = state;
	}

	void PositionerController::queue_length_callback(std::uint16_t length)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_trigger_queue_length_callback(_queue_length = length);
	}

	void PositionerController::tick_count_callback(std::uint32_t count)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_tick_count = count;
	}

	void PositionerController::velocity_callback(std::uint16_t velocity)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_velocity = static_cast<std::float_t>(velocity) / static_cast<std::float_t>(std::numeric_limits<std::uint16_t>::max());
	}
}