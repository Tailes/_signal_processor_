#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <mutex>
#include <cmath>



namespace Ethernet
{
	class Tracker;



	class TrackerController
	{
	public:
		TrackerController(Tracker &model);
		
		std::uint16_t adc_state() const;
		std::float_t current() const;
		std::float_t voltage() const;
		
	private:
		std::recursive_mutex mutable _guard;
		Tracker &_model;
		std::uint16_t _adc_state;
		std::float_t _current;
		std::float_t _voltage;
		
		void adc_state_callback(std::uint16_t state);
		void current_callback(std::uint8_t current);
		void voltage_callback(std::uint8_t voltage);
	};
}
