#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <functional>




namespace Ethernet
{
	class Settings;



	class SettingsController
	{
	public:
		SettingsController(Settings &model);
		
		void tick_length_callback(std::function<void(std::float_t)> &&callback);
		
		std::float_t tick_length() const;
		
		void tick_length(std::float_t value);
		
	private:
		Settings &_model;
		std::function<void(std::float_t)> _tick_length_callback;
	};
}