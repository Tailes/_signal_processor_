#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Controller\Utility\Callbacks.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSettings.hpp"
#include "..\..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetSettingsController.hpp"



namespace Ethernet
{
	SettingsController::SettingsController(Settings &model):
		_model(model)
	{
	}

	void SettingsController::tick_length_callback(std::function<void(std::float_t)> &&callback)
	{
		_tick_length_callback = _tick_length_callback + std::move(callback);
	}

	std::float_t SettingsController::tick_length() const
	{
		return _model.tick_length();
	}

	void SettingsController::tick_length(std::float_t value)
	{
		_model.tick_length(value);
		_tick_length_callback(value);
	}
}