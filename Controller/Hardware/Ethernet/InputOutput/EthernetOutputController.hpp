#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <functional>
#include <cstddef>
#include <map>



namespace Ethernet
{
	class Output;
	
	
	
	class OutputController
	{
		typedef std::pair<std::uint8_t, std::uint8_t> StepsScansPair;
	public:
		OutputController(Output &model);
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		void trigger_mode(TriggerMode mode);
		void charges_count(std::uint8_t count);
		void probing_period(std::uint16_t period);
		void scans_per_trigger(std::uint8_t scans);
		void steps_per_trigger(std::uint8_t steps);
		void strobe_pulse_length(std::uint8_t level);
		void trigger_mode_callback(std::function<void(TriggerMode)> &&callback);
		void trigger_scans_callback(std::function<void(std::uint8_t)> &&callback);
		void trigger_steps_callback(std::function<void(std::uint8_t)> &&callback);
		void power_on(bool status);
		
		TriggerMode trigger_mode() const;
		std::uint8_t charges_count() const;
		std::uint8_t scans_per_trigger() const;
		std::uint8_t steps_per_trigger() const;
		std::uint8_t strobe_pulse_length() const;
		std::uint8_t homogenous_accumulation() const;
		std::uint16_t probing_period() const;
		
	private:
		Output &_model;
		std::function<void(std::uint8_t)> _trigger_scans_callback;
		std::function<void(std::uint8_t)> _trigger_steps_callback;
		std::function<void(TriggerMode)> _trigger_mode_callback;
		std::map<TriggerMode, StepsScansPair> _triggers;
	};
}