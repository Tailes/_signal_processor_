#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalRecorder.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetSignalRecorderController.hpp"



namespace Ethernet
{
	SignalRecorderController::SignalRecorderController(SignalRecorder &model):
		_model(model)
	{
	}

	void SignalRecorderController::flush(const std::array<std::wstring, static_cast<std::size_t>(Channel::Total)> &paths)
	{
		_model.flush(paths);
	}

	void SignalRecorderController::enable(bool enabled)
	{
		_model.enable(enabled);
	}

	bool SignalRecorderController::enabled() const
	{
		return _model.enabled();
	}
}