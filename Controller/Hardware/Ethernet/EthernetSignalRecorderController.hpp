#pragma once

#include <string>
#include <array>


namespace Ethernet
{
	class SignalRecorder;
	
	
	
	class SignalRecorderController
	{
	public:
		SignalRecorderController(SignalRecorder &model);
		
		void flush(const std::array<std::wstring, static_cast<std::size_t>(Channel::Total)> &paths);
		
		void enable(bool value = true);
		
		bool enabled() const;
		
	private:
		SignalRecorder &_model;
	};
}