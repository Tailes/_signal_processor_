#pragma once
#include "..\..\..\Controller\Hardware\Ethernet\EthernetSignalSourceController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetSignalRecorderController.hpp"


namespace Ethernet
{
	class SignalProvider;



	class SignalProviderController
	{
	public:
		SignalProviderController(SignalProvider &model);
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		const SignalRecorderController& signal_recorder() const;
		const SignalSourceController& signal_source() const;
		const SignalProvider& model() const;
		
		SignalRecorderController& signal_recorder();
		SignalSourceController& signal_source();
		
	private:
		SignalProvider &_model;
		SignalRecorderController _signal_recorder;
		SignalSourceController _signal_source;
	};
}