#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetInterpolator.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetInterpolatorController.hpp"



namespace Ethernet
{
	InterpolatorController::InterpolatorController(Interpolator &model):
		_model(model)
	{
	}

	void InterpolatorController::enable(bool enabled)
	{
		_model.enable(enabled);
	}

	bool InterpolatorController::enabled() const
	{
		return _model.enabled();
	}
}