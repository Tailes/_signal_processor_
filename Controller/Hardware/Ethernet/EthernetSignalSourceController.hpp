#pragma once
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetInputController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetOutputController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetTrackerController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetPositionerController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetSocketSettingsController.hpp"

namespace Ethernet
{
	class SignalSource;



	class SignalSourceController
	{
	public:
		SignalSourceController(SignalSource &model);
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		InputController& input();
		OutputController& output();
		TrackerController& tracker();
		PositionerController& positioner();
		SocketSettingsController& settings();
		const InputController& input() const;
		const OutputController& output() const;
		const TrackerController& tracker() const;
		const PositionerController& positioner() 	const;
		const SocketSettingsController& settings() const;
		
	private:
		SignalSource &_model;
		SocketSettingsController _socket_settings;
		PositionerController _positioner;
		TrackerController _tracker;
		OutputController _output;
		InputController _input;
	};
}