#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <cstddef>
#include <mutex>



namespace Ethernet
{
	class DelayerModeCounter;


	class DelayerModeCounterController
	{
	public:
		DelayerModeCounterController(DelayerModeCounter &model);
		
		void interval(std::size_t value);
		
		std::size_t interval() const;
		
	private:
		std::recursive_mutex mutable _guard;
		DelayerModeCounter& _model;
	};
}