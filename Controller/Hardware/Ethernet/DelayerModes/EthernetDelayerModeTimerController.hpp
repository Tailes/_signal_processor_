#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <cstddef>
#include <chrono>
#include <mutex>


namespace Ethernet
{
	class DelayerModeTimer;


	class DelayerModeTimerController
	{
	public:
		DelayerModeTimerController(DelayerModeTimer &model);
		
		void interval(std::chrono::milliseconds value);
		
		std::chrono::milliseconds interval() const;
		
	private:
		std::recursive_mutex mutable _guard;
		DelayerModeTimer& _model;
	};
}