#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\DelayerModes\EthernetDelayerModeCounter.hpp"
#include "..\..\..\..\Controller\Hardware\Ethernet\DelayerModes\EthernetDelayerModeCounterController.hpp"


namespace Ethernet
{
	DelayerModeCounterController::DelayerModeCounterController(DelayerModeCounter &model):
		_model(model)
	{
	}

	void DelayerModeCounterController::interval(std::size_t value)
	{
		_model.interval(value);
	}

	std::size_t DelayerModeCounterController::interval() const
	{
		return _model.interval();
	}
}