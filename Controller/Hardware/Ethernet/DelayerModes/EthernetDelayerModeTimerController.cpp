#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\DelayerModes\EthernetDelayerModeTimer.hpp"
#include "..\..\..\..\Controller\Hardware\Ethernet\DelayerModes\EthernetDelayerModeTimerController.hpp"


namespace Ethernet
{
	DelayerModeTimerController::DelayerModeTimerController(DelayerModeTimer &model):
		_model(model)
	{
	}

	void DelayerModeTimerController::interval(std::chrono::milliseconds value)
	{
		_model.interval(value);
	}

	std::chrono::milliseconds DelayerModeTimerController::interval() const
	{
		return _model.interval();
	}
}