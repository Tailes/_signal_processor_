#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetChanneler.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetChannelerController.hpp"



namespace Ethernet
{
	ChannelerController::ChannelerController(Channeler &model):
		_model(model), _signal_provider(model.signal_provider()), _scanning_aglorithm_slice(model.slice_algorithm())
	{
		_selection.emplace(Mode::ActiveAntenna, model.channel());
		_selection.emplace(Mode::ActiveAntennaCrossInput, model.channel());
		_selection.emplace(Mode::TransmitRecieve, model.channel());
		_selection.emplace(Mode::QuadroAntenna, model.channel());
		_selection.emplace(Mode::Slice, model.channel());
	}

	void ChannelerController::load(const std::wstring &root)
	{
		_signal_provider.load(root);
	}

	void ChannelerController::save(const std::wstring &root) const
	{
		_signal_provider.save(root);
	}

	const ScanningAlgorithmSliceController & ChannelerController::scanning_algorithm_slice() const
	{
		return _scanning_aglorithm_slice;
	}

	const SignalProviderController& ChannelerController::signal_provider() const
	{
		return _signal_provider;
	}

	ScanningAlgorithmSliceController & ChannelerController::scanning_algorithm_slice()
	{
		return _scanning_aglorithm_slice;
	}

	SignalProviderController& ChannelerController::signal_provider()
	{
		return _signal_provider;
	}

	void ChannelerController::channel(Channel channel)
	{
		_selection.at(_model.mode()) = channel;
		_model.channel(channel);
	}

	void ChannelerController::mode(Mode mode)
	{
		_model.mode(mode);
		_model.channel(_selection.at(mode));
	}


	Channel ChannelerController::channel() const
	{
		return _model.channel();
	}

	Mode ChannelerController::mode() const
	{
		return _model.mode();
	}
}