#pragma once


namespace Ethernet
{
	class Interpolator;
	
	
	
	class InterpolatorController
	{
	public:
		InterpolatorController(Interpolator &model);
		
		void enable(bool enabled);
		
		bool enabled() const;
		
	private:
		Interpolator &_model;
	};
}