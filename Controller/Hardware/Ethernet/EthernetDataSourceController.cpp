#include "..\..\..\stdafx.hpp"
#include "..\..\..\Utility\Registry.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetDataSource.hpp"
#include "..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetDataSourceController.hpp"



namespace Ethernet
{
	DataSourceController::DataSourceController(DataSource &model):
		_model(model), _settings(model.settings()), _channeler(model.channeler()), _timer(model.timer(), model.timing()), _interpolator(model.interpolator()), _delayer(model.delayer()),
		_enable_advanced_settings(true),
		_advanced_settings_change_handler{ [](bool) {} }
	{
	}

	void DataSourceController::save(const std::wstring &root) const
	{
		Registry registry(root);
		registry.write_bool(L"EthernetAdvancedSettings", _enable_advanced_settings);
		_channeler.save(root);
	}

	void DataSourceController::load(const std::wstring &root)
	{
		Registry registry(root);
		registry.read_bool(L"EthernetAdvancedSettings", _enable_advanced_settings);
		_channeler.load(root);
	}




	void DataSourceController::advanced_settings_change_handler(std::function<void(bool)> callback)
	{
		_advanced_settings_change_handler = std::move(callback);
	}

	void DataSourceController::signal_length(SignalLength length)
	{
		_model.signal_length(length);
	}

	void DataSourceController::delays_basis(const Basis &basis)
	{
		_model.delays_basis(basis);
	}

	void DataSourceController::scanning_delay(Delay delay)
	{
		_model.scanning_delay(delay);
	}

	void DataSourceController::time_frame_single(TimeFrameSingle frame)
	{
		_model.time_frame_single(frame);
	}

	void DataSourceController::time_frame_double(TimeFrameDouble frame)
	{
		_model.time_frame_double(frame);
	}

	void DataSourceController::time_frame_quadruple(TimeFrameQuadruple frame)
	{
		_model.time_frame_quadruple(frame);
	}

	void DataSourceController::time_frame_octuple(TimeFrameOctuple frame)
	{
		_model.time_frame_octuple(frame);
	}

	void DataSourceController::time_frame_sedecuple(TimeFrameSedecuple frame)
	{
		_model.time_frame_sedecuple(frame);
	}

	void DataSourceController::advanced_settings(bool enable)
	{
		_advanced_settings_change_handler(_enable_advanced_settings = enable);
	}




	std::array<std::uint16_t, DelayTotalLinesCount> DataSourceController::delays_basis() const
	{
		return _model.delays_basis();
	}

	Delay DataSourceController::scanning_delay() const
	{
		return _model.scanning_delay();
	}

	SignalLength DataSourceController::signal_length() const
	{
		return _model.signal_length();
	}
	
	TimeFrameSingle DataSourceController::time_frame_single() const
	{
		return _model.time_frame_single();
	}

	TimeFrameDouble DataSourceController::time_frame_double() const
	{
		return _model.time_frame_double();
	}

	TimeFrameQuadruple DataSourceController::time_frame_quadruple() const
	{
		return _model.time_frame_quadruple();
	}

	TimeFrameOctuple DataSourceController::time_frame_octuple() const
	{
		return _model.time_frame_octuple();
	}

	TimeFrameSedecuple DataSourceController::time_frame_sedecuple() const
	{
		return _model.time_frame_sedecuple();
	}





	const InterpolatorController& DataSourceController::interpolator() const
	{
		return _interpolator;
	}

	const ChannelerController& DataSourceController::channeler() const
	{
		return _channeler;
	}

	const SettingsController& DataSourceController::settings() const
	{
		return _settings;
	}

	const DelayerController & DataSourceController::delayer() const
	{
		return _delayer;
	}

	const TimerController& DataSourceController::timer() const
	{
		return _timer;
	}

	bool DataSourceController::advanced_settings() const
	{
		return _enable_advanced_settings;
	}



	InterpolatorController& DataSourceController::interpolator()
	{
		return _interpolator;
	}
	
	ChannelerController& DataSourceController::channeler()
	{
		return _channeler;
	}

	SettingsController& DataSourceController::settings()
	{
		return _settings;
	}

	DelayerController & DataSourceController::delayer()
	{
		return _delayer;
	}

	TimerController& DataSourceController::timer()
	{
		return _timer;
	}
}