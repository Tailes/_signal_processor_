#pragma once
#include "..\..\Model\Hardware\HardwareDataSourceEnums.hpp"
#include "..\..\Controller\Hardware\Serial\SerialDataSourceController.hpp"
#include "..\..\Controller\Hardware\Ethernet\EthernetDataSourceController.hpp"


class HardwareDataSource;



class HardwareDataSourceController
{
public:
	HardwareDataSourceController(HardwareDataSource &model);
	
	void load(const std::wstring &root);
	void save(const std::wstring &root) const;
	
	const Ethernet::DataSourceController& ethernet_data_source() const;
	const Serial::DataSourceController& serial_data_source() const;
	
	Ethernet::DataSourceController& ethernet_data_source();
	Serial::DataSourceController& serial_data_source();
	
	HardwareDataSourceMode mode() const;
	
	void mode(HardwareDataSourceMode source);
	
private:
	HardwareDataSource &_model;
	Ethernet::DataSourceController _ethernet_data_source_controller;
	Serial::DataSourceController _serial_data_source_controller;
};

