#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialPort.hpp"
#include "..\..\..\Controller\Hardware\Serial\SerialPortController.hpp"



namespace Serial
{
	PortController::PortController(Port &model):
		_model(model)
	{
	}

	void PortController::name(const std::wstring &value)
	{
		_model.name(value);
	}

	std::wstring PortController::name() const
	{
		return _model.name();
	}

	unsigned long PortController::output_buffer_size() const
	{
		return _model.output_buffer_size();
	}

	unsigned long PortController::input_buffer_size() const
	{
		return _model.input_buffer_size();
	}

	void PortController::output_buffer_size(unsigned long size)
	{
		_model.output_buffer_size(size);
	}

	void PortController::input_buffer_size(unsigned long size)
	{
		_model.input_buffer_size(size);
	}
}