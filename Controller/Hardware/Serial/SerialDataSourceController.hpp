#pragma once

#include "..\..\..\Controller\Hardware\Serial\SerialPortController.hpp"
#include "..\..\..\Controller\Hardware\Serial\SerialCommTimeoutsController.hpp"
#include "..\..\..\Controller\Hardware\Serial\SerialControlPacketController.hpp"
#include "..\..\..\Controller\Hardware\Serial\SerialDeviceControlBlockController.hpp"

#include "..\..\..\Model\Hardware\Serial\SerialCommunicationEnums.hpp"


namespace Serial
{
	class DataSource;
	
	
	
	class DataSourceController
	{
	public:
		DataSourceController(DataSource &model);
		
		void save(const std::wstring &root) const;
		void load(const std::wstring &root);
		
		const DeviceControlBlockController& device_control_block() const;
		const ControlPacketController& packet() const;
		const CommTimeoutsController& comm_timeouts() const;
		const PortController& port() const;
		
		DeviceControlBlockController& device_control_block();
		ControlPacketController& packet();
		CommTimeoutsController& comm_timeouts();
		PortController& port();
		
		ScanningAlgorithm scanning_algorithm() const;
		PacketLength packet_length() const;
		DataSize point_size() const;
		unsigned long handshakes() const;
		
		void scanning_algorithm(ScanningAlgorithm value);
		void packet_length(PacketLength value);
		void point_size(DataSize value);
		void handshakes(unsigned long value);
		
	private:
		DataSource &_model;
		DeviceControlBlockController _device_control_block;
		ControlPacketController _packet;
		CommTimeoutsController _comm_timeouts;
		PortController _port;
	};
}