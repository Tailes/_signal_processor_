#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialCommTimeouts.hpp"
#include "..\..\..\Controller\Hardware\Serial\SerialCommTimeoutsController.hpp"



namespace Serial
{
	CommTimeoutsController::CommTimeoutsController(CommTimeouts &model):
		_model(model)
	{
	}


	std::uint32_t CommTimeoutsController::read_interval_timeout() const
	{
		return _model.read_interval_timeout();
	}

	std::uint32_t CommTimeoutsController::read_total_timeout_constant() const
	{
		return _model.read_total_timeout_constant();
	}

	std::uint32_t CommTimeoutsController::read_total_timeout_multiplier() const
	{
		return _model.read_total_timeout_multiplier();
	}

	std::uint32_t CommTimeoutsController::write_total_timeout_constant() const
	{
		return _model.write_total_timeout_constant();
	}

	std::uint32_t CommTimeoutsController::write_total_timeout_multiplier() const
	{
		return _model.write_total_timeout_multiplier();
	}


	void CommTimeoutsController::read_interval_timeout(std::uint32_t value)
	{
		_model.read_interval_timeout(value);
	}

	void CommTimeoutsController::read_total_timeout_constant(std::uint32_t value)
	{
		_model.read_total_timeout_constant(value);
	}

	void CommTimeoutsController::read_total_timeout_multiplier(std::uint32_t value)
	{
		_model.read_total_timeout_multiplier(value);
	}

	void CommTimeoutsController::write_total_timeout_constant(std::uint32_t value)
	{
		_model.write_total_timeout_constant(value);
	}

	void CommTimeoutsController::write_total_timeout_multiplier(std::uint32_t value)
	{
		_model.write_total_timeout_multiplier(value);
	}
}