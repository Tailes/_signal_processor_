#pragma once
#include "..\..\..\Model\Hardware\Serial\SerialCommunicationEnums.hpp"


namespace Serial
{
	class ControlPacket;
	
	
	
	class ControlPacketController
	{
	public:
		ControlPacketController(ControlPacket &model);
		
		HardwareDataSourceTimeFrame scanning_time_frame() const;
		ScanningMode scanning_mode() const;
		unsigned short probing_period() const;
		unsigned short start_delay() const;
		unsigned char accumulation() const;
		unsigned char marker() const;
		
		void scanning_time_frame(HardwareDataSourceTimeFrame frame);
		void probing_period(unsigned short period);
		void scanning_mode(ScanningMode mode);
		void accumulation(unsigned char accumulation);
		void start_delay(unsigned short delay);
		void marker(unsigned char value);
		
	private:
		ControlPacket &_model;
	};
}