#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialControlPacket.hpp"
#include "..\..\..\Controller\Hardware\Serial\SerialControlPacketController.hpp"



namespace Serial
{
	ControlPacketController::ControlPacketController(ControlPacket &model):
		_model(model)
	{
	}

	HardwareDataSourceTimeFrame ControlPacketController::scanning_time_frame() const
	{
		return _model.scanning_time_frame();
	}

	ScanningMode ControlPacketController::scanning_mode() const
	{
		return _model.scanning_mode();
	}

	unsigned short ControlPacketController::probing_period() const
	{
		return _model.probing_period();
	}

	unsigned short ControlPacketController::start_delay() const
	{
		return _model.start_delay();
	}

	unsigned char ControlPacketController::accumulation() const
	{
		return _model.accumulation();
	}

	unsigned char ControlPacketController::marker() const
	{
		return _model.marker();
	}


	void ControlPacketController::scanning_time_frame(HardwareDataSourceTimeFrame frame)
	{
		_model.scanning_time_frame(frame);
	}

	void ControlPacketController::probing_period(unsigned short period)
	{
		_model.probing_period(period);
	}

	void ControlPacketController::scanning_mode(ScanningMode mode)
	{
		_model.scanning_mode(mode);
	}

	void ControlPacketController::accumulation(unsigned char accumulation)
	{
		_model.accumulation(accumulation);
	}

	void ControlPacketController::start_delay(unsigned short delay)
	{
		_model.start_delay(delay);
	}

	void ControlPacketController::marker(unsigned char value)
	{
		_model.marker(value);
	}
}