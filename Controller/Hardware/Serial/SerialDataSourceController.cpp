#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialDataSource.hpp"
#include "..\..\..\Controller\Hardware\Serial\SerialDataSourceController.hpp"



namespace Serial
{
	DataSourceController::DataSourceController(DataSource &model):
		_model(model),
		_port(model.port()),
		_packet(model.control_packet()),
		_comm_timeouts(model.comm_timeouts()),
		_device_control_block(model.device_control_block())
	{
	}

	void DataSourceController::save(const std::wstring &root) const
	{
	}

	void DataSourceController::load(const std::wstring &root)
	{
	}


	const DeviceControlBlockController& DataSourceController::device_control_block() const
	{
		return _device_control_block;
	}

	const ControlPacketController& DataSourceController::packet() const
	{
		return _packet;
	}

	const CommTimeoutsController& DataSourceController::comm_timeouts() const
	{
		return _comm_timeouts;
	}

	const PortController& DataSourceController::port() const
	{
		return _port;
	}

	DeviceControlBlockController& DataSourceController::device_control_block()
	{
		return _device_control_block;
	}

	ControlPacketController& DataSourceController::packet()
	{
		return _packet;
	}

	CommTimeoutsController& DataSourceController::comm_timeouts()
	{
		return _comm_timeouts;
	}

	PortController& DataSourceController::port()
	{
		return _port;
	}



	ScanningAlgorithm DataSourceController::scanning_algorithm() const
	{
		return _model.scanning_algorithm();
	}

	PacketLength DataSourceController::packet_length() const
	{
		return _model.packet_length();
	}

	DataSize DataSourceController::point_size() const
	{
		return _model.point_size();
	}

	unsigned long DataSourceController::handshakes() const
	{
		return _model.handshakes();
	}



	void DataSourceController::scanning_algorithm(ScanningAlgorithm value)
	{
		_model.scanning_algorithm(value);
	}

	void DataSourceController::packet_length(PacketLength value)
	{
		_model.packet_length(value);
	}

	void DataSourceController::point_size(DataSize value)
	{
		_model.point_size(value);
	}

	void DataSourceController::handshakes(unsigned long value)
	{
		_model.handshakes(value);
	}
}