#include "..\..\..\stdafx.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialDeviceControlBlock.hpp"
#include "..\..\..\Controller\Hardware\Serial\SerialDeviceControlBlockController.hpp"



namespace Serial
{
	DeviceControlBlockController::DeviceControlBlockController(DeviceControlBlock &model):
		_model(model)
	{
	}

	BaudRate DeviceControlBlockController::baud_rate() const
	{
		return _model.baud_rate();
	}

	unsigned short DeviceControlBlockController::x_on_limit() const
	{
		return _model.x_on_limit();
	}

	unsigned short DeviceControlBlockController::x_off_limit() const
	{
		return _model.x_off_limit();
	}

	unsigned char DeviceControlBlockController::byte_size() const
	{
		return _model.byte_size();
	}

	ParityMode DeviceControlBlockController::parity_mode() const
	{
		return _model.parity_mode();
	}

	StopBits DeviceControlBlockController::stop_bits() const
	{
		return _model.stop_bits();
	}

	DtrMode DeviceControlBlockController::dtr_control_mode() const
	{
		return _model.dtr_control_mode();
	}

	RtsMode DeviceControlBlockController::rts_control_mode() const
	{
		return _model.rts_control_mode();
	}

	char DeviceControlBlockController::x_on_char() const
	{
		return _model.x_on_char();
	}

	char DeviceControlBlockController::x_off_char() const
	{
		return _model.x_off_char();
	}

	char DeviceControlBlockController::error_char() const
	{
		return _model.error_char();
	}

	char DeviceControlBlockController::eof_char() const
	{
		return _model.eof_char();
	}

	char DeviceControlBlockController::evt_char() const
	{
		return _model.evt_char();
	}

	bool DeviceControlBlockController::parity_enabled() const
	{
		return _model.parity_enabled();
	}

	bool DeviceControlBlockController::output_cts_flow_enabled() const
	{
		return _model.output_cts_flow_enabled();
	}

	bool DeviceControlBlockController::output_dsr_flow_enabled() const
	{
		return _model.output_dsr_flow_enabled();
	}

	bool DeviceControlBlockController::dsr_sensitivity_enabled() const
	{
		return _model.dsr_sensitivity_enabled();
	}

	bool DeviceControlBlockController::transmission_continue_on_x_off_enabled() const
	{
		return _model.transmission_continue_on_x_off_enabled();
	}

	bool DeviceControlBlockController::out_x_enabled() const
	{
		return _model.out_x_enabled();
	}

	bool DeviceControlBlockController::in_x_enabled() const
	{
		return _model.in_x_enabled();
	}

	bool DeviceControlBlockController::error_char_enabled() const
	{
		return _model.error_char_enabled();
	}

	bool DeviceControlBlockController::null_enabled() const
	{
		return _model.null_enabled();
	}

	bool DeviceControlBlockController::abort_on_error_enabled() const
	{
		return _model.abort_on_error_enabled();
	}


	void DeviceControlBlockController::baud_rate(BaudRate rate)
	{
		return _model.baud_rate(rate);
	}

	void DeviceControlBlockController::x_on_limit(unsigned short limit)
	{
		return _model.x_on_limit(limit);
	}

	void DeviceControlBlockController::x_off_limit(unsigned short limit)
	{
		return _model.x_off_limit(limit);
	}

	void DeviceControlBlockController::byte_size(unsigned char size)
	{
		return _model.byte_size(size);
	}

	void DeviceControlBlockController::parity_mode(ParityMode mode)
	{
		return _model.parity_mode(mode);
	}

	void DeviceControlBlockController::stop_bits(StopBits count)
	{
		return _model.stop_bits(count);
	}

	void DeviceControlBlockController::dtr_control_mode(DtrMode mode)
	{
		return _model.dtr_control_mode(mode);
	}

	void DeviceControlBlockController::rts_control_mode(RtsMode mode)
	{
		return _model.rts_control_mode(mode);
	}

	void DeviceControlBlockController::x_on_char(char value)
	{
		return _model.x_on_char(value);
	}

	void DeviceControlBlockController::x_off_char(char value)
	{
		return _model.x_off_char(value);
	}

	void DeviceControlBlockController::error_char(char value)
	{
		return _model.error_char(value);
	}

	void DeviceControlBlockController::eof_char(char value)
	{
		return _model.eof_char(value);
	}

	void DeviceControlBlockController::evt_char(char value)
	{
		return _model.evt_char(value);
	}

	void DeviceControlBlockController::enable_parity(bool enable)
	{
		return _model.enable_parity(enable);
	}

	void DeviceControlBlockController::enable_output_cts_flow(bool enable)
	{
		return _model.enable_output_cts_flow(enable);
	}

	void DeviceControlBlockController::enable_output_dsr_flow(bool enable)
	{
		return _model.enable_output_dsr_flow(enable);
	}

	void DeviceControlBlockController::enable_dsr_sensitivity(bool enable)
	{
		return _model.enable_dsr_sensitivity(enable);
	}

	void DeviceControlBlockController::enable_transmission_continue_on_x_off(bool enable)
	{
		return _model.enable_transmission_continue_on_x_off(enable);
	}

	void DeviceControlBlockController::enable_out_x(bool enable)
	{
		return _model.enable_out_x(enable);
	}

	void DeviceControlBlockController::enable_in_x(bool enable)
	{
		return _model.enable_in_x(enable);
	}

	void DeviceControlBlockController::enable_error_char(bool enable)
	{
		return _model.enable_error_char(enable);
	}

	void DeviceControlBlockController::enable_null(bool enable)
	{
		return _model.enable_null(enable);
	}

	void DeviceControlBlockController::enable_abort_on_error(bool enable)
	{
		return _model.enable_abort_on_error(enable);
	}

}