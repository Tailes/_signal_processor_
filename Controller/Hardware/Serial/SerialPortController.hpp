#pragma once

#include <string>

namespace Serial
{
	class Port;
	
	
	
	class PortController
	{
	public:
		PortController(Port &model);
		
		std::wstring name() const;
		unsigned long output_buffer_size() const;
		unsigned long input_buffer_size() const;
		
		void name(const std::wstring &name);
		void output_buffer_size(unsigned long size);
		void input_buffer_size(unsigned long size);
		
	private:
		Port &_model;
	};
}