#pragma once

#include <cstdint>

namespace Serial
{
	class CommTimeouts;
	
	
	
	class CommTimeoutsController
	{
	public:
		CommTimeoutsController(CommTimeouts &model);
		
		std::uint32_t read_interval_timeout() const;
		std::uint32_t read_total_timeout_constant() const;
		std::uint32_t read_total_timeout_multiplier() const;
		std::uint32_t write_total_timeout_constant() const;
		std::uint32_t write_total_timeout_multiplier() const;
		
		void read_interval_timeout(std::uint32_t value);
		void read_total_timeout_constant(std::uint32_t value);
		void read_total_timeout_multiplier(std::uint32_t value);
		void write_total_timeout_constant(std::uint32_t value);
		void write_total_timeout_multiplier(std::uint32_t value);
		
	private:
		CommTimeouts &_model;
	};
}