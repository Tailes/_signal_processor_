#pragma once
#include "..\Controller\Processing\SliceProcessorController.hpp"

#include <forward_list>
#include <functional>
#include <string>
#include <mutex>



class SignalProcessorModel;



class SignalProcessorController
{
public:
	SignalProcessorController(SignalProcessorModel &model);
	~SignalProcessorController();
	
	void reset();
	void save(const std::wstring &root) const;
	void load(const std::wstring &root);
	
	void report(const std::wstring &message);
	void report(const std::string &message);
	void start_experiment();
	void stop_experiment();
	
	std::forward_list<std::wstring> messages();
	
	const SliceProcessorController& slice_processor() const;
	
	SliceProcessorController& slice_processor();
	
	void experiment_begin_handler(const std::function<void()> &callback);
	void experiment_end_handler(const std::function<void()> &callback);
	void report_handler(const std::function<void()> &callback);
	void redraw_handler(const std::function<void()> &callback);
	void update_handler(const std::function<void()> &callback);
	void reset_handler(const std::function<void()> &callback);
	
private:
	SignalProcessorModel &_model;
	std::recursive_mutex mutable _guard;
	SliceProcessorController _slice_processor_controller;
	std::forward_list<std::wstring> _messages;
	std::function<void()> _experiment_begin_handler;
	std::function<void()> _experiment_end_handler;
	std::function<void()> _report_handler;
	//std::function<void()> _update_handler;
	std::function<void()> _reset_handler;
};

