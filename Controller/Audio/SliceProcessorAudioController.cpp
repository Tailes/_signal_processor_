#include "..\..\stdafx.hpp"
#include "..\..\targetver.hpp"
#include "..\..\Controller\Audio\SliceProcessorAudioController.hpp"

#include <Windows.h>
#include <mmsystem.h>


SliceProcessorAudioController::SliceProcessorAudioController()
{
}

SliceProcessorAudioController::~SliceProcessorAudioController()
{
}

void SliceProcessorAudioController::alert_speeding(bool alert) const
{
	if (alert)
	{
		::PlaySound(reinterpret_cast<PCTSTR>(SND_ALIAS_SYSTEMHAND), nullptr, SND_ALIAS_ID | SND_NOSTOP | SND_ASYNC);
	}
}
