#include "..\stdafx.hpp"
#include "..\Model\SignalProcessorModel.hpp"
#include "..\Controller\SignalProcessorController.hpp"

#include <cvt\wstring>
#include <cvt\utf8>



SignalProcessorController::SignalProcessorController(SignalProcessorModel &model):
	_model(model), _slice_processor_controller(model.slice_processor())
{
	_model.report_handler([this](const std::wstring &message) { this->report(message); });
}

SignalProcessorController::~SignalProcessorController()
{
}

void SignalProcessorController::report(const std::wstring &message)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_messages.push_front(message);
	_report_handler();
}

void SignalProcessorController::report(const std::string &message)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_messages.push_front(std::wstring_convert<stdext::cvt::codecvt_utf8<wchar_t>>().from_bytes(message));
	_report_handler();
}

std::forward_list<std::wstring> SignalProcessorController::messages()
{
	std::forward_list<std::wstring> clone;
	std::lock_guard<std::recursive_mutex> lock(_guard);
	std::swap(_messages, clone);
	return clone;
}

void SignalProcessorController::save(const std::wstring &root) const
{
	_slice_processor_controller.save(root);
}

void SignalProcessorController::load(const std::wstring &root)
{
	_slice_processor_controller.load(root);
}

void SignalProcessorController::start_experiment()
{
	try
	{
		_model.start_experiment();
		_experiment_begin_handler();
	}
	catch ( std::exception &ex )
	{
		this->report(ex.what());
	}
}

void SignalProcessorController::stop_experiment()
{
	try
	{
		_model.stop_experiment();
		_experiment_end_handler();
	}
	catch ( std::exception &ex )
	{
		this->report(ex.what());
	}
}

void SignalProcessorController::reset()
{
	try
	{
		_slice_processor_controller.reset();
		_reset_handler();
	}
	catch ( std::exception &ex )
	{
		this->report(ex.what());
	};
}



#pragma region Accessors
const SliceProcessorController& SignalProcessorController::slice_processor() const
{
	return _slice_processor_controller;
}

SliceProcessorController& SignalProcessorController::slice_processor()
{
	return _slice_processor_controller;
}
#pragma endregion



#pragma region Event Handlers
void SignalProcessorController::experiment_begin_handler(const std::function<void()> &callback)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_experiment_begin_handler = callback;
}

void SignalProcessorController::experiment_end_handler(const std::function<void()> &callback)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_experiment_end_handler = callback;
}

void SignalProcessorController::report_handler(const std::function<void()> &callback)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_report_handler = callback;
}

void SignalProcessorController::redraw_handler(const std::function<void()> &callback)
{
	_model.redraw_handler(callback);
}

void SignalProcessorController::update_handler(const std::function<void()> &callback)
{
	_model.update_handler(/*_update_handler = */callback);
}

void SignalProcessorController::reset_handler(const std::function<void()> &callback)
{
	_model.reset_handler(_reset_handler = callback);
}
#pragma endregion
