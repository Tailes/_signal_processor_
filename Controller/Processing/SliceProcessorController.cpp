#include "..\..\stdafx.hpp"
#include "..\..\Model\Processing\SliceProcessor.hpp"
#include "..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\Controller\Processing\SliceProcessorController.hpp"



SliceProcessorController::SliceProcessorController(SliceProcessor &model):
	_model(model),
	_slice_signal_monitoring_filter_controller(model.slice_probing_filter()),
	_slice_trace_monitoring_filter_controller(model.trace_probing_filter()),
	_slice_running_average_filter_controller(model.slice_running_average_filter()),
	_slice_dynamic_gain_filter_controller(model.slice_dynamic_gain_filter()),
	_slice_subtraction_filter_controller(model.slice_subtraction_filter()),
	_slice_adjustment_filter_controller(model.slice_adjustment_filter()),
	_slice_averaging_filter_controller(model.slice_averaging_filter()),
	_slice_recording_filter_controller(model.slice_recording_filter()),
	_slice_counting_filter_controller(model.slice_counting_filter()),
	_hardware_data_source_controller(model.hardware_filter())
	
{
	_slice_averaging_filter_controller.amount_callback([this](std::size_t amount) {this->on_slice_averaging_amount_callback(amount); });
	
	_hardware_data_source_controller.ethernet_data_source().channeler().signal_provider().signal_source().positioner().trigger_queue_length_callback([this](std::uint16_t queue_length) { this->ethernet_trigger_queue_length_callback(queue_length); });
	_hardware_data_source_controller.ethernet_data_source().channeler().signal_provider().signal_source().output().trigger_scans_callback([this](std::uint8_t scans) { this->on_ethernet_trigger_scans_callback(scans); });
	_hardware_data_source_controller.ethernet_data_source().channeler().signal_provider().signal_source().output().trigger_scans_callback([this](std::uint8_t scans) { _slice_recording_filter_controller.trace().on_ethernet_scans(scans); });
	_hardware_data_source_controller.ethernet_data_source().channeler().signal_provider().signal_source().output().trigger_steps_callback([this](std::uint8_t steps) { _slice_recording_filter_controller.trace().on_ethernet_steps(steps); });
	_hardware_data_source_controller.ethernet_data_source().settings().tick_length_callback([this](std::float_t length) {_slice_recording_filter_controller.trace().on_ethernet_tick_length(length); });
	
	//TODO: REFACTOR ME!!!!
	_model.slice_recording_filter().trace().step_length(_model.hardware_filter().ethernet_data_source().settings().tick_length());
	_model.slice_recording_filter().trace().step_count(_model.hardware_filter().ethernet_data_source().channeler().signal_provider().signal_source().output().steps_per_trigger());
}

void SliceProcessorController::save(const std::wstring &root) const
{
	_slice_signal_monitoring_filter_controller.save(root);
	_slice_trace_monitoring_filter_controller.save(root);
	_slice_running_average_filter_controller.save(root);
	_slice_dynamic_gain_filter_controller.save(root);
	_slice_subtraction_filter_controller.save(root);
	_slice_adjustment_filter_controller.save(root);
	_slice_averaging_filter_controller.save(root);
	_slice_recording_filter_controller.save(root);
	_hardware_data_source_controller.save(root);
}

void SliceProcessorController::load(const std::wstring &root)
{
	_slice_signal_monitoring_filter_controller.load(root);
	_slice_trace_monitoring_filter_controller.load(root);
	_slice_running_average_filter_controller.load(root);
	_slice_dynamic_gain_filter_controller.load(root);
	_slice_subtraction_filter_controller.load(root);
	_slice_adjustment_filter_controller.load(root);
	_slice_averaging_filter_controller.load(root);
	_slice_recording_filter_controller.load(root);
	_hardware_data_source_controller.load(root);
	
	//TODO: REFACTOR ME!!!!
	_model.slice_recording_filter().trace().step_length(_model.hardware_filter().ethernet_data_source().settings().tick_length());
	_model.slice_recording_filter().trace().step_count(_model.hardware_filter().ethernet_data_source().channeler().signal_provider().signal_source().output().steps_per_trigger());
}

void SliceProcessorController::reset()
{
	_model.reset();
}

const SliceSignalMonitoringFilterController& SliceProcessorController::slice_signal_monitoring_filter() const
{
	return _slice_signal_monitoring_filter_controller;
}

const SliceTraceMonitoringFilterController& SliceProcessorController::slice_trace_monitoring_filter() const
{
	return _slice_trace_monitoring_filter_controller;
}

const SliceRunningAverageFilterController & SliceProcessorController::slice_running_average_filter() const
{
	return _slice_running_average_filter_controller;
}

const SliceDynamicGainFilterController & SliceProcessorController::slice_dynamic_gain_filter() const
{
	return _slice_dynamic_gain_filter_controller;
}

const SliceSubtractionFilterController& SliceProcessorController::slice_subtraction_filter() const
{
	return _slice_subtraction_filter_controller;
}

const SliceAdjustmentFilterController& SliceProcessorController::slice_adjustment_filter() const
{
	return _slice_adjustment_filter_controller;
}

const SliceAveragingFilterController& SliceProcessorController::slice_averaging_filter() const
{
	return _slice_averaging_filter_controller;
}

const SliceRecordingFilterController& SliceProcessorController::slice_recording_filter() const
{
	return _slice_recording_filter_controller;
}

const SliceCountingFilterController & SliceProcessorController::slice_counting_filter() const
{
	return _slice_counting_filter_controller;
}

const HardwareDataSourceController& SliceProcessorController::hardware_data_source() const
{
	return _hardware_data_source_controller;
}




SliceSignalMonitoringFilterController& SliceProcessorController::slice_signal_monitoring_filter()
{
	return _slice_signal_monitoring_filter_controller;
}

SliceTraceMonitoringFilterController& SliceProcessorController::slice_trace_monitoring_filter()
{
	return _slice_trace_monitoring_filter_controller;
}

SliceRunningAverageFilterController & SliceProcessorController::slice_running_average_filter()
{
	return _slice_running_average_filter_controller;
}

SliceDynamicGainFilterController & SliceProcessorController::slice_dynamic_gain_filter()
{
	return _slice_dynamic_gain_filter_controller;
}

SliceSubtractionFilterController& SliceProcessorController::slice_subtraction_filter()
{
	return _slice_subtraction_filter_controller;
}

SliceAdjustmentFilterController& SliceProcessorController::slice_adjustment_filter()
{
	return _slice_adjustment_filter_controller;
}

SliceAveragingFilterController& SliceProcessorController::slice_averaging_filter()
{
	return _slice_averaging_filter_controller;
}

SliceRecordingFilterController& SliceProcessorController::slice_recording_filter()
{
	return _slice_recording_filter_controller;
}

SliceCountingFilterController & SliceProcessorController::slice_counting_filter()
{
	return _slice_counting_filter_controller;
}

HardwareDataSourceController& SliceProcessorController::hardware_data_source()
{
	return _hardware_data_source_controller;
}



void SliceProcessorController::ethernet_trigger_queue_length_callback(std::uint16_t queue_length)
{
	_slice_signal_monitoring_filter_controller.speeding(0 < queue_length);
	_slice_trace_monitoring_filter_controller.speeding(0 < queue_length);
	_slice_processor_audio_controller.alert_speeding(0 < queue_length);
}

void SliceProcessorController::on_ethernet_trigger_scans_callback(std::uint16_t scans)
{
	_slice_averaging_filter_controller.amount(scans);
}

void SliceProcessorController::on_slice_averaging_amount_callback(std::size_t amount)
{
	_hardware_data_source_controller.ethernet_data_source().channeler().signal_provider().signal_source().output().scans_per_trigger(static_cast<std::uint8_t>(amount));
}