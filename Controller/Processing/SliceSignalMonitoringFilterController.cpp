#include "..\..\stdafx.hpp"
#include "..\..\resource.hpp"
#include "..\..\Utility\Registry.hpp"
#include "..\..\OS\Drawing\Drawing.hpp"
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\Model\Processing\SliceProbingFilter.hpp"
#include "..\..\Controller\Data\GridController.hpp"
#include "..\..\Controller\Processing\SliceSignalMonitoringFilterController.hpp"


#include <atlbase.h>

#include "..\..\WTL\atlapp.h"
#include "..\..\WTL\atlgdi.h"
#include "..\..\WTL\atlmisc.h"


static const unsigned int GridLabelWidth = 80; //pix
static const unsigned int IconSize = 256; //pix

static const float DefaultGridBegin = 0.f;
static const float DefaultGridEnd = 1000.f;

static const WTL::CIcon SpeedingIcon = {static_cast<HICON>(::LoadImage(ModuleHelper::GetResourceInstance(), MAKEINTRESOURCEW(IDI_SPEEDING), IMAGE_ICON, IconSize, IconSize, 0))};



SliceSignalMonitoringFilterController::SliceSignalMonitoringFilterController(SliceProbingFilter &model) :
	_model(model), _visibility(-1), _speeding(false)
{
	_model.prepare_handler([this](Slice &slice) { this->prepare_handler(slice); });
	_model.apply_handler([this](Slice &slice, bool &interrupt) { this->apply_handler(slice, interrupt); });
	_model.close_handler([this]() { this->close_handler(); });
	_model.reset_handler([this]() { this->close_handler(); });
	_model.open_handler([this]() { this->close_handler(); });
}

void SliceSignalMonitoringFilterController::save(const std::wstring &root) const
{
	Registry registry(root);
	std::lock_guard<std::recursive_mutex> lock(_guard);
	registry.write_integer(L"SliceChannel1Color", _palette.at(0));
	registry.write_integer(L"SliceChannel2Color", _palette.at(1));
	registry.write_integer(L"SliceChannel3Color", _palette.at(2));
	registry.write_integer(L"SliceChannel4Color", _palette.at(3));
	registry.write_integer(L"SliceChannel5Color", _palette.at(4));
	registry.write_integer(L"SliceChannel6Color", _palette.at(5));
	registry.write_integer(L"SliceChannel7Color", _palette.at(6));
	registry.write_integer(L"SliceChannel8Color", _palette.at(7));
}

void SliceSignalMonitoringFilterController::load(const std::wstring &root)
{
	Registry registry(root);
	std::lock_guard<std::recursive_mutex> lock(_guard);
	registry.read_integer(L"SliceChannel1Color", _palette.at(0));
	registry.read_integer(L"SliceChannel2Color", _palette.at(1));
	registry.read_integer(L"SliceChannel3Color", _palette.at(2));
	registry.read_integer(L"SliceChannel4Color", _palette.at(3));
	registry.read_integer(L"SliceChannel5Color", _palette.at(4));
	registry.read_integer(L"SliceChannel6Color", _palette.at(5));
	registry.read_integer(L"SliceChannel7Color", _palette.at(6));
	registry.read_integer(L"SliceChannel8Color", _palette.at(7));
}

void SliceSignalMonitoringFilterController::draw_axis(WTL::CDCT<false> &dc, const WTL::CRect &rect) const
{
	utility::draw_background(dc, rect);
	std::lock_guard<std::recursive_mutex> lock(_guard);
	utility::draw_axis_labels(dc, rect, GridController(dc, this->timeline_begin(), this->timeline_end(), rect.Width()));
}

void SliceSignalMonitoringFilterController::speeding(bool exceeds)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_speeding = exceeds;
}

void SliceSignalMonitoringFilterController::draw_signals(WTL::CDCT<false> &dc, const WTL::CRect &rect) const
{
	utility::draw_background(dc, rect);
	std::lock_guard<std::recursive_mutex> lock(_guard);
	utility::draw_uniform_grid(dc, rect, GridController(dc, this->timeline_begin(), this->timeline_end(), rect.Width()));
	for ( std::size_t i = 0; i < _storage.batch().size(); ++i )
		if ( _visibility[i] )
			utility::draw_signal(dc, rect, _storage.batch().at(i), _palette.at(i));
	if (_speeding)
		utility::draw_icon(dc, rect, SpeedingIcon);
}

void SliceSignalMonitoringFilterController::track(WTL::CDCT<false> &dc, const WTL::CRect &rect, const WTL::CPoint &point) const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_tracking_callback(GridController(dc, this->timeline_begin(), this->timeline_end(), rect.Width()).value(static_cast<float>(point.x - rect.left) / static_cast<float>(rect.Width())));
}

void SliceSignalMonitoringFilterController::color(std::size_t index, const Color::rgba &color)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_palette.at(index) = color;
}

void SliceSignalMonitoringFilterController::clear()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for ( std::size_t i = 0; i < _storage.batch().size(); ++i )
		_storage.batch().at(i).clear();
}

void SliceSignalMonitoringFilterController::show(std::size_t index, bool visible)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_visibility[index] = visible;
}

bool SliceSignalMonitoringFilterController::visible(std::size_t index) const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _visibility[index];
}

Color::rgba SliceSignalMonitoringFilterController::color(std::size_t index) const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _palette.at(index);
}

void SliceSignalMonitoringFilterController::tracking_callback(std::function<void(float)> &&callback)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_tracking_callback = std::forward<std::function<void(float)>>(callback);
}




float SliceSignalMonitoringFilterController::timeline_begin() const
{
	return _storage.timeline().empty() ? DefaultGridBegin : static_cast<float>(_storage.timeline().front());
}

float SliceSignalMonitoringFilterController::timeline_end() const
{
	return _storage.timeline().empty() ? DefaultGridEnd : static_cast<float>(_storage.timeline().back());
}




void SliceSignalMonitoringFilterController::open_handler()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_storage.timeline().assign(0, real_t());
	_storage.batch().fill(Signal());
}

void SliceSignalMonitoringFilterController::prepare_handler(Slice &slice)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for ( std::size_t i = 0; i < _storage.batch().size(); ++i )
		_storage.batch().at(i).reserve(slice.batch().at(i).size());
	_storage.timeline().reserve(slice.timeline().size());
}

void SliceSignalMonitoringFilterController::apply_handler(Slice &slice, bool &interrupt)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_storage = slice;
}

void SliceSignalMonitoringFilterController::close_handler()
{
}

void SliceSignalMonitoringFilterController::reset_handler()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_storage = Slice();
}

