#pragma once
#include <functional>
#include <cstddef>
#include <string>



class SliceDynamicGainFilter;



class SliceDynamicGainFilterController
{
public:
	SliceDynamicGainFilterController(SliceDynamicGainFilter &model);
	
	void amount_callback(std::function<void(float)> &&callback);
	
	void save(const std::wstring &root) const;
	void load(const std::wstring &root);
	
	void amount(float amount);
	
	float amount() const;
	
private:
	SliceDynamicGainFilter &_model;
	std::function<void(float)> _amount_callback;
};

