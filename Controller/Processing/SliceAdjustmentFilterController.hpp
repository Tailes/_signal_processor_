#pragma once

#include <cstdint>



class SliceAdjustmentFilter;



class SliceAdjustmentFilterController
{
public:
	SliceAdjustmentFilterController(SliceAdjustmentFilter &model);
	
	void save(const std::wstring &root) const;
	void load(const std::wstring &root);
	
	void amplification(std::size_t index, float amplification);
	void magnitude(std::size_t index, float magnitude);
	void zero(std::size_t index, float zero);
	
	float amplification(std::size_t index) const;
	float magnitude(std::size_t index) const;
	float zero(std::size_t index) const;
	
private:
	SliceAdjustmentFilter &_model;
};

