#include "..\..\stdafx.hpp"
#include "..\..\Controller\Utility\Callbacks.hpp"
#include "..\..\Model\Processing\SliceDynamicGainFilter.hpp"
#include "..\..\Controller\Processing\SliceDynamicGainFilterController.hpp"



SliceDynamicGainFilterController::SliceDynamicGainFilterController(SliceDynamicGainFilter &model):
	_model(model)
{
}

void SliceDynamicGainFilterController::amount_callback(std::function<void(float)>&& callback)
{
	_amount_callback = _amount_callback + std::move(callback);
}

void SliceDynamicGainFilterController::save(const std::wstring &root) const
{
}

void SliceDynamicGainFilterController::load(const std::wstring &root)
{
}

void SliceDynamicGainFilterController::amount(float amount)
{
	_model.amount(static_cast<real_t>(amount));
	_amount_callback(amount);
}

float SliceDynamicGainFilterController::amount() const
{
	return static_cast<float>(_model.amount());
}