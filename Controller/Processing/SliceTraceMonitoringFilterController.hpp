#pragma once
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\OS\Drawing\Color.hpp"
#include "..\..\OS\Drawing\Bitmap.hpp"
#include "..\..\OS\Drawing\Gradient.hpp"

#include <cstdint>
#include <mutex>
#include <array>


class SliceProbingFilter;


namespace WTL { class CRect; }
namespace WTL { template<bool Managed> class CDCT; }



class SliceTraceMonitoringFilterController
{
public:
	SliceTraceMonitoringFilterController(SliceProbingFilter &model);
	
	void activate(int index);
	void speeding(bool exceeds);
	void enable(bool state = true);
	void color(std::size_t index, const Color::rgba &color);
	void draw(WTL::CDCT<false> &dc, const WTL::CRect &rect) const;
	void load(const std::wstring &root);
	void save(const std::wstring &root) const;
	
	Color::rgba color(std::size_t index) const;
	bool enabled() const;
	int active() const;
	
private:
	SliceProbingFilter &_model;
	std::recursive_mutex mutable _guard;
	std::array<Color::rgba, std::tuple_size<Batch>::value> _palette;
	std::array<Gradient, std::tuple_size<Batch>::value> _gradients;
	std::array<Bitmap, std::tuple_size<Batch>::value> _bitmaps;
	std::size_t _active;
	std::size_t _line;
	bool _speeding;
	bool _enabled;
	
	void apply_handler(Slice &slice, bool &interrupt);
	void reset_handler();
	void open_handler();
};

