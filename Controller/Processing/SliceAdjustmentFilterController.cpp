#include "..\..\stdafx.hpp"
#include "..\..\Model\Types\Real.hpp"
#include "..\..\Model\Processing\SliceAdjustmentFilter.hpp"
#include "..\..\Controller\Processing\SliceAdjustmentFilterController.hpp"



SliceAdjustmentFilterController::SliceAdjustmentFilterController(SliceAdjustmentFilter &model):
	_model(model)
{
}

void SliceAdjustmentFilterController::save(const std::wstring &root) const
{
}

void SliceAdjustmentFilterController::load(const std::wstring &root)
{
}

void SliceAdjustmentFilterController::amplification(std::size_t index, float amplification)
{
	_model.amplification(index, static_cast<real_t>(amplification));
}

void SliceAdjustmentFilterController::magnitude(std::size_t index, float magnitude)
{
	_model.magnitude(index, static_cast<real_t>(magnitude));
}

void SliceAdjustmentFilterController::zero(std::size_t index, float zero)
{
	_model.zero(index, static_cast<real_t>(zero));
}


float SliceAdjustmentFilterController::amplification(std::size_t index) const
{
	return static_cast<float>(_model.amplification(index));
}

float SliceAdjustmentFilterController::magnitude(std::size_t index) const
{
	return static_cast<float>(_model.magnitude(index));
}

float SliceAdjustmentFilterController::zero(std::size_t index) const
{
	return static_cast<float>(_model.zero(index));
}


