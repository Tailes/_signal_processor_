#include "..\..\stdafx.hpp"
#include "..\..\resource.hpp"
#include "..\..\Utility\Registry.hpp"
#include "..\..\OS\Drawing\Drawing.hpp"
#include "..\..\Model\Processing\SliceProbingFilter.hpp"
#include "..\..\Controller\Processing\SliceTraceMonitoringFilterController.hpp"

#include <functional>

#include <atlbase.h>

#include "..\..\WTL\atlapp.h"
#include "..\..\WTL\atlgdi.h"




static const unsigned long TraceWindowWidth = 2048;
static const unsigned long TraceWindowHeight = 1024;
static const unsigned int IconSize = 256; //pix

static const WTL::CIcon SpeedingIcon = {static_cast<HICON>(::LoadImage(ModuleHelper::GetResourceInstance(), MAKEINTRESOURCEW(IDI_SPEEDING), IMAGE_ICON, IconSize, IconSize, 0))};



SliceTraceMonitoringFilterController::SliceTraceMonitoringFilterController(SliceProbingFilter &model):
	_model(model), _active(0), _enabled(true), _line(0), _speeding(false)
{
	for ( auto it = _bitmaps.begin(); it != _bitmaps.end(); ++it )
		it->size(TraceWindowWidth, TraceWindowHeight);
	_model.apply_handler([this](Slice &slice, bool &interrupt) { this->apply_handler(slice, interrupt); });
	_model.reset_handler([this]() { this->reset_handler(); });
	_model.open_handler([this]() { this->open_handler(); });
}

void SliceTraceMonitoringFilterController::save(const std::wstring &root) const
{
	Registry registry(root);
	std::lock_guard<std::recursive_mutex> lock(_guard);
	registry.write_integer(L"TraceChannel1Color", _palette.at(0));
	registry.write_integer(L"TraceChannel2Color", _palette.at(1));
	registry.write_integer(L"TraceChannel3Color", _palette.at(2));
	registry.write_integer(L"TraceChannel4Color", _palette.at(3));
	registry.write_integer(L"TraceChannel5Color", _palette.at(4));
	registry.write_integer(L"TraceChannel6Color", _palette.at(5));
	registry.write_integer(L"TraceChannel7Color", _palette.at(6));
	registry.write_integer(L"TraceChannel8Color", _palette.at(7));
}

void SliceTraceMonitoringFilterController::load(const std::wstring &root)
{
	Registry registry(root);
	std::lock_guard<std::recursive_mutex> lock(_guard);
	registry.read_integer(L"TraceChannel1Color", _palette.at(0));
	registry.read_integer(L"TraceChannel2Color", _palette.at(1));
	registry.read_integer(L"TraceChannel3Color", _palette.at(2));
	registry.read_integer(L"TraceChannel4Color", _palette.at(3));
	registry.read_integer(L"TraceChannel5Color", _palette.at(4));
	registry.read_integer(L"TraceChannel6Color", _palette.at(5));
	registry.read_integer(L"TraceChannel7Color", _palette.at(6));
	registry.read_integer(L"TraceChannel8Color", _palette.at(7));
	for ( std::size_t i = 0; i < std::min(_bitmaps.size(), _palette.size()); ++i )
		_gradients.at(i).fill(0, _palette.at(i));
}

void SliceTraceMonitoringFilterController::draw(WTL::CDCT<false> &dc, const WTL::CRect &rect) const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	if (_active != -1) utility::draw_split_bitmap(dc, rect, _bitmaps.at(_active), _gradients.at(_active), _line);
	if (_speeding) utility::draw_icon(dc, rect, SpeedingIcon);
	utility::draw_grid(dc, rect);
}

void SliceTraceMonitoringFilterController::color(std::size_t index, const Color::rgba &color)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_gradients.at(index).fill(0, color);
	_palette.at(index) = color;
}

void SliceTraceMonitoringFilterController::activate(int index)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_active = index;
}

void SliceTraceMonitoringFilterController::speeding(bool exceeds)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_speeding = exceeds;
}

void SliceTraceMonitoringFilterController::enable(bool state)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_enabled = state;
}

int SliceTraceMonitoringFilterController::active() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _active;
}

Color::rgba SliceTraceMonitoringFilterController::color(std::size_t index) const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _palette.at(index);
}


bool SliceTraceMonitoringFilterController::enabled() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _enabled;
}

void SliceTraceMonitoringFilterController::apply_handler(Slice &slice, bool &interrupt)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	if ( !_enabled ) return;
	for ( std::size_t i = 0; i < slice.batch().size(); ++i )
	{
		unsigned long width = _bitmaps.at(i).width();
		unsigned long height = _bitmaps.at(i).height();
		for ( std::size_t j = 0; j < slice.batch().at(i).size(); ++j )
		{
			for ( std::size_t k = j*height/slice.batch().at(i).size(); k < (1 + j)*height/slice.batch().at(i).size(); ++k )
			{
				_bitmaps.at(i).pixel(_line, height - k - 1, static_cast<std::uint8_t>(std::min<real_t>(255.0, (slice.batch().at(i).at(j) + 1.0)*128.0)));
			}
		}
	}
	_line = ++_line % TraceWindowWidth;
}

void SliceTraceMonitoringFilterController::reset_handler()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	std::for_each(_bitmaps.begin(), _bitmaps.end(), std::mem_fun_ref(&Bitmap::clear));
}

void SliceTraceMonitoringFilterController::open_handler()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	std::for_each(_bitmaps.begin(), _bitmaps.end(), std::mem_fun_ref(&Bitmap::clear));
	_line = 0;
}
