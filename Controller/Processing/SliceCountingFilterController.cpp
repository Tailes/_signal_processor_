#include "..\..\stdafx.hpp"
#include "..\..\Model\Types\Real.hpp"
#include "..\..\Model\Processing\SliceCountingFilter.hpp"
#include "..\..\Controller\Processing\SliceCountingFilterController.hpp"



SliceCountingFilterController::SliceCountingFilterController(SliceCountingFilter &model):
	_model(model)
{
}

std::size_t SliceCountingFilterController::amount() const
{
	return _model.amount();
}


