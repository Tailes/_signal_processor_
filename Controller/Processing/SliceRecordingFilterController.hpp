#pragma once

#include "..\..\Controller\Data\TraceController.hpp"

#include <cstdint>



class SliceRecordingFilter;



class SliceRecordingFilterController
{
public:
	SliceRecordingFilterController(SliceRecordingFilter &model);
	
	void save(const std::wstring &root) const;
	void load(const std::wstring &root);
	
	const TraceController& trace() const;
	TraceController& trace();
	
	void reset_counter();
	
	void limit(std::size_t value);
	void enable(bool state = true);
	void limited(bool state);
	
	std::size_t counter() const;
	std::size_t limit() const;
	bool suspended() const;
	bool limited() const;
	bool enabled() const;
	
private:
	SliceRecordingFilter &_model;
	TraceController _trace;
};

