#pragma once

#include <cstdint>



class SliceCountingFilter;



class SliceCountingFilterController
{
public:
	SliceCountingFilterController(SliceCountingFilter &model);
	
	std::size_t amount() const;
	
private:
	SliceCountingFilter &_model;
};

