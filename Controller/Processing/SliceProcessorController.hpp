#pragma once
#include "..\..\Controller\Audio\SliceProcessorAudioController.hpp"
#include "..\..\Controller\Hardware\HardwareDataSourceController.hpp"
#include "..\..\Controller\Processing\SliceCountingFilterController.hpp"
#include "..\..\Controller\Processing\SliceAveragingFilterController.hpp"
#include "..\..\Controller\Processing\SliceRecordingFilterController.hpp"
#include "..\..\Controller\Processing\SliceAdjustmentFilterController.hpp"
#include "..\..\Controller\Processing\SliceSubtractionFilterController.hpp"
#include "..\..\Controller\Processing\SliceDynamicGainFilterController.hpp"
#include "..\..\Controller\Processing\SliceRunningAverageFilterController.hpp"
#include "..\..\Controller\Processing\SliceTraceMonitoringFilterController.hpp"
#include "..\..\Controller\Processing\SliceSignalMonitoringFilterController.hpp"



class Color::rgba;
class SliceProcessor;
namespace WTL{class CRect;}
namespace WTL{template<bool> class CDCT;}



class SliceProcessorController
{
public:
	SliceProcessorController(SliceProcessor &model);
	
	void save(const std::wstring &root) const;
	void load(const std::wstring &root);
	
	void reset();
	
	const SliceSignalMonitoringFilterController& slice_signal_monitoring_filter() const;
	const SliceTraceMonitoringFilterController& slice_trace_monitoring_filter() const;
	const SliceRunningAverageFilterController& slice_running_average_filter() const;
	const SliceDynamicGainFilterController& slice_dynamic_gain_filter() const;
	const SliceSubtractionFilterController& slice_subtraction_filter() const;
	const SliceAdjustmentFilterController& slice_adjustment_filter() const;
	const SliceAveragingFilterController& slice_averaging_filter() const;
	const SliceRecordingFilterController& slice_recording_filter() const;
	const SliceCountingFilterController& slice_counting_filter() const;
	const HardwareDataSourceController& hardware_data_source() const;
	
	SliceSignalMonitoringFilterController& slice_signal_monitoring_filter();
	SliceTraceMonitoringFilterController& slice_trace_monitoring_filter();
	SliceRunningAverageFilterController& slice_running_average_filter();
	SliceDynamicGainFilterController& slice_dynamic_gain_filter();
	SliceSubtractionFilterController& slice_subtraction_filter();
	SliceAdjustmentFilterController& slice_adjustment_filter();
	SliceAveragingFilterController& slice_averaging_filter();
	SliceRecordingFilterController& slice_recording_filter();
	SliceCountingFilterController& slice_counting_filter();
	HardwareDataSourceController& hardware_data_source();
	
private:
	SliceProcessor &_model;
	SliceSignalMonitoringFilterController _slice_signal_monitoring_filter_controller;
	SliceTraceMonitoringFilterController _slice_trace_monitoring_filter_controller;
	SliceRunningAverageFilterController _slice_running_average_filter_controller;
	SliceDynamicGainFilterController _slice_dynamic_gain_filter_controller;
	SliceSubtractionFilterController _slice_subtraction_filter_controller;
	SliceAdjustmentFilterController _slice_adjustment_filter_controller;
	SliceAveragingFilterController _slice_averaging_filter_controller;
	SliceRecordingFilterController _slice_recording_filter_controller;
	SliceCountingFilterController _slice_counting_filter_controller;
	SliceProcessorAudioController _slice_processor_audio_controller;
	HardwareDataSourceController _hardware_data_source_controller;
	
	void ethernet_trigger_queue_length_callback(std::uint16_t queue_length);
	void on_ethernet_trigger_scans_callback(std::uint16_t scans);
	void on_slice_averaging_amount_callback(std::size_t amount);
};

