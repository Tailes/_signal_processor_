#include "..\..\stdafx.hpp"
#include "..\..\Controller\Utility\Callbacks.hpp"
#include "..\..\Model\Processing\SliceAveragingFilter.hpp"
#include "..\..\Controller\Processing\SliceAveragingFilterController.hpp"



SliceAveragingFilterController::SliceAveragingFilterController(SliceAveragingFilter &model):
	_model(model)
{
}

void SliceAveragingFilterController::amount_callback(std::function<void(std::size_t)>&& callback)
{
	_amount_callback = _amount_callback + std::move(callback);
}

void SliceAveragingFilterController::save(const std::wstring &root) const
{
}

void SliceAveragingFilterController::load(const std::wstring &root)
{
}

void SliceAveragingFilterController::amount(std::size_t amount)
{
	if (_model.amount() != amount)
	{
		_model.amount(amount);
		_amount_callback(amount);
	}
}

std::size_t SliceAveragingFilterController::amount() const
{
	return _model.amount();
}