#pragma once



class SliceSubtractionFilter;



class SliceSubtractionFilterController
{
public:
	SliceSubtractionFilterController(SliceSubtractionFilter &model);
	
	void save(const std::wstring &root) const;
	void load(const std::wstring &root);
	
	void accept();
	void cancel();
	void enable(bool state = true);
	
	bool enabled() const;
	
private:
	SliceSubtractionFilter &_model;
};

