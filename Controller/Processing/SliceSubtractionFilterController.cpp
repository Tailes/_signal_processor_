#include "..\..\stdafx.hpp"
#include "..\..\Model\Processing\SliceSubtractionFilter.hpp"
#include "..\..\Controller\Processing\SliceSubtractionFilterController.hpp"




SliceSubtractionFilterController::SliceSubtractionFilterController(SliceSubtractionFilter &model):
	_model(model)
{
}

void SliceSubtractionFilterController::save(const std::wstring &root) const
{
}

void SliceSubtractionFilterController::load(const std::wstring &root)
{
}

void SliceSubtractionFilterController::accept()
{
	_model.accept();
}

void SliceSubtractionFilterController::cancel()
{
	_model.cancel();
}

void SliceSubtractionFilterController::enable(bool state)
{
	_model.enable(state);
}

bool SliceSubtractionFilterController::enabled() const
{
	return _model.enabled();
}
