#include "..\..\stdafx.hpp"
#include "..\..\Controller\Utility\Callbacks.hpp"
#include "..\..\Model\Processing\SliceRunningAverageFilter.hpp"
#include "..\..\Controller\Processing\SliceRunningAverageFilterController.hpp"



SliceRunningAverageFilterController::SliceRunningAverageFilterController(SliceRunningAverageFilter &model):
	_model(model)
{
}

void SliceRunningAverageFilterController::amount_callback(std::function<void(std::size_t)>&& callback)
{
	_amount_callback = _amount_callback + std::move(callback);
}

void SliceRunningAverageFilterController::save(const std::wstring &root) const
{
}

void SliceRunningAverageFilterController::load(const std::wstring &root)
{
}

void SliceRunningAverageFilterController::amount(std::size_t amount)
{
	_model.amount(amount);
	_amount_callback(amount);
}

std::size_t SliceRunningAverageFilterController::amount() const
{
	return _model.amount();
}