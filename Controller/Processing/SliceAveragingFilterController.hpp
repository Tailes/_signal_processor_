#pragma once
#include <functional>
#include <cstddef>



class SliceAveragingFilter;



class SliceAveragingFilterController
{
public:
	SliceAveragingFilterController(SliceAveragingFilter &model);
	
	void amount_callback(std::function<void(std::size_t)> &&callback);
	
	void save(const std::wstring &root) const;
	void load(const std::wstring &root);
	
	void amount(std::size_t amount);
	
	std::size_t amount() const;
	
private:
	SliceAveragingFilter &_model;
	std::function<void(std::size_t)> _amount_callback;
};

