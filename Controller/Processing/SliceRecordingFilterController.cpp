#include "..\..\stdafx.hpp"
#include "..\..\Model\Processing\SliceRecordingFilter.hpp"
#include "..\..\Controller\Data\TraceController.hpp"
#include "..\..\Controller\Processing\SliceRecordingFilterController.hpp"



SliceRecordingFilterController::SliceRecordingFilterController(SliceRecordingFilter &model):
	_model(model), _trace(model.trace())
{
}

void SliceRecordingFilterController::save(const std::wstring &root) const
{
}

void SliceRecordingFilterController::load(const std::wstring &root)
{
}

const TraceController &SliceRecordingFilterController::trace() const
{
	return _trace;
}

TraceController &SliceRecordingFilterController::trace()
{
	return _trace;
}

void SliceRecordingFilterController::reset_counter()
{
	_model.reset_counter();
}

void SliceRecordingFilterController::limit(std::size_t value)
{
	_model.limit(value);
}

void SliceRecordingFilterController::enable(bool state)
{
	_model.enable(state);
}

void SliceRecordingFilterController::limited(bool state)
{
	_model.limited(state);
}


std::size_t SliceRecordingFilterController::counter() const
{
	return _model.counter();
}

std::size_t SliceRecordingFilterController::limit() const
{
	return _model.limit();
}

bool SliceRecordingFilterController::suspended() const
{
	return _model.suspended();
}

bool SliceRecordingFilterController::limited() const
{
	return _model.limited();
}

bool SliceRecordingFilterController::enabled() const
{
	return _model.enabled();
}