#pragma once
#include "..\..\OS\Drawing\Color.hpp"
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\Model\Data\Signal.hpp"

#include <functional>
#include <cstdint>
#include <memory>
#include <bitset>
#include <array>
#include <mutex>




class SliceProbingFilter;

namespace WTL { class CRect; class CPoint; }
namespace WTL { template<bool> class CDCT; }


class SliceSignalMonitoringFilterController
{
public:
	SliceSignalMonitoringFilterController(SliceProbingFilter &model);
	
	void draw_signals(WTL::CDCT<false> &dc, const WTL::CRect &rect) const;
	void draw_axis(WTL::CDCT<false> &dc, const WTL::CRect &rect) const;
	void speeding(bool exceeds);
	void track(WTL::CDCT<false> &dc, const WTL::CRect &rect, const WTL::CPoint &point) const;
	void clear();
	void save(const std::wstring &root) const;
	void load(const std::wstring &root);
	
	void tracking_callback(std::function<void(float)> &&callback);
	void color(std::size_t index, const Color::rgba &color);
	void show(std::size_t index, bool visible);
	
	Color::rgba color(std::size_t index) const;
	bool visible(std::size_t index) const;
	
private:
	SliceProbingFilter &_model;
	std::recursive_mutex mutable _guard;
	std::array<Color::rgba, std::tuple_size<Batch>::value> _palette;
	std::function<void(float)> _tracking_callback;
	std::bitset<std::tuple_size<Batch>::value> _visibility;
	Slice _storage;
	bool _speeding;
	
	float timeline_begin() const;
	float timeline_end() const;
	
	void prepare_handler(Slice &slice);
	void apply_handler(Slice &slice, bool &interrupt);
	void close_handler();
	void reset_handler();
	void open_handler();
};

