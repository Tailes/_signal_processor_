#pragma once
#include "..\..\Model\Implementations\SliceFilterImpl.hpp"

#include <atomic>



class Slice;
class Trace;



class SliceCountingFilter:
	public SliceFilterImpl
{
public:
	SliceCountingFilter();
	
	virtual void prepare(Slice &slice);
	virtual void apply(Slice &slice, bool &interrupt);
	virtual void reset();
	
	std::size_t amount() const;
	
private:
	std::atomic<std::size_t> _amount;
};

