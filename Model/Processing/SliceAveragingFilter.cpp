#include "..\..\StdAfx.hpp"
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\Model\Processing\SliceAveragingFilter.hpp"

#include <algorithm>



SliceAveragingFilter::SliceAveragingFilter():
	_counter(0), _storage(1)
{
}

void SliceAveragingFilter::prepare(Slice &slice)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	std::fill(_storage.begin(), _storage.end(), Slice());
	_counter = 0;
}

void SliceAveragingFilter::apply(Slice &slice, bool &interrupt)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_storage.at(_counter) = slice;
	if (!(interrupt = !(++_counter == _storage.size())))
	{
		_counter = 0;
		for (std::size_t i = 0; i < slice.batch().size(); ++i)
		{
			for (std::size_t j = 0; j < slice.batch().at(i).size(); ++j)
			{
				for (std::size_t k = 0; k + 1 < _storage.size(); ++k)
				{
					slice.batch().at(i).at(j) += _storage.at(k).batch().at(i).at(j);
				}
				slice.batch().at(i).at(j) /= _storage.size();
			}
		}
	}
}

void SliceAveragingFilter::reset()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_counter = 0;
}



void SliceAveragingFilter::amount(std::size_t amount)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_storage.resize(amount);
	_counter = std::min<std::size_t>(_counter, amount - 1);
}

std::size_t SliceAveragingFilter::amount() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _storage.size();
}
