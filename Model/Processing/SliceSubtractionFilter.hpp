#pragma once
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\Model\Implementations\SliceFilterImpl.hpp"

#include <mutex>



class SliceSubtractionFilter:
	public SliceFilterImpl
{
public:
	SliceSubtractionFilter();
	
	virtual void open();
	virtual void prepare(Slice &slice);
	virtual void apply(Slice &slice, bool &interrupt);
	virtual void reset();
	
	void accept();
	void cancel();
	void enable(bool state = true);
	
	bool enabled() const;
	
private:
	std::recursive_mutex mutable _guard;
	Slice _subtraction;
	Batch _adjusted;
	Slice _current;
	bool _enabled;
	
private:
	void interpolate(Amplitudes &target, const Timeline &target_grid, const Amplitudes &source, const Timeline &source_grid) const;
	void subtract(Amplitudes &target, const Amplitudes &left, const Amplitudes &right) const;
	void record(const Slice &slice);
	void apply(Batch &batch, const Timeline &timeline);
};