#pragma once
#include "..\..\Model\Implementations\SliceFilterImpl.hpp"

#include <functional>
#include <memory>
#include <mutex>



class Slice;
class Trace;



class SliceProbingFilter:
	public SliceFilterImpl
{
public:
	SliceProbingFilter();
	
	virtual void open();
	virtual void prepare(Slice &slice);
	virtual void apply(Slice &slice, bool &interrupt);
	virtual void close();
	virtual void reset();
	
	void prepare_handler(std::function<void(Slice &)> &&handler);
	void apply_handler(std::function<void(Slice &, bool &)> &&handler);
	void close_handler(std::function<void()> &&handler);
	void reset_handler(std::function<void()> &&handler);
	void open_handler(std::function<void()> &&handler);
	
private:
	std::recursive_mutex mutable _guard;
	std::function<void(Slice &, bool &)> _apply_handler;
	std::function<void(Slice &)> _prepare_handler;
	std::function<void()> _close_handler;
	std::function<void()> _reset_handler;
	std::function<void()> _open_handler;
};

