#include "..\..\StdAfx.hpp"
#include "..\..\Model\Processing\SliceAdjustmentFilter.hpp"

#include <algorithm>



SliceAdjustmentFilter::SliceAdjustmentFilter()
{
	std::fill(_amplifications.begin(), _amplifications.end(), 1.0);
	std::fill(_magnitudes.begin(), _magnitudes.end(), 1.0);
	std::fill(_zeros.begin(), _zeros.end(), 0.0);
}

void SliceAdjustmentFilter::apply(Slice &slice, bool &interrupt)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for ( std::size_t i = 0; i < slice.batch().size(); ++i )
		for ( std::size_t j = 0; j < slice.batch().at(i).size(); ++j )
			slice.batch().at(i).at(j) = (slice.batch().at(i).at(j) + _zeros.at(i)) * _amplifications.at(i) * _magnitudes.at(i);
}

void SliceAdjustmentFilter::amplification(std::size_t index, real_t amplification)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_amplifications.at(index) = amplification;
}

void SliceAdjustmentFilter::magnitude(std::size_t index, real_t magnitude)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_magnitudes.at(index) = magnitude;
}

void SliceAdjustmentFilter::zero(std::size_t index, real_t zero)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_zeros.at(index) = zero;
}

real_t SliceAdjustmentFilter::amplification(std::size_t index) const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _amplifications.at(index);
}

real_t SliceAdjustmentFilter::magnitude(std::size_t index) const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _magnitudes.at(index);
}

real_t SliceAdjustmentFilter::zero(std::size_t index) const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _zeros.at(index);
}