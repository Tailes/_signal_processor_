#include "..\..\StdAfx.hpp"
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\Model\Data\Interpolator.hpp"
#include "..\..\Model\Processing\SliceSubtractionFilter.hpp"



SliceSubtractionFilter::SliceSubtractionFilter():
	_enabled(false)
{
}

void SliceSubtractionFilter::open()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_subtraction = Slice();
	_adjusted = Batch();
	_current = Slice();
}

void SliceSubtractionFilter::prepare(Slice &slice)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	auto adjust_it = _adjusted.begin();
	auto current_bit = _current.batch().begin();
	auto subtraction_bit = _subtraction.batch().begin();
	_current.timeline().resize(slice.timeline().size());
	_subtraction.timeline().resize(slice.timeline().size());
	for (auto const &signal : slice.batch())
	{
		(*adjust_it++).resize(signal.size());
		(*current_bit++).resize(signal.size());
		(*subtraction_bit++).resize(signal.size());
	}
}

void SliceSubtractionFilter::apply(Slice &slice, bool &interrupt)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	if (slice.completed()) this->record(slice);
	if (_enabled) this->apply(slice.batch(), slice.timeline());
}

void SliceSubtractionFilter::reset()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	std::for_each(std::begin(_subtraction.batch()), std::end(_subtraction.batch()), [](Batch::value_type &signal) { signal.clear(); });
	std::for_each(std::begin(_current.batch()), std::end(_current.batch()), [](Batch::value_type &signal) { signal.clear(); });
	std::for_each(std::begin(_adjusted), std::end(_adjusted), [](Batch::value_type &signal) { signal.clear(); });
	_subtraction.timeline().clear();
	_current.timeline().clear();
}

void SliceSubtractionFilter::accept()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_subtraction = _current;
}

void SliceSubtractionFilter::cancel()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	std::for_each(std::begin(_subtraction.batch()), std::end(_subtraction.batch()), [](Batch::value_type &signal) { signal.clear(); });
	std::for_each(std::begin(_adjusted), std::end(_adjusted), [](Batch::value_type &signal) { signal.clear(); });
	_subtraction.timeline().clear();
}

void SliceSubtractionFilter::enable(bool state)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_enabled = state;
}

bool SliceSubtractionFilter::enabled() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _enabled;
}





void SliceSubtractionFilter::record(const Slice &slice)
{
	_current = slice;
}

void SliceSubtractionFilter::apply(Batch &batch, const Timeline &timeline)
{
	auto sub_it = _subtraction.batch().begin();
	auto adjusted_it = _adjusted.begin();
	for (auto target_it = batch.begin(); target_it != batch.end(); ++target_it, ++sub_it)
	{
		if (!(*sub_it).empty() && !(*target_it).empty() && (*sub_it).size() == (*target_it).size())
		{
			this->interpolate(*adjusted_it, timeline, *sub_it, _subtraction.timeline());
			this->subtract(*target_it, *target_it, *adjusted_it++);
		}
	}
}

void SliceSubtractionFilter::interpolate(Amplitudes &target, const Timeline &target_grid, const Amplitudes &source, const Timeline &source_grid) const
{
	std::copy(std::begin(source), std::end(source), std::begin(target));
	//Interpolator(source_grid, source).run(target, target_grid, &Interpolator::linear_kernel);
}

void SliceSubtractionFilter::subtract(Amplitudes &target, const Amplitudes &left, const Amplitudes &right) const
{
	std::transform(std::begin(left), std::end(left), std::begin(right), std::begin(target), std::minus<Amplitudes::value_type>());
}

