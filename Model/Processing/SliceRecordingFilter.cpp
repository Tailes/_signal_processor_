#include "..\..\StdAfx.hpp"
#include "..\..\Model\Processing\SliceRecordingFilter.hpp"



SliceRecordingFilter::SliceRecordingFilter():
	_enabled(false), _counter(0), _limit(0), _limited(false)
{
}

void SliceRecordingFilter::open()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_trace.clear();
}

void SliceRecordingFilter::prepare(Slice &slice)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_trace.clear();
	_trace.prepare(slice);
	_trace.mark_start();
}

void SliceRecordingFilter::apply(Slice &slice, bool &interrupt)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	if (_enabled)
	{
		_trace.advance();
		_trace.write(slice);
		_enabled = !_limited || ++_counter < _limit;
	}
	_trace.mark_stop();
}

void SliceRecordingFilter::close()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_trace.mark_stop();
}

void SliceRecordingFilter::reset()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_trace.clear();
	_counter = 0;
}



void SliceRecordingFilter::reset_counter()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_counter = 0;
}

const Trace& SliceRecordingFilter::trace() const
{
	return _trace;
}

std::size_t SliceRecordingFilter::counter() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _counter;
}

std::size_t SliceRecordingFilter::limit() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _limit;
}

Trace& SliceRecordingFilter::trace()
{
	return _trace;
}

void SliceRecordingFilter::enable(bool enabled)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_enabled = enabled;
}

void SliceRecordingFilter::limit(std::size_t limit)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_limit = limit;
}

void SliceRecordingFilter::limited(bool enabled)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_limited= enabled;
}

bool SliceRecordingFilter::suspended() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _limited && _limit <= _counter;
}

bool SliceRecordingFilter::limited() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _limited;
}

bool SliceRecordingFilter::enabled() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _enabled;
}
