#include "..\..\StdAfx.hpp"
#include "..\..\Model\Processing\SliceProcessor.hpp"

#include <algorithm>
#include <sstream>
#include <vector>




SliceProcessor::SliceProcessor()
{
	_serializable_filters.push_back(&_hardware);
	
	_persistent_filters.push_back(&_hardware);
	
	//_filters.push_back(&_slice_clearing);
	_filters.push_back(&_hardware);
	_filters.push_back(&_slice_counting);
	_filters.push_back(&_slice_averaging);
	_filters.push_back(&_slice_recording);
	_filters.push_back(&_slice_subtraction);
	_filters.push_back(&_slice_adjustment);
	_filters.push_back(&_slice_running_average_filter);
	_filters.push_back(&_slice_dynamic_gain_filter);
	_filters.push_back(&_slice_probing);
	_filters.push_back(&_trace_probing);
}

void SliceProcessor::open()
{
	std::for_each(_filters.begin(), _filters.end(), [](ISliceFilter *filter) { filter->open(); });
}

void SliceProcessor::prepare(Slice &slice)
{
	std::for_each(_filters.begin(), _filters.end(), [&](ISliceFilter *filter) { filter->prepare(slice); });
}

void SliceProcessor::apply(Slice &slice)
{
	bool interrupt = false;
	std::find_if(_filters.begin(), _filters.end(), [&](ISliceFilter *filter) { filter->apply(slice, interrupt); return interrupt; });
}

void SliceProcessor::close()
{
	std::for_each(_filters.rbegin(), _filters.rend(), [](ISliceFilter *filter) { filter->close(); });
}

void SliceProcessor::reset()
{
	std::for_each(_filters.begin(), _filters.end(), [](ISliceFilter *filter) { filter->reset(); });
}

void SliceProcessor::serialize(Trace &target)
{
	std::for_each(_serializable_filters.begin(), _serializable_filters.end(), [&](ISerializableSliceFilter* filter) { filter->serialize(target); });
}

void SliceProcessor::bind()
{
	std::for_each(_persistent_filters.begin(), _persistent_filters.end(), [](IPersistentSliceFilter* filter) { filter->bind(); });
}

void SliceProcessor::load(const std::wstring &root)
{
	std::for_each(_persistent_filters.begin(), _persistent_filters.end(), [&](IPersistentSliceFilter* filter) { filter->load(root); });
}

void SliceProcessor::save(const std::wstring &root) const
{
	std::for_each(_persistent_filters.begin(), _persistent_filters.end(), [&](IPersistentSliceFilter* filter) { filter->save(root); });
}



SliceRunningAverageFilter & SliceProcessor::slice_running_average_filter()
{
	return _slice_running_average_filter;
}

SliceDynamicGainFilter & SliceProcessor::slice_dynamic_gain_filter()
{
	return _slice_dynamic_gain_filter;
}

SliceSubtractionFilter& SliceProcessor::slice_subtraction_filter()
{
	return _slice_subtraction;
}

SliceAdjustmentFilter& SliceProcessor::slice_adjustment_filter()
{
	return _slice_adjustment;
}

SliceAveragingFilter& SliceProcessor::slice_averaging_filter()
{
	return _slice_averaging;
}

SliceRecordingFilter& SliceProcessor::slice_recording_filter()
{
	return _slice_recording;
}

SliceCountingFilter& SliceProcessor::slice_counting_filter()
{
	return _slice_counting;
}
//
//SliceCleaningFilter & SliceProcessor::slice_cleaning_filter()
//{
//	return _slice_clearing;
//}

SliceProbingFilter& SliceProcessor::trace_probing_filter()
{
	return _trace_probing;
}

SliceProbingFilter& SliceProcessor::slice_probing_filter()
{
	return _slice_probing;
}

HardwareDataSource& SliceProcessor::hardware_filter()
{
	return _hardware;
}



const SliceRunningAverageFilter & SliceProcessor::slice_running_average_filter() const
{
	return _slice_running_average_filter;
}

const SliceDynamicGainFilter & SliceProcessor::slice_dynamic_gain_filter() const
{
	return _slice_dynamic_gain_filter;
}

const SliceSubtractionFilter& SliceProcessor::slice_subtraction_filter() const
{
	return _slice_subtraction;
}

const SliceAdjustmentFilter& SliceProcessor::slice_adjustment_filter() const
{
	return _slice_adjustment;
}

const SliceAveragingFilter& SliceProcessor::slice_averaging_filter() const
{
	return _slice_averaging;
}

const SliceRecordingFilter& SliceProcessor::slice_recording_filter() const
{
	return _slice_recording;
}

const SliceCountingFilter& SliceProcessor::slice_counting_filter() const
{
	return _slice_counting;
}

//const SliceCleaningFilter & SliceProcessor::slice_cleaning_filter() const
//{
//	return _slice_clearing;
//}

const SliceProbingFilter& SliceProcessor::trace_probing_filter() const
{
	return _trace_probing;
}

const SliceProbingFilter& SliceProcessor::slice_probing_filter() const
{
	return _slice_probing;
}

const HardwareDataSource& SliceProcessor::hardware_filter() const
{
	return _hardware;
}