#pragma once
#include "..\..\Model\Data\Trace.hpp"
#include "..\..\Model\Implementations\SliceFilterImpl.hpp"

#include <mutex>



class SliceRecordingFilter:
	public SliceFilterImpl
{
public:
	SliceRecordingFilter();
	
	virtual void open();
	virtual void prepare(Slice &slice);
	virtual void apply(Slice &slice, bool &interrupt);
	virtual void close();
	virtual void reset();
	
	void reset_counter();
	
	void limit(std::size_t limit);
	void enable(bool enabled = true);
	void limited(bool limited);
	
	bool suspended() const;
	bool enabled() const;
	bool limited() const;

	Trace& trace();
	
	const Trace& trace() const;
	
	std::size_t counter() const;
	std::size_t limit() const;
	
protected:
	Trace _trace;
	std::recursive_mutex mutable _guard;
	std::size_t _counter;
	std::size_t _limit;
	bool _limited;
	bool _enabled;
};