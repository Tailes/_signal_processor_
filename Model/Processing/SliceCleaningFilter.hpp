#pragma once
#include "..\..\Model\Implementations\SliceFilterImpl.hpp"

#include <atomic>

class Slice;
class Trace;



class SliceCleaningFilter:
	public SliceFilterImpl
{
public:
	SliceCleaningFilter();
	
	virtual void prepare(Slice &slice) override;
	
	void enable(bool enabled);
	
	bool enabled() const;
	
private:
	std::atomic<bool> _enabled;
};

