#include "..\..\StdAfx.hpp"
#include "..\..\Utility\Registry.hpp"
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\Model\Processing\SliceCleaningFilter.hpp"


SliceCleaningFilter::SliceCleaningFilter():
	_enabled(true)
{
}

void SliceCleaningFilter::prepare(Slice &slice)
{
	if (_enabled)
	{
		slice = Slice();
	}
}

void SliceCleaningFilter::enable(bool enabled)
{
	_enabled = enabled;
}

bool SliceCleaningFilter::enabled() const
{
	return _enabled;
}
