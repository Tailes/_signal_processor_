#include "..\..\StdAfx.hpp"
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\Model\Processing\SliceRunningAverageFilter.hpp"

#include <algorithm>
#include <mutex>



SliceRunningAverageFilter::SliceRunningAverageFilter():
	_amount(1)
{
}

void SliceRunningAverageFilter::apply(Slice &slice, bool &interrupt)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for (std::size_t i = 0; i < slice.batch().size(); ++i)
	{
		apply(slice.batch().at(i));
	}
}

void SliceRunningAverageFilter::amount(std::size_t amount)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_amount = std::max<std::size_t>(1, amount);
}

std::size_t SliceRunningAverageFilter::amount() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _amount;
}

void SliceRunningAverageFilter::apply(Signal & signal) const
{
	real_t sum = 0;
	std::size_t count = std::min<std::size_t>(_amount, signal.size());
	for (std::size_t i = 0; i < count; ++i)
		sum += std::abs(signal.at(i));
	for (std::size_t i = 0; i < signal.size(); ++i)
	{
		sum -= std::abs(signal.at(i));
		signal.at(i) = (sum + std::abs(signal.at(i))) / static_cast<real_t>(count);
		sum += (i + count < signal.size()) ? std::abs(signal.at(i + count)) : std::abs(signal.back());
	}
}
