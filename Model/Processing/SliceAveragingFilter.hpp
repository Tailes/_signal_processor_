#pragma once
#include "..\..\Model\Implementations\SliceFilterImpl.hpp"

#include <vector>
#include <mutex>



class SliceAveragingFilter :
	public SliceFilterImpl
{
public:
	SliceAveragingFilter();
	
	virtual void prepare(Slice &slice);
	virtual void apply(Slice &slice, bool &interrupt);
	virtual void reset();
	
	void amount(std::size_t amount);
	
	std::size_t amount() const;
	
protected:
	std::recursive_mutex mutable _guard;
	std::vector<Slice> _storage;
	std::size_t _counter;
};