#pragma once
#include "..\..\Model\Implementations\SliceFilterImpl.hpp"

#include <vector>
#include <mutex>



class Signal;

class SliceRunningAverageFilter :
	public SliceFilterImpl
{
public:
	SliceRunningAverageFilter();
	
	virtual void apply(Slice &slice, bool &interrupt);
	
	void amount(std::size_t amount);
	
	std::size_t amount() const;
	
private:
	void apply(Signal & signal) const;
	
protected:
	std::recursive_mutex mutable _guard;
	std::size_t _amount;
};