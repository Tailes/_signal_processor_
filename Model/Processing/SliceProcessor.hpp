#pragma once
#include "..\..\Model\Hardware\HardwareDataSource.hpp"
#include "..\..\Model\Processing\SliceProbingFilter.hpp"
#include "..\..\Model\Processing\SliceCleaningFilter.hpp"
#include "..\..\Model\Processing\SliceCountingFilter.hpp"
#include "..\..\Model\Processing\SliceRecordingFilter.hpp"
#include "..\..\Model\Processing\SliceAveragingFilter.hpp"
#include "..\..\Model\Processing\SliceAdjustmentFilter.hpp"
#include "..\..\Model\Processing\SliceDynamicGainFilter.hpp"
#include "..\..\Model\Processing\SliceSubtractionFilter.hpp"
#include "..\..\Model\Processing\SliceRunningAverageFilter.hpp"
#include "..\..\Model\Interfaces\ISerializableSliceFilter.hpp"

#include <string>
#include <mutex>
#include <list>




class SliceProcessor
{
public:
	SliceProcessor();
	
	void open();
	void prepare(Slice &slice);
	void apply(Slice &slice);
	void close();
	void reset();
	void bind();
	void load(const std::wstring &root);
	void save(const std::wstring &root) const;
	void serialize(Trace &target);
	
	SliceRunningAverageFilter& slice_running_average_filter();
	SliceDynamicGainFilter& slice_dynamic_gain_filter();
	SliceSubtractionFilter& slice_subtraction_filter();
	SliceAdjustmentFilter& slice_adjustment_filter();
	SliceAveragingFilter& slice_averaging_filter();
	SliceRecordingFilter& slice_recording_filter();
	SliceCountingFilter& slice_counting_filter();
	//SliceCleaningFilter& slice_cleaning_filter();
	SliceProbingFilter& trace_probing_filter();
	SliceProbingFilter& slice_probing_filter();
	HardwareDataSource& hardware_filter();
	
	const SliceRunningAverageFilter& slice_running_average_filter() const;
	const SliceDynamicGainFilter& slice_dynamic_gain_filter() const;
	const SliceSubtractionFilter& slice_subtraction_filter() const;
	const SliceAdjustmentFilter& slice_adjustment_filter() const;
	const SliceAveragingFilter& slice_averaging_filter() const;
	const SliceRecordingFilter& slice_recording_filter() const;
	const SliceCountingFilter& slice_counting_filter() const;
	//const SliceCleaningFilter& slice_cleaning_filter() const;
	const SliceProbingFilter& trace_probing_filter() const;
	const SliceProbingFilter& slice_probing_filter() const;
	const HardwareDataSource& hardware_filter() const;
	
private:
	//std::recursive_mutex mutable _guard;
	std::list<ISerializableSliceFilter*> _serializable_filters;
	std::list<IPersistentSliceFilter*> _persistent_filters;
	std::list<ISliceFilter*> _filters;
	SliceRunningAverageFilter _slice_running_average_filter;
	SliceDynamicGainFilter _slice_dynamic_gain_filter;
	SliceSubtractionFilter _slice_subtraction;
	SliceAdjustmentFilter _slice_adjustment;
	SliceAveragingFilter _slice_averaging;
	SliceRecordingFilter _slice_recording;
	SliceCountingFilter _slice_counting;
	//SliceCleaningFilter _slice_clearing;
	SliceProbingFilter _trace_probing;
	SliceProbingFilter _slice_probing;
	HardwareDataSource _hardware;
};


