#include "..\..\StdAfx.hpp"
#include "..\..\Utility\Registry.hpp"
#include "..\..\Model\Processing\SliceCountingFilter.hpp"


SliceCountingFilter::SliceCountingFilter():
	_amount(0)
{
}

void SliceCountingFilter::prepare(Slice &slice)
{
	_amount = 0;
}

void SliceCountingFilter::apply(Slice &slice, bool &interrupt)
{
	_amount++;
}

void SliceCountingFilter::reset()
{
	_amount = 0;
}

std::size_t SliceCountingFilter::amount() const
{
	return _amount;
}