#include "..\..\StdAfx.hpp"
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\Model\Processing\SliceDynamicGainFilter.hpp"

#include <algorithm>



static const real_t PicoSecondsToM = 0.000299792458; // c * 1ps (m)


SliceDynamicGainFilter::SliceDynamicGainFilter():
	_amount(0.f)
{
}

void SliceDynamicGainFilter::apply(Slice &slice, bool &interrupt)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for (std::size_t i = 0; i < slice.batch().size(); ++i)
	{
		for (std::size_t j = 0; j < slice.batch().at(i).size(); ++j)
		{
			slice.batch().at(i).at(j) *= (static_cast<real_t>(1.) + _amount * PicoSecondsToM * slice.timeline().at(j));
		}
	}
}

void SliceDynamicGainFilter::amount(real_t amount)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_amount = amount;
}

real_t SliceDynamicGainFilter::amount() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _amount;
}

