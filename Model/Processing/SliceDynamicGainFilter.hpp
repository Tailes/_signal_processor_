#pragma once
#include "..\..\Model\Types\Real.hpp"
#include "..\..\Model\Implementations\SliceFilterImpl.hpp"

#include <mutex>



class Signal;

class SliceDynamicGainFilter :
	public SliceFilterImpl
{
public:
	SliceDynamicGainFilter();
	
	virtual void apply(Slice &slice, bool &interrupt);
	
	void amount(real_t amount);
	
	real_t amount() const;
	
protected:
	std::recursive_mutex mutable _guard;
	real_t _amount;
};