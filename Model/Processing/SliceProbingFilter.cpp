#include "..\..\StdAfx.hpp"
#include "..\..\Utility\Registry.hpp"
#include "..\..\Model\Processing\SliceProbingFilter.hpp"


SliceProbingFilter::SliceProbingFilter()
{
	_prepare_handler = [](Slice &) {};
	_apply_handler = [](Slice &, bool &) {};
	_close_handler = []() {};
	_reset_handler = []() {};
	_open_handler = []() {};
}

void SliceProbingFilter::open()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_open_handler();
}

void SliceProbingFilter::prepare(Slice &slice)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_prepare_handler(slice);
}

void SliceProbingFilter::apply(Slice &slice, bool &interrupt)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_apply_handler(slice, interrupt);
}

void SliceProbingFilter::close()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_close_handler();
}

void SliceProbingFilter::reset()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_reset_handler();
}

void SliceProbingFilter::prepare_handler(std::function<void(Slice &)> &&handler)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_prepare_handler = std::move(handler);
}

void SliceProbingFilter::apply_handler(std::function<void(Slice &, bool &)> &&handler)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_apply_handler = std::move(handler);
}

void SliceProbingFilter::close_handler(std::function<void()> &&handler)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_close_handler = std::move(handler);
}

void SliceProbingFilter::reset_handler(std::function<void()> &&handler)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_reset_handler = std::move(handler);
}

void SliceProbingFilter::open_handler(std::function<void()> &&handler)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_open_handler = std::move(handler);
}
