#pragma once
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\Model\Types\Real.hpp"
#include "..\..\Model\Implementations\SliceFilterImpl.hpp"

#include <array>
#include <mutex>



class SliceAdjustmentFilter:
	public SliceFilterImpl
{
public:
	SliceAdjustmentFilter();
	
	virtual void apply(Slice &slice, bool &interrupt);
	
	void amplification(std::size_t index, real_t amplification);
	void magnitude(std::size_t index, real_t magnitude);
	void zero(std::size_t index, real_t zero);
	
	real_t amplification(std::size_t index) const;
	real_t magnitude(std::size_t index) const;
	real_t zero(std::size_t index) const;
	
protected:
	std::recursive_mutex mutable _guard;
	std::array<real_t, std::tuple_size<Batch>::value> _amplifications;
	std::array<real_t, std::tuple_size<Batch>::value> _magnitudes;
	std::array<real_t, std::tuple_size<Batch>::value> _zeros;
};