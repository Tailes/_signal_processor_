#include "..\StdAfx.hpp"
#include "..\Model\SignalProcessorModel.hpp"

#include <codecvt>
#include <locale>
#include <array>



SignalProcessorModel::SignalProcessorModel():
	_terminated(false), _completed(false), _suspended(true)
{
	_scanning_thread = std::thread(&SignalProcessorModel::scanning, this);
}

SignalProcessorModel::~SignalProcessorModel()
{
	_terminated = true;
	_execution.notify_all();
	_completion.wait(std::unique_lock<std::recursive_mutex>(_guard), [this]() { return _completed.load(); });
	_scanning_thread.join();
}

void SignalProcessorModel::start_experiment()
{
	_suspended = false;
	_execution.notify_all();
}

void SignalProcessorModel::stop_experiment()
{
	_suspended = true;
	_execution.notify_all();
}

void SignalProcessorModel::open()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_processor.open();
}

void SignalProcessorModel::prepare(Slice &slice)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_processor.prepare(slice);
	_processor.serialize(_processor.slice_recording_filter().trace());
}

void SignalProcessorModel::apply(Slice &slice)
{
	while (!_terminated && !_suspended)
	{
		_processor.apply(slice);
		_redraw_handler();
		_update_handler();
	}
}

void SignalProcessorModel::close()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_processor.close();
}

void SignalProcessorModel::save(const std::wstring &root) const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_processor.save(root);
}

void SignalProcessorModel::load(const std::wstring &root)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_processor.load(root);
	_processor.bind();
}

void SignalProcessorModel::scanning()
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
	while (!_terminated)
	{
		_execution.wait(std::unique_lock<std::recursive_mutex>(_guard), [this]() { return _terminated.load() || !_suspended.load(); });
		if (!_terminated)
		{
			_completed = false;
			_completion.notify_all();
			//_completion.reset();
			try
			{
				this->open();
				this->prepare(Slice());
				this->apply(Slice());
				this->close();
			}
			catch (std::exception &excpt)
			{
				_report_handler(converter.from_bytes(excpt.what()));
				_suspended = true;
				_completed = true;
				_completion.notify_all();
				_reset_handler();
				continue;
			}
			_completed = true;
			_completion.notify_all();
		}
	}
	_completed = true;
	_completion.notify_all();
}

void SignalProcessorModel::report_handler(const std::function<void(const std::wstring &)> &callback)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_report_handler = callback;
}

void SignalProcessorModel::update_handler(const std::function<void()> &callback)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_update_handler = callback;
}

void SignalProcessorModel::reset_handler(const std::function<void()> &callback)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_reset_handler = callback;
}

void SignalProcessorModel::redraw_handler(const std::function<void()> &callback)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_redraw_handler = callback;
}

SliceProcessor& SignalProcessorModel::slice_processor()
{
	return _processor;
}

const SliceProcessor& SignalProcessorModel::slice_processor() const
{
	return _processor;
}
