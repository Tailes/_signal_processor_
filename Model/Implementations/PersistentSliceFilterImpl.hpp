#pragma once
#include "..\..\Model\Implementations\SliceFilterImpl.hpp"
#include "..\..\Model\Interfaces\IPersistentSliceFilter.hpp"


struct PersistentSliceFilterImpl:
	public IPersistentSliceFilter,
	public SliceFilterImpl
{
	virtual void bind();
	virtual void load(const std::wstring &root);
	virtual void save(const std::wstring &root) const;
};
