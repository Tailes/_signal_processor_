#pragma once
#include "..\..\Model\Interfaces\ISerializableSliceFilter.hpp"
#include "..\..\Model\Implementations\PersistentSliceFilterImpl.hpp"



struct SerializableSliceFilterImpl:
	public ISerializableSliceFilter,
	public PersistentSliceFilterImpl
{
	virtual void serialize(Trace &target);
};
