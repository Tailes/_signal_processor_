#pragma once
#include "..\..\Model\Interfaces\ISliceFilter.hpp"


struct SliceFilterImpl:
	public ISliceFilter
{
	virtual void open();
	virtual void prepare(Slice &slice);
	virtual void apply(Slice &slice, bool &interrupt);
	virtual void close();
	virtual void reset();
};
