#pragma once
#include "..\..\Model\Data\Timeline.hpp"
#include "..\..\Model\Data\Amplitudes.hpp"

#include <vector>




class Interpolator
{
public:
	typedef Amplitudes::iterator value_it;
	typedef Timeline::const_iterator time_cit;
	typedef Amplitudes::const_iterator value_cit;
	typedef real_t(Interpolator::* kernel_t)(real_t t, time_cit time_ll, value_cit value_ll, time_cit time_l, value_cit value_l, time_cit time_r, value_cit value_r, time_cit time_rr, value_cit value_rr) const;
	
public:
	Interpolator(const Timeline &source_grid, const Amplitudes &source_signal);
	
	void run(Amplitudes &target, const Timeline &grid, kernel_t kernel);
	
	real_t catmull_rom_kernel(real_t t, time_cit time_ll, value_cit value_ll, time_cit time_l, value_cit value_l, time_cit time_r, value_cit value_r, time_cit time_rr, value_cit value_rr) const;
	real_t linear_kernel(real_t t, time_cit time_ll, value_cit value_ll, time_cit time_l, value_cit value_l, time_cit time_r, value_cit value_r, time_cit time_rr, value_cit value_rr) const;
	
private:
	Amplitudes::const_iterator _value_rightmost;
	Amplitudes::const_iterator _value_leftmost;
	Amplitudes::const_iterator _value_begin;
	Amplitudes::const_iterator _value_right;
	Amplitudes::const_iterator _value_left;
	Amplitudes::const_iterator _value_end;
	Timeline::const_iterator _time_rightmost;
	Timeline::const_iterator _time_leftmost;
	Timeline::const_iterator _time_begin;
	Timeline::const_iterator _time_right;
	Timeline::const_iterator _time_left;
	Timeline::const_iterator _time_end;
	
	void interpolate(value_it target_begin, time_cit time_begin, time_cit time_end, kernel_t kernel);
	
	real_t catmull_rom(real_t t, real_t t_prev_prev, real_t f_prev_prev, real_t t_prev, real_t f_prev, real_t t_next, real_t f_next, real_t t_next_next, real_t f_next_next) const;
	real_t linear(real_t t, real_t t_prev, real_t f_prev, real_t t_next, real_t f_next) const;
};