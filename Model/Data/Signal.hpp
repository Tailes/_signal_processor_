#pragma once
#include "..\..\Model\Data\Amplitudes.hpp"

#include <vector>




class Signal:
	public Amplitudes
{
public:
	Signal();
	
	void number(std::size_t number);
	
	std::size_t number() const;
	
private:
	std::size_t _number;
};