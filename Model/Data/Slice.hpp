#pragma once
#include "..\..\Model\Data\Batch.hpp"
#include "..\..\Model\Data\Timeline.hpp"



class Slice
{
public:
	Slice();
	
	Batch &batch();
	Timeline &timeline();
	
	void complete(bool completed);
	
	const Batch &batch() const;
	const Timeline &timeline() const;
	
	bool completed() const;
	bool empty() const;
	
private:
	Timeline _timeline;
	Batch _batch;
	bool _completed;
};