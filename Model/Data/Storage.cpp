#include "..\..\StdAfx.hpp"
#include "..\..\Model\Data\Storage.hpp"

#include <stdexcept>
#include <array>

#include <Windows.h>


static const std::size_t CommentOffset = 512;
static const std::size_t DataOffset = 1024;




Storage::Header::Header()
{
	_file_header = {{'I', 'R', 'E'}};
	_scanning_frame_ns = 0.0;
	_duration_minutes = 0.0;
	_scanning_epsilon = 1.0;
	_distance_meters = 0.0;
	_scanning_tagc = 1.0;
	_scanning_gain = 1.0;
	_points_count = 0;
	_file_version = 0;
	_type_marker = 'F';
	_date_month = 0;
	_date_year = 0;
	_date_day = 0;
}



Storage::Storage():
	_handle(nullptr)
{
	DWORD written = 0;
	Header header;
	handle_t handle = nullptr;
	std::vector<char> placeholder(DataOffset, ' ');
	std::array<wchar_t, _MAX_PATH> buffer;
	if ( ::GetTempPathW(buffer.size(), buffer.data()) == 0 )
		throw std::runtime_error("Cannot get temporary folder.");
	if ( ::GetTempFileNameW(buffer.data(), L"IRE", 0, buffer.data()) == 0 )
		throw std::runtime_error("Cannot get temporary file name.");
	if ( (handle = ::CreateFileW(buffer.data(), GENERIC_WRITE, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NOT_CONTENT_INDEXED | FILE_ATTRIBUTE_TEMPORARY, nullptr)) == INVALID_HANDLE_VALUE )
		throw std::runtime_error("Cannot open temporary file.");
	if ( ::WriteFile(handle, placeholder.data(), placeholder.size() * sizeof(char), &written, nullptr) == FALSE || written != placeholder.size() * sizeof(char) )
		throw std::runtime_error("Cannot write temporary file data.");
	if ( ::SetFilePointer(handle, 0, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, &header, sizeof(header), &written, nullptr) == FALSE || written != sizeof(header) )
		throw std::runtime_error("Cannot write temporary file data.");
	std::swap(_handle, handle);
	std::swap(_name, buffer);
}

Storage::~Storage()
{
	if ( _handle != nullptr )
	{
		::CloseHandle(_handle);
		::DeleteFileW(_name.data());
	}
}

void Storage::record_duration(float minutes)
{
	DWORD written = 0;
	handle_t handle = nullptr;
	std::swap(_handle, handle);
	if ( ::SetFilePointer(handle, offsetof(Header, Header::_duration_minutes), nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, &minutes, sizeof(minutes), &written, nullptr) == FALSE || written != sizeof(minutes) )
		throw std::runtime_error("Cannot write temporary file data.");
	std::swap(_handle, handle);
}

void Storage::record_size(std::uint16_t points_count)
{
	DWORD written = 0;
	handle_t handle = nullptr;
	std::swap(_handle, handle);
	if ( ::SetFilePointer(handle, offsetof(Header, Header::_points_count), nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, &points_count, sizeof(points_count), &written, nullptr) == FALSE || written != sizeof(points_count) )
		throw std::runtime_error("Cannot write temporary file data.");
	std::swap(_handle, handle);
}

void Storage::record_signal(const std::vector<float> &signal)
{
	DWORD written = 0;
	handle_t handle = nullptr;
	std::swap(_handle, handle);
	if ( ::SetFilePointer(handle, 0, nullptr, FILE_END) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, signal.data(), signal.size() * sizeof(float), &written, nullptr) == FALSE || written != signal.size() * sizeof(float) )
		throw std::runtime_error("Cannot write temporary file data.");
	std::swap(_handle, handle);
}

void Storage::record_comments(const std::string &comments)
{
	DWORD written = 0;
	handle_t handle = nullptr;
	std::swap(_handle, handle);
	if ( ::SetFilePointer(handle, CommentOffset, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, comments.c_str(), std::strlen(comments.c_str()) + 1, &written, nullptr) == FALSE || written != std::strlen(comments.c_str()) + 1 )
		throw std::runtime_error("Cannot write temporary file data.");
	std::swap(_handle, handle);
}

void Storage::record_distance(float meters)
{
	DWORD written = 0;
	handle_t handle = nullptr;
	std::swap(_handle, handle);
	if ( ::SetFilePointer(handle, offsetof(Header, Header::_distance_meters), nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, &meters, sizeof(meters), &written, nullptr) == FALSE || written != sizeof(meters) )
		throw std::runtime_error("Cannot write temporary file data.");
	std::swap(_handle, handle);
}

void Storage::record_epsilon(float epsilon)
{
	DWORD written = 0;
	handle_t handle = nullptr;
	std::swap(_handle, handle);
	if ( ::SetFilePointer(handle, offsetof(Header, Header::_scanning_epsilon), nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, &epsilon, sizeof(epsilon), &written, nullptr) == FALSE || written != sizeof(epsilon) )
		throw std::runtime_error("Cannot write temporary file data.");
	std::swap(_handle, handle);
}

void Storage::record_frame(float nanoseconds)
{
	DWORD written = 0;
	handle_t handle = nullptr;
	std::swap(_handle, handle);
	if ( ::SetFilePointer(handle, offsetof(Header, Header::_scanning_frame_ns), nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, &nanoseconds, sizeof(nanoseconds), &written, nullptr) == FALSE || written != sizeof(nanoseconds) )
		throw std::runtime_error("Cannot write temporary file data.");
	std::swap(_handle, handle);
}

void Storage::record_tagc(float tagc)
{
	DWORD written = 0;
	handle_t handle = nullptr;
	std::swap(_handle, handle);
	if ( ::SetFilePointer(handle, offsetof(Header, Header::_scanning_tagc), nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, &tagc, sizeof(tagc), &written, nullptr) == FALSE || written != sizeof(tagc) )
		throw std::runtime_error("Cannot write temporary file data.");
	std::swap(_handle, handle);
}

void Storage::record_date(std::uint16_t day, std::uint16_t month, std::uint16_t year)
{
	DWORD written = 0;
	handle_t handle = nullptr;
	std::swap(_handle, handle);
	if ( ::SetFilePointer(handle, offsetof(Header, Header::_date_day), nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, &day, sizeof(day), &written, nullptr) == FALSE || written != sizeof(day) )
		throw std::runtime_error("Cannot write temporary file data.");
	if ( ::SetFilePointer(handle, offsetof(Header, Header::_date_month), nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, &month, sizeof(month), &written, nullptr) == FALSE || written != sizeof(month) )
		throw std::runtime_error("Cannot write temporary file data.");
	if ( ::SetFilePointer(handle, offsetof(Header, Header::_date_year), nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::WriteFile(handle, &year, sizeof(year), &written, nullptr) == FALSE || written != sizeof(year) )
		throw std::runtime_error("Cannot write temporary file data.");
	std::swap(_handle, handle);
}

void Storage::release(const std::wstring &name)
{
	handle_t handle = nullptr;
	std::swap(_handle, handle);
	if ( ::CopyFileW(_name.data(), name.c_str(), FALSE) == FALSE )
		throw std::runtime_error("Cannot copy temporary file to the destination.");
	if ( ::SetFilePointer(handle, DataOffset, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::SetEndOfFile(handle) == FALSE )
		throw std::runtime_error("Cannot truncate temporary file.");
	std::swap(_handle, handle);
}

void Storage::reset()
{
	handle_t handle = nullptr;
	std::swap(_handle, handle);
	if ( ::SetFilePointer(handle, DataOffset, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER )
		throw std::runtime_error("Cannot relocate temporary file pointer.");
	if ( ::SetEndOfFile(handle) == FALSE )
		throw std::runtime_error("Cannot truncate temporary file.");
	std::swap(_handle, handle);
}

bool Storage::is_empty()
{
	handle_t handle = nullptr;
	LARGE_INTEGER size = LARGE_INTEGER();
	std::swap(_handle, handle);
	if ( ::GetFileSizeEx(handle, &size) == FALSE )
		throw std::runtime_error("Cannot get temporary file size.");
	std::swap(_handle, handle);
	return size.LowPart <= DataOffset && size.HighPart == 0;
}