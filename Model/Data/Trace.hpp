#pragma once
#include "..\..\Model\Data\Batch.hpp"
#include "..\..\Model\Data\Storage.hpp"

#include <cstdint>
#include <string>
#include <ctime>
#include <array>
#include <mutex>



class Slice;
class Positioner;



class Trace
{
public:
	Trace();
	
	void advance();
	void prepare(const Slice &slice);
	void write(const Slice &slice);
	void clear();
	void save(const std::array<std::wstring, std::tuple_size<Batch>::value> &names);
	void mark_start();
	void mark_stop();
	
	std::float_t step_length() const;
	std::string comments() const;
	std::size_t count() const;
	float survey_time() const;
	float time_frame() const;
	float distance() const;
	float epsilon() const;
	float tagc() const;
	
	void step_length(std::float_t length);
	void scan_count(std::uint8_t count);
	void step_count(std::uint8_t count);
	void time_frame(float frame);
	void distance(float distance);
	void comments(const std::string &comments);
	void epsilon(float epsilon);
	void tagc(float tagc);
	
	bool is_dirty() const;
	
private:
	std::recursive_mutex mutable _guard;
	std::array<Storage, std::tuple_size<Batch>::value> _files;
	std::float_t _step_length;
	std::uint8_t _step_count;
	std::uint8_t _scan_count;
	std::uint8_t _scans;
	std::string _comments;
	std::size_t _counter;
	std::time_t _start;
	std::time_t _stop;
	float _time_frame;
	float _distance;
	float _epsilon;
	float _tagc;
	bool _dirty;
};