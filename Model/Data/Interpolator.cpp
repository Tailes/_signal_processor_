#include "..\..\stdafx.hpp"
#include "..\..\Model\Data\Interpolator.hpp"

#include <cmath>




Interpolator::Interpolator(const Timeline &source_grid, const Amplitudes &source_signal)
{
	_time_end = source_grid.cend();
	_time_begin = source_grid.cbegin();
	_time_leftmost = source_grid.cbegin();
	_time_left = _time_leftmost == _time_end ? _time_leftmost : std::next(_time_leftmost);
	_time_right = _time_left == _time_end ? _time_left : std::next(_time_left);
	_time_rightmost = _time_right == _time_end ? _time_right : std::next(_time_right);
	
	_value_end = source_signal.cend();
	_value_begin = source_signal.cbegin();
	_value_leftmost = source_signal.cbegin();
	_value_left = _value_leftmost == _value_end ? _value_leftmost : std::next(_value_leftmost);
	_value_right = _value_left == _value_end ? _value_left : std::next(_value_left);
	_value_rightmost = _value_right == _value_end ? _value_right : std::next(_value_right);
}

void Interpolator::run(Amplitudes &target, const Timeline &grid, kernel_t kernel)
{
	this->interpolate(target.begin(), grid.cbegin(), grid.cend(), kernel);
}


void Interpolator::interpolate(value_it target_begin, time_cit time_begin, time_cit time_end, kernel_t kernel)
{
	for (time_begin; time_begin != time_end && *time_begin < *_time_leftmost; ++time_begin, ++target_begin)
	{
		*target_begin = (this->*kernel)(*time_begin, _time_leftmost, _value_leftmost, _time_leftmost, _value_leftmost, _time_leftmost, _value_leftmost, _time_leftmost, _value_leftmost);
	}
	for (time_begin; time_begin != time_end && *time_begin < *_time_left; ++time_begin, ++target_begin)
	{
		*target_begin = (this->*kernel)(*time_begin, _time_leftmost, _value_leftmost, _time_leftmost, _value_leftmost, _time_left, _value_left, _time_right, _value_right);
	}
	while (time_begin != time_end && std::next(_time_rightmost) != _time_end)
	{
		for (time_begin; time_begin != time_end && *time_begin < *_time_right; ++time_begin, ++target_begin)
		{
			*target_begin = (this->*kernel)(*time_begin, _time_leftmost, _value_leftmost, _time_left, _value_left, _time_right, _value_right, _time_rightmost, _value_rightmost);
		}
		while (time_begin != time_end && *_time_right <= *time_begin && std::next(_time_rightmost) != _time_end)
		{
			std::advance(_value_leftmost, 1); std::advance(_value_left, 1); std::advance(_value_right, 1); std::advance(_value_rightmost, 1);
			std::advance(_time_leftmost, 1); std::advance(_time_left, 1); std::advance(_time_right, 1); std::advance(_time_rightmost, 1);
		}
	}
	for (time_begin; time_begin != time_end && *time_begin < *_time_rightmost; ++time_begin, ++target_begin)
	{
		*target_begin = (this->*kernel)(*time_begin, _time_left, _value_left, _time_right, _value_right, _time_rightmost, _value_rightmost, _time_rightmost, _value_rightmost);
	}
	for (time_begin; time_begin != time_end; ++time_begin, ++target_begin)
	{
		*target_begin = (this->*kernel)(*time_begin, _time_rightmost, _value_rightmost, _time_rightmost, _value_rightmost, _time_rightmost, _value_rightmost, _time_rightmost, _value_rightmost);
	}
}

real_t Interpolator::catmull_rom_kernel(real_t t, time_cit time_ll, value_cit value_ll, time_cit time_l, value_cit value_l, time_cit time_r, value_cit value_r, time_cit time_rr, value_cit value_rr) const
{
	return this->catmull_rom(t, *time_ll, *value_ll, *time_l, *value_l, *time_r, *value_r, *time_rr, *value_rr);
}

real_t Interpolator::linear_kernel(real_t t, time_cit time_ll, value_cit value_ll, time_cit time_l, value_cit value_l, time_cit time_r, value_cit value_r, time_cit time_rr, value_cit value_rr) const
{
	return this->linear(t, *time_l, *value_l, *time_r, *value_r);
}




real_t Interpolator::linear(real_t t, real_t t_prev, real_t f_prev, real_t t_next, real_t f_next) const
{
	return std::isnormal(t_next - t_prev) ? (f_prev*(t_next - t) + f_next*(t - t_prev)) / (t_next - t_prev) : (f_prev + f_next) / 2;
}

real_t Interpolator::catmull_rom(real_t t, real_t t_prev_prev, real_t f_prev_prev, real_t t_prev, real_t f_prev, real_t t_next, real_t f_next, real_t t_next_next, real_t f_next_next) const
{
	real_t s0 = 0 + std::hypot(t_prev_prev - t_prev_prev, f_prev_prev - f_prev_prev);
	real_t s1 = s0 + std::hypot(t_prev - t_prev_prev, f_prev - f_prev_prev);
	real_t s2 = s1 + std::hypot(t_next - t_prev, f_next - f_prev);
	real_t s3 = s2 + std::hypot(t_next_next - t_next, f_next_next - f_next);
	real_t si = this->linear(t, t_prev, s1, t_next, s2);
	real_t a0_1v = this->linear(si, s0, f_prev_prev, s1, f_prev);
	real_t a1_2v = this->linear(si, s1, f_prev, s2, f_next);
	real_t a2_3v = this->linear(si, s2, f_next, s3, f_next_next);
	real_t b0_1_2v = this->linear(si, s0, a0_1v, s2, a1_2v);
	real_t b1_2_3v = this->linear(si, s1, a1_2v, s3, a2_3v);
	return this->linear(si, s1, b0_1_2v, s2, b1_2_3v);
}






