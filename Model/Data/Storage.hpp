#pragma once

#include <cstdint>
#include <string>
#include <vector>
#include <array>



class Storage
{
private:
	#pragma pack(push, 1)
	struct Header
	{
		std::array<char, 3> _file_header;
		std::uint16_t _file_version;
		std::uint16_t _points_count;
		char _type_marker;
		float _scanning_epsilon;
		float _distance_meters;
		float _duration_minutes;
		float _scanning_frame_ns;
		float _scanning_gain;
		float _scanning_tagc;
		std::uint16_t _date_year;
		std::uint16_t _date_month;
		std::uint16_t _date_day;
		
		Header();
	};
	#pragma pack(pop)
	
public:
	Storage();
	~Storage();
	
	void record_comments(const std::string &comments);
	void record_duration(float minutes);
	void record_distance(float meters);
	void record_epsilon(float epsilon);
	void record_signal(const std::vector<float> &signal);
	void record_frame(float nanoseconds);
	void record_tagc(float tagc);
	void record_date(std::uint16_t day, std::uint16_t month, std::uint16_t year);
	void record_size(std::uint16_t points_count);
	void release(const std::wstring &name);
	void reset();
	
	bool is_empty();
	
private:
	typedef void * handle_t;
	
	std::array<wchar_t, _MAX_PATH> _name;
	handle_t _handle;
};


