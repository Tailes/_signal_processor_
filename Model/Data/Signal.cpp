#include "..\..\stdafx.hpp"
#include "..\..\Model\Data\Signal.hpp"



Signal::Signal():
	_number(0)
{
}

void Signal::number(std::size_t number)
{
	_number = number;
}

std::size_t Signal::number() const
{
	return _number;
}

