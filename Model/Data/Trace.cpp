#include "..\..\StdAfx.hpp"
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\Model\Data\Trace.hpp"

#include <functional>
#include <algorithm>
#include <array>



static const float PicosecondsToNanoseconds = 0.001f;




Trace::Trace()
{
	_step_length = 0.0;
	_scan_count = 1;
	_step_count = 1;
	_time_frame = 0.0;
	_distance = 0.0;
	_epsilon = 0.0;
	_counter = 0;
	_scans = 0;
	_dirty = false;
	_start = -1;
	_stop = -1;
	_tagc = 1.0;
}

void Trace::advance()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	if (!(++_scans < _scan_count))
	{
		_distance += _step_length * static_cast<std::float_t>(_step_count);
		_scans = 0;
	}
}

void Trace::prepare(const Slice &slice)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for ( std::size_t i = 0; i < _files.size(); ++i )
		_files.at(i).record_size(static_cast<std::uint16_t>(slice.batch().at(i).size()));
}

void Trace::clear()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for ( auto &file : _files ) file.reset();
	_distance = 0;
	_counter = 0;
	_dirty = false;
	_scans = 0;
	_start = -1;
	_stop = -1;
}

void Trace::write(const Slice &slice)
{
	std::vector<float> line;
	std::lock_guard<std::recursive_mutex> lock(_guard);
	std::array<Storage, std::tuple_size<Batch>::value>::iterator file = _files.begin();
	std::function<float(real_t)> convertor = [](real_t source) { return static_cast<float>(source); };
	for ( const auto &signal : slice.batch())
	{
		if ( !signal.empty() )
		{
			line.resize(signal.size());
			std::transform(signal.cbegin(), signal.cend(), line.begin(), convertor);
			file->record_frame(static_cast<float>(PicosecondsToNanoseconds*(slice.timeline().back() - slice.timeline().front())));
			file->record_signal(line);
		}
		file++;
	}
	_counter++;
	_dirty = true;
}

void Trace::save(const std::array<std::wstring, std::tuple_size<Batch>::value> &names)
{
	std::tm now = {};
	std::time_t now_time = ::time(nullptr);
	if ( now_time <= 0 || ::localtime_s(&now, &now_time) != 0 ) throw std::runtime_error("Cannot get current time.");
	float minutes = 0.0f, seconds = ::modf(static_cast<float>(::difftime(_stop, _start)) / 60.0f, &minutes) * 0.6f;
	std::lock_guard<std::recursive_mutex> lock(_guard);
	std::array<std::wstring, std::tuple_size<Batch>::value>::const_iterator name = names.begin();
	for ( auto &file : _files )
	{
		if ( !file.is_empty() )
		{
			file.record_duration(minutes + seconds);
			file.record_distance(_distance);
			file.record_comments(_comments);
			file.record_date(now.tm_mday, now.tm_mon, now.tm_year + 1900);
			file.release(*name);
		}
		name++;
	}
	_distance = 0;
	_counter = 0;
	_scans = 0;
	_dirty = false;
}

void Trace::mark_start()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	::time(&_start);
	::time(&_stop);
}

void Trace::mark_stop()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	::time(&_stop);
}

std::float_t Trace::step_length() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _step_length;
}

std::string Trace::comments() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _comments;
}

std::size_t Trace::count() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _counter;
}

float Trace::epsilon() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _epsilon;
}

float Trace::distance() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _distance;
}

float Trace::survey_time() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return static_cast<float>(::difftime(_stop, _start));
}

float Trace::tagc() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _tagc;
}

float Trace::time_frame() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _time_frame;
}

void Trace::comments(const std::string &comments)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for ( auto &file : _files ) file.record_comments(comments);
	_comments = comments;
}

void Trace::epsilon(float epsilon)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for ( auto &file : _files ) file.record_epsilon(epsilon);
	_epsilon = epsilon;
}

void Trace::distance(float distance)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for ( auto &file : _files ) file.record_distance(distance);
	_distance = distance;
}

void Trace::tagc(float tagc)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for ( auto &file : _files ) file.record_tagc(tagc);
	_tagc = tagc;
}

void Trace::step_length(std::float_t length)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_step_length = length;
	_distance = static_cast<std::float_t>(_step_count) * length * static_cast<std::float_t>(_counter);
}

void Trace::scan_count(std::uint8_t count)
{
	_scan_count = count;
}

void Trace::step_count(std::uint8_t count)
{
	_step_count = count;
	_distance = static_cast<std::float_t>(count) * _step_length * static_cast<std::float_t>(_counter);
}

void Trace::time_frame(float frame)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	for ( auto &file : _files ) file.record_frame(frame);
	_time_frame = frame;
}

bool Trace::is_dirty() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _dirty;
}
