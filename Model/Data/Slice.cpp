#include "..\..\stdafx.hpp"
#include "..\..\Model\Data\Slice.hpp"

#include <algorithm>



Slice::Slice():
	_completed(false)
{
}

Batch& Slice::batch()
{
	return _batch;
}

std::vector<real_t>& Slice::timeline()
{
	return _timeline;
}

void Slice::complete(bool completed)
{
	_completed = completed;
}

const Batch& Slice::batch() const
{
	return _batch;
}

const std::vector<real_t>& Slice::timeline() const
{
	return _timeline;
}


bool Slice::completed() const
{
	return _completed;
}

bool Slice::empty() const
{
	return std::all_of(std::cbegin(_batch), std::cend(_batch), [](Batch::const_reference signal) { return signal.empty(); });
}