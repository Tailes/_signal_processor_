#pragma once
//#include "..\..\Model\Interfaces\IPersistentSliceFilter.hpp"



class Trace;



struct ISerializableSliceFilter/*:
	public IPersistentSliceFilter*/
{
	virtual void serialize(Trace &target) = 0;
};
