#pragma once
//#include "..\..\Model\Interfaces\ISliceFilter.hpp"

#include <string>



struct IPersistentSliceFilter/* :
	public ISliceFilter*/
{
	virtual void bind() = 0;
	virtual void load(const std::wstring &root) = 0;
	virtual void save(const std::wstring &root) const = 0;
};
