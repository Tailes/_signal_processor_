#pragma once



class Slice;



struct ISliceFilter
{
	virtual void open() = 0;
	virtual void prepare(Slice &slice) = 0;
	virtual void apply(Slice &slice, bool &interrupt) = 0;
	virtual void close() = 0;
	virtual void reset() = 0;
};
