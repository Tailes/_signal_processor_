#pragma once
#include "..\..\..\Model\Hardware\Ethernet\Interfaces\IEthernetSignalFilter.hpp"

#include <mutex>


class Trace;
class Signal;
namespace Ethernet
{
	class Signal;


	class SignalSpeedometer:
		public ISignalFilter
	{
		typedef ::Trace Trace;
		typedef ::Signal Signal;
		typedef ::Ethernet::Signal EthernetSignal;
		
	public:
		SignalSpeedometer();
		
		virtual void open();
		virtual void prepare(Signal &signal, EthernetSignal &ethernet_signal);
		virtual void apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt);
		virtual void close();
		virtual void bind();
		virtual void serialize(Trace &target);
		virtual void load(const std::wstring &root);
		virtual void save(const std::wstring &root) const;
		
		std::size_t duration() const;
		
	private:
		std::recursive_mutex mutable _guard;
		std::size_t _duration;
	};


}