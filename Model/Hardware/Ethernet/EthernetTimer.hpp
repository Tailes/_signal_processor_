#pragma once
#include "..\..\..\Model\Types\Real.hpp"
#include "..\..\..\Model\Implementations\SerializableSliceFilterImpl.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetDelayLines.hpp"

#include <vector>
#include <mutex>
#include <array>
#include <map>


class Trace;
class Slice;
class Signal;


namespace Ethernet
{
	class Sizing;
	class Timing;
	class Signal;
	
	
	class Timer:
		public SerializableSliceFilterImpl
	{
		
		typedef ::Trace Trace;
		typedef ::Signal Signal;
		typedef std::vector<real_t> timeline_t;
		
		typedef std::array<real_t, DelaysCount> delays_t;
		
	public:
		Timer(const Timing &timing, const Sizing &sizing);
		
		virtual void open();
		virtual void prepare(Slice &slice);
		virtual void apply(Slice &slice, bool &interrupt);
		virtual void close();
		virtual void reset();
		virtual void bind(const Timing &timing, const Sizing &sizing);
		virtual void serialize(Trace &target);
		virtual void load(const std::wstring &root);
		virtual void save(const std::wstring &root) const;
		
		void time_frame_sedecuple(TimeFrameSedecuple frame, const Timing &timing);
		void time_frame_quadruple(TimeFrameQuadruple frame, const Timing &timing);
		void time_frame_octuple(TimeFrameOctuple frame, const Timing &timing);
		void time_frame_single(TimeFrameSingle frame, const Timing &timing);
		void time_frame_double(TimeFrameDouble frame, const Timing &timing);
		void scanning_delay(Delay delay, const Timing &timing);
		void signal_length(SignalLength length, const Timing &timing);
		void delay_basis(const std::array<std::uint16_t, DelayTotalLinesCount> &basis, const Timing &timing);
		
	private:
		std::recursive_mutex mutable _guard;
		delays_t::const_iterator _delays_begin;
		delays_t::const_iterator _delays_end;
		timeline_t _timeline;
		delays_t _delays;
		
		class Mutator;
		
		void find_first(delays_t::const_iterator &first, const delays_t &delays, std::uint16_t delay) const;
		void find_last(delays_t::const_iterator &last, delays_t::const_iterator first, const delays_t &delays, std::uint16_t frame) const;
		void calculate(delays_t &timestamps, const std::array<std::uint16_t, DelayTotalLinesCount> &slice) const;
		void resize(timeline_t &timeline, std::size_t count) const;
		void select(timeline_t &timeline, const Timing &timing) const;
		
		real_t delay(const std::array<std::uint16_t, DelayTotalLinesCount> &slice, std::size_t index) const;
	};
}