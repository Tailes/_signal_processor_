#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Utility\Registry.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetSizing.hpp"



namespace Ethernet
{
	Sizing::Sizing():
		_length(SignalLength::Single)
	{
	}

	void Sizing::load(const std::wstring &root)
	{
		Registry registry(root);
		std::uint16_t length = static_cast<std::uint16_t>(_length);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.read_integer(L"PacketLength", length); _length = static_cast<SignalLength>(length);
	}

	void Sizing::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"PacketLength", static_cast<std::uint16_t>(_length));
	}



	SignalLength Sizing::signal_length() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _length;
	}



	void Sizing::signal_length(SignalLength length)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_length = length;
	}
}