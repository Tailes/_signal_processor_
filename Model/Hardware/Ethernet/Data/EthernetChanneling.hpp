#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetDelayLines.hpp"

#include <cstdint>
#include <string>
#include <array>
#include <mutex>



namespace Ethernet
{
	class Channeling
	{
	public:
		Channeling();
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		Channel channel() const;
		Mode mode() const;
		
		void channel(Channel channel);
		void mode(Mode mode);
		
	private:
		std::recursive_mutex mutable _guard;
		Channel _channel;
		Mode _mode;
	};
}