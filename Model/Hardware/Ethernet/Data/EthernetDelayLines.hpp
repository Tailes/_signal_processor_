#pragma once

#include <cstddef>



namespace Ethernet
{
	static const std::size_t DelayMinorLinesCount = 10;
	static const std::size_t DelayMajorLinesCount = 5;
	static const std::size_t DelayTotalLinesCount = DelayMinorLinesCount + DelayMajorLinesCount;
	
	static const std::size_t DelaysMinorCount = (1 << DelayMinorLinesCount);
	static const std::size_t DelaysCount = (1 << (DelayMinorLinesCount + DelayMajorLinesCount - 1)); //"-1" because once on, line never turns off.
}