#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetReal.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetInputPackage.hpp"

#include <array>



namespace Ethernet
{
	class Signal:
		public std::array<real_t, InputPackageDataLength>
	{
	public:
		Signal();
		
		void mode(std::size_t mode);
		void index(std::size_t index);
		void number(std::size_t number);
		void channel(std::size_t mode);
		void cluster_size(std::size_t size);
		void clusters_count(std::size_t count);
		void tick_count_end(std::size_t count);
		void tick_count_begin(std::size_t count);
		
		std::size_t mode() const;
		std::size_t index() const;
		std::size_t number() const;
		std::size_t channel() const;
		std::size_t cluster_size() const;
		std::size_t clusters_count() const;
		std::size_t tick_count_end() const;
		std::size_t tick_count_begin() const;
	
	private:
		std::size_t _tick_count_begin;
		std::size_t _tick_count_end;
		std::size_t _clusters_count;
		std::size_t _cluster_size;
		std::size_t _channel;
		std::size_t _number;
		std::size_t _index;
		std::size_t _mode;
	};
}