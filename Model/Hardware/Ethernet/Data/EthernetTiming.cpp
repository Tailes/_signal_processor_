#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Utility\Registry.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetTiming.hpp"

#include <initializer_list>



namespace Ethernet
{
	Timing::Timing():
		//_delay(0), _basis({{14, 30, 47, 97, 140, 335, 650, 1180, 2400, 4800, 9290, 9290, 9290}}), _step(1)
		_delay(0), _basis({{10, 20, 40, 80, 160, 320, 640, 1280, 2560, 5120, 10240, 10240, 10240}}), _step(1)
	{
	}

	void Timing::load(const std::wstring &root)
	{
		Registry registry(root);
		std::uint8_t frame = 1;
		std::lock_guard<std::recursive_mutex> lock(_guard);
		//registry.read_integer(L"Delay", _delay);
		registry.read_integer(L"TimingFrame", _step);
		registry.read_integer(L"TimingFrameSingle", frame); _time_frame_single = static_cast<TimeFrameSingle>(frame);
		registry.read_integer(L"TimingFrameDouble", frame); _time_frame_double = static_cast<TimeFrameDouble>(frame);
		registry.read_integer(L"TimingFrameOctuple", frame); _time_frame_octuple = static_cast<TimeFrameOctuple>(frame);
		registry.read_integer(L"TimingFrameQuadruple", frame); _time_frame_quadruple = static_cast<TimeFrameQuadruple>(frame);
		registry.read_integer(L"TimingFrameSedecuple", frame); _time_frame_sedecuple = static_cast<TimeFrameSedecuple>(frame);
		//registry.read_integer(L"D0_Delay", _basis.at(0));
		//registry.read_integer(L"D1_Delay", _basis.at(1));
		//registry.read_integer(L"D2_Delay", _basis.at(2));
		//registry.read_integer(L"D3_Delay", _basis.at(3));
		//registry.read_integer(L"D4_Delay", _basis.at(4));
		//registry.read_integer(L"D5_Delay", _basis.at(5));
		//registry.read_integer(L"D6_Delay", _basis.at(6));
		//registry.read_integer(L"D7_Delay", _basis.at(7));
		//registry.read_integer(L"D8_Delay", _basis.at(8));
		//registry.read_integer(L"D9_Delay", _basis.at(9));
		//registry.read_integer(L"D10_Delay", _basis.at(10));
		//registry.read_integer(L"D11_Delay", _basis.at(11));
		//registry.read_integer(L"D12_Delay", _basis.at(12));
	}

	void Timing::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		//registry.write_integer(L"Delay", _delay);
		registry.write_integer(L"TimingFrame", _step);
		registry.write_integer(L"TimingFrameSingle", static_cast<std::uint8_t>(_time_frame_single));
		registry.write_integer(L"TimingFrameDouble", static_cast<std::uint8_t>(_time_frame_double));
		registry.write_integer(L"TimingFrameOctuple", static_cast<std::uint8_t>(_time_frame_octuple));
		registry.write_integer(L"TimingFrameQuadruple", static_cast<std::uint8_t>(_time_frame_quadruple));
		registry.write_integer(L"TimingFrameSedecuple", static_cast<std::uint8_t>(_time_frame_sedecuple));
		registry.write_integer(L"D0_Delay", _basis.at(0));
		registry.write_integer(L"D1_Delay", _basis.at(1));
		registry.write_integer(L"D2_Delay", _basis.at(2));
		registry.write_integer(L"D3_Delay", _basis.at(3));
		registry.write_integer(L"D4_Delay", _basis.at(4));
		registry.write_integer(L"D5_Delay", _basis.at(5));
		registry.write_integer(L"D6_Delay", _basis.at(6));
		registry.write_integer(L"D7_Delay", _basis.at(7));
		registry.write_integer(L"D8_Delay", _basis.at(8));
		registry.write_integer(L"D9_Delay", _basis.at(9));
		registry.write_integer(L"D10_Delay", _basis.at(10));
		registry.write_integer(L"D11_Delay", _basis.at(11));
		registry.write_integer(L"D12_Delay", _basis.at(12));
	}

	TimeFrameSingle Timing::time_frame_single() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _time_frame_single;
	}

	TimeFrameDouble Timing::time_frame_double() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _time_frame_double;
	}

	TimeFrameQuadruple Timing::time_frame_quadruple() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _time_frame_quadruple;
	}

	TimeFrameOctuple Timing::time_frame_octuple() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _time_frame_octuple;
	}

	TimeFrameSedecuple Timing::time_frame_sedecuple() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _time_frame_sedecuple;
	}



	DelayBasis Timing::delay_basis() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _basis;
	}

	Delay Timing::scanning_delay() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _delay;
	}

	Step Timing::step() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _step;
	}



	void Timing::delay_callback(std::function<void(Delay)>&& callback)
	{
	}

	void Timing::time_frame_single(TimeFrameSingle frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_step = static_cast<std::uint8_t>(frame);
		_time_frame_single = frame;
	}

	void Timing::time_frame_double(TimeFrameDouble frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_step = static_cast<std::uint8_t>(frame);
		_time_frame_double = frame;
	}

	void Timing::time_frame_quadruple(TimeFrameQuadruple frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_step = static_cast<std::uint8_t>(frame);
		_time_frame_quadruple = frame;
	}

	void Timing::time_frame_octuple(TimeFrameOctuple frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_step = static_cast<std::uint8_t>(frame);
		_time_frame_octuple = frame;
	}

	void Timing::time_frame_sedecuple(TimeFrameSedecuple frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_step = static_cast<std::uint8_t>(frame);
		_time_frame_sedecuple = frame;
	}

	void Timing::delay_basis(const DelayBasis &basis)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_basis = basis;
	}

	void Timing::scanning_delay(Delay delay)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_delay = delay;
	}
	
	void Timing::step(Step step)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_step = step;
	}
}