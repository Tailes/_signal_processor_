#pragma once
#include <filesystem>
#include <string>
#include <mutex>



namespace Ethernet
{
	class Signal;



	class Trace
	{
	public:
		Trace();
		~Trace();
		
		void prepare();
		void write(const Signal &signal);
		void save(const std::wstring &name);
		void close();
		void reset();
		
	private:
		std::recursive_mutex mutable _guard;
		std::wstring _path;
		std::FILE *_file;
	};
}