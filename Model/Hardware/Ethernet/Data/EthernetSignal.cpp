#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"



namespace Ethernet
{
	Signal::Signal():
		_index(0), _clusters_count(0), _cluster_size(0), _number(0), _tick_count_begin(0), _tick_count_end(0)
	{
	}

	void Signal::mode(std::size_t mode)
	{
		_mode = mode;
	}

	void Signal::index(std::size_t index)
	{
		_index = index;
	}

	void Signal::number(std::size_t number)
	{
		_number = number;
	}

	void Signal::channel(std::size_t channel)
	{
		_channel = channel;
	}

	void Signal::tick_count_end(std::size_t count)
	{
		_tick_count_end = count;
	}

	void Signal::tick_count_begin(std::size_t count)
	{
		_tick_count_begin = count;
	}

	void Signal::clusters_count(std::size_t count)
	{
		_clusters_count = count;
	}

	void Signal::cluster_size(std::size_t size)
	{
		_cluster_size = size;
	}



	std::size_t Signal::mode() const
	{
		return _mode;
	}

	std::size_t Signal::index() const
	{
		return _index;
	}

	std::size_t Signal::number() const
	{
		return _number;
	}

	std::size_t Signal::channel() const
	{
		return _channel;
	}

	std::size_t Signal::tick_count_end() const
	{
		return _tick_count_end;
	}

	std::size_t Signal::tick_count_begin() const
	{
		return _tick_count_begin;
	}

	std::size_t Signal::cluster_size() const
	{
		return _cluster_size;
	}

	std::size_t Signal::clusters_count() const
	{
		return _clusters_count;
	}
}