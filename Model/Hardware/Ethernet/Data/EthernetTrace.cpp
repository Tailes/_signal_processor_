#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Utility\TypeArithmetics.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetTrace.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"

#include <filesystem>
#include <cstdint>




namespace Ethernet
{
	#pragma pack(push, 1)
	struct TraceFormat
	{
		struct FileHeader
		{
			std::uint16_t _version;
		};
		
		struct EntityHeader
		{
			std::uint32_t _cluster_count;
			std::uint32_t _cluster_size;
			std::uint32_t _number;
			std::uint32_t _index;
		};
	};
	#pragma pack(pop)



	Trace::Trace():
		_file(nullptr)
	{
		wchar_t buffer[L_tmpnam_s] = {};
		_wtmpnam_s(buffer);
		if (_wfopen_s(&_file, buffer, L"wb") != 0)
			throw std::runtime_error("Cannot create a temporary file.");
		_path = buffer;
	}

	Trace::~Trace()
	{
		std::fclose(_file);
	}

	void Trace::prepare()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::rewind(_file);
		if ( std::fwrite(&TraceFormat::FileHeader(), sizeof(TraceFormat::FileHeader), 1, _file) != 1 )
			throw std::runtime_error("Cannot write ethernet file signal.");
	}

	void Trace::write(const Signal &signal)
	{
		TraceFormat::EntityHeader header;
		header._cluster_count = signal.clusters_count();
		header._cluster_size = signal.cluster_size();
		header._number = signal.number();
		header._index = signal.index();
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( std::fwrite(&header, sizeof(header), 1, _file) != 1 )
			throw std::runtime_error("Cannot write ethernet entity header.");
		if ( std::fwrite(signal.data(), sizeof(Signal::value_type), signal.size(), _file) != signal.size() )
			throw std::runtime_error("Cannot write ethernet signal.");
	}

	void Trace::save(const std::wstring &name)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( std::fflush(_file) != 0 )
			throw std::runtime_error("Cannot flush ethernet file.");
		std::experimental::filesystem::copy_file(_path, name, std::experimental::filesystem::copy_options::overwrite_existing);
	}

	void Trace::close()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::fflush(_file);
	}

	void Trace::reset()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::fseek(_file, 0, SEEK_SET);
	}
}