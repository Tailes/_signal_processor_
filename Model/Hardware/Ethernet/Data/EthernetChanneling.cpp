#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Utility\Registry.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetChanneling.hpp"



namespace Ethernet
{
	Channeling::Channeling():
		_channel(Channel::_1), _mode(Mode::ActiveAntenna)
	{
	}

	void Channeling::load(const std::wstring &root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::uint8_t mode = static_cast<std::uint8_t>(_mode), channel = static_cast<std::uint8_t>(_channel);
		registry.read_integer(L"Algorithm", mode); _mode = static_cast<Mode>(mode);
		registry.read_integer(L"Channel", channel); _channel = static_cast<Channel>(channel);
	}

	void Channeling::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"Algorithm", static_cast<std::uint8_t>(_mode));
		registry.write_integer(L"Channel", static_cast<std::uint8_t>(_channel));
	}



	Channel Channeling::channel() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _channel;
	}

	Mode Channeling::mode() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _mode;
	}

	


	void Channeling::channel(Channel channel)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channel = channel;
	}

	void Channeling::mode(Mode mode)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_mode = mode;
	}
}