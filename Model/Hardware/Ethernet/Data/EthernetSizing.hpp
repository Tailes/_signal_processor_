#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetDelayLines.hpp"

#include <cstdint>
#include <string>
#include <array>
#include <mutex>



namespace Ethernet
{
	typedef std::uint8_t Step;
	typedef std::array<std::uint16_t, DelayTotalLinesCount> DelayBasis;
	
	
	
	class Sizing
	{
	public:
		Sizing();
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		SignalLength signal_length() const;
		
		void signal_length(SignalLength length);
		
	private:
		std::recursive_mutex mutable _guard;
		SignalLength _length;
	};

}