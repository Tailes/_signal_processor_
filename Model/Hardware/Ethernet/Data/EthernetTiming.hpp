#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetDelayLines.hpp"

#include <functional>
#include <cstdint>
#include <string>
#include <array>
#include <mutex>



namespace Ethernet
{
	typedef std::uint8_t Step;
	typedef std::array<std::uint16_t, DelayTotalLinesCount> DelayBasis;
	
	
	
	class Timing
	{
	public:
		Timing();
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		TimeFrameQuadruple time_frame_quadruple() const;
		TimeFrameSedecuple time_frame_sedecuple() const;
		TimeFrameOctuple time_frame_octuple() const;
		TimeFrameSingle time_frame_single() const;
		TimeFrameDouble time_frame_double() const;
		DelayBasis delay_basis() const;
		Delay scanning_delay() const;
		Step step() const;
		
		void delay_callback(std::function<void(Delay)> &&callback);
		
		void time_frame_quadruple(TimeFrameQuadruple frame);
		void time_frame_sedecuple(TimeFrameSedecuple frame);
		void time_frame_octuple(TimeFrameOctuple frame);
		void time_frame_single(TimeFrameSingle frame);
		void time_frame_double(TimeFrameDouble frame);
		void scanning_delay(Delay delay);
		void delay_basis(const DelayBasis &basis);
		void step(Step step);
		
	private:
		std::recursive_mutex mutable _guard;
		std::function<void(DelayBasis)> _delay_basis_callback;
		std::function<void(Delay)> _delay_callback;
		TimeFrameSedecuple _time_frame_sedecuple;
		TimeFrameQuadruple _time_frame_quadruple;
		TimeFrameOctuple _time_frame_octuple;
		TimeFrameSingle _time_frame_single;
		TimeFrameDouble _time_frame_double;
		DelayBasis _basis;
		Delay _delay;
		Step _step;
	};

}