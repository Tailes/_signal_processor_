#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Utility\Registry.hpp"
#include "..\..\..\Model\Data\Slice.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetChanneler.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetChanneling.hpp"


namespace Ethernet
{
	Channeler::Channeler(const Timing &timing, const Sizing &sizing):
		_provider(timing, sizing, _channeling)
	{
		_algorithms[Mode::ActiveAntennaCrossInput] = &_active_antenna_cross_input_algorithm;
		_algorithms[Mode::ActiveAntenna] = &_active_antenna_algorithm;
		_algorithms[Mode::Slice] = &_slice_algorithm;
	}

	void Channeler::open()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_provider.open();
	}

	void Channeler::prepare(Slice &slice)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		for ( std::size_t i = 0; i < std::min<std::size_t>(slice.batch().size(), static_cast<std::size_t>(Channel::Total)); ++i )
			_provider.prepare(slice.batch().at(i), EthernetSignal());
		for ( std::size_t i = std::min<std::size_t>(slice.batch().size(), static_cast<std::size_t>(Channel::Total)); i < slice.batch().size(); ++i )
			slice.batch().at(i).assign(0, static_cast<real_t>(0.0));
		_channeling.channel(_algorithms.at(_channeling.mode())->first(_channeling.channel()));
		_provider.signal_source().output().scanning_channel(_channeling.channel());
	}

	void Channeler::apply(Slice &slice, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if (!interrupt) this->apply(slice.batch().at(static_cast<std::uint16_t>(_channeling.channel())), interrupt);
		if (!interrupt) this->advance();
		if (!interrupt) interrupt = !this->completed();
		if (!interrupt) slice.complete(true);
	}

	void Channeler::reset()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channeling.channel(_algorithms.at(_channeling.mode())->first(_channeling.channel()));
		_provider.signal_source().output().scanning_channel(_channeling.channel());
	}

	void Channeler::close()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_provider.close();
	}

	void Channeler::bind(const Timing &timing, const Sizing &sizing)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_provider.bind(timing, sizing, _channeling);
	}

	void Channeler::serialize(Trace &target)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_provider.serialize(target);
	}

	void Channeler::load(const std::wstring &root)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channeling.load(root);
		_provider.load(root);
	}

	void Channeler::save(const std::wstring &root) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channeling.save(root);
		_provider.save(root);
	}




	bool Channeler::completed() const
	{
		return _algorithms.at(_channeling.mode())->completed(_channeling.channel());
	}

	void Channeler::advance()
	{
		_channeling.channel(_algorithms.at(_channeling.mode())->next(_channeling.channel()));
		_provider.signal_source().output().scanning_channel(_channeling.channel());
	}

	void Channeler::apply(Signal &signal, bool &interrupt)
	{
		_provider.apply(signal, _signal, interrupt);
	}





	void Channeler::mode(Mode mode)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_provider.signal_source().output().scanning_mode(mode);
		_channeling.mode(mode);
	}

	void Channeler::channel(Channel channel)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_provider.signal_source().output().scanning_channel(channel);
		_channeling.channel(channel);
	}




	Mode Channeler::mode() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _channeling.mode();
	}

	Channel Channeler::channel() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _channeling.channel();
	}




	SignalProvider& Channeler::signal_provider()
	{
		return _provider;
	}

	ScanningAlgorithmSlice & Channeler::slice_algorithm()
	{
		return _slice_algorithm;
	}

	const SignalProvider& Channeler::signal_provider() const
	{
		return _provider;
	}

	const ScanningAlgorithmSlice & Channeler::slice_algorithm() const
	{
		return _slice_algorithm;
	}
}