#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Model\Data\Signal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalAssembler.hpp"

#include <numeric>




namespace Ethernet
{
	SignalAssembler::SignalAssembler()
	{
	}

	void SignalAssembler::open()
	{
	}

	void SignalAssembler::prepare(Signal &signal, EthernetSignal &ethernet_signal)
	{
	}

	void SignalAssembler::apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);

		std::ldiv_t prefix_ratio = std::ldiv(ethernet_signal.index() * ethernet_signal.size(), ethernet_signal.cluster_size());
		std::ldiv_t postfix_ratio = std::ldiv((ethernet_signal.index() + 1) * ethernet_signal.size(), ethernet_signal.cluster_size());
		EthernetSignal::iterator prefix_end = ethernet_signal.begin() + (prefix_ratio.rem ? ethernet_signal.cluster_size() - prefix_ratio.rem : 0);
		EthernetSignal::iterator body_end = ethernet_signal.end() - (postfix_ratio.rem ? postfix_ratio.rem : 0);

		if ( prefix_ratio.rem )
			signal.at(prefix_ratio.quot) += std::accumulate(ethernet_signal.begin(), prefix_end, real_t()) / ethernet_signal.cluster_size();

		::Signal::iterator signal_iterator = signal.begin() + prefix_ratio.quot + (prefix_ratio.rem ? 1 : 0);
		for ( EthernetSignal::iterator it = prefix_end; it != body_end; it += ethernet_signal.cluster_size() )
			*signal_iterator++ = std::accumulate(it, it + ethernet_signal.cluster_size(), real_t()) / ethernet_signal.cluster_size();

		if ( postfix_ratio.rem )
			signal.at(postfix_ratio.quot) += std::accumulate(body_end, ethernet_signal.end(), real_t()) / ethernet_signal.cluster_size();
	}

	void SignalAssembler::close()
	{
	}

	void SignalAssembler::serialize(Trace &target)
	{
	}

	void SignalAssembler::bind()
	{
	}

	void SignalAssembler::load(const std::wstring &root)
	{
	}

	void SignalAssembler::save(const std::wstring &root) const
	{
	}
}