#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Utility\Registry.hpp"
#include "..\..\..\Model\Data\Slice.hpp"
#include "..\..\..\Model\Data\Trace.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSizing.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetTiming.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetTimer.hpp"

#include <algorithm>
#include <numeric>
#include <bitset>
#include <array>



namespace Ethernet
{
	static const real_t DelayConversionCoeff = 10.f;



	class Timer::Mutator
	{
	public:
		Mutator(delays_t::value_type delay, std::ptrdiff_t step);
		
		real_t operator()();
		
	private:
		delays_t::difference_type _current;
		delays_t::difference_type _step;
		delays_t::value_type _delay;
	};





	Timer::Timer(const Timing &timing, const Sizing &sizing):
		/*_timing(timing), *//*_sizing(sizing), */_delays({{0}}),
		_timeline(static_cast<std::size_t>(sizing.signal_length()), timeline_t::value_type()),
		_delays_begin(_delays.cbegin()), _delays_end(_delays.cend())
	{
		this->calculate(_delays, timing.delay_basis());
		this->find_first(_delays_begin, _delays, timing.scanning_delay());
		this->find_last(_delays_end, _delays_begin, _delays, timing.step());
		this->resize(_timeline, static_cast<std::size_t>(sizing.signal_length()));
		this->select(_timeline, timing);
	}

	void Timer::open()
	{
	}

	void Timer::prepare(Slice &slice)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		slice.timeline().assign(_timeline.cbegin(), _timeline.cend());
	}

	void Timer::apply(Slice &slice, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		slice.timeline().assign(_timeline.cbegin(), _timeline.cend());
	}

	void Timer::reset()
	{
	}

	void Timer::close()
	{
	}

	void Timer::serialize(Trace &target)
	{
	}

	void Timer::bind(const Timing &timing, const Sizing &sizing)
	{
		this->calculate(_delays, timing.delay_basis());
		this->find_first(_delays_begin, _delays, 0);
		this->find_last(_delays_end, _delays_begin, _delays, timing.step());
		this->resize(_timeline, static_cast<std::size_t>(sizing.signal_length()));
		this->select(_timeline, timing);
	}

	void Timer::load(const std::wstring &root)
	{
	}

	void Timer::save(const std::wstring &root) const
	{
	}



	void Timer::delay_basis(const std::array<std::uint16_t, DelayTotalLinesCount> &basis, const Timing &timing)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		this->calculate(_delays, basis);
		this->find_first(_delays_begin, _delays, 0);
		this->find_last(_delays_end, _delays_begin, _delays, timing.step());
		this->select(_timeline, timing);
	}

	void Timer::signal_length(SignalLength length, const Timing &timing)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		this->resize(_timeline, static_cast<std::size_t>(length));
		this->select(_timeline, timing);
	}

	void Timer::time_frame_single(TimeFrameSingle frame, const Timing &timing)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		this->find_last(_delays_end, _delays_begin, _delays, static_cast<std::uint16_t>(frame));
		this->select(_timeline, timing);
	}

	void Timer::time_frame_double(TimeFrameDouble frame, const Timing &timing)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		this->find_last(_delays_end, _delays_begin, _delays, static_cast<std::uint16_t>(frame));
		this->select(_timeline, timing);
	}

	void Timer::time_frame_octuple(TimeFrameOctuple frame, const Timing & timing)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		this->find_last(_delays_end, _delays_begin, _delays, static_cast<std::uint16_t>(frame));
		this->select(_timeline, timing);
	}

	void Timer::time_frame_quadruple(TimeFrameQuadruple frame, const Timing & timing)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		this->find_last(_delays_end, _delays_begin, _delays, static_cast<std::uint16_t>(frame));
		this->select(_timeline, timing);
	}

	void Timer::time_frame_sedecuple(TimeFrameSedecuple frame, const Timing & timing)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		this->find_last(_delays_end, _delays_begin, _delays, static_cast<std::uint16_t>(frame));
		this->select(_timeline, timing);
	}


	void Timer::scanning_delay(Delay delay, const Timing &timing)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		this->find_first(_delays_begin, _delays, 0);
		this->find_last(_delays_end, _delays_begin, _delays, timing.step());
		this->select(_timeline, timing);
	}



	//TimeFrameDouble Timer::time_frame_double() const
	//{
	//	std::lock_guard<std::recursive_mutex> lock(_guard);
	//	return static_cast<TimeFrameDouble>(_step);
	//}

	//TimeFrameSingle Timer::time_frame_single() const
	//{
	//	std::lock_guard<std::recursive_mutex> lock(_guard);
	//	return static_cast<TimeFrameSingle>(_step);
	//}

	//std::uint16_t Timer::scanning_delay() const
	//{
	//	std::lock_guard<std::recursive_mutex> lock(_guard);
	//	return _delay;
	//}

	//std::array<std::uint16_t, DelayTotalLinesCount> Timer::basis() const
	//{
	//	std::lock_guard<std::recursive_mutex> lock(_guard);
	//	return _basis;
	//}




	void Timer::calculate(delays_t &timestamps, const std::array<std::uint16_t, DelayTotalLinesCount> &slice) const
	{
		for ( std::size_t j = 0, delta = 0; j < DelaysCount; delta = 1, j += DelaysMinorCount )
			for ( std::size_t i = 0; i < DelaysMinorCount; ++i )
				timestamps.at(j + i) = timestamps.at(j - delta) + this->delay(slice, i);
		std::sort(timestamps.begin(), timestamps.end());
	}

	void Timer::find_first(delays_t::const_iterator &first, const delays_t &delays, std::uint16_t delay) const
	{
		first = std::lower_bound(delays.cbegin(), delays.cend(), delay * DelayConversionCoeff);
	}

	void Timer::find_last(delays_t::const_iterator &last, delays_t::const_iterator first, const delays_t &delays, std::uint16_t frame) const
	{
		last = std::next(first, static_cast<std::ptrdiff_t>(frame) * (std::distance(first, delays.cend()) / static_cast<std::ptrdiff_t>(frame)));
	}

	real_t Timer::delay(const std::array<std::uint16_t, DelayTotalLinesCount> &slice, std::size_t index) const
	{
		real_t picoseconds = 0.0;
		std::bitset<DelayMinorLinesCount> bits(index);
		for ( std::size_t i = 0; i < bits.size(); ++i )
			if ( bits.test(i) )
				picoseconds += static_cast<real_t>(slice.at(i));
		return picoseconds;
	}

	void Timer::resize(timeline_t &timeline, std::size_t count) const
	{
		timeline.resize(count, timeline_t::value_type());
	}
	
	void Timer::select(timeline_t &timeline, const Timing &timing) const
	{
		std::generate(timeline.begin(), timeline.end(), Mutator(timing.scanning_delay(), timing.step()));
	}





	Timer::Mutator::Mutator(delays_t::value_type delay, std::ptrdiff_t step):
		_delay(delay), _step(step), _current(0)
	{
	}

	real_t Timer::Mutator::operator()()
	{
		return (_delay + _step*_current++)*10;
	}
}