#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetDataSourceBase.hpp"



namespace Ethernet
{
	Timing & DataSourceBase::timing()
	{
		return _timing;
	}

	Sizing & DataSourceBase::sizing()
	{
		return _sizing;
	}

	const Timing & DataSourceBase::timing() const
	{
		return _timing;
	}

	const Sizing & DataSourceBase::sizing() const
	{
		return _sizing;
	}
}