#pragma once
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSizing.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetTiming.hpp"


namespace Ethernet
{
	class DataSourceBase
	{
	protected:
		DataSourceBase() = default;
		
	public:
		Timing& timing();
		Sizing& sizing();
		
		const Timing& timing() const;
		const Sizing& sizing() const;
		
	protected:
		Timing _timing;
		Sizing _sizing;
	};
}