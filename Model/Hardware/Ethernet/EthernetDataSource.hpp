#pragma once
#include "..\..\..\Model\Implementations\SerializableSliceFilterImpl.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetTimer.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetDelayer.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetChanneler.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetInterpolator.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetDataSourceBase.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetDelayLines.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmActiveAntenna.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmActiveAntennaCrossInput.hpp"
#include "..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSettings.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Interfaces\IEthernetScanningAlgorithm.hpp"

#include <string>
#include <mutex>
#include <map>


class Slice;
class Trace;
namespace Ethernet
{
	class Output;
	
	class DataSource:
		public SerializableSliceFilterImpl,
		public DataSourceBase
	{
		typedef ::Trace Trace;
		typedef ::Signal Signal;
		typedef ::Ethernet::Signal EthernetSignal;
		
		typedef std::uint16_t Frequency;
		typedef std::array<std::uint16_t, DelayTotalLinesCount> Basis;
		
	public:
		DataSource();
		
		virtual void open();
		virtual void prepare(Slice &slice);
		virtual void apply(Slice &slice, bool &interrupt);
		virtual void close();
		virtual void reset();
		virtual void bind();
		virtual void serialize(Trace &target);
		virtual void load(const std::wstring &root);
		virtual void save(const std::wstring &root) const;
		
		void delays_basis(const Basis &basis);
		void signal_length(SignalLength length);
		void scanning_delay(Delay delay);
		void time_frame_single(TimeFrameSingle frame);
		void time_frame_double(TimeFrameDouble frame);
		void time_frame_octuple(TimeFrameOctuple frame);
		void time_frame_quadruple(TimeFrameQuadruple frame);
		void time_frame_sedecuple(TimeFrameSedecuple frame);
		
		Basis delays_basis() const;
		Delay scanning_delay() const;
		Frequency generator_sync_frequency() const;
		SignalLength signal_length() const;
		TimeFrameSingle time_frame_single() const;
		TimeFrameDouble time_frame_double() const;
		TimeFrameOctuple time_frame_octuple() const;
		TimeFrameQuadruple time_frame_quadruple() const;
		TimeFrameSedecuple time_frame_sedecuple() const;
		
		Timer& timer();
		Delayer& delayer();
		Settings& settings();
		Channeler& channeler();
		Interpolator& interpolator();
		const Timer& timer() const;
		const Delayer& delayer() const;
		const Settings& settings() const;
		const Channeler& channeler() const;
		const Interpolator& interpolator() const;
		
	private:
		std::recursive_mutex mutable _guard;
		Interpolator _interpolator;
		Channeler _channeler;
		Settings _settings;
		Delayer _delayer;
		Timer _timer;
	};
}