#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Model\Data\Signal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalCounter.hpp"

#include <numeric>




namespace Ethernet
{
	SignalCounter::SignalCounter()
	{
		_number = 0;
	}

	void SignalCounter::open()
	{
	}

	void SignalCounter::prepare(Signal &signal, EthernetSignal &ethernet_signal)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_markers.resize(signal.size());
		_number = 0;
	}

	void SignalCounter::apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		
		if ( _number != ethernet_signal.number() )
			_markers.assign(_markers.size(), false);
		_number = ethernet_signal.number();
		
		std::ldiv_t prefix_ratio = std::ldiv(ethernet_signal.index() * ethernet_signal.size(), ethernet_signal.cluster_size());
		std::ldiv_t postfix_ratio = std::ldiv((ethernet_signal.index() + 1) * ethernet_signal.size(), ethernet_signal.cluster_size());
		EthernetSignal::iterator prefix_end = ethernet_signal.begin() + (prefix_ratio.rem ? ethernet_signal.cluster_size() - prefix_ratio.rem : 0);
		EthernetSignal::iterator body_end = ethernet_signal.end() - (postfix_ratio.rem ? postfix_ratio.rem : 0);
		
		if ( prefix_ratio.rem )
			_markers.at(prefix_ratio.quot) = true;
		
		std::vector<bool>::iterator marker_iterator = _markers.begin() + prefix_ratio.quot + (prefix_ratio.rem ? 1 : 0);
		for ( EthernetSignal::iterator it = prefix_end; it != body_end; it += ethernet_signal.cluster_size() )
			*marker_iterator++ = true;
		
		if ( postfix_ratio.rem )
			_markers.at(postfix_ratio.quot) = true;
		
		if ( !(interrupt = std::find(_markers.begin(), _markers.end(), false) != _markers.end()) )
			_markers.flip();
	}

	void SignalCounter::close()
	{
	}

	void SignalCounter::bind()
	{
	}

	void SignalCounter::serialize(Trace &target)
	{
	}

	void SignalCounter::load(const std::wstring &root)
	{
	}

	void SignalCounter::save(const std::wstring &root) const
	{
	}

	std::size_t SignalCounter::number() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _number;
	}

	bool SignalCounter::completed() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return std::adjacent_find(std::cbegin(_markers), std::cend(_markers), [](bool left, bool right) { return left != right; }) == std::cend(_markers);
	}
}