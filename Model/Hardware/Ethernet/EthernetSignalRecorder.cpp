#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalRecorder.hpp"
#include "..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetInput.hpp"
#include "..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"

#include <strstream>
#include <sstream>



namespace Ethernet
{
	SignalRecorder::SignalRecorder()
	{
		_enabled = false;
	}

	void SignalRecorder::open()
	{
	}

	void SignalRecorder::prepare(Signal &signal, EthernetSignal &ethernet_signal)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		for ( auto &channel : _channels ) channel.prepare();
	}

	void SignalRecorder::close()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		for ( auto &channel : _channels ) channel.close();
	}

	void SignalRecorder::apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( _enabled ) _channels.at(ethernet_signal.channel()).write(ethernet_signal);
	}

	void SignalRecorder::flush(const std::array<std::wstring, static_cast<std::size_t>(Channel::Total)> &paths)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::array<std::wstring, static_cast<std::size_t>(Channel::Total)>::const_iterator path = paths.cbegin();
		for ( auto &channel : _channels ) channel.save(*path++);
	}

	void SignalRecorder::enable(bool value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_enabled = value;
	}

	bool SignalRecorder::enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _enabled;
	}

	void SignalRecorder::serialize(::Trace &target)
	{
	}

	void SignalRecorder::bind()
	{
	}

	void SignalRecorder::load(const std::wstring &root)
	{
	}

	void SignalRecorder::save(const std::wstring &root) const
	{
	}
}