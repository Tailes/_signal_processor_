#pragma once
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetDelayerModes.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Interfaces\IEthernetDelayerMode.hpp"
#include "..\..\..\Model\Hardware\Ethernet\DelayerModes\EthernetDelayerModeTimer.hpp"
#include "..\..\..\Model\Hardware\Ethernet\DelayerModes\EthernetDelayerModeCounter.hpp"

#include <string>
#include <mutex>
#include <map>



namespace Ethernet
{
	class Timer;
	class Timing;
	class Output;
	
	
	class Delayer
	{
	public:
		Delayer();
		
		void bind();
		void open();
		void prepare();
		void apply(Timer &timer, Timing &timing, Output &output);
		void advance();
		void close();
		void reset();
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		const DelayerModeCounter& counter() const;
		const DelayerModeTimer& timer() const;
		DelayerMode mode() const;
		Delay lower_limit() const;
		Delay upper_limit() const;
		Delay delta() const;
		
		bool enabled() const;
		
		DelayerModeCounter& counter();
		DelayerModeTimer& timer();
		void direction(bool forward);
		void enable(bool enabled);
		void limit(Delay lower, Delay upper);
		void delta(Delay delay);
		void mode(DelayerMode mode);
		
	private:
		std::recursive_mutex mutable _guard;
		std::map<DelayerMode, IDelayerMode*> _delayers;
		DelayerModeCounter _counter;
		DelayerModeTimer _timer;
		DelayerMode _mode;
		Delay _lower_limit;
		Delay _upper_limit;
		Delay _delta;
		bool _enabled;
	};
}