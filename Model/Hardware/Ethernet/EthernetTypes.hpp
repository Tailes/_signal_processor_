#pragma once

#include <cstdint>



namespace Ethernet
{
	typedef std::uint8_t Step;
	typedef std::int32_t Delay;


	enum class SignalLength: std::uint16_t
	{
		Single = 512,
		Double = 1024,
		Quadruple = 2048,
		Octuple = 4096,
		Sedecuple = 8192
	};

	enum class Mode: std::uint8_t
	{
		ActiveAntenna = 0,
		ActiveAntennaCrossInput = 1,
		TransmitRecieve = 2,
		QuadroAntenna = 3,
		Slice = 4
	};

	enum class Channel: std::uint8_t
	{
		_1 = 0,
		_2 = 1,
		_3 = 2,
		_4 = 3,
		Total
	};

	enum class TimeFrameSingle: std::uint8_t
	{
		_05_ns = 1,
		_10_ns = 2,
		_20_ns = 4,
		_25_ns = 5,
		_40_ns = 8,
		_80_ns = 16,
		_160_ns = 32,
	};

	enum class TimeFrameDouble: std::uint8_t
	{
		_10_ns = 1,
		_20_ns = 2,
		_40_ns = 4,
		_80_ns = 8,
		_160_ns = 16,
	};

	enum class TimeFrameQuadruple: std::uint8_t
	{
		_20_ns = 1,
		_40_ns = 2,
		_80_ns = 4,
		_160_ns = 8,
		_320_ns = 16,
	};

	enum class TimeFrameOctuple: std::uint8_t
	{
		_40_ns = 1,
		_80_ns = 2,
		_160_ns = 4,
		_320_ns = 8,
		_640_ns = 16,
	};

	enum class TimeFrameSedecuple: std::uint8_t
	{
		_80_ns = 1,
		_160_ns = 2,
		_320_ns = 4,
		_640_ns = 8,
		_1280_ns = 16,
	};

	enum class TriggerMode: std::uint16_t
	{
		None = 0,
		WheelDirect = 1,
		WheelReversed = 2,
		Velocity = 3,
	};
}