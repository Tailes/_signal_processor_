#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalProvider.hpp"



namespace Ethernet
{
	SignalProvider::SignalProvider(const Timing &timing, const Sizing &sizing, const Channeling &channeling):
		_source(timing, sizing, channeling)
	{
		_filters.push_back(&_source);
		_filters.push_back(&_recorder);
		_filters.push_back(&_validator);
		_filters.push_back(&_assembler);
		_filters.push_back(&_counter);
		_filters.push_back(&_speedometer);
	}

	void SignalProvider::open()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		for ( auto &filter : _filters ) filter->open();
	}

	void SignalProvider::prepare(Signal &signal, EthernetSignal &ethernet_signal)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		for ( auto &filter : _filters ) filter->prepare(signal, ethernet_signal);
	}

	void SignalProvider::apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		for ( auto &filter : _filters ) if ( interrupt ) break; else filter->apply(signal, ethernet_signal, interrupt);
	}

	void SignalProvider::close()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		for ( auto &filter : _filters ) filter->close();
	}

	void SignalProvider::serialize(Trace &target)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		for ( auto &filter : _filters ) filter->serialize(target);
	}

	void SignalProvider::bind(const Timing &timing, const Sizing &sizing, const Channeling &channeling)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_source.bind(timing, sizing, channeling);
		_recorder.bind();
		_validator.bind();
		_assembler.bind();
		_counter.bind();
		_speedometer.bind();
	}

	void SignalProvider::load(const std::wstring &root)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		for ( auto &filter : _filters ) filter->load(root);
	}

	void SignalProvider::save(const std::wstring &root) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		for ( auto &filter : _filters ) filter->save(root);
	}




	SignalSource& SignalProvider::signal_source()
	{
		return _source;
	}

	SignalCounter & SignalProvider::signal_counter()
	{
		return _counter;
	}

	SignalRecorder& SignalProvider::signal_recorder()
	{
		return _recorder;
	}

	SignalAssembler& SignalProvider::signal_assembler()
	{
		return _assembler;
	}

	SignalSpeedometer& SignalProvider::signal_speedometer()
	{
		return _speedometer;
	}

	//SignalTimer& SignalProvider::signal_timer()
	//{
	//	return _timer;
	//}

	//SignalInterpolator& SignalProvider::signal_interpolator()
	//{
	//	return _interpolator;
	//}



	const SignalSource& SignalProvider::signal_source() const
	{
		return _source;
	}

	const SignalCounter & SignalProvider::signal_counter() const
	{
		return _counter;
	}

	const SignalRecorder& SignalProvider::signal_recorder() const
	{
		return _recorder;
	}

	const SignalAssembler& SignalProvider::signal_assembler() const
	{
		return _assembler;
	}

	const SignalSpeedometer& SignalProvider::signal_speedometer() const
	{
		return _speedometer;
	}

	//const SignalTimer& SignalProvider::signal_timer() const
	//{
	//	return _timer;
	//}

	//const SignalInterpolator& SignalProvider::signal_interpolator() const
	//{
	//	return _interpolator;
	//}
}