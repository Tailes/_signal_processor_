#pragma once

namespace Ethernet
{
	enum class DelayerMode
	{
		Counter,
		Timer
	};
}