#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Model\Data\Signal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalSpeedometer.hpp"

#include <algorithm>



namespace Ethernet
{
	SignalSpeedometer::SignalSpeedometer():_duration(0)
	{
	}

	void SignalSpeedometer::open()
	{
	}

	void SignalSpeedometer::prepare(Signal &signal, EthernetSignal &ethernet_signal)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_duration = 0;
	}

	void SignalSpeedometer::apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt)
	{
		std::size_t begin = std::max<std::size_t>(ethernet_signal.tick_count_begin(), ethernet_signal.tick_count_end());
		std::size_t end = std::min<std::size_t>(ethernet_signal.tick_count_begin(), ethernet_signal.tick_count_end());
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_duration = begin - end;
	}

	void SignalSpeedometer::close()
	{
	}

	void SignalSpeedometer::serialize(Trace &target)
	{
	}

	void SignalSpeedometer::bind()
	{
	}

	void SignalSpeedometer::load(const std::wstring &root)
	{
	}

	void SignalSpeedometer::save(const std::wstring &root) const
	{
	}


	std::size_t SignalSpeedometer::duration() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _duration;
	}
}