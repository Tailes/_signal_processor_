#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetOutputPackage.hpp"



namespace Ethernet
{
	OutputPackage::OutputPackage():
		_delays_basis({{0}})
	{
		_strobe_pulse_length = 220;
		_scanning_time_frame = 0;
		_signal_accumulation = 0;
		_scanning_channel = 0;
		_scanning_period = 0;
		_scanning_delay = 0;
		_trigger_steps = 0;
		_trigger_scans = 0;
		_signal_length = 0;
		_scanning_mode = 0;
		_charges_count = 1;
		_trigger_mode = 0;
		_bitmask = 0;
		_marker = 0;
	}
}