#pragma once

#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetDelayLines.hpp"

#include <cstdint>
#include <array>


namespace Ethernet
{
	enum class OutputBits: std::uint16_t
	{
		Power =				0x0001,
		UpdateDelaysBasis = 0x0002
	};
	
	
	
	#pragma pack (push, 1)
	struct OutputPackage
	{
	public:
		OutputPackage();
		
	public:
		std::uint32_t _marker;
		std::uint16_t _bitmask;
		std::uint16_t _scanning_period;
		std::uint32_t _scanning_delay;
		std::uint16_t _scanning_time_frame;
		std::uint8_t _scanning_channel;
		std::uint8_t _scanning_mode;
		std::uint16_t _signal_length;
		std::uint8_t _signal_accumulation;
		std::uint8_t _trigger_mode;
		std::uint8_t _trigger_steps;
		std::uint8_t _trigger_scans;
		std::uint8_t _charges_count;
		std::uint8_t _strobe_pulse_length;
		std::array<uint16_t, DelayTotalLinesCount> _delays_basis;
	};
	#pragma pack (pop)
}