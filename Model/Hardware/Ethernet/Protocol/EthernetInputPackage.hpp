#pragma once
#include <cstdint>
#include <array>


namespace Ethernet
{
	static const std::size_t InputPackageDataLength = 512;


	enum class InputBits: std::uint16_t
	{
		DataPresent = 0x0002,
	};



	#pragma pack (push, 1)
	struct InputPackage
	{
	public:
		struct Header
		{
		public:
			Header();
			
		public:
			std::uint32_t _marker;
			std::uint16_t _bitmask;
			std::uint8_t _scanning_channel;
			std::uint8_t _scanning_mode;
			std::uint32_t _signal_number;
			std::uint16_t _signal_length;
			std::uint16_t _signal_index;
			std::uint8_t _signal_accumulation;
			std::uint8_t _signal_reserved;
			std::uint16_t _adc_state;
			std::uint8_t _battery_voltage;
			std::uint8_t _battery_current;
			std::uint16_t _velocity;
			std::uint16_t _trigger_state;
			std::uint16_t _trigger_queue;
			std::uint32_t _trigger_tick;
		};
		
		typedef std::array<std::uint16_t, InputPackageDataLength> Data;
		
	public:
		InputPackage();
		
	public:
		Header _header;
		Data _data;
	};
	#pragma pack (pop)
}