#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetInputPackage.hpp"

#include <algorithm>



namespace Ethernet
{
	InputPackage::Header::Header()
	{
		_marker = 0;
		_bitmask = 0;
		_velocity = 0;
		_adc_state = 0;
		_trigger_tick = 0;
		_signal_index = 0;
		_trigger_queue = 0;
		_trigger_state = 0;
		_signal_number = 0;
		_signal_length = 0;
		_scanning_mode = 0;
		_battery_voltage = 0;
		_battery_current = 0;
		_signal_reserved = 0;
		_scanning_channel = 0;
		_signal_accumulation = 0;
	}


	InputPackage::InputPackage()
	{
		std::fill(std::begin(_data), std::end(_data), 0);
	}
}