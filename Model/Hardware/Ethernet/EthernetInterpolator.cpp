#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Utility\Registry.hpp"
#include "..\..\..\Model\Data\Slice.hpp"
#include "..\..\..\Model\Data\Signal.hpp"
#include "..\..\..\Model\Data\Interpolator.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSizing.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetInterpolator.hpp"




namespace Ethernet
{
	Interpolator::Interpolator(const Sizing &sizing):
		_enabled(false)//, _sizing(sizing)
	{
		this->resize_buffers(_timesteps, _result, static_cast<std::size_t>(sizing.signal_length()));
	}

	void Interpolator::open()
	{
	}

	void Interpolator::prepare(Slice &slice)
	{

	}

	void Interpolator::apply(Slice &slice, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if (_enabled)
		{
			this->enumerate_steps(_timesteps, slice.timeline().front(), slice.timeline().back() - slice.timeline().front());
			for (auto &signal : slice.batch())
			{
				if (!signal.empty())
				{
					this->interpolate(_result, slice.timeline(), signal, _timesteps);
					this->clone(signal, _result);
				}
			}
			this->clone(slice.timeline(), _timesteps);
		}
	}

	void Interpolator::close()
	{
	}

	void Interpolator::reset()
	{
	}

	void Interpolator::serialize(Trace &target)
	{
	}

	void Interpolator::bind(const Sizing &sizing)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		this->resize_buffers(_timesteps, _result, static_cast<std::size_t>(sizing.signal_length()));
	}

	void Interpolator::load(const std::wstring &root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.read_bool(L"EthernetTimeInterpolationEnabled", _enabled);
	}

	void Interpolator::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_bool(L"EthernetTimeInterpolationEnabled", _enabled);
	}



	void Interpolator::signal_length(SignalLength length)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		this->resize_buffers(_timesteps, _result, static_cast<std::size_t>(length));
	}

	void Interpolator::enable(bool enabled)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_enabled = enabled;
	}





	bool Interpolator::enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _enabled;
	}






	void Interpolator::enumerate_steps(std::vector<real_t> &steps, real_t delay, real_t time_frame) const
	{
		for ( std::size_t i = 0; i < steps.size(); ++i )
			steps.at(i) = delay + time_frame * static_cast<real_t>(i) / static_cast<real_t>(steps.size() - 1);
	}

	void Interpolator::resize_buffers(std::vector<real_t> &result, std::vector<real_t> &scans, std::size_t count) const
	{
		result.resize(count);
		scans.resize(count);
	}

	void Interpolator::interpolate(std::vector<real_t> &result, const std::vector<real_t> &timeline, const std::vector<real_t> &signal, const std::vector<real_t> &steps) const
	{
		::Interpolator(timeline, signal).run(result, steps, &::Interpolator::linear_kernel);
	}

	void Interpolator::clone(std::vector<real_t> &target, const std::vector<real_t> &source) const
	{
		target.assign(source.cbegin(), source.cend());
	}
}