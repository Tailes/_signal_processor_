#pragma once
#include "..\..\..\Model\Types\Real.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\Model\Interfaces\ISerializableSliceFilter.hpp"


#include <vector>
#include <mutex>



class Trace;
class Slice;
class Signal;



namespace Ethernet
{
	class Sizing;
	
	
	
	class Interpolator:
		public ISerializableSliceFilter
	{
		typedef ::Trace Trace;
		typedef ::Signal Signal;
		
	public:
		Interpolator(const Sizing &sizing);
		
		virtual void open();
		virtual void prepare(Slice &slice);
		virtual void apply(Slice &slice, bool &interrupt);
		virtual void close();
		virtual void reset();
		virtual void bind(const Sizing &sizing);
		virtual void serialize(Trace &target);
		virtual void load(const std::wstring &root);
		virtual void save(const std::wstring &root) const;
		
		void enable(bool enabled);
		void signal_length(SignalLength length);
		
		bool enabled() const;
		
	protected:
		//const Sizing &_sizing;
		std::recursive_mutex mutable _guard;
		std::vector<real_t> _timesteps;
		std::vector<real_t> _result;
		bool _enabled;
		
		void enumerate_steps(std::vector<real_t> &steps, real_t delay, real_t time_frame) const;
		void resize_buffers(std::vector<real_t> &steps, std::vector<real_t> &result, std::size_t count) const;
		void interpolate(std::vector<real_t> &result, const std::vector<real_t> &timeline, const std::vector<real_t> &signal, const std::vector<real_t> &steps) const;
		void clone(std::vector<real_t> &target, const std::vector<real_t> &source) const;
	};
}