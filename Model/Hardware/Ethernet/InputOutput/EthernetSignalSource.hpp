#pragma once
#include "..\..\..\..\OS\Network\SocketLibrary.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Interfaces\IEthernetSignalFilter.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetInput.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSocket.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetTracker.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSeparator.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetValidator.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetPositioner.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSocketSettings.hpp"

#include <string>
#include <mutex>


class Trace;
class Signal;


namespace Ethernet
{
	class Timing;
	class Sizing;
	class Signal;
	class Channeling;
	
	
	
	class SignalSource:
		public ISignalFilter
	{
		typedef ::Trace Trace;
		typedef ::Signal Signal;
		typedef ::Ethernet::Signal EthernetSignal;
		
	public:
		SignalSource(const Timing &timing, const Sizing &sizing, const Channeling &channeling);
		
		virtual void open();
		virtual void prepare(Signal &signal, EthernetSignal &ethernet_signal);
		virtual void apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt);
		virtual void close();
		virtual void serialize(Trace &target);
		virtual void bind(const Timing &timing, const Sizing &sizing, const Channeling &channeling);
		virtual void load(const std::wstring &root);
		virtual void save(const std::wstring &root) const;
		
		Input& input();
		Output& output();
		Tracker& tracker();
		Positioner& positioner();
		SocketSettings& settings();
		const Input& input() const;
		const Output& output() const;
		const Tracker& tracker() const;
		const Positioner& positioner() const;
		const SocketSettings& settings() const;
		
	private:
		OS::SocketLibrary _library;
		std::recursive_mutex mutable _guard;
		SocketSettings _settings;
		Positioner _positioner;
		Validator _validator;
		Separator _separator;
		Tracker _tracker;
		Socket _socket;
		Output _output;
		Input _input;
	};
}