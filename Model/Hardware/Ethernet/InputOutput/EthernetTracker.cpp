#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Utility\TypeArithmetics.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetTracker.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetInputPackage.hpp"


#include <algorithm>




namespace Ethernet
{
	Tracker::Tracker()
	{
	}

	bool Tracker::track(const InputPackage &package) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_adc_state_callback(package._header._adc_state);
		_current_callback(package._header._battery_current);
		_voltage_callback(package._header._battery_voltage);
		return true;
	}

	void Tracker::adc_state_callback(const std::function<void(std::uint16_t)> callback)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_adc_state_callback = callback;
	}

	void Tracker::current_callback(const std::function<void(std::uint8_t)> callback)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_current_callback = callback;
	}

	void Tracker::voltage_callback(const std::function<void(std::uint8_t)> callback)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_voltage_callback = callback;
	}
}