#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Utility\TypeArithmetics.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetInput.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSocket.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetTracker.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSeparator.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetValidator.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetPositioner.hpp"


#include <algorithm>




namespace Ethernet
{
	Input::Input()
	{
	}


	void Input::open()
	{
	}

	void Input::prepare(EthernetSignal &ethernet_signal)
	{
	}

	void Input::close()
	{
	}

	void Input::receive(Socket &socket, Separator &separator, Validator &validator, Tracker &tracker, Positioner &positioner, EthernetSignal &signal, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		this->receive(socket);
		if (interrupt |= !this->track(tracker)) return;
		if (interrupt |= !this->position(positioner)) return;
		if (interrupt |= !this->separate(separator)) return;
		if (interrupt |= !this->validate(validator)) return;
		this->apply(signal);
	}



	void Input::serialize(Trace &target) const
	{
	}

	void Input::load(const std::wstring &root)
	{
	}

	void Input::save(const std::wstring &root) const
	{
	}

	void Input::apply(EthernetSignal &signal) const
	{
		std::function<real_t(std::uint16_t)> converter = [](std::uint16_t value) { return static_cast<real_t>(value) / static_cast<real_t>(std::numeric_limits<std::int16_t>::max()) - static_cast<real_t>(1.0f); };
		std::transform(_package._data.begin(), _package._data.end(), signal.begin(), converter);
		signal.clusters_count(_package._header._signal_length);
		signal.cluster_size(1); // Per specific request. This disables sequential averaging. TODO: consider removing the mode.
		signal.channel(_package._header._scanning_channel);
		signal.number(_package._header._signal_number);
		signal.index(_package._header._signal_index);
		signal.mode(_package._header._scanning_mode);
	}


	void Input::receive(Socket &socket)
	{
		socket.receive(_package);
	}

	bool Input::validate(Validator &validator) const
	{
		return validator.validate(_package);
	}

	bool Input::track(Tracker &tracker) const
	{
		return tracker.track(_package);
	}


	bool Input::position(Positioner &positoiner) const
	{
		return positoiner.position(_package);
	}

	bool Input::separate(Separator &separator) const
	{
		return separator.separate(_package);
	}
}