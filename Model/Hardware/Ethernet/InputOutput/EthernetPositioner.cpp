#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Utility\TypeArithmetics.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetInputPackage.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetPositioner.hpp"


#include <algorithm>




namespace Ethernet
{
	Positioner::Positioner()
	{
	}

	bool Positioner::position(const InputPackage &package) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_trigger_state_callback(package._header._trigger_state);
		_queue_length_callback(package._header._trigger_queue);
		_tick_count_callback(package._header._trigger_tick);
		_velocity_callback(package._header._velocity);
		return true;
	}


	void Positioner::trigger_state_callback(const std::function<void(std::uint16_t)> callback)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_trigger_state_callback = callback;
	}

	void Positioner::queue_length_callback(const std::function<void(std::uint16_t)> callback)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_queue_length_callback = callback;
	}

	void Positioner::tick_count_callback(const std::function<void(std::uint32_t)> callback)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_tick_count_callback = callback;
	}

	void Positioner::velocity_callback(const std::function<void(std::uint16_t)> callback)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_velocity_callback = callback;
	}
}