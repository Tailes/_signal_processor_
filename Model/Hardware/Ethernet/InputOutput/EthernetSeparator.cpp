#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetInputPackage.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetOutputPackage.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSeparator.hpp"


namespace Ethernet
{
	bool Separator::separate(const InputPackage &target) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( (target._header._bitmask & static_cast<std::uint16_t>(InputBits::DataPresent)) == 0 ) return false;
		return true;
	}
}