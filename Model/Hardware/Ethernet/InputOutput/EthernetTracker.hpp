#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetReal.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetInputPackage.hpp"

#include <functional>
#include <string>
#include <mutex>




namespace Ethernet
{
	class Tracker
	{
	public:
		Tracker();
		
		bool track(const InputPackage &package) const;
		
		void adc_state_callback(const std::function<void(std::uint16_t)> callback);
		void current_callback(const std::function<void(std::uint8_t)> callback);
		void voltage_callback(const std::function<void(std::uint8_t)> callback);
		
	private:
		std::recursive_mutex mutable _guard;
		std::function<void(std::uint16_t)> _adc_state_callback;
		std::function<void(std::uint8_t)> _current_callback;
		std::function<void(std::uint8_t)> _voltage_callback;
	};
}