#pragma once

#include <mutex>



namespace Ethernet
{
	struct InputPackage;
	struct OutputPackage;



	class Separator
	{
	public:
		bool separate(const InputPackage &target) const;
		
	private:
		std::recursive_mutex mutable _guard;
	};

}