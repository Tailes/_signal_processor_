#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <string>
#include <mutex>
#include <cmath>



namespace Ethernet
{
	class Settings
	{
	public:
		Settings();
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		std::float_t tick_length() const;
		
		void tick_length(std::float_t length);
		
	private:
		std::recursive_mutex mutable _guard;
		std::float_t _tick_length;
	};
}