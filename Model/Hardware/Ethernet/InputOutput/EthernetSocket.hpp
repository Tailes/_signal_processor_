#pragma once
#include "..\..\..\..\OS\Network\Socket.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetInputPackage.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetOutputPackage.hpp"

#include <random>
#include <string>
#include <mutex>


namespace Ethernet
{
	class SocketSettings;

	struct InputPackage;
	struct OutputPackage;



#if !defined(USE_RANDOM_GENERATOR)
	class Socket
	{
	public:
		Socket();
		
		void open(const SocketSettings &settings);
		void close();
		void receive(InputPackage &package);
		void send(const OutputPackage &package);
		void shutdown(int direction);
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
	private:
		std::recursive_mutex mutable _guard;
		OS::Socket _socket;
	};

#else
	class Socket
	{
	public:
		Socket();

		void open(const SocketSettings &settings);
		void close();
		void receive(InputPackage &package);
		void send(const OutputPackage &package);
		void shutdown(int direction);

		void load(const std::wstring &root);
		void save(const std::wstring &root) const;

	private:
		std::recursive_mutex mutable _guard;
		std::default_random_engine _engine;
		OutputPackage _output;
		InputPackage _input;
	};
#endif //#if !defined(USE_RANDOM_GENERATOR)
}