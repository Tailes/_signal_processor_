#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Utility\Registry.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSettings.hpp"



namespace Ethernet
{

	static const std::float_t DefaultTickLength = 10.0f;



	Settings::Settings()
	{
		_tick_length = DefaultTickLength;
	}

	void Settings::load(const std::wstring &root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.read_float(L"TickLength", _tick_length);
	}

	void Settings::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_float(L"TickLength", _tick_length);
	}



	std::float_t Settings::tick_length() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _tick_length;
	}

	void Settings::tick_length(std::float_t length)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_tick_length = length;
	}
}