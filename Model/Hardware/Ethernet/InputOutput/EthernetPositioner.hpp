#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetReal.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetInputPackage.hpp"

#include <functional>
#include <string>
#include <mutex>




class Trace;
class Signal;


namespace Ethernet
{
	class Signal;
	class Socket;
	class Separator;
	class Validator;

	class Positioner
	{
		typedef ::Trace Trace;
		typedef ::Ethernet::Signal EthernetSignal;
		
	public:
		Positioner();
		
		bool position(const InputPackage &package) const;
		
		void trigger_state_callback(const std::function<void(std::uint16_t)> callback);
		void queue_length_callback(const std::function<void(std::uint16_t)> callback);
		void tick_count_callback(const std::function<void(std::uint32_t)> callback);
		void velocity_callback(const std::function<void(std::uint16_t)> callback);
		
	private:
		std::recursive_mutex mutable _guard;
		std::function<void(std::uint16_t)> _trigger_state_callback;
		std::function<void(std::uint16_t)> _queue_length_callback;
		std::function<void(std::uint32_t)> _tick_count_callback;
		std::function<void(std::uint16_t)> _velocity_callback;
	};
}