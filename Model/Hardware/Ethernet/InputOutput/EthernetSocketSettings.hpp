#pragma once
#include <string>
#include <mutex>



namespace Ethernet
{
	class SocketSettings
	{
	public:
		SocketSettings();
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		void linger(bool enable);
		void keep_alive(bool enable);
		void address_reuse(bool enable);
		void nagle_algorithm(bool enable);
		void exclusive_address_use(bool enable);
		
		int address_family() const;
		int type() const;
		int protocol() const;
		int source_port() const;
		int target_port() const;
		int receive_timeout() const;
		int send_timeout() const;
		int input_buffer_size() const;
		int output_buffer_size() const;
		unsigned short linger_delay() const;
		unsigned long source_ip() const;
		unsigned long target_ip() const;
		
		void address_family(int family);
		void type(int type);
		void protocol(int protocol);
		void source_ip(unsigned long ip);
		void target_ip(unsigned long ip);
		void source_port(int port);
		void target_port(int port);
		void receive_timeout(int milliseconds);
		void input_buffer_size(int size);
		void output_buffer_size(int size);
		void send_timeout(int milliseconds);
		void linger_delay(unsigned short delay);
		
		bool lingering() const;
		bool keep_alive() const;
		bool address_reuse() const;
		bool nagle_algorithm() const;
		bool exclusive_address_use() const;
		
	private:
		std::recursive_mutex mutable _guard;
		int _type;
		int _family;
		int _protocol;
		int _send_timeout;
		int _receive_timeout;
		int _input_buffer_size;
		int _output_buffer_size;
		bool _linger;
		bool _keep_alive;
		bool _nagle_enabled;
		bool _address_reuse;
		bool _exclusive_address_use;
		unsigned short _linger_delay;
		unsigned long _target_ip;
		unsigned long _source_ip;
		int _source_port;
		int _target_port;
	};
}