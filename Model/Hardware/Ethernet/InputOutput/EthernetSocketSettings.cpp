#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Utility\Registry.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSocketSettings.hpp"


#include <WinSock2.h>




namespace Ethernet
{
	SocketSettings::SocketSettings()
	{
		//_target_port = 80;
		//_source_ip = ::inet_addr( "194.44.186.88" ); //IP for myself ( ADC wants it )
		//_target_ip = ::inet_addr( "194.44.186.87" ); //IP for ADC
		_family = AF_INET;
		_type = SOCK_DGRAM;
		_protocol = IPPROTO_UDP;
		_source_port = 0;
		_target_port = 1480;
		_source_ip = ::inet_addr("192.168.0.1"); //IP for myself ( ADC wants it )
		_target_ip = ::inet_addr("192.168.0.100"); //IP for ADC
		_receive_timeout = 1000;
		_send_timeout = 1000;
		_input_buffer_size = 1500;
		_output_buffer_size = 1500;
		_linger = FALSE;
		_linger_delay = 0;
		_keep_alive = FALSE;
		_nagle_enabled = true;
		_address_reuse = false;
		_exclusive_address_use = true;
	}

	void SocketSettings::load(const std::wstring &root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.read_integer(L"Family", _family);
		registry.read_integer(L"Type", _type);
		registry.read_integer(L"Protocol", _protocol);
		registry.read_integer(L"ReceiveTimeout", _receive_timeout);
		registry.read_integer(L"SendTimeout", _send_timeout);
		registry.read_integer(L"TargetIp", _target_ip);
		registry.read_integer(L"SourceIp", _source_ip);
		registry.read_integer(L"SourcePort", _source_port);
		registry.read_integer(L"TargetPort", _target_port);
	}

	void SocketSettings::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"Family", _family);
		registry.write_integer(L"Type", _type);
		registry.write_integer(L"Protocol", _protocol);
		registry.write_integer(L"ReceiveTimeout", _receive_timeout);
		registry.write_integer(L"SendTimeout", _send_timeout);
		registry.write_integer(L"TargetIp", _target_ip);
		registry.write_integer(L"SourceIp", _source_ip);
		registry.write_integer(L"SourcePort", _source_port);
		registry.write_integer(L"TargetPort", _target_port);
	}

	void SocketSettings::keep_alive(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_keep_alive = enable;
	}

	void SocketSettings::linger(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_linger = enable;
	}

	void SocketSettings::nagle_algorithm(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_nagle_enabled = enable;
	}

	void SocketSettings::exclusive_address_use(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_exclusive_address_use = enable;
	}

	void SocketSettings::address_reuse(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_address_reuse = enable;
	}

	int SocketSettings::address_family() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _family;
	}

	int SocketSettings::type() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _type;
	}

	int SocketSettings::protocol() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _protocol;
	}

	int SocketSettings::source_port() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _source_port;
	}

	int SocketSettings::target_port() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _target_port;
	}

	unsigned long SocketSettings::source_ip() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _source_ip;
	}

	unsigned long SocketSettings::target_ip() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _target_ip;
	}

	int SocketSettings::receive_timeout() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _receive_timeout;
	}

	int SocketSettings::send_timeout() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _send_timeout;
	}

	int SocketSettings::input_buffer_size() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _input_buffer_size;
	}

	int SocketSettings::output_buffer_size() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _output_buffer_size;
	}

	unsigned short SocketSettings::linger_delay() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _linger_delay;
	}

	void SocketSettings::address_family(int family)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_family = family;
	}

	void SocketSettings::type(int type)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_type = type;
	}

	void SocketSettings::protocol(int protocol)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_protocol = protocol;
	}

	void SocketSettings::receive_timeout(int milliseconds)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_receive_timeout = milliseconds;
	}

	void SocketSettings::send_timeout(int milliseconds)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_send_timeout = milliseconds;
	}

	void SocketSettings::source_port(int port)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_source_port = port;
	}

	void SocketSettings::target_port(int port)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_target_port = port;
	}

	void SocketSettings::source_ip(unsigned long ip)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_source_ip = ip;
	}

	void SocketSettings::target_ip(unsigned long ip)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_target_ip = ip;
	}

	void SocketSettings::input_buffer_size(int size)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_input_buffer_size = size;
	}

	void SocketSettings::output_buffer_size(int size)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_output_buffer_size = size;
	}

	void SocketSettings::linger_delay(unsigned short delay)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_linger_delay = delay;
	}

	bool SocketSettings::keep_alive() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _keep_alive;
	}

	bool SocketSettings::lingering() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _linger;
	}

	bool SocketSettings::nagle_algorithm() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _nagle_enabled;
	}

	bool SocketSettings::exclusive_address_use() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _exclusive_address_use;
	}

	bool SocketSettings::address_reuse() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _address_reuse;
	}
}