#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Utility\TypeArithmetics.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSocket.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSocketSettings.hpp"

#include <WinSock2.h>
#if defined(USE_RANDOM_GENERATOR)
#include <iterator>
#include <cassert>
#include <fstream>
#include <iosfwd>
#include <random>
#endif

namespace Ethernet
{
#if !defined(USE_RANDOM_GENERATOR)
	Socket::Socket()
	{
	}

	void Socket::open(const SocketSettings &settings)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		sockaddr_in source_address = {};
		source_address.sin_family = settings.address_family();
		source_address.sin_port = ::htons(settings.source_port());
		source_address.sin_addr.S_un.S_addr = settings.source_ip();
		sockaddr_in target_address = {};
		target_address.sin_family = settings.address_family();
		target_address.sin_port = ::htons(settings.target_port());
		target_address.sin_addr.S_un.S_addr = settings.target_ip();
		int send_timeout = settings.send_timeout();
		int receive_timeout = settings.receive_timeout();
		int input_buffer_size = settings.input_buffer_size();
		int output_buffer_size = settings.output_buffer_size();
		BOOL exclusive = settings.exclusive_address_use() ? TRUE : FALSE;
		BOOL reuse = settings.address_reuse() ? TRUE : FALSE;
		OS::Socket handle(settings.address_family(), settings.type(), settings.protocol());
		handle.configure(SOL_SOCKET, SO_EXCLUSIVEADDRUSE, &exclusive, sizeof(exclusive));
		handle.configure(SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));
		handle.configure(SOL_SOCKET, SO_SNDBUF, &output_buffer_size, sizeof(output_buffer_size));
		handle.configure(SOL_SOCKET, SO_RCVBUF, &input_buffer_size, sizeof(input_buffer_size));
		handle.configure(SOL_SOCKET, SO_SNDTIMEO, &send_timeout, sizeof(send_timeout));
		handle.configure(SOL_SOCKET, SO_RCVTIMEO, &receive_timeout, sizeof(receive_timeout));
		handle.bind(reinterpret_cast<const sockaddr*>(&source_address), sizeof(source_address));
		handle.connect(reinterpret_cast<const sockaddr*>(&target_address), sizeof(target_address));
		handle.swap(_socket);
	}

	void Socket::close()
	{
		OS::Socket handle;
		std::lock_guard<std::recursive_mutex> lock(_guard);
		handle.swap(_socket);
		handle.shutdown(SD_BOTH);
		handle.close();
		handle.swap(_socket);
	}

	void Socket::receive(InputPackage &package)
	{
		OS::Socket handle;
		std::lock_guard<std::recursive_mutex> lock(_guard);
		handle.swap(_socket);
		handle.receive(&package, sizeof(package));
		handle.swap(_socket);
	}

	void Socket::send(const OutputPackage &package)
	{
		OS::Socket handle;
		std::lock_guard<std::recursive_mutex> lock(_guard);
		handle.swap(_socket);
		handle.send(&package, sizeof(package));
		handle.swap(_socket);
	}

	void Socket::shutdown(int direction)
	{
		OS::Socket handle;
		std::lock_guard<std::recursive_mutex> lock(_guard);
		handle.swap(_socket);
		handle.shutdown(direction);
		handle.swap(_socket);
	}


	void Socket::load(const std::wstring &root)
	{
	}

	void Socket::save(const std::wstring &root) const
	{
	}
#else //#if !defined(USE_RANDOM_GENERATOR)
	//#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSocketSineWave.hpp"


	std::fstream output;
	std::fstream input;

	Socket::Socket()
	{
	}

	void Socket::open(const SocketSettings &settings)
	{
		std::srand(0);
		output.open(L"socket_output", std::ios_base::out | std::ios_base::binary);
		input.open(L"socket_input", std::ios_base::in | std::ios_base::binary);
	}

	void Socket::close()
	{
		output.close();
		input.close();
	}

	void Socket::receive(InputPackage &package)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if (input.is_open())
		{
			if (!input.eof())
			{
				_input._header._marker = _output._marker;
				auto dummy = *std::istream_iterator<decltype(_output._scanning_delay)>(input);
				_input._header._bitmask = *std::istream_iterator<decltype(package._header._bitmask)>(input);
				_input._header._scanning_channel = *std::istream_iterator<decltype(package._header._scanning_channel)>(input);
				_input._header._scanning_mode = *std::istream_iterator<decltype(package._header._scanning_mode)>(input);
				_input._header._signal_number = *std::istream_iterator<decltype(package._header._signal_number)>(input);
				_input._header._signal_length = *std::istream_iterator<decltype(package._header._signal_length)>(input);
				_input._header._signal_index = *std::istream_iterator<decltype(package._header._signal_index)>(input);
				_input._header._signal_accumulation = *std::istream_iterator<decltype(package._header._signal_accumulation)>(input);
				_input._header._adc_state = *std::istream_iterator<decltype(package._header._adc_state)>(input);
				_input._header._battery_voltage = *std::istream_iterator<decltype(package._header._battery_voltage)>(input);
				_input._header._battery_current = *std::istream_iterator<decltype(package._header._battery_current)>(input);
				_input._header._velocity = *std::istream_iterator<decltype(package._header._velocity)>(input);
				_input._header._trigger_state = *std::istream_iterator<decltype(package._header._trigger_state)>(input);
				_input._header._trigger_queue = *std::istream_iterator<decltype(package._header._trigger_queue)>(input);
				_input._header._trigger_tick = *std::istream_iterator<decltype(package._header._trigger_tick)>(input);
				std::copy_n(std::istream_iterator<decltype(_input._data)::value_type>(input), _input._data.size(), std::begin(_input._data));
			}
			package = _input;
		}
		else
		{
			if (_output._signal_length >> 9 <= _input._header._signal_index)
				_input._header._signal_index = 0;
			if (_input._header._signal_index == 0)
				_input._header._signal_number++;
			for (std::size_t i = 0; i < 512; ++i)
				//_input._data[i] = 65535 / 4;
				//_input._data[i] = Ethernet::SineWave[(i * _output._scanning_time_frame) % _countof(Ethernet::SineWave)];
				//_input._data[i] = 0xFFFF & static_cast<int>((sin(3.1415926 * (i + _input._header._signal_index * 512) * _output._scanning_time_frame / 512.f + _output._scanning_delay/51.2) + 1.0)*32767 * (_input._header._scanning_channel ? 1. : 0.33));
				//_input._data[i] = 0xFFFF & std::uniform_int_distribution<>{0, 65535}(_engine);
				_input._data[i] = 0xFFFF & (std::uint16_t)(128 * 256 * (1.0f + 65535.f / 65536.f*(float)std::sin(3.1415926 * 5 * i / (double)512)));
			
			_input._header._bitmask = 1 << 1;// (::rand() < 20000 ? 1 : 0) << 1;
			
			//_input._header._battery_current = static_cast<std::uint8_t>(0xFF & 256*::rand()/RAND_MAX);
			//_input._header._battery_voltage = static_cast<std::uint8_t>(0xFF & 256*::rand()/RAND_MAX);
			
			//_input._header._trigger_queue = static_cast<std::uint8_t>(1.001*::rand()/RAND_MAX);
			
			//_input._header._trigger_tick = static_cast<std::uint32_t>(_input._header._signal_number);
			//Sleep(2000);
			package = _input;
			//reinterpret_cast<std::uint8_t*>(&package._header)[sizeof(EthernetInputPackage::Header) * ::rand()/RAND_MAX] = 255*::rand()/RAND_MAX;
			_input._header._signal_index++;
			//if ( (float)::rand() / RAND_MAX < 0.0002 )
			//	throw std::runtime_error("Some Receive Error");
			*std::ostream_iterator<decltype(_output._scanning_delay)>(output, " ") = _output._scanning_delay;
			*std::ostream_iterator<decltype(package._header._bitmask)>(output, " ") = package._header._bitmask;
			*std::ostream_iterator<decltype(package._header._scanning_channel)>(output, " ") = package._header._scanning_channel;
			*std::ostream_iterator<decltype(package._header._scanning_mode)>(output, " ") = package._header._scanning_mode;
			*std::ostream_iterator<decltype(package._header._signal_number)>(output, " ") = package._header._signal_number;
			*std::ostream_iterator<decltype(package._header._signal_length)>(output, " ") = package._header._signal_length;
			*std::ostream_iterator<decltype(package._header._signal_index)>(output, " ") = package._header._signal_index;
			*std::ostream_iterator<decltype(package._header._signal_accumulation)>(output, " ") = package._header._signal_accumulation;
			*std::ostream_iterator<decltype(package._header._adc_state)>(output, " ") = package._header._adc_state;
			*std::ostream_iterator<decltype(package._header._battery_voltage)>(output, " ") = package._header._battery_voltage;
			*std::ostream_iterator<decltype(package._header._battery_current)>(output, " ") = package._header._battery_current;
			*std::ostream_iterator<decltype(package._header._velocity)>(output, " ") = package._header._velocity;
			*std::ostream_iterator<decltype(package._header._trigger_state)>(output, " ") = package._header._trigger_state;
			*std::ostream_iterator<decltype(package._header._trigger_queue)>(output, " ") = package._header._trigger_queue;
			*std::ostream_iterator<decltype(package._header._trigger_tick)>(output, " ") = package._header._trigger_tick;
			std::copy_n(std::cbegin(package._data), package._data.size(), std::ostream_iterator<decltype(package._data)::value_type>(output, " "));
		}
	}

	void Socket::send(const OutputPackage &package)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		//if ( (float)::rand() / RAND_MAX < 0.2 )
		//	throw std::runtime_error("Some Send Error");
		_input._header._marker = package._marker;
		_input._header._signal_length = package._signal_length;
		_input._header._signal_accumulation = package._signal_accumulation;
		_input._header._scanning_mode = package._scanning_mode;
		_input._header._scanning_channel = package._scanning_channel;
		//static decltype(package._scanning_delay) stdelay = package._scanning_delay;
		//assert(stdelay == package._scanning_delay || _input._header._signal_index != 1);
		//stdelay = package._scanning_delay;
		//assert(_input._header._signal_index != 1);
		_output = package;
	}

	void Socket::shutdown(int direction)
	{
	}


	void Socket::load(const std::wstring &root)
	{
	}

	void Socket::save(const std::wstring &root) const
	{
	}
#endif //#if !defined(USE_RANDOM_GENERATOR)
}