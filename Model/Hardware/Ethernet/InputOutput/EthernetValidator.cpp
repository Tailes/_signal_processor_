#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetInputPackage.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetOutputPackage.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetValidator.hpp"



namespace Ethernet
{
	Validator::Validator()
	{
		_scanning_channel = 0;
		_signal_length = 0;
		_scanning_mode = 0;
		_accumulation = 0;
		_marker = 0;
	}

	void Validator::prepare(const OutputPackage &source)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_scanning_channel = source._scanning_channel;
		_signal_length = source._signal_length;
		_scanning_mode = source._scanning_mode;
		_accumulation = source._signal_accumulation;
		_marker = source._marker;
	}

	bool Validator::validate(const InputPackage &target) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( _marker != target._header._marker ) return false;
		if ( _scanning_channel != target._header._scanning_channel ) return false;
		if ( _signal_length != target._header._signal_length ) return false;
		if ( _scanning_mode != target._header._scanning_mode ) return false;
		if ( _accumulation != target._header._signal_accumulation ) return false;
		if ( _accumulation*_signal_length / InputPackageDataLength <= target._header._signal_index ) return false;
		return true;
	}
}