#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetDelayLines.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetOutputPackage.hpp"

#include <string>
#include <mutex>
#include <array>
#include <map>



class Trace;
class Signal;



namespace Ethernet
{
	class Timing;
	class Sizing;
	class Signal;
	class Socket;
	class Validator;
	class Channeling;
	
	
	
	class Output
	{
		typedef ::Trace Trace;
		typedef ::Signal Signal;
		typedef ::Ethernet::Signal EthernetSignal;
		
	public:
		Output(const Timing &timing, const Sizing &sizing, const Channeling &channeling);
		
		void open();
		void prepare(Signal &signal);
		void prepare(EthernetSignal &ethernet_signal);
		void close();
		void bind(const Timing &timing, const Sizing &sizing, const Channeling &channeling);
		void send(Socket &socket, Validator &validator);
		void send(Socket &socket);
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		void serialize(Trace &target) const;
		
		void prepare(Validator &validator) const;
		
		Mode scanning_mode() const;
		Channel scanning_channel() const;
		TriggerMode trigger_mode() const;
		std::size_t values_count() const;
		std::uint8_t charges_count() const;
		std::uint8_t scans_per_trigger() const;
		std::uint8_t steps_per_trigger() const;
		std::uint8_t strobe_pulse_length() const;
		std::uint8_t homogenous_accumulation() const;
		std::uint16_t probing_period() const;
		
		void scanning_mode(Mode mode);
		void trigger_mode(TriggerMode mode);
		void signal_length(SignalLength length);
		void scanning_delay(std::uint32_t delay);
		void scanning_channel(Channel channel);
		void time_frame_single(TimeFrameSingle frame);
		void time_frame_double(TimeFrameDouble frame);
		void time_frame_octuple(TimeFrameOctuple frame);
		void time_frame_quadruple(TimeFrameQuadruple frame);
		void time_frame_sedecuple(TimeFrameSedecuple frame);
		void charges_count(std::uint8_t count);
		void probing_period(std::uint16_t period);
		void scans_per_trigger(std::uint8_t scans);
		void steps_per_trigger(std::uint8_t steps);
		void strobe_pulse_length(std::uint8_t level);
		void homogenous_accumulation(std::uint8_t count);
		void delays_basis(const std::array<uint16_t, DelayTotalLinesCount> &basis);
		void power_on(bool status);
		
		bool modified() const;
		
	private:
		std::recursive_mutex mutable _guard;
		OutputPackage _package;
		bool _modified;
	};
}