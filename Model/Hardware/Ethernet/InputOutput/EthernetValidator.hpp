#pragma once

#include <mutex>
#include <cstdint>



namespace Ethernet
{
	struct InputPackage;
	struct OutputPackage;



	class Validator
	{
	public:
		Validator();
		
		void prepare(const OutputPackage &source);
		
		bool validate(const InputPackage &target) const;
		
	private:
		std::recursive_mutex mutable _guard;
		std::uint32_t _marker;
		std::uint16_t _scanning_channel;
		std::uint16_t _signal_length;
		std::uint16_t _scanning_mode;
		std::uint16_t _accumulation;
	};
}