#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetReal.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetInputPackage.hpp"

#include <string>
#include <mutex>




class Trace;
class Signal;


namespace Ethernet
{
	class Signal;
	class Socket;
	class Tracker;
	class Separator;
	class Validator;
	class Positioner;
	
	class Input
	{
		typedef ::Trace Trace;
		typedef ::Ethernet::Signal EthernetSignal;
		
	public:
		Input();
		
		void open();
		void close();
		void prepare(EthernetSignal &ethernet_signal);
		void receive(Socket &socket, Separator &separator, Validator &validator, Tracker &tracker, Positioner &positioner, EthernetSignal &signal, bool &interrupt);
		void serialize(Trace &target) const;
		
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
	private:
		std::recursive_mutex mutable _guard;
		InputPackage _package;
		
		bool position(Positioner &positoiner) const;
		bool separate(Separator &separator) const;
		bool validate(Validator &validator) const;
		bool track(Tracker &tracker) const;
		void receive(Socket &socket);
		void apply(EthernetSignal &signal) const;
	};
}