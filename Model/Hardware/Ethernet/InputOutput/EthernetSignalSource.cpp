#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Utility\Registry.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSignalSource.hpp"




namespace Ethernet
{
	SignalSource::SignalSource(const Timing &timing, const Sizing &sizing, const Channeling &channeling):
		_output(timing, sizing, channeling)
	{
	}

	void SignalSource::open()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_input.open();
		_output.open();
		_socket.open(_settings);
	}

	void SignalSource::prepare(Signal &signal, EthernetSignal &ethernet_signal)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_input.prepare(ethernet_signal);
		_output.prepare(ethernet_signal);
		_output.prepare(signal);
	}

	void SignalSource::apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_output.send(_socket, _validator);
		_input.receive(_socket, _separator, _validator, _tracker, _positioner, ethernet_signal, interrupt);
	}

	void SignalSource::close()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_output.power_on(false);
		_output.send(_socket);
		_socket.close();
		_output.close();
		_input.close();
	}

	void SignalSource::serialize(Trace &target)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_input.serialize(target);
		_output.serialize(target);
	}

	void SignalSource::bind(const Timing &timing, const Sizing &sizing, const Channeling &channeling)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_output.bind(timing, sizing, channeling);
	}

	void SignalSource::load(const std::wstring &root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_socket.load(root + L"Socket");
		_output.load(root);
		_input.load(root);
	}

	void SignalSource::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_socket.save(root + L"Socket");
		_output.save(root);
		_input.save(root);
	}



	SocketSettings& SignalSource::settings()
	{
		return _settings;
	}

	Input& SignalSource::input()
	{
		return _input;
	}

	Tracker& SignalSource::tracker()
	{
		return _tracker;
	}

	Positioner & SignalSource::positioner()
	{
		return _positioner;
	}

	Output& SignalSource::output()
	{
		return _output;
	}

	const SocketSettings& SignalSource::settings() const
	{
		return _settings;
	}

	const Input& SignalSource::input() const
	{
		return _input;
	}

	const Tracker& SignalSource::tracker() const
	{
		return _tracker;
	}

	const Positioner & SignalSource::positioner() const
	{
		return _positioner;
	}

	const Output& SignalSource::output() const
	{
		return _output;
	}
}