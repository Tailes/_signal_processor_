#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Utility\Registry.hpp"
#include "..\..\..\..\Utility\TypeArithmetics.hpp"
#include "..\..\..\..\Model\Data\Trace.hpp"
#include "..\..\..\..\Model\Data\Signal.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetTiming.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetSizing.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetChanneling.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetInputPackage.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Protocol\EthernetOutputPackage.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetInput.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSocket.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetValidator.hpp"





namespace Ethernet
{
	static const std::uint16_t DefaultProbingPeriod = 100;
	static const std::uint16_t DefaultGeneratorFrequency = 100;



	Output::Output(const Timing &timing, const Sizing &sizing, const Channeling &channeling)
	{
		_package._marker = utility::make_dword('S', 'P', 'E', 'X');
		_package._bitmask = static_cast<std::uint16_t>(OutputBits::UpdateDelaysBasis);
		_package._scanning_period = static_cast<std::uint16_t>(DefaultProbingPeriod);
		_package._scanning_time_frame = timing.step(); //* 5000 picoseconds | *10000 picoseconds
		_package._signal_length = static_cast<std::uint16_t>(sizing.signal_length()); //*512 points
		_package._signal_accumulation = 1;
		_package._trigger_scans = 0;
		_package._charges_count = 1;
		_package._strobe_pulse_length = 220;
		_package._scanning_channel = static_cast<std::uint8_t>(channeling.channel());
		_package._scanning_mode = static_cast<std::uint8_t>(channeling.mode());
		_package._delays_basis = timing.delay_basis();
		_modified = true; //must send delays!
	}

	void Output::open()
	{
	}

	void Output::prepare(Signal &signal)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		signal.resize(_package._signal_length, Signal::value_type());
	}

	void Output::prepare(EthernetSignal &ethernet_signal)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_package._bitmask |= static_cast<std::uint16_t>(OutputBits::Power);
		_modified = true;
	}

	void Output::send(Socket &socket, Validator &validator)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( _modified )
		{
			socket.send(_package);
			validator.prepare(_package);
			_package._bitmask &= ~static_cast<std::uint16_t>(OutputBits::UpdateDelaysBasis);
			_modified = false;
		}
	}

	void Output::send(Socket &socket)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		socket.send(_package);
		_package._bitmask &= ~static_cast<std::uint16_t>(OutputBits::UpdateDelaysBasis);
		_modified = false;
	}

	void Output::close()
	{
	}

	void Output::serialize(Trace &target) const
	{
	}

	void Output::bind(const Timing &timing, const Sizing &sizing, const Channeling &channeling)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_package._bitmask |= static_cast<std::uint16_t>(OutputBits::UpdateDelaysBasis);
		_package._delays_basis = timing.delay_basis();
		_package._scanning_delay = timing.scanning_delay();
		_package._scanning_time_frame = timing.step();
		_package._signal_length = static_cast<std::uint16_t>(sizing.signal_length());
		_package._scanning_channel = static_cast<std::uint8_t>(channeling.channel());
		_package._scanning_mode = static_cast<std::uint8_t>(channeling.mode());
		_package._delays_basis = timing.delay_basis();
		_modified = true;
	}

	void Output::load(const std::wstring &root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.read_integer(L"StrobePulseLength", _package._strobe_pulse_length);
		//registry.read_integer(L"Algorithm", _package._scanning_mode);
		registry.read_integer(L"ProbingPeriod", _package._scanning_period);
		//registry.read_integer(L"TimeFrame", _package._scanning_time_frame);
		registry.read_integer(L"ScansPerTrigger", _package._trigger_scans);
		registry.read_integer(L"StepsPerTrigger", _package._trigger_steps);
		//registry.read_integer(L"Channel", _package._scanning_channel);
		registry.read_integer(L"TriggerMode", _package._trigger_mode);
	}

	void Output::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"StrobePulseLength", _package._strobe_pulse_length);
		//registry.write_integer(L"Algorithm", _package._scanning_mode);
		registry.write_integer(L"ProbingPeriod", _package._scanning_period);
		//registry.write_integer(L"TimeFrame", _package._scanning_time_frame);
		registry.write_integer(L"ScansPerTrigger", _package._trigger_scans);
		registry.write_integer(L"StepsPerTrigger", _package._trigger_steps);
		//registry.write_integer(L"Channel", _package._scanning_channel);
		registry.write_integer(L"TriggerMode", _package._trigger_mode);
	}

	void Output::prepare(Validator &validator) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		validator.prepare(_package);
	}




	std::uint16_t Output::probing_period() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _package._scanning_period;
	}

	Channel Output::scanning_channel() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return static_cast<Channel>(_package._scanning_channel);
	}

	Mode Output::scanning_mode() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return static_cast<Mode>(_package._scanning_mode);
	}

	TriggerMode Output::trigger_mode() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return static_cast<TriggerMode>(_package._trigger_mode);
	}

	// scanning series. every scanning event N scans are issued.
	std::uint8_t Output::scans_per_trigger() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _package._trigger_scans;
	}

	// step of positioneer. every N tick one scanning event is issued.
	std::uint8_t Output::steps_per_trigger() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _package._trigger_steps;
	}

	std::uint8_t Output::strobe_pulse_length() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _package._strobe_pulse_length;
	}

	std::uint8_t Output::homogenous_accumulation() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _package._signal_accumulation;
	}

	std::size_t Output::values_count() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return static_cast<std::size_t>(_package._signal_length) * static_cast<std::size_t>(_package._signal_accumulation);
	}

	std::uint8_t Output::charges_count() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _package._charges_count;
	}



	void Output::time_frame_single(TimeFrameSingle frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._scanning_time_frame != static_cast<std::uint16_t>(frame);
		_package._scanning_time_frame = static_cast<std::uint16_t>(frame);
	}

	void Output::time_frame_double(TimeFrameDouble frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._scanning_time_frame != static_cast<std::uint16_t>(frame);
		_package._scanning_time_frame = static_cast<std::uint16_t>(frame);
	}

	void Output::time_frame_octuple(TimeFrameOctuple frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._scanning_time_frame != static_cast<std::uint16_t>(frame);
		_package._scanning_time_frame = static_cast<std::uint16_t>(frame);
	}

	void Output::time_frame_quadruple(TimeFrameQuadruple frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._scanning_time_frame != static_cast<std::uint16_t>(frame);
		_package._scanning_time_frame = static_cast<std::uint16_t>(frame);
	}

	void Output::time_frame_sedecuple(TimeFrameSedecuple frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._scanning_time_frame != static_cast<std::uint16_t>(frame);
		_package._scanning_time_frame = static_cast<std::uint16_t>(frame);
	}

	void Output::charges_count(std::uint8_t count)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._charges_count != count;
		_package._charges_count = count;
	}

	void Output::probing_period(std::uint16_t period)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._scanning_period != period;
		_package._scanning_period = period;
	}

	void Output::scanning_channel(Channel channel)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._scanning_channel != static_cast<std::uint8_t>(channel);
		_package._scanning_channel = static_cast<std::uint8_t>(channel);
	}

	void Output::scanning_delay(std::uint32_t delay)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._scanning_delay != delay;
		_package._scanning_delay = delay;
	}

	void Output::signal_length(SignalLength length)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._signal_length != static_cast<std::uint16_t>(length);
		_package._signal_length = static_cast<std::uint16_t>(length);
	}

	void Output::scanning_mode(Mode mode)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._scanning_mode != static_cast<std::uint8_t>(mode);
		_package._scanning_mode = static_cast<std::uint8_t>(mode);
	}

	void Output::scans_per_trigger(std::uint8_t scans)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._trigger_scans != scans;
		_package._trigger_scans = scans;
	}

	void Output::steps_per_trigger(std::uint8_t steps)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._trigger_steps != steps;
		_package._trigger_steps = steps;
	}

	void Output::strobe_pulse_length(std::uint8_t level)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._strobe_pulse_length != level;
		_package._strobe_pulse_length = level;
	}

	void Output::homogenous_accumulation(std::uint8_t count)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._signal_accumulation != count;
		_package._signal_accumulation = count;
	}
	
	void Output::delays_basis(const std::array<uint16_t, DelayTotalLinesCount> &basis)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified = _package._delays_basis != basis;
		_package._delays_basis = basis;
		_package._bitmask |= _modified ? static_cast<std::uint16_t>(OutputBits::UpdateDelaysBasis) : 0;
	}



	void Output::power_on(bool status)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified = ((_package._bitmask & static_cast<std::uint16_t>(OutputBits::Power)) != 0) != status;
		_package._bitmask = (_package._bitmask ^ static_cast<std::uint16_t>(OutputBits::Power) | (status ? static_cast<std::uint16_t>(OutputBits::Power) : 0x0000));
	}

	void Output::trigger_mode(TriggerMode mode)
	{
 		std::lock_guard<std::recursive_mutex> lock(_guard);
		_modified |= _package._trigger_mode != static_cast<std::uint16_t>(mode);
		_package._trigger_mode = static_cast<std::uint8_t>(mode);
	}

	bool Output::modified() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _modified;
	}
}