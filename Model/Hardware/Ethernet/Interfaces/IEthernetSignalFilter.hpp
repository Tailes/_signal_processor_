#pragma once
#include <string>



class Trace;
class Signal;

namespace Ethernet
{
	class Signal;
	
	
	
	struct ISignalFilter
	{
		virtual void open() = 0;
		virtual void prepare(::Signal &signal, ::Ethernet::Signal &ethernet_signal) = 0;
		virtual void apply(::Signal &signal, ::Ethernet::Signal &ethernet_signal, bool &interrupt) = 0;
		virtual void close() = 0;
		
		virtual void serialize(::Trace &target) = 0;
		
		virtual void load(const std::wstring &root) = 0;
		virtual void save(const std::wstring &root) const = 0;
	};
}