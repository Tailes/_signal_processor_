#pragma once

#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"


namespace Ethernet
{
	
	struct IScanningAlgorithm
	{
		virtual Channel first(Channel channel) = 0;
		virtual Channel next(Channel channel) = 0;
		
		virtual bool completed(Channel channel) const = 0;
	};
}