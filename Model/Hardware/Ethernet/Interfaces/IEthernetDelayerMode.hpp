#pragma once

#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <string>



namespace Ethernet
{
	class Timer;
	class Timing;
	class Output;


	struct IDelayerMode
	{
		virtual void open() = 0;
		virtual void prepare() = 0;
		virtual void apply(Timer &timer, Timing &timing, Output &output) = 0;
		virtual void advance() = 0;
		virtual void close() = 0;
		virtual void reset() = 0;
		
		virtual void load(const std::wstring &root) = 0;
		virtual void save(const std::wstring &root) const = 0;
		
		virtual void limit(Delay lower, Delay upper) = 0;
		virtual void direction(bool forward) = 0;
		virtual void delta(Delay step) = 0;
	};
}