#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Utility\Registry.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetDataSource.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSocket.hpp"



namespace Ethernet
{
	DataSource::DataSource():
		_channeler(_timing, _sizing),
		_timer(_timing, _sizing),
		_interpolator(_sizing)
	{
	}

	void DataSource::open()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_interpolator.open();
		_channeler.open();
		_delayer.open();
		_timer.open();
	}

	void DataSource::prepare(Slice &slice)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_interpolator.prepare(slice);
		_channeler.prepare(slice);
		_delayer.prepare();
		_timer.prepare(slice);
	}

	void DataSource::apply(Slice &slice, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if (!interrupt) _delayer.apply(_timer, _timing, _channeler.signal_provider().signal_source().output());
		if (!interrupt) _channeler.apply(slice, interrupt);
		if (!interrupt) _timer.apply(slice, interrupt);
		if (!interrupt) _interpolator.apply(slice, interrupt);
		if (!interrupt) _delayer.advance();
	}

	void DataSource::reset()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_interpolator.reset();
		_channeler.reset();
		_delayer.reset();
		_timer.reset();
	}

	void DataSource::close()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_interpolator.close();
		_channeler.close();
		_delayer.close();
		_timer.close();
	}

	void DataSource::serialize(Trace &target)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_interpolator.serialize(target);
		_channeler.serialize(target);
		_timer.serialize(target);
	}

	void DataSource::bind()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_interpolator.bind(_sizing);
		_channeler.bind(_timing, _sizing);
		_timer.bind(_timing, _sizing);
		_delayer.bind();
	}

	void DataSource::load(const std::wstring &root)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_interpolator.load(root);
		_channeler.load(root);
		_settings.load(root);
		_delayer.load(root);
		_timing.load(root);
		_sizing.load(root);
		_timer.load(root);
	}

	void DataSource::save(const std::wstring &root) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_interpolator.save(root);
		_channeler.save(root);
		_settings.save(root);
		_delayer.save(root);
		_sizing.save(root);
		_timing.save(root);
		_timer.save(root);
	}




	void DataSource::delays_basis(const Basis &basis)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channeler.signal_provider().signal_source().output().delays_basis(basis);
		_timing.delay_basis(basis);
		_timer.delay_basis(basis, _timing);
	}

	void DataSource::time_frame_single(TimeFrameSingle frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channeler.signal_provider().signal_source().output().time_frame_single(frame);
		_timing.time_frame_single(frame);
		_timer.time_frame_single(frame, _timing);
	}

	void DataSource::time_frame_double(TimeFrameDouble frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channeler.signal_provider().signal_source().output().time_frame_double(frame);
		_timing.time_frame_double(frame);
		_timer.time_frame_double(frame, _timing);
	}

	void DataSource::time_frame_octuple(TimeFrameOctuple frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channeler.signal_provider().signal_source().output().time_frame_octuple(frame);
		_timing.time_frame_octuple(frame);
		_timer.time_frame_octuple(frame, _timing);
	}

	void DataSource::time_frame_quadruple(TimeFrameQuadruple frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channeler.signal_provider().signal_source().output().time_frame_quadruple(frame);
		_timing.time_frame_quadruple(frame);
		_timer.time_frame_quadruple(frame, _timing);

	}

	void DataSource::time_frame_sedecuple(TimeFrameSedecuple frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channeler.signal_provider().signal_source().output().time_frame_sedecuple(frame);
		_timing.time_frame_sedecuple(frame);
		_timer.time_frame_sedecuple(frame, _timing);
	}

	void DataSource::scanning_delay(Delay delay)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channeler.signal_provider().signal_source().output().scanning_delay(delay);
		_timing.scanning_delay(delay);
		_timer.scanning_delay(delay, _timing);
	}

	void DataSource::signal_length(SignalLength length)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channeler.signal_provider().signal_source().output().signal_length(length);
		_interpolator.signal_length(length);
		_sizing.signal_length(length);
		_timer.signal_length(length, _timing);
	}

	Delay DataSource::scanning_delay() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _timing.scanning_delay();
	}

	std::array<std::uint16_t, DelayTotalLinesCount> DataSource::delays_basis() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _timing.delay_basis();
	}

	SignalLength DataSource::signal_length() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _sizing.signal_length();
	}

	TimeFrameSingle DataSource::time_frame_single() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _timing.time_frame_single();
	}

	TimeFrameDouble DataSource::time_frame_double() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _timing.time_frame_double();
	}

	TimeFrameOctuple DataSource::time_frame_octuple() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _timing.time_frame_octuple();
	}

	TimeFrameQuadruple DataSource::time_frame_quadruple() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _timing.time_frame_quadruple();
	}

	TimeFrameSedecuple DataSource::time_frame_sedecuple() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _timing.time_frame_sedecuple();
	}



	Timer& DataSource::timer()
	{
		return _timer;
	}

	Delayer& DataSource::delayer()
	{
		return _delayer;
	}

	Settings& DataSource::settings()
	{
		return _settings;
	}

	Channeler& DataSource::channeler()
	{
		return _channeler;
	}

	Interpolator& DataSource::interpolator()
	{
		return _interpolator;
	}

	const Timer& DataSource::timer() const
	{
		return _timer;
	}

	const Delayer& DataSource::delayer() const
	{
		return _delayer;
	}

	const Settings& DataSource::settings() const
	{
		return _settings;
	}

	const Channeler& DataSource::channeler() const
	{
		return _channeler;
	}

	const Interpolator& DataSource::interpolator() const
	{
		return _interpolator;
	}
}