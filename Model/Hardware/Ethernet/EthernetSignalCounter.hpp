#pragma once
#include "..\..\..\Model\Hardware\Ethernet\Interfaces\IEthernetSignalFilter.hpp"

#include <functional>
#include <vector>
#include <mutex>



namespace Ethernet
{
	class SignalCounter:
		public ISignalFilter
	{
		typedef ::Trace Trace;
		typedef ::Signal Signal;
		typedef ::Ethernet::Signal EthernetSignal;
		
	public:
		SignalCounter();
		
		virtual void open();
		virtual void prepare(Signal &signal, EthernetSignal &ethernet_signal);
		virtual void apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt);
		virtual void close();
		virtual void serialize(Trace &target);
		virtual void bind();
		virtual void load(const std::wstring &root);
		virtual void save(const std::wstring &root) const;
		
		std::size_t number() const;
		
		bool completed() const;
		
	private:
		std::recursive_mutex mutable _guard;
		std::vector<bool> _markers;
		std::size_t _number;
	};
}