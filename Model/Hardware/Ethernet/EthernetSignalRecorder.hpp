#pragma once
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetTrace.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Interfaces\IEthernetSignalFilter.hpp"

#include <string>
#include <array>
#include <mutex>



class Trace;
class Signal;


namespace Ethernet
{
	class Signal;



	class SignalRecorder:
		public ISignalFilter
	{
		typedef ::Trace Trace;
		typedef ::Signal Signal;
		typedef ::Ethernet::Trace EthernetTrace;
		typedef ::Ethernet::Signal EthernetSignal;
		
	public:
		SignalRecorder();
		
		virtual void open();
		virtual void close();
		virtual void prepare(Signal &signal, EthernetSignal &ethernet_signal);
		virtual void apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt);
		virtual void serialize(Trace &target);
		virtual void bind();
		virtual void load(const std::wstring &root);
		virtual void save(const std::wstring &root) const;
		
		void flush(const std::array<std::wstring, static_cast<std::size_t>(Channel::Total)> &paths);
		
		void enable(bool value = true);
		
		bool enabled() const;
		
	private:
		std::recursive_mutex mutable _guard;
		std::array<EthernetTrace, static_cast<std::size_t>(Channel::Total)> _channels;
		bool _enabled;
	};
}