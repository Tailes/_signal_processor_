#include "..\..\..\StdAfx.hpp"
//#include "..\..\..\Model\Data\Slice.hpp"
//#include "..\..\..\Model\Data\Signal.hpp"
#include "..\..\..\Utility\Registry.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetTimer.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetDelayer.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetTiming.hpp"
#include "..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"



namespace Ethernet
{
	Delayer::Delayer():
		_mode(DelayerMode::Counter), _enabled(false), _lower_limit(), _upper_limit(), _delta()
	{
		_delayers.emplace(DelayerMode::Counter, &_counter);
		_delayers.emplace(DelayerMode::Timer, &_timer);
	}

	void Delayer::bind()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::for_each(std::begin(_delayers), std::end(_delayers), [=](auto &record) { record.second->limit(_lower_limit, _upper_limit); });
		std::for_each(std::begin(_delayers), std::end(_delayers), [=](auto &record) { record.second->delta(_delta); });
	}

	void Delayer::open()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::for_each(std::begin(_delayers), std::end(_delayers), [](auto &record) { record.second->open(); });
	}

	void Delayer::prepare()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::for_each(std::begin(_delayers), std::end(_delayers), [](auto &record) { record.second->prepare(); });
	}

	void Delayer::apply(Timer &timer, Timing &timing, Output &output)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if (_enabled)
		{
			_delayers.at(_mode)->apply(timer, timing, output);
		}
	}

	void Delayer::advance()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::for_each(std::begin(_delayers), std::end(_delayers), [](auto &record) { record.second->advance(); });
	}

	void Delayer::close()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::for_each(std::begin(_delayers), std::end(_delayers), [](auto &record) { record.second->close(); });
	}

	void Delayer::reset()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::for_each(std::begin(_delayers), std::end(_delayers), [](auto &record) { record.second->reset(); });
	}

	void Delayer::load(const std::wstring & root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		Delay lower_limit = _lower_limit, upper_limit = _upper_limit, delta = _delta;
		std::underlying_type<DelayerMode>::type mode = static_cast<std::underlying_type<DelayerMode>::type>(_mode);
		registry.read_integer(L"DelayerMode", mode); _mode = static_cast<DelayerMode>(mode);
		registry.read_integer(L"DelayerDelta", delta); _delta = static_cast<Delay>(delta);
		registry.read_integer(L"DelayerLowerLimit", lower_limit); _lower_limit = static_cast<Delay>(lower_limit);
		registry.read_integer(L"DelayerUpperLimit", upper_limit); _upper_limit = static_cast<Delay>(upper_limit);
		std::for_each(std::begin(_delayers), std::end(_delayers), [&](auto &record) { record.second->load(root); });
	}

	void Delayer::save(const std::wstring & root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"DelayerMode", static_cast<std::underlying_type<DelayerMode>::type>(_mode));
		registry.write_integer(L"DelayerDelta", static_cast<Delay>(_delta));
		registry.write_integer(L"DelayerLowerLimit", static_cast<Delay>(_lower_limit));
		registry.write_integer(L"DelayerUpperLimit", static_cast<Delay>(_upper_limit));
		std::for_each(std::begin(_delayers), std::end(_delayers), [&](auto &record) { record.second->save(root); });
	}

	const DelayerModeCounter & Delayer::counter() const
	{
		return _counter;
	}

	const DelayerModeTimer & Delayer::timer() const
	{
		return _timer;
	}

	DelayerMode Delayer::mode() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _mode;
	}

	Delay Delayer::lower_limit() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _lower_limit;
	}

	Delay Delayer::upper_limit() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _upper_limit;
	}

	Delay Delayer::delta() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _delta;
	}

	bool Delayer::enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _enabled;
	}

	DelayerModeCounter & Delayer::counter()
	{
		return _counter;
	}

	DelayerModeTimer & Delayer::timer()
	{
		return _timer;
	}

	void Delayer::enable(bool enabled)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_enabled = enabled;
	}

	void Delayer::limit(Delay lower, Delay upper)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::for_each(std::begin(_delayers), std::end(_delayers), [=](auto &record) { record.second->limit(lower, upper); });
		_lower_limit = lower; _upper_limit = upper;
	}

	void Delayer::delta(Delay delay)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::for_each(std::begin(_delayers), std::end(_delayers), [=](auto &record) { record.second->delta(delay); });
		_delta = delay;
	}

	void Delayer::mode(DelayerMode mode)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_mode = mode;
	}

	void Delayer::direction(bool forward)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::for_each(std::begin(_delayers), std::end(_delayers), [=](auto &record) { record.second->direction(forward); });
	}
}