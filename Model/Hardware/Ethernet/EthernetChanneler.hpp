#pragma once
#include "..\..\..\Model\Interfaces\ISerializableSliceFilter.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalProvider.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetChanneling.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Interfaces\IEthernetScanningAlgorithm.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmSlice.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmActiveAntenna.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmActiveAntennaCrossInput.hpp"

#include <mutex>
#include <map>



namespace Ethernet
{
	class Timing;
	class Sizing;
	
	
	
	
	class ChannelerBase
	{
	protected:
		ChannelerBase() = default;
		
	protected:
		Channeling _channeling;
	};
	
	
	
	
	class Channeler:
		public ISerializableSliceFilter,
		private ChannelerBase
	{
		typedef ::Trace Trace;
		typedef ::Signal Signal;
		typedef ::Ethernet::Signal EthernetSignal;
		
	public:
		Channeler(const Timing &timing, const Sizing &sizing);
		
		virtual void open();
		virtual void prepare(Slice &slice);
		virtual void apply(Slice &slice, bool &interrupt);
		virtual void close();
		virtual void reset();
		virtual void bind(const Timing &timing, const Sizing &sizing);
		virtual void serialize(Trace &target);
		virtual void load(const std::wstring &root);
		virtual void save(const std::wstring &root) const;
		
		void mode(Mode mode);
		void channel(Channel channel);
		
		Mode mode() const;
		Channel channel() const;
		
		SignalProvider& signal_provider();
		ScanningAlgorithmSlice& slice_algorithm();
		
		const SignalProvider& signal_provider() const;
		const ScanningAlgorithmSlice& slice_algorithm() const;
		
	private:
		std::recursive_mutex mutable _guard;
		std::map<Mode, IScanningAlgorithm*> _algorithms;
		ScanningAlgorithmActiveAntennaCrossInput _active_antenna_cross_input_algorithm;
		ScanningAlgorithmActiveAntenna _active_antenna_algorithm;
		ScanningAlgorithmSlice _slice_algorithm;
		SignalProvider _provider;
		EthernetSignal _signal;
		
		bool completed() const;
		
		void advance();
		void apply(Signal &signal, bool &interrupt);
	};
}