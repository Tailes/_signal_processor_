#pragma once
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalCounter.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalRecorder.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalValidator.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalAssembler.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalSpeedometer.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Interfaces\IEthernetSignalFilter.hpp"
#include "..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetSignalSource.hpp"

#include <string>
#include <mutex>
#include <list>


class Trace;
class Slice;
class Signal;



namespace Ethernet
{
	class Timing;
	class Sizing;
	class Channeling;
	
	
	
	class SignalProvider:
		public ISignalFilter
	{
		typedef ::Trace Trace;
		typedef ::Signal Signal;
		typedef ::Ethernet::Signal EthernetSignal;
		
	public:
		SignalProvider(const Timing &timing, const Sizing &sizing, const Channeling &channeling);
		
		virtual void open();
		virtual void prepare(Signal &signal, EthernetSignal &ethernet_signal);
		virtual void apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt);
		virtual void close();
		virtual void serialize(Trace &target);
		virtual void bind(const Timing &timing, const Sizing &sizing, const Channeling &channeling);
		virtual void load(const std::wstring &root);
		virtual void save(const std::wstring &root) const;
		
		SignalSource& signal_source();
		SignalCounter& signal_counter();
		SignalRecorder& signal_recorder();
		SignalAssembler& signal_assembler();
		SignalSpeedometer& signal_speedometer();
		const SignalSource& signal_source() const;
		const SignalCounter& signal_counter() const;
		const SignalRecorder& signal_recorder() const;
		const SignalAssembler& signal_assembler() const;
		const SignalSpeedometer& signal_speedometer() const;
		
	private:
		std::recursive_mutex mutable _guard;
		std::list<ISignalFilter*> _filters;
		SignalSpeedometer _speedometer;
		SignalAssembler _assembler;
		SignalValidator _validator;
		SignalRecorder _recorder;
		SignalCounter _counter;
		SignalSource _source;
	};
}