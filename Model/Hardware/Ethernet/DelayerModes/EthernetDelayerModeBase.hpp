#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Interfaces\IEthernetDelayerMode.hpp"

#include <cstddef>
#include <mutex>



namespace Ethernet
{
	class DelayerModeBase:
		public IDelayerMode
	{
	public:
		DelayerModeBase();
		
		virtual void open() override;
		virtual void prepare() override;
		virtual void apply(Timer &timer, Timing &timing, Output &output) override;
		virtual void close() override;
		virtual void reset() override;
		virtual void advance() override;
		
		virtual void delta(Delay step) override;
		virtual void limit(Delay lower, Delay upper) override;
		virtual void direction(bool forward) override;
		
		Delay wrap_next(Delay delay) const;
		
		Delay delta() const;
		
	private:
		std::recursive_mutex mutable _guard;
		Delay _lower_limit;
		Delay _upper_limit;
		Delay _delta;
	};
}


