#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\DelayerModes\EthernetDelayerModeBase.hpp"

#include <cstddef>
#include <mutex>



namespace Ethernet
{
	class DelayerModeCounter:
		public DelayerModeBase
	{
	public:
		DelayerModeCounter();
		
		virtual void prepare() override;
		virtual void open() override;
		virtual void close() override;
		virtual void apply(Timer &timer, Timing &timing, Output &output) override;
		virtual void advance() override;
		virtual void reset() override;
		
		virtual void load(const std::wstring &root) override;
		virtual void save(const std::wstring &root) const override;
		
		void interval(std::size_t value);
		
		std::size_t interval() const;
		
	private:
		std::recursive_mutex mutable _guard;
		std::size_t _interval;
		std::size_t _counter;
		std::size_t _last;
		bool _advance;
	};
}


