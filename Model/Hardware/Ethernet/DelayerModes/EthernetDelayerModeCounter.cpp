#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Utility\Registry.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTimer.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetTiming.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\DelayerModes\EthernetDelayerModeCounter.hpp"



namespace Ethernet
{
	DelayerModeCounter::DelayerModeCounter():
		_interval(), _counter(), _last(), _advance(false)
	{
	}

	void DelayerModeCounter::prepare()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_advance = false;
		_counter = 0;
		_last = 0;
	}

	void DelayerModeCounter::open()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_advance = false;
	}

	void DelayerModeCounter::close()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_advance = false;
	}

	void DelayerModeCounter::apply(Timer &timer, Timing &timing, Output &output)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if (_advance && !(++_counter < _last + _interval))
		{
			Delay wrapped = this->wrap_next(timing.scanning_delay());
			output.scanning_delay(wrapped);
			timing.scanning_delay(wrapped);
			timer.scanning_delay(wrapped, timing);
			_advance = false;
			_last = _counter;
		}
	}

	void DelayerModeCounter::advance()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_advance = true;
	}

	void DelayerModeCounter::reset()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_advance = false;
		_counter = 0;
		_last = 0;
	}

	void DelayerModeCounter::load(const std::wstring & root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::size_t interval = _interval;
		registry.read_integer(L"DelayerModeCounterInterval", interval); _interval = static_cast<std::size_t>(interval);
	}

	void DelayerModeCounter::save(const std::wstring & root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"DelayerModeCounterInterval", _interval);
	}

	void DelayerModeCounter::interval(std::size_t value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_interval = value;
	}

	std::size_t DelayerModeCounter::interval() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _interval;
	}
}
