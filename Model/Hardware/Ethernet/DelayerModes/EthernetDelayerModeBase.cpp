#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTimer.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetTiming.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\DelayerModes\EthernetDelayerModeBase.hpp"



namespace Ethernet
{
	DelayerModeBase::DelayerModeBase():
		_lower_limit(), _delta(), _upper_limit()
	{
	}

	void DelayerModeBase::open()
	{
	}

	void DelayerModeBase::prepare()
	{
	}

	void DelayerModeBase::apply(Timer &timer, Timing &timing, Output &output)
	{
	}
	
	void DelayerModeBase::close()
	{
	}

	void DelayerModeBase::reset()
	{
	}

	void DelayerModeBase::advance()
	{
	}

	void DelayerModeBase::direction(bool forward)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_delta = (forward ? std::abs(_delta) : -std::abs(_delta));
	}

	Delay DelayerModeBase::wrap_next(Delay delay) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		Delay next = delay + _delta;
		if (!(_lower_limit == _upper_limit))
		{
			while (next < _lower_limit) next += _upper_limit - _lower_limit;
			while (_upper_limit < next) next -= _upper_limit - _lower_limit;
		}
		return next;
	}

	void DelayerModeBase::delta(Delay delay)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_delta = delay;
	}

	void DelayerModeBase::limit(Delay lower, Delay upper)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_lower_limit = lower;
		_upper_limit = upper;
	}

	Delay DelayerModeBase::delta() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _delta;
	}
}
