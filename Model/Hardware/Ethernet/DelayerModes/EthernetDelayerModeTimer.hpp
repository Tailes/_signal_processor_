#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\DelayerModes\EthernetDelayerModeBase.hpp"

#include <cstddef>
#include <chrono>
#include <mutex>



namespace Ethernet
{
	class DelayerModeTimer:
		public DelayerModeBase
	{
	public:
		DelayerModeTimer();
		
		virtual void prepare() override;
		virtual void open() override;
		virtual void close() override;
		virtual void apply(Timer &timer, Timing &timing, Output &output) override;
		virtual void advance() override;
		virtual void reset() override;
		
		virtual void load(const std::wstring &root) override;
		virtual void save(const std::wstring &root) const override;
		
		void interval(std::chrono::milliseconds interval);
		
		std::chrono::milliseconds interval() const;
		
	private:
		std::recursive_mutex mutable _guard;
		std::chrono::high_resolution_clock::time_point _last;
		std::chrono::milliseconds _interval;
		bool _advance;
	};
}


