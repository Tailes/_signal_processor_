#include "..\..\..\..\StdAfx.hpp"
#include "..\..\..\..\Utility\Registry.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTimer.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Data\EthernetTiming.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\DelayerModes\EthernetDelayerModeTimer.hpp"



namespace Ethernet
{
	DelayerModeTimer::DelayerModeTimer():
		_interval(0), _last(), _advance(false)
	{
	}

	void DelayerModeTimer::prepare()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_last = std::chrono::high_resolution_clock::now();
		_advance = false;
	}

	void DelayerModeTimer::open()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_advance = false;
	}

	void DelayerModeTimer::close()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_advance = false;
	}

	void DelayerModeTimer::apply(Timer &timer, Timing &timing, Output &output)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if (_advance && !(std::chrono::high_resolution_clock::now() < _last + _interval))
		{
			Delay wrapped = this->wrap_next(timing.scanning_delay());
			output.scanning_delay(wrapped);
			timing.scanning_delay(wrapped);
			timer.scanning_delay(wrapped, timing);
			_last = std::chrono::high_resolution_clock::now();
			_advance = false;
		}
	}

	void DelayerModeTimer::advance()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_advance = true;
	}

	void DelayerModeTimer::reset()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_last = std::chrono::high_resolution_clock::time_point();
		_advance = false;
	}

	void DelayerModeTimer::load(const std::wstring & root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::chrono::milliseconds::rep interval = _interval.count();
		registry.read_integer(L"DelayerModeTimerInterval", interval); _interval = std::chrono::milliseconds(interval);
	}

	void DelayerModeTimer::save(const std::wstring & root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"DelayerModeTimerInterval", _interval.count());
	}

	void DelayerModeTimer::interval(std::chrono::milliseconds value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_interval = value;
	}

	std::chrono::milliseconds DelayerModeTimer::interval() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _interval;
	}
}
