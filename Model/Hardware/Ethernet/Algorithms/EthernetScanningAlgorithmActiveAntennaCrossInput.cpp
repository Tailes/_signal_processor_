#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmActiveAntennaCrossInput.hpp"




namespace Ethernet
{
	Channel ScanningAlgorithmActiveAntennaCrossInput::next(Channel channel)
	{
		return channel == Channel::_1 ? Channel::_2 : Channel::_1;
	}

	Channel ScanningAlgorithmActiveAntennaCrossInput::first(Channel channel)
	{
		return Channel::_1;
	}

	bool ScanningAlgorithmActiveAntennaCrossInput::completed(Channel channel) const
	{
		return channel == Channel::_1;
	}
}