#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\Interfaces\IEthernetScanningAlgorithm.hpp"



namespace Ethernet
{
	struct ScanningAlgorithmActiveAntennaCrossInput:
		public IScanningAlgorithm
	{
	public:
		virtual Channel first(Channel channel) final;
		virtual Channel next(Channel channel) final;
		virtual bool completed(Channel channel) const final;
	};
}