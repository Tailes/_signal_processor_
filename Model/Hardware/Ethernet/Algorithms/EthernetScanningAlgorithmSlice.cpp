#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Utility\Registry.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmSlice.hpp"



namespace Ethernet
{
	ScanningAlgorithmSlice::ScanningAlgorithmSlice():
		_channels{ Channel::_1 }
	{
	}

	Channel ScanningAlgorithmSlice::first(Channel channel)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return *_channels.begin();
	}

	Channel ScanningAlgorithmSlice::next(Channel channel)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		std::set<Channel>::const_iterator it = _channels.find(channel);
		if (it == _channels.end() || ++it == _channels.end())
			return *_channels.begin();
		return *it;
	}

	bool ScanningAlgorithmSlice::completed(Channel channel) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _channels.begin() == _channels.find(channel);
	}

	void ScanningAlgorithmSlice::channels(std::set<Channel> channels)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_channels = channels;
	}

	std::set<Channel> ScanningAlgorithmSlice::channels() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _channels;
	}
}