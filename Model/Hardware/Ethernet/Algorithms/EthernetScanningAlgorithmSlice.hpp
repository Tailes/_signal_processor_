#pragma once
#include "..\..\..\..\Model\Hardware\Ethernet\Interfaces\IEthernetScanningAlgorithm.hpp"

#include <mutex>
#include <set>


namespace Ethernet
{
	class ScanningAlgorithmSlice :
		public IScanningAlgorithm
	{
	public:
		ScanningAlgorithmSlice();
		
		virtual Channel first(Channel channel) final;
		virtual Channel next(Channel channel) final;
		virtual bool completed(Channel channel) const final;
		
		void channels(std::set<Channel> channels);
		
		std::set<Channel> channels() const;
		
	private:
		std::recursive_mutex mutable _guard;
		std::set<Channel> _channels;
	};
}