#include "..\..\..\..\stdafx.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\InputOutput\EthernetOutput.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmActiveAntenna.hpp"



namespace Ethernet
{
	Channel ScanningAlgorithmActiveAntenna::first(Channel channel)
	{
		return channel;
	}

	Channel ScanningAlgorithmActiveAntenna::next(Channel channel)
	{
		return channel;
	}

	bool ScanningAlgorithmActiveAntenna::completed(Channel channel) const
	{
		return true;
	}
}