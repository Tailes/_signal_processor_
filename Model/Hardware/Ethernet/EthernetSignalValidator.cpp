#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Model\Data\Signal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\Data\EthernetSignal.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetSignalValidator.hpp"




namespace Ethernet
{
	SignalValidator::SignalValidator()
	{
	}

	void SignalValidator::open()
	{
	}

	void SignalValidator::prepare(Signal &signal, EthernetSignal &ethernet_signal)
	{
	}

	void SignalValidator::apply(Signal &signal, EthernetSignal &ethernet_signal, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( !this->validate(signal, ethernet_signal) )
			this->reset(signal, ethernet_signal);
	}

	void SignalValidator::close()
	{
	}

	void SignalValidator::serialize(Trace &target)
	{
	}

	void SignalValidator::bind()
	{
	}

	void SignalValidator::load(const std::wstring &root)
	{
	}

	void SignalValidator::save(const std::wstring &root) const
	{
	}



	bool SignalValidator::validate(const Signal &signal, const EthernetSignal &ethernet_signal) const
	{
		if ( signal.number() != ethernet_signal.number() ) return false;
		if ( signal.size() != ethernet_signal.clusters_count() ) return false;
		return true;
	}

	void SignalValidator::reset(Signal &signal, const EthernetSignal &ethernet_signal) const
	{
		signal.assign(ethernet_signal.clusters_count(), Signal::value_type());
		signal.number(ethernet_signal.number());
	}
}