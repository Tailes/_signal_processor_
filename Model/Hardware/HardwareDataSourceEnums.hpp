#pragma once

#include <cstdint>



enum class HardwareDataSourceMode: std::uint32_t
{
	SerialPort,
	Ethernet,
};

