#include "..\..\StdAfx.hpp"
#include "..\..\Model\Hardware\HardwareDataSource.hpp"




HardwareDataSource::HardwareDataSource():
	_mode(HardwareDataSourceMode::Ethernet)
{
	_serializable_filters.emplace(HardwareDataSourceMode::SerialPort, &_serial_data_source);
	_serializable_filters.emplace(HardwareDataSourceMode::Ethernet, &_ethernet_data_source);
	
	_filters.emplace(HardwareDataSourceMode::SerialPort, &_serial_data_source);
	_filters.emplace(HardwareDataSourceMode::Ethernet, &_ethernet_data_source);
}

void HardwareDataSource::open()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_filters.at(_mode)->open();
}

void HardwareDataSource::prepare(Slice &slice)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_filters.at(_mode)->prepare(slice);
}

void HardwareDataSource::apply(Slice &slice, bool &interrupt)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_filters.at(_mode)->apply(slice, interrupt);
}

void HardwareDataSource::close()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_filters.at(_mode)->close();
}

void HardwareDataSource::serialize(Trace &target)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_serializable_filters.at(_mode)->serialize(target);
}

void HardwareDataSource::reset()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_filters.at(_mode)->reset();
}

void HardwareDataSource::bind()
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_ethernet_data_source.bind();
	_serial_data_source.bind();
}

void HardwareDataSource::load(const std::wstring &root)
{
	//Registry registry(root);
	std::lock_guard<std::recursive_mutex> lock(_guard);
	//registry.readInteger(L"ActiveDataSource", reinterpret_cast<std::add_lvalue_reference<std::underlying_type<HardwareDataSourceMode>::type>::type>(_mode));
	_ethernet_data_source.load(root + L"\\Ethernet");
	_serial_data_source.load(root + L"\\Serial");
}

void HardwareDataSource::save(const std::wstring &root) const
{
	//Registry registry(root);
	std::lock_guard<std::recursive_mutex> lock(_guard);
	//registry.write_integer(L"ActiveDataSource", reinterpret_cast<std::add_lvalue_reference<const std::underlying_type<HardwareDataSourceMode>::type>::type>(_mode));
	_ethernet_data_source.save(root + L"\\Ethernet");
	_serial_data_source.save(root + L"\\Serial");
}

HardwareDataSourceMode HardwareDataSource::mode() const
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	return _mode;
}

void HardwareDataSource::mode(HardwareDataSourceMode mode)
{
	std::lock_guard<std::recursive_mutex> lock(_guard);
	_mode = mode;
}

const Ethernet::DataSource& HardwareDataSource::ethernet_data_source() const
{
	return _ethernet_data_source;
}

const Serial::DataSource& HardwareDataSource::serial_data_source() const
{
	return _serial_data_source;
}

Ethernet::DataSource& HardwareDataSource::ethernet_data_source()
{
	return _ethernet_data_source;
}

Serial::DataSource& HardwareDataSource::serial_data_source()
{
	return _serial_data_source;
}
