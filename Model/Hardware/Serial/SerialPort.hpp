#pragma once
#include "..\..\..\OS\Handle.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialCommTimeouts.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialDeviceControlBlock.hpp"

#include <mutex>
#include <string>



namespace Serial
{
	class CommTimeouts;
	class ControlPacket;
	class DeviceControlBlock;
	
	
	
	class Port
	{
	public:
		Port();
		
		void open();
		void read(ControlPacket &packet);
		void read(CommTimeouts &timeouts);
		void read(DeviceControlBlock &dcb);
		void read(void *buffer, std::size_t length);
		void write(const ControlPacket &packet);
		void write(const CommTimeouts &timeouts);
		void write(const DeviceControlBlock &dcb);
		void write(const void *buffer, std::size_t length);
		void close();
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		
		DeviceControlBlock& device_control_block();
		CommTimeouts& comm_timeouts();
		
		const DeviceControlBlock& device_control_block() const;
		const CommTimeouts& comm_timeouts() const;
		
		std::wstring name() const;
		std::uint32_t output_buffer_size() const;
		std::uint32_t input_buffer_size() const;
		
		void name(const std::wstring &name);
		void output_buffer_size(std::uint32_t size);
		void input_buffer_size(std::uint32_t size);
		
	private:
		std::recursive_mutex mutable _guard;
		OS::Handle _port;
		std::uint32_t _output_buffer_size;
		std::uint32_t _input_buffer_size;
		std::wstring _name;
	};
}