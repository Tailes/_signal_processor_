#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Utility\Registry.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialPort.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialControlPacket.hpp"

#include <array>



#include <Windows.h>


namespace Serial
{
	Port::Port()
	{
		_output_buffer_size = 4096;
		_input_buffer_size = 4096;
		_name = L"COM1";
	}

	void Port::load(const std::wstring &root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.read_integer(L"InputBufferSize", _input_buffer_size);
		registry.read_integer(L"OutputBufferSize", _output_buffer_size);
		registry.read_text(L"PortName", _name);
	}

	void Port::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"InputBufferSize", _input_buffer_size);
		registry.write_integer(L"OutputBufferSize", _output_buffer_size);
		registry.write_text(L"PortName", _name);
	}

	void Port::open()
	{
		OS::Handle handle;
		std::lock_guard<std::recursive_mutex> lock(_guard);
#if !defined( USE_RANDOM_GENERATOR )
		if ( !(handle = ::CreateFileW(_name.c_str(), GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr)) )
			throw std::runtime_error("Cannot Open Communication Port.");
		if ( ::SetupComm(handle, _input_buffer_size, _output_buffer_size) == FALSE )
			throw std::runtime_error("Cannot modify Communication Port buffer sizes.");
		std::swap(_port, handle);
#endif
	}

	void Port::read(void *buffer, std::size_t length)
	{
		OS::Handle handle;
		unsigned long count = 0;
		std::lock_guard<std::recursive_mutex> lock(_guard);
#if !defined( USE_RANDOM_GENERATOR ) // RealType input.
		std::swap(_port, handle);
		if ( ::ReadFile(handle, buffer, length, &count, nullptr) == FALSE )
			throw std::runtime_error("Cannot read data from Communication Port.");
		if ( count != length )
			throw std::runtime_error("Cannot read enough data from Communication Port.");
		std::swap(_port, handle);
#else // Random input.
		int rnd = 0;
		for ( unsigned int i = 0; i < length; ++i )
			(reinterpret_cast<unsigned char*>(buffer))[i] = static_cast<unsigned char>(0xFF & ::MulDiv(i, 255, length) + ::MulDiv(rnd, 16, RAND_MAX)); //+ MulDiv( rand(), 16, RAND_MAX ) );
		//Sleep( 1000 );
#endif
	}

	void Port::write(const void *buffer, std::size_t length)
	{
		OS::Handle handle;
		unsigned long count = 0;
		std::lock_guard<std::recursive_mutex> lock(_guard);
#if !defined( USE_RANDOM_GENERATOR )
		std::swap(_port, handle);
		if ( ::WriteFile(handle, buffer, length, &count, nullptr) == FALSE )
			throw std::runtime_error("Cannot write data into Communication Port.");
		if ( count != length )
			throw std::runtime_error("Cannot write all the data into Communication Port.");
		std::swap(_port, handle);
#endif
	}

	void Port::close()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
#if !defined( USE_RANDOM_GENERATOR )
		std::swap(_port, OS::Handle());
#endif
	}

	void Port::read(DeviceControlBlock &dcb)
	{
		OS::Handle handle;
		std::lock_guard<std::recursive_mutex> lock(_guard);
#if !defined( USE_RANDOM_GENERATOR )
		std::swap(_port, handle);
		dcb.read(handle);
		//	if ( ::GetCommState(_port, &dcb) == FALSE )
		//		throw std::runtime_error("Cannot get Communication State.");
		std::swap(_port, handle);
#endif
	}

	void Port::read(CommTimeouts &timeouts)
	{
		OS::Handle handle;
		std::lock_guard<std::recursive_mutex> lock(_guard);
#if !defined( USE_RANDOM_GENERATOR )
		std::swap(_port, handle);
		timeouts.read(handle);
		//	if ( ::GetCommTimeouts(_port, &timeouts) == FALSE )
		//		throw std::runtime_error("Cannot get Communication Timeouts.");
		std::swap(_port, handle);
#endif
	}

	void Port::read(ControlPacket &packet)
	{
		OS::Handle handle;
		unsigned long count = 0;
		std::lock_guard<std::recursive_mutex> lock(_guard);
#if !defined( USE_RANDOM_GENERATOR ) // RealType input.
		std::swap(_port, handle);
		packet.read(handle);
		//if ( ::ReadFile(handle, packet.data(), packet.size(), &count, nullptr) == FALSE )
		//	throw std::runtime_error("Cannot read data from Communication Port.");
		//if ( count != packet.size() )
		//	throw std::runtime_error("Cannot read enough data from Communication Port.");
		std::swap(_port, handle);
#else // Random input.
		//	for ( unsigned int i = 0; i < packet.size(); ++i )
		//		packet.at(i) = static_cast<unsigned char>(::MulDiv(i, 255, packet.size()) + ::MulDiv(rand(), 16, RAND_MAX)); //+ MulDiv( rand(), 16, RAND_MAX ) );
		//Sleep( 1000 );
#endif
	}

	void Port::write(const DeviceControlBlock &dcb)
	{
		OS::Handle handle;
		std::lock_guard<std::recursive_mutex> lock(_guard);
#if !defined( USE_RANDOM_GENERATOR )
		std::swap(_port, handle);
		dcb.write(handle);
		//	if ( ::SetCommState(_port, const_cast<DCB*>(&dcb)) == FALSE )
		//		throw std::runtime_error("Cannot set Communication State.");
		std::swap(_port, handle);
#endif
	}

	void Port::write(const CommTimeouts &timeouts)
	{
		OS::Handle handle;
		std::lock_guard<std::recursive_mutex> lock(_guard);
#if !defined( USE_RANDOM_GENERATOR )
		std::swap(_port, handle);
		timeouts.write(handle);
		//	if ( ::SetCommTimeouts(_port, const_cast<COMMTIMEOUTS*>(&timeouts)) == FALSE )
		//		throw std::runtime_error("Cannot set Communication Timeouts.");
		std::swap(_port, handle);
#endif
	}

	void Port::write(const ControlPacket &packet)
	{
		OS::Handle handle;
		std::lock_guard<std::recursive_mutex> lock(_guard);
#if !defined( USE_RANDOM_GENERATOR )
		std::swap(_port, handle);
		packet.write(handle);
		//unsigned long count = 0;
		//if ( ::WriteFile(handle, packet.data(), packet.size(), &count, nullptr) == FALSE )
		//	throw std::runtime_error("Cannot write data into Communication Port.");
		//if ( count != packet.size() )
		//	throw std::runtime_error("Cannot write all the data into Communication Port.");
		std::swap(_port, handle);
#endif
	}


	std::wstring Port::name() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _name;
	}

	std::uint32_t Port::input_buffer_size() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _input_buffer_size;
	}

	std::uint32_t Port::output_buffer_size() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _output_buffer_size;
	}

	void Port::name(const std::wstring &name)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_name = name;
	}

	void Port::input_buffer_size(std::uint32_t size)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_input_buffer_size = size;
	}

	void Port::output_buffer_size(std::uint32_t size)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_output_buffer_size = size;
	}
}