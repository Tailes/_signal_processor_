#pragma once



namespace Serial
{
	enum class BaudRate: unsigned long
	{
		_110 = 110,
		_300 = 300,
		_600 = 600,
		_1200 = 1200,
		_2400 = 2400,
		_4800 = 4800,
		_9600 = 9600,
		_14400 = 14400,
		_19200 = 19200,
		_38400 = 38400,
		_56000 = 56000,
		_57600 = 57600,
		_115200 = 115200,
		_128000 = 128000,
		_256000 = 256000
	};



	enum class ParityMode: unsigned char
	{
		None,
		Odd,
		Even,
		Mark,
		Space
	};



	enum class StopBits: unsigned char
	{
		_10,
		_15,
		_20
	};



	enum class DtrMode: unsigned long
	{
		Disable,
		Enable,
		Handshake
	};



	enum class RtsMode: unsigned long
	{
		Disable,
		Enable,
		Handshake,
		Toggle
	};
}