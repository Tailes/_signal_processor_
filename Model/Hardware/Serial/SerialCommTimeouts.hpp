#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <mutex>

//#include <windows.h>

namespace OS
{
	class Handle;
	struct CommTimeouts;
}


namespace Serial
{
	
	class CommTimeouts
	{
		typedef OS::Handle Handle;
		
	public:
		CommTimeouts();
		~CommTimeouts();
		
		void read(Handle &source);
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		void write(Handle &target) const;
		
		std::uint32_t read_interval_timeout() const;
		std::uint32_t read_total_timeout_constant() const;
		std::uint32_t read_total_timeout_multiplier() const;
		std::uint32_t write_total_timeout_constant() const;
		std::uint32_t write_total_timeout_multiplier() const;
		
		void read_interval_timeout(std::uint32_t value);
		void read_total_timeout_constant(std::uint32_t value);
		void read_total_timeout_multiplier(std::uint32_t value);
		void write_total_timeout_constant(std::uint32_t value);
		void write_total_timeout_multiplier(std::uint32_t value);
		
	private:
		std::recursive_mutex mutable _guard;
		std::unique_ptr<OS::CommTimeouts> _cto;
	};
}