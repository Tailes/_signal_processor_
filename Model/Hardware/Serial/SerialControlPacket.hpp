#pragma once
#include "..\..\..\Model\Hardware\Serial\SerialCommunicationEnums.hpp"

#include <string>
#include <array>
#include <mutex>



namespace OS
{
	class Handle;
}

namespace Serial
{
	class ControlPacket
	{
		typedef OS::Handle Handle;
		
	public:
		ControlPacket();
		
		bool operator == (const ControlPacket &target) const;
		bool operator != (const ControlPacket &target) const;
		
		void read(Handle &handle);
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		void write(Handle &handle) const;
		
		HardwareDataSourceTimeFrame scanning_time_frame() const;
		ScanningMode scanning_mode() const;
		unsigned short probing_period() const;
		unsigned short start_delay() const;
		unsigned char accumulation() const;
		unsigned char channel() const;
		unsigned char marker() const;
		
		void scanning_time_frame(HardwareDataSourceTimeFrame frame);
		void probing_period(unsigned short period);
		void scanning_mode(ScanningMode mode);
		void accumulation(unsigned char accumulation);
		void start_delay(unsigned short delay);
		void channel(unsigned char channel);
		void marker(unsigned char marker);
		
	private:
		std::recursive_mutex mutable _guard;
		std::array<unsigned char, static_cast<std::size_t>(ControlPacketByte::Total)> _packet;
	};
}