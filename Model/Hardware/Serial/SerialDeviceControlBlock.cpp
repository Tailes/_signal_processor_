#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Utility\Registry.hpp"
#include "..\..\..\OS\DeviceControlBlock.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialPort.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialDeviceControlBlock.hpp"

#include <Windows.h>



namespace Serial
{
	DeviceControlBlock::DeviceControlBlock():
		_dcb(new OS::DeviceControlBlock)
	{
		std::memset(_dcb.get(), 0, sizeof(DCB));
		_dcb->DCBlength = sizeof(DCB);
		_dcb->StopBits = ONESTOPBIT;
		_dcb->BaudRate = CBR_9600;
		_dcb->ByteSize = 8;
		_dcb->fBinary = TRUE;
		_dcb->Parity = NOPARITY;
	}

	DeviceControlBlock::~DeviceControlBlock()
	{
	}


	void DeviceControlBlock::read(Handle &source)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( ::GetCommState(source, _dcb.get()) == FALSE )
			throw std::runtime_error("Cannot read Communication Port state.");
	}

	void DeviceControlBlock::write(Handle &target) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( ::SetCommState(target, const_cast<DCB*>(static_cast<const DCB*>(_dcb.get()))) == FALSE )
			throw std::runtime_error("Cannot modify Communication Port state.");
	}

	void DeviceControlBlock::load(const std::wstring &root)
	{
		DWORD value = 0;
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.read_integer(L"BaudRate", _dcb->BaudRate);
		registry.read_integer(L"XonLim", _dcb->XonLim);
		registry.read_integer(L"XoffLim", _dcb->XoffLim);
		registry.read_integer(L"ByteSize", _dcb->ByteSize);
		registry.read_integer(L"Parity", _dcb->Parity);
		registry.read_integer(L"StopBits", _dcb->StopBits);
		registry.read_integer(L"XonChar", _dcb->XonChar);
		registry.read_integer(L"XoffChar", _dcb->XoffChar);
		registry.read_integer(L"ErrorChar", _dcb->ErrorChar);
		registry.read_integer(L"EofChar", _dcb->EofChar);
		registry.read_integer(L"EvtChar", _dcb->EvtChar);
		registry.read_integer(L"fParity", value);			_dcb->fParity = value;				value = 0;
		registry.read_integer(L"fOutxCtsFlow", value);		_dcb->fOutxCtsFlow = value;			value = 0;
		registry.read_integer(L"fOutxDsrFlow", value);		_dcb->fOutxDsrFlow = value;			value = 0;
		registry.read_integer(L"fDtrControl", value);		_dcb->fDtrControl = value;			value = 0;
		registry.read_integer(L"fDsrSensitivity", value);	_dcb->fDsrSensitivity = value;		value = 0;
		registry.read_integer(L"fTXContinueOnXoff", value);	_dcb->fTXContinueOnXoff = value;	value = 0;
		registry.read_integer(L"fOutX", value);				_dcb->fOutX = value;				value = 0;
		registry.read_integer(L"fInX", value);				_dcb->fInX = value;					value = 0;
		registry.read_integer(L"fErrorChar", value);			_dcb->fErrorChar = value;			value = 0;
		registry.read_integer(L"fNull", value);				_dcb->fNull = value;				value = 0;
		registry.read_integer(L"fRtsControl", value);		_dcb->fRtsControl = value;			value = 0;
		registry.read_integer(L"fAbortOnError", value);		_dcb->fAbortOnError = value;		value = 0;
	}

	void DeviceControlBlock::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"BaudRate", _dcb->BaudRate);
		registry.write_integer(L"XonLim", _dcb->XonLim);
		registry.write_integer(L"XoffLim", _dcb->XoffLim);
		registry.write_integer(L"ByteSize", _dcb->ByteSize);
		registry.write_integer(L"Parity", _dcb->Parity);
		registry.write_integer(L"StopBits", _dcb->StopBits);
		registry.write_integer(L"XonChar", _dcb->XonChar);
		registry.write_integer(L"XoffChar", _dcb->XoffChar);
		registry.write_integer(L"ErrorChar", _dcb->ErrorChar);
		registry.write_integer(L"EofChar", _dcb->EofChar);
		registry.write_integer(L"EvtChar", _dcb->EvtChar);
		registry.write_integer(L"fParity", _dcb->fParity);
		registry.write_integer(L"fOutxCtsFlow", _dcb->fOutxCtsFlow);
		registry.write_integer(L"fOutxDsrFlow", _dcb->fOutxDsrFlow);
		registry.write_integer(L"fDtrControl", _dcb->fDtrControl);
		registry.write_integer(L"fDsrSensitivity", _dcb->fDsrSensitivity);
		registry.write_integer(L"fTXContinueOnXoff", _dcb->fTXContinueOnXoff);
		registry.write_integer(L"fOutX", _dcb->fOutX);
		registry.write_integer(L"fInX", _dcb->fInX);
		registry.write_integer(L"fErrorChar", _dcb->fErrorChar);
		registry.write_integer(L"fNull", _dcb->fNull);
		registry.write_integer(L"fRtsControl", _dcb->fRtsControl);
		registry.write_integer(L"fAbortOnError", _dcb->fAbortOnError);
	}

	BaudRate DeviceControlBlock::baud_rate() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return static_cast<BaudRate>(_dcb->BaudRate);
	}

	unsigned short DeviceControlBlock::x_on_limit() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->XonLim;
	}

	unsigned short DeviceControlBlock::x_off_limit() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->XoffLim;
	}

	unsigned char DeviceControlBlock::byte_size() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->ByteSize;
	}

	ParityMode DeviceControlBlock::parity_mode() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return static_cast<ParityMode>(_dcb->Parity);
	}

	StopBits DeviceControlBlock::stop_bits() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return static_cast<StopBits>(_dcb->StopBits);
	}

	DtrMode DeviceControlBlock::dtr_control_mode() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return static_cast<DtrMode>(_dcb->fDtrControl);
	}

	RtsMode DeviceControlBlock::rts_control_mode() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return static_cast<RtsMode>(_dcb->fRtsControl);
	}

	char DeviceControlBlock::x_on_char() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->XonChar;
	}

	char DeviceControlBlock::x_off_char() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->XoffChar;
	}

	char DeviceControlBlock::error_char() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->ErrorChar;
	}

	char DeviceControlBlock::eof_char() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->EofChar;
	}

	char DeviceControlBlock::evt_char() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->EvtChar;
	}

	bool DeviceControlBlock::parity_enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->fParity;
	}

	bool DeviceControlBlock::output_cts_flow_enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->fOutxCtsFlow;
	}

	bool DeviceControlBlock::output_dsr_flow_enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->fOutxDsrFlow;
	}

	bool DeviceControlBlock::dsr_sensitivity_enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->fDsrSensitivity;
	}

	bool DeviceControlBlock::transmission_continue_on_x_off_enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->fTXContinueOnXoff;
	}

	bool DeviceControlBlock::out_x_enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->fOutX;
	}

	bool DeviceControlBlock::in_x_enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->fInX;
	}

	bool DeviceControlBlock::error_char_enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->fErrorChar;
	}

	bool DeviceControlBlock::null_enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->fNull;
	}

	bool DeviceControlBlock::abort_on_error_enabled() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _dcb->fAbortOnError;
	}


	void DeviceControlBlock::baud_rate(BaudRate rate)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->BaudRate = static_cast<DWORD>(rate);
	}

	void DeviceControlBlock::x_on_limit(unsigned short limit)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->XonLim = limit;
	}

	void DeviceControlBlock::x_off_limit(unsigned short limit)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->XoffLim = limit;
	}

	void DeviceControlBlock::byte_size(unsigned char size)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->ByteSize = size;
	}

	void DeviceControlBlock::parity_mode(ParityMode mode)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->Parity = static_cast<BYTE>(mode);
	}

	void DeviceControlBlock::stop_bits(StopBits count)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->StopBits = static_cast<BYTE>(count);
	}

	void DeviceControlBlock::dtr_control_mode(DtrMode mode)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fDtrControl = static_cast<DWORD>(mode);
	}

	void DeviceControlBlock::rts_control_mode(RtsMode mode)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fRtsControl = static_cast<DWORD>(mode);
	}

	void DeviceControlBlock::x_on_char(char value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->XonChar = value;
	}

	void DeviceControlBlock::x_off_char(char value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->XoffChar = value;
	}

	void DeviceControlBlock::error_char(char value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->ErrorChar = value;
	}

	void DeviceControlBlock::eof_char(char value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->EofChar = value;
	}

	void DeviceControlBlock::evt_char(char value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->EvtChar = value;
	}

	void DeviceControlBlock::enable_parity(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fParity = enable ? TRUE : FALSE;
	}

	void DeviceControlBlock::enable_output_cts_flow(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fOutxCtsFlow = enable ? TRUE : FALSE;
	}

	void DeviceControlBlock::enable_output_dsr_flow(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fOutxDsrFlow = enable ? TRUE : FALSE;
	}

	void DeviceControlBlock::enable_dsr_sensitivity(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fDsrSensitivity = enable ? TRUE : FALSE;
	}

	void DeviceControlBlock::enable_transmission_continue_on_x_off(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fTXContinueOnXoff = enable ? TRUE : FALSE;
	}

	void DeviceControlBlock::enable_out_x(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fOutX = enable ? TRUE : FALSE;
	}

	void DeviceControlBlock::enable_in_x(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fInX = enable ? TRUE : FALSE;
	}

	void DeviceControlBlock::enable_error_char(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fErrorChar = enable ? TRUE : FALSE;
	}

	void DeviceControlBlock::enable_null(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fNull = enable ? TRUE : FALSE;
	}

	void DeviceControlBlock::enable_abort_on_error(bool enable)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_dcb->fAbortOnError = enable ? TRUE : FALSE;
	}
}