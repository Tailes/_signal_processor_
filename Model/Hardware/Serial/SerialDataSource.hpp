#pragma once
#include "..\..\..\Model\Data\Slice.hpp"
#include "..\..\..\Model\Data\Trace.hpp"
#include "..\..\..\Model\Data\Signal.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialPort.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialCommTimeouts.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialControlPacket.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialDeviceControlBlock.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialCommunicationEnums.hpp"
#include "..\..\..\Model\Implementations\SerializableSliceFilterImpl.hpp"

#include <functional>
#include <valarray>
#include <vector>
#include <array>
#include <map>



namespace Serial
{
	class DataSource:
		public SerializableSliceFilterImpl
	{
	public:
		DataSource();
		
		virtual void open();
		virtual void prepare(Slice &slice);
		virtual void apply(Slice &slice, bool &interrupt);
		virtual void close();
		virtual void reset();
		virtual void bind();
		virtual void load(const std::wstring &root);
		virtual void save(const std::wstring &root) const;
		virtual void serialize(Trace &target);
		
		DeviceControlBlock& device_control_block();
		ControlPacket& control_packet();
		CommTimeouts& comm_timeouts();
		Port& port();
		
		const DeviceControlBlock& device_control_block() const;
		const ControlPacket& control_packet() const;
		const CommTimeouts& comm_timeouts() const;
		const Port& port() const;
		
		ScanningAlgorithm scanning_algorithm() const;
		PacketLength packet_length() const;
		DataSize point_size() const;
		unsigned long handshakes() const;
		
		void scanning_algorithm(ScanningAlgorithm value);
		void packet_length(PacketLength value);
		void point_size(DataSize value);
		void handshakes(unsigned long value);
		
	private:
		typedef void(DataSource::* SignalRoutine)(Signal &signal, PacketLength length);
		typedef void(DataSource::* SliceRoutine)(Slice& slice);
		
		std::recursive_mutex mutable _guard;
		std::map<DataSize, std::function<void(Signal &, PacketLength)>> _readers;
		std::map<ScanningAlgorithm, std::function<void(Slice&)>> _routines;
		std::vector<real_t> _buffer;
		DeviceControlBlock _device_control_block;
		ControlPacket _settings_packet;
		CommTimeouts _comm_timeouts;
		Port _port;
		ScanningAlgorithm _algorithm;
		PacketLength _packet_length;
		DataSize _point_size;
		unsigned long _handshakes;
		
		template< class T >
		void read_data(Signal &signal, PacketLength length);
		
		void read_slice_in_active_antenna_cross_input_mode(Slice& slice);
		void read_slice_in_transmit_receive_mode(Slice& slice);
		void read_slice_in_active_antenna_mode(Slice& slice);
		void read_slice_in_quadro_antenna_mode(Slice& slice);
		void establish_handshake(const ControlPacket &request);
		void convert_signal(Signal &signal);
		void read_channel(Signal& signal, std::size_t channel);
		void read_signal(Signal &signal);
		void read_slice(Slice& slice, std::size_t count);
		void channel(std::size_t);
	};
}