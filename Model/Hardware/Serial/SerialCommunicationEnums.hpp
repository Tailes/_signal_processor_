#pragma once



namespace Serial
{
	enum class HardwareDataSourceTimeFrame: unsigned char
	{
		_01 = 1 * 0x0B,
		_02 = 2 * 0x0B,
		_04 = 4 * 0x0B,
		_10 = 10 * 0x0B,
		_16 = 16 * 0x0B,
		_20 = 20 * 0x0B,
		Base = 0x0B
	};




	enum class PacketLength: unsigned int
	{
		_512 = 512,
		_1024 = 1024,
		Max = 1024,
	};



	enum class DataSize: int
	{
		Byte = 8,
		Dozen = 12,
		Word = 16,
		Total
	};



	enum class ScanningAlgorithm: int
	{
		ActiveAntenna,
		ActiveAntennaCrossInput,
		TransmitRecieve,
		QuadroAntenna,
	};



	enum class ScanningMode: unsigned char
	{
		Straight = 0x00,
		CrossPolarized = 0x10
	};



	enum class TimeFrame: unsigned char
	{
		_01 = 1 * 0x0B,
		_02 = 2 * 0x0B,
		_04 = 4 * 0x0B,
		_10 = 10 * 0x0B,
		_20 = 20 * 0x0B
	};



	enum class ControlPacketByte: unsigned char
	{
		Marker = 0,
		LoStartDelay = 1,
		HiStartDelay = 2,
		LoProbingPeriod = 3,
		HiProbingPeriod = 4,
		TimeFrame = 5,
		ScanningMode = 6,
		Accumulation1 = 7,
		Accumulation2 = 8,
		Total = 9
	};



	enum class ChunkSize: unsigned char
	{
		Min = 1,
		Default = 1,
		Max = 255
	};



	enum class ProbingDelay: unsigned short
	{
		Min = 0,
		Default = 0,
		Max = 65535
	};



	enum class ProbingPeriod: unsigned short
	{
		Min = 10,
		Default = 255,
		Max = 512
	};
}