#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Utility\Registry.hpp"
#include "..\..\..\Utility\TypeArithmetics.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialControlPacket.hpp"
#include "..\..\..\OS\Handle.hpp"


#include <windows.h>




namespace Serial
{
	ControlPacket::ControlPacket()
	{
		_packet.at(static_cast<std::size_t>(ControlPacketByte::Marker)) = 0xFF;
		_packet.at(static_cast<std::size_t>(ControlPacketByte::LoStartDelay)) = utility::get_low_byte(static_cast<std::uint16_t>(ProbingDelay::Default));
		_packet.at(static_cast<std::size_t>(ControlPacketByte::HiStartDelay)) = utility::get_high_byte(static_cast<std::uint16_t>(ProbingDelay::Default));
		_packet.at(static_cast<std::size_t>(ControlPacketByte::LoProbingPeriod)) = utility::get_low_byte(static_cast<std::uint16_t>(ProbingPeriod::Default));
		_packet.at(static_cast<std::size_t>(ControlPacketByte::HiProbingPeriod)) = utility::get_high_byte(static_cast<std::uint16_t>(ProbingPeriod::Default));
		_packet.at(static_cast<std::size_t>(ControlPacketByte::TimeFrame)) = static_cast<std::uint8_t>(TimeFrame::_01);
		_packet.at(static_cast<std::size_t>(ControlPacketByte::ScanningMode)) = static_cast<std::uint8_t>(ScanningMode::Straight);
		_packet.at(static_cast<std::size_t>(ControlPacketByte::Accumulation1)) = 1;
		_packet.at(static_cast<std::size_t>(ControlPacketByte::Accumulation2)) = 1;
	}

	bool ControlPacket::operator == (const ControlPacket &target) const
	{
		std::lock_guard<std::recursive_mutex> source_lock(_guard);
		std::lock_guard<std::recursive_mutex> target_lock(target._guard);
		return _packet == target._packet;
	}

	bool ControlPacket::operator != (const ControlPacket &target) const
	{
		std::lock_guard<std::recursive_mutex> source_lock(_guard);
		std::lock_guard<std::recursive_mutex> target_lock(target._guard);
		return _packet != target._packet;
	}


	void ControlPacket::read(Handle &handle)
	{
		unsigned long count = 0;
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( ::ReadFile(handle, _packet.data(), _packet.size(), &count, nullptr) == FALSE )
			throw std::runtime_error("Cannot read data from the Communication Port.");
		if ( count != _packet.size() )
			throw std::runtime_error("Cannot read all the data from the Communication Port.");
	}

	void ControlPacket::write(Handle &handle) const
	{
		unsigned long count = 0;
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( ::WriteFile(handle, _packet.data(), _packet.size(), &count, nullptr) == FALSE )
			throw std::runtime_error("Cannot write data into the Communication Port.");
		if ( count != _packet.size() )
			throw std::runtime_error("Cannot write all the data into the Communication Port.");
	}

	void ControlPacket::load(const std::wstring &root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.read_integer(L"Marker", _packet.at(static_cast<std::size_t>(ControlPacketByte::Marker)));
		registry.read_integer(L"TimeFrame", _packet.at(static_cast<std::size_t>(ControlPacketByte::TimeFrame)));
		registry.read_integer(L"ScanningMode", _packet.at(static_cast<std::size_t>(ControlPacketByte::ScanningMode)));
		registry.read_integer(L"Accumulation", _packet.at(static_cast<std::size_t>(ControlPacketByte::Accumulation1)));
		registry.read_integer(L"Accumulation", _packet.at(static_cast<std::size_t>(ControlPacketByte::Accumulation2)));
	}

	void ControlPacket::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"Marker", _packet.at(static_cast<std::size_t>(ControlPacketByte::Marker)));
		registry.write_integer(L"ProbingPeriod", utility::make_word(_packet.at(static_cast<std::size_t>(ControlPacketByte::LoProbingPeriod)), _packet.at(static_cast<std::size_t>(ControlPacketByte::HiProbingPeriod))));
		registry.write_integer(L"StartDelay", utility::make_word(_packet.at(static_cast<std::size_t>(ControlPacketByte::LoStartDelay)), _packet.at(static_cast<std::size_t>(ControlPacketByte::HiStartDelay))));
		registry.write_integer(L"TimeFrame", _packet.at(static_cast<std::size_t>(ControlPacketByte::TimeFrame)));
		registry.write_integer(L"ScanningMode", _packet.at(static_cast<std::size_t>(ControlPacketByte::ScanningMode)));
		registry.write_integer(L"Accumulation", _packet.at(static_cast<std::size_t>(ControlPacketByte::Accumulation1)));
	}

	unsigned char ControlPacket::marker() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _packet.at(static_cast<std::size_t>(ControlPacketByte::Marker));
	}

	unsigned short ControlPacket::start_delay() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return utility::make_word(_packet.at(static_cast<std::size_t>(ControlPacketByte::LoStartDelay)), _packet.at(static_cast<std::size_t>(ControlPacketByte::HiStartDelay)));
	}

	unsigned short ControlPacket::probing_period() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return utility::make_word(_packet.at(static_cast<std::size_t>(ControlPacketByte::LoProbingPeriod)), _packet.at(static_cast<std::size_t>(ControlPacketByte::HiProbingPeriod)));
	}

	HardwareDataSourceTimeFrame ControlPacket::scanning_time_frame() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return static_cast<HardwareDataSourceTimeFrame>(_packet.at(static_cast<std::size_t>(ControlPacketByte::TimeFrame)));
	}

	ScanningMode ControlPacket::scanning_mode() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return static_cast<ScanningMode>(_packet.at(static_cast<std::size_t>(ControlPacketByte::ScanningMode))& 0xF0);
	}

	unsigned char ControlPacket::channel() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _packet.at(static_cast<std::size_t>(ControlPacketByte::ScanningMode))& 0x0F;
	}

	unsigned char ControlPacket::accumulation() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _packet.at(static_cast<std::size_t>(ControlPacketByte::Accumulation1));
	}

	//ControlPacketBuffer ControlPacket::packet() const
	//{
	//	std::lock_guard<std::recursive_mutex> lock(_guard);
	//	return _packet;
	//}

	void ControlPacket::marker(unsigned char marker)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_packet.at(static_cast<std::size_t>(ControlPacketByte::Marker)) = marker;
	}

	void ControlPacket::start_delay(unsigned short delay)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_packet.at(static_cast<std::size_t>(ControlPacketByte::LoStartDelay)) = utility::get_low_byte(delay);
		_packet.at(static_cast<std::size_t>(ControlPacketByte::HiStartDelay)) = utility::get_high_byte(delay);
	}

	void ControlPacket::probing_period(unsigned short period)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_packet.at(static_cast<std::size_t>(ControlPacketByte::LoProbingPeriod)) = utility::get_low_byte(period);
		_packet.at(static_cast<std::size_t>(ControlPacketByte::HiProbingPeriod)) = utility::get_high_byte(period);
	}

	void ControlPacket::scanning_time_frame(HardwareDataSourceTimeFrame frame)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_packet.at(static_cast<std::size_t>(ControlPacketByte::TimeFrame)) = static_cast<std::uint8_t>(frame);
	}

	void ControlPacket::scanning_mode(ScanningMode mode)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_packet.at(static_cast<std::size_t>(ControlPacketByte::ScanningMode)) = (static_cast<std::uint8_t>(mode)& 0xF0) | (_packet.at(static_cast<std::size_t>(ControlPacketByte::ScanningMode)) & 0x0F);
	}

	void ControlPacket::channel(unsigned char channel)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_packet.at(static_cast<std::size_t>(ControlPacketByte::ScanningMode)) = (_packet.at(static_cast<std::size_t>(ControlPacketByte::ScanningMode))& 0xF0) | (channel & 0x0F);
	}

	void ControlPacket::accumulation(unsigned char accumulation)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_packet.at(static_cast<std::size_t>(ControlPacketByte::Accumulation1)) = accumulation;
		_packet.at(static_cast<std::size_t>(ControlPacketByte::Accumulation2)) = accumulation;
	}
}