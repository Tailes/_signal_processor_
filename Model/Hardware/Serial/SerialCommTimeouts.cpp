#include "..\..\..\StdAfx.hpp"
#include "..\..\..\OS\Handle.hpp"
#include "..\..\..\OS\CommTimeouts.hpp"
#include "..\..\..\Utility\Registry.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialCommTimeouts.hpp"

#include <Windows.h>



namespace Serial
{
	CommTimeouts::CommTimeouts():
		_cto(new OS::CommTimeouts)
	{
		std::memset(_cto.get(), 0, sizeof(COMMTIMEOUTS));
		_cto->ReadIntervalTimeout = 1000;
		_cto->ReadTotalTimeoutConstant = 1000;
		_cto->WriteTotalTimeoutConstant = 1000;
		_cto->ReadTotalTimeoutMultiplier = 10;
		_cto->WriteTotalTimeoutMultiplier = 10;
	}

	CommTimeouts::~CommTimeouts()
	{
	}

	void CommTimeouts::read(Handle &source)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( ::GetCommTimeouts(source, _cto.get()) == FALSE )
			throw std::runtime_error("Cannot get Communication Port timeouts.");
	}

	void CommTimeouts::write(Handle &target) const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		if ( ::SetCommTimeouts(target, const_cast<COMMTIMEOUTS*>(static_cast<const COMMTIMEOUTS*>(_cto.get()))) == FALSE )
			throw std::runtime_error("Cannot set Communication Port timeouts.");
	}

	void CommTimeouts::load(const std::wstring &root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.read_integer(L"ReadIntervalTimeout", _cto->ReadIntervalTimeout);
		registry.read_integer(L"ReadTotalTimeoutConstant", _cto->ReadTotalTimeoutConstant);
		registry.read_integer(L"ReadTotalTimeoutMultiplier", _cto->ReadTotalTimeoutMultiplier);
		registry.read_integer(L"WriteTotalTimeoutConstant", _cto->WriteTotalTimeoutConstant);
		registry.read_integer(L"WriteTotalTimeoutMultiplier", _cto->WriteTotalTimeoutMultiplier);
	}

	void CommTimeouts::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"ReadIntervalTimeout", _cto->ReadIntervalTimeout);
		registry.write_integer(L"ReadTotalTimeoutConstant", _cto->ReadTotalTimeoutConstant);
		registry.write_integer(L"ReadTotalTimeoutMultiplier", _cto->ReadTotalTimeoutMultiplier);
		registry.write_integer(L"WriteTotalTimeoutConstant", _cto->WriteTotalTimeoutConstant);
		registry.write_integer(L"WriteTotalTimeoutMultiplier", _cto->WriteTotalTimeoutMultiplier);
	}


	std::uint32_t CommTimeouts::read_interval_timeout() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _cto->ReadIntervalTimeout;
	}

	std::uint32_t CommTimeouts::read_total_timeout_constant() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _cto->ReadTotalTimeoutConstant;
	}

	std::uint32_t CommTimeouts::read_total_timeout_multiplier() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _cto->ReadTotalTimeoutMultiplier;
	}

	std::uint32_t CommTimeouts::write_total_timeout_constant() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _cto->WriteTotalTimeoutConstant;
	}

	std::uint32_t CommTimeouts::write_total_timeout_multiplier() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _cto->WriteTotalTimeoutMultiplier;
	}


	void CommTimeouts::read_interval_timeout(std::uint32_t value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_cto->ReadIntervalTimeout = value;
	}

	void CommTimeouts::read_total_timeout_constant(std::uint32_t value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_cto->ReadTotalTimeoutConstant = value;
	}

	void CommTimeouts::read_total_timeout_multiplier(std::uint32_t value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_cto->ReadTotalTimeoutMultiplier = value;
	}

	void CommTimeouts::write_total_timeout_constant(std::uint32_t value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_cto->WriteTotalTimeoutConstant = value;
	}

	void CommTimeouts::write_total_timeout_multiplier(std::uint32_t value)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_cto->WriteTotalTimeoutMultiplier = value;
	}
}