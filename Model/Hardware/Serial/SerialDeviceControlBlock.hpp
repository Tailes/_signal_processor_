#pragma once
#include "..\..\..\Model\Hardware\Serial\SerialPortEnums.hpp"

#include <memory>
#include <string>
#include <mutex>



namespace OS
{
	class Handle;
	struct DeviceControlBlock;
}




namespace Serial
{
	class DeviceControlBlock
	{
		typedef OS::Handle Handle;
		
	public:
		DeviceControlBlock();
		~DeviceControlBlock();
		
		void read(Handle &source);
		void load(const std::wstring &root);
		void save(const std::wstring &root) const;
		void write(Handle &target) const;
		
		DtrMode dtr_control_mode() const;
		RtsMode rts_control_mode() const;
		ParityMode parity_mode() const;
		unsigned short x_off_limit() const;
		unsigned short x_on_limit() const;
		StopBits stop_bits() const;
		BaudRate baud_rate() const;
		unsigned char byte_size() const;
		char error_char() const;
		char x_off_char() const;
		char x_on_char() const;
		char eof_char() const;
		char evt_char() const;
		bool transmission_continue_on_x_off_enabled() const;
		bool dsr_sensitivity_enabled() const;
		bool output_cts_flow_enabled() const;
		bool output_dsr_flow_enabled() const;
		bool abort_on_error_enabled() const;
		bool error_char_enabled() const;
		bool parity_enabled() const;
		bool null_enabled() const;
		bool out_x_enabled() const;
		bool in_x_enabled() const;
		
		void dtr_control_mode(DtrMode mode);
		void rts_control_mode(RtsMode mode);
		void parity_mode(ParityMode mode);
		void x_off_limit(unsigned short limit);
		void x_on_limit(unsigned short limit);
		void x_off_char(char value);
		void error_char(char value);
		void stop_bits(StopBits count);
		void baud_rate(BaudRate rate);
		void byte_size(unsigned char size);
		void x_on_char(char value);
		void eof_char(char value);
		void evt_char(char value);
		void enable_transmission_continue_on_x_off(bool enable);
		void enable_dsr_sensitivity(bool enable);
		void enable_output_cts_flow(bool enable);
		void enable_output_dsr_flow(bool enable);
		void enable_abort_on_error(bool enable);
		void enable_error_char(bool enable);
		void enable_parity(bool enable);
		void enable_out_x(bool enable);
		void enable_null(bool enable);
		void enable_in_x(bool enable);
		
	private:
		std::recursive_mutex mutable _guard;
		std::unique_ptr<OS::DeviceControlBlock> _dcb;
	};
}