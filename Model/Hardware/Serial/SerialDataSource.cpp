#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Utility\Registry.hpp"
#include "..\..\..\Utility\TypeArithmetics.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialPort.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialDataSource.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialCommunicationEnums.hpp"

#include <cstdint>
#include <vector>

#include <Windows.h>




namespace Serial
{
	DataSource::DataSource()
	{
		_buffer.resize(static_cast<std::size_t>(PacketLength::Max), real_t());
		_packet_length = PacketLength::_1024;
		_point_size = DataSize::Word;
		_handshakes = 3;
		_algorithm = ScanningAlgorithm::ActiveAntenna;
		
		_routines.insert(std::make_pair(ScanningAlgorithm::ActiveAntennaCrossInput, std::bind(&DataSource::read_slice_in_active_antenna_cross_input_mode, this, std::placeholders::_1)));
		_routines.insert(std::make_pair(ScanningAlgorithm::TransmitRecieve, std::bind(&DataSource::read_slice_in_transmit_receive_mode, this, std::placeholders::_1)));
		_routines.insert(std::make_pair(ScanningAlgorithm::ActiveAntenna, std::bind(&DataSource::read_slice_in_active_antenna_mode, this, std::placeholders::_1)));
		_routines.insert(std::make_pair(ScanningAlgorithm::QuadroAntenna, std::bind(&DataSource::read_slice_in_quadro_antenna_mode, this, std::placeholders::_1)));
		
		_readers[DataSize::Dozen] = [this](Signal &signal, PacketLength length) { this->read_data<std::uint16_t>(signal, length); };
		_readers[DataSize::Word] = [this](Signal &signal, PacketLength length) { this->read_data<std::uint16_t>(signal, length); };
		_readers[DataSize::Byte] = [this](Signal &signal, PacketLength length) { this->read_data<std::uint8_t>(signal, length); };
	}

	void DataSource::open()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_port.open();
		_port.write(_comm_timeouts);
		_port.write(_device_control_block);
	}

	void DataSource::prepare(Slice &slice)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		for ( std::size_t i = 0; i < std::min<std::size_t>(std::tuple_size<Batch>::value, 4); ++i )
			slice.batch().at(i).assign(static_cast<std::size_t>(_packet_length), static_cast<real_t>(0.0));
		for ( std::size_t i = std::min<std::size_t>(std::tuple_size<Batch>::value, 4); i < std::tuple_size<Batch>::value; ++i )
			slice.batch().at(i).assign(0, static_cast<real_t>(0.0));
	}

	void DataSource::apply(Slice &slice, bool &interrupt)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_routines.at(_algorithm)(slice);
	}

	void DataSource::close()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_port.close();
	}

	void DataSource::reset()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
	}

	void DataSource::serialize(Trace &target)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		target.time_frame((static_cast<float>(_settings_packet.scanning_time_frame()) / static_cast<float>(HardwareDataSourceTimeFrame::Base)) * 2.5f);
	}

	void DataSource::bind()
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
	}

	void DataSource::load(const std::wstring &root)
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.read_integer(L"Handshakes", _handshakes);
		registry.read_integer(L"PointSize", reinterpret_cast<std::add_lvalue_reference<std::underlying_type<DataSize>::type>::type>(_point_size));
		registry.read_integer(L"PacketLength", reinterpret_cast<std::add_lvalue_reference<std::underlying_type<PacketLength>::type>::type>(_packet_length));
		registry.read_integer(L"Algorithm", reinterpret_cast<std::add_lvalue_reference<std::underlying_type<ScanningAlgorithm>::type>::type>(_algorithm));
		_device_control_block.load(root + L"\\device control block");
		_settings_packet.load(root);
		_comm_timeouts.load(root + L"\\timeouts");
		_port.load(root);
	}

	void DataSource::save(const std::wstring &root) const
	{
		Registry registry(root);
		std::lock_guard<std::recursive_mutex> lock(_guard);
		registry.write_integer(L"Handshakes", _handshakes);
		registry.write_integer(L"PointSize", static_cast<int>(_point_size));
		registry.write_integer(L"PacketLength", static_cast<unsigned int>(_packet_length));
		registry.write_integer(L"Algorithm", static_cast<int>(_algorithm));
		_device_control_block.save(root + L"\\device control block");
		_settings_packet.save(root);
		_comm_timeouts.save(root + L"\\timeouts");
		_port.save(root);
	}




	unsigned long DataSource::handshakes() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _handshakes;
	}

	DataSize DataSource::point_size() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _point_size;
	}

	PacketLength DataSource::packet_length() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _packet_length;
	}

	ScanningAlgorithm DataSource::scanning_algorithm() const
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		return _algorithm;
	}

	DeviceControlBlock& DataSource::device_control_block()
	{
		return _device_control_block;
	}

	ControlPacket& DataSource::control_packet()
	{
		return _settings_packet;
	}

	CommTimeouts& DataSource::comm_timeouts()
	{
		return _comm_timeouts;
	}

	Port& DataSource::port()
	{
		return _port;
	}



	const DeviceControlBlock& DataSource::device_control_block() const
	{
		return _device_control_block;
	}

	const ControlPacket& DataSource::control_packet() const
	{
		return _settings_packet;
	}

	const CommTimeouts& DataSource::comm_timeouts() const
	{
		return _comm_timeouts;
	}

	const Port& DataSource::port() const
	{
		return _port;
	}




	void DataSource::handshakes(unsigned long count)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_handshakes = count;
	}

	void DataSource::point_size(DataSize size)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_point_size = size;
	}

	void DataSource::packet_length(PacketLength length)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_packet_length = length;
	}

	void DataSource::scanning_algorithm(ScanningAlgorithm algorithm)
	{
		std::lock_guard<std::recursive_mutex> lock(_guard);
		_algorithm = algorithm;
	}




	template<class T>
	void DataSource::read_data(Signal &signal, PacketLength length)
	{
		signal.assign(static_cast<std::size_t>(length), real_t());
		if ( _buffer.size() < static_cast<std::size_t>(length) )
			throw std::runtime_error("Insufficient internal buffer size.");
		_port.read(_buffer.data(), static_cast<std::size_t>(length)* sizeof(T));
		for ( unsigned int i = static_cast<std::size_t>(length); 0 < i--; )
			signal.at(i) = static_cast<real_t>(reinterpret_cast<const T*>(_buffer.data())[i]);
	}

	void DataSource::establish_handshake(const ControlPacket &request)
	{
		ControlPacket response;
		for ( unsigned int i = 0; i < _handshakes; ++i )
		{
			_port.write(request);
			_port.read(response);
#if !defined( USE_RANDOM_GENERATOR )
			if ( request == response )
#endif
				return;
		}
		throw std::runtime_error("Cannot establish handshake with port.");
	}

	void DataSource::channel(std::size_t channel)
	{
		_settings_packet.channel(static_cast<unsigned char>(channel));
	}

	void DataSource::read_signal(Signal &signal)
	{
		_readers.at(_point_size)(signal, _packet_length);
	}

	void DataSource::convert_signal(Signal &signal)
	{
		real_t range_median = static_cast<real_t>(0x01 << (static_cast<int>(_point_size) - 1));
		real_t accumulation = static_cast<real_t>(_settings_packet.accumulation());
		for ( std::size_t i = 0; i < signal.size(); ++i )
			(signal.at(i) /= range_median * accumulation) -= 1.0;
	}

	void DataSource::read_channel(Signal &signal, std::size_t channel)
	{
		this->channel(channel);
		this->establish_handshake(_settings_packet);
		this->read_signal(signal);
		this->convert_signal(signal);
	}

	void DataSource::read_slice(Slice &slice, std::size_t count)
	{
		for ( std::size_t i = 0; i < count; ++i )
			this->read_channel(slice.batch().at(i), i);
	}

	void DataSource::read_slice_in_active_antenna_mode(Slice &slice)
	{
		_settings_packet.scanning_mode(ScanningMode::Straight);
		return this->read_slice(slice, 1);
	}

	void DataSource::read_slice_in_active_antenna_cross_input_mode(Slice &slice)
	{
		_settings_packet.scanning_mode(ScanningMode::CrossPolarized);
		_settings_packet.probing_period(utility::make_word(0x48, 0x00));
		return this->read_slice(slice, 1);
	}

	void DataSource::read_slice_in_transmit_receive_mode(Slice &slice)
	{
		_settings_packet.scanning_mode(ScanningMode::Straight);
		return this->read_slice(slice, 2);
	}

	void DataSource::read_slice_in_quadro_antenna_mode(Slice &slice)
	{
		_settings_packet.scanning_mode(ScanningMode::Straight);
		return this->read_slice(slice, 4);
	}
}