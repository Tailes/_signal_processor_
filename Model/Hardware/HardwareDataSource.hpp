#pragma once
#include "..\..\Model\Hardware\HardwareDataSourceEnums.hpp"
#include "..\..\Model\Hardware\Serial\SerialDataSource.hpp"
#include "..\..\Model\Hardware\Ethernet\EthernetDataSource.hpp"
#include "..\..\Model\Implementations\SerializableSliceFilterImpl.hpp"

#include <array>
#include <map>
#include <mutex>



class Slice;
class Trace;



class HardwareDataSource:
	public SerializableSliceFilterImpl
{
public:
	HardwareDataSource();
	
	virtual void open();
	virtual void prepare(Slice &slice);
	virtual void apply(Slice &slice, bool &interrupt);
	virtual void close();
	virtual void reset();
	virtual void bind();
	virtual void load(const std::wstring &root);
	virtual void save(const std::wstring &root) const;
	virtual void serialize(Trace &target);
	
	const Ethernet::DataSource& ethernet_data_source() const;
	const Serial::DataSource& serial_data_source() const;
	Ethernet::DataSource& ethernet_data_source();
	Serial::DataSource& serial_data_source();
	
	HardwareDataSourceMode mode() const;
	
	void mode(HardwareDataSourceMode source);
	
private:
	std::recursive_mutex mutable _guard;
	std::map<HardwareDataSourceMode, ISerializableSliceFilter*> _serializable_filters;
	std::map<HardwareDataSourceMode, ISliceFilter*> _filters;
	Ethernet::DataSource _ethernet_data_source;
	Serial::DataSource _serial_data_source;
	HardwareDataSourceMode _mode;
};
