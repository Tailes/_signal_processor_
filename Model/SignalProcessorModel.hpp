#pragma once
#include "..\Model\Processing\SliceProcessor.hpp"

#include <condition_variable>
#include <atomic>
#include <thread>
#include <mutex>




class SignalProcessorModel
{
public:
	SignalProcessorModel();
	~SignalProcessorModel();
	
	void start_experiment();
	void stop_experiment();
	
	void save(const std::wstring &root) const;
	void load(const std::wstring &root);
	
	const Positioner& positioner() const;
	const SliceProcessor& slice_processor() const;
	Positioner& positioner();
	SliceProcessor& slice_processor();
	
	void report_handler(const std::function<void(const std::wstring &)> &callback);
	void redraw_handler(const std::function<void()> &callback);
	void update_handler(const std::function<void()> &callback);
	void reset_handler(const std::function<void()> &callback);
	
private:
	std::recursive_mutex mutable _guard;
	std::condition_variable_any _completion;
	std::condition_variable_any _execution;
	std::function<void(const std::wstring &)> _report_handler;
	std::function<void()> _redraw_handler;
	std::function<void()> _update_handler;
	std::function<void()> _reset_handler;
	std::atomic<bool> _terminated;
	std::atomic<bool> _completed;
	std::atomic<bool> _suspended;
	std::thread _scanning_thread;
	SliceProcessor _processor;
	
	void open();
	void prepare(Slice &slice);
	void apply(Slice &slice);
	void close();
	void scanning();
};