#pragma once

#include <cstdint>

namespace utility
{
	std::uint16_t make_word(std::uint8_t low, std::uint8_t high);
	std::uint8_t get_low_byte(std::uint16_t word);
	std::uint8_t get_high_byte(std::uint16_t word);
	std::uint16_t get_low_word(std::uint32_t dword);
	std::uint16_t get_high_word(std::uint32_t dword);
	std::uint32_t make_dword(std::uint16_t low, std::uint16_t high);
	std::uint32_t make_dword(std::uint8_t low_low, std::uint8_t low_high, std::uint8_t high_low, std::uint8_t high_high);
	std::uint32_t reverse_dword(std::uint32_t dword);
	std::uintptr_t align(std::uintptr_t value, std::uintptr_t bitmask);
};
