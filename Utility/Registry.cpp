#include "..\StdAfx.hpp"
#include "..\Utility\Registry.hpp"

#include <stdexcept>
#include <vector>

#include <Windows.h>




Registry::Registry(const std::wstring &folder):
	_folder(folder)
{
}

void Registry::read_bool(const std::wstring &name, bool &value)
{
	try
	{
		value = this->read(name) == L"true\0";
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_text(const std::wstring &name, std::wstring &value)
{
	try
	{
		value = this->read(name);
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_float(const std::wstring &name, float &value)
{
	try
	{
		value = std::stof(this->read(name));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_float(const std::wstring &name, double &value)
{
	try
	{
		value = std::stod(this->read(name));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_integer(const std::wstring &name, int &value)
{
	try
	{
		value = std::stoi(this->read(name));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_integer(const std::wstring &name, char &value)
{
	try
	{
		value = static_cast<signed char>(std::stoi(this->read(name)));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_integer(const std::wstring &name, short &value)
{
	try
	{
		value = static_cast<signed short>(std::stoi(this->read(name)));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_integer(const std::wstring &name, long &value)
{
	try
	{
		value = std::stol(this->read(name));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_integer(const std::wstring &name, long long &value)
{
	try
	{
		value = std::stoll(this->read(name));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_integer(const std::wstring &name, unsigned int &value)
{
	try
	{
		value = static_cast<unsigned int>(std::stoul(this->read(name)));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_integer(const std::wstring &name, unsigned char &value)
{
	try
	{
		value = static_cast<unsigned char>(std::stoul(this->read(name)));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_integer(const std::wstring &name, unsigned short &value)
{
	try
	{
		value = static_cast<unsigned short>(std::stoul(this->read(name)));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_integer(const std::wstring &name, unsigned long &value)
{
	try
	{
		value = std::stoul(this->read(name));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::read_integer(const std::wstring &name, unsigned long long &value)
{
	try
	{
		value = std::stoull(this->read(name));
	}
	catch ( std::exception & )
	{
	}
}



void Registry::write_bool(const std::wstring &name, const bool &value)
{
	try
	{
		this->write(name, value ? L"true" : L"false");
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_text(const std::wstring &name, const std::wstring &value)
{
	try
	{
		this->write(name, value);
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_float(const std::wstring &name, const float &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_float(const std::wstring &name, const double &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_integer(const std::wstring &name, const int &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_integer(const std::wstring &name, const char &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_integer(const std::wstring &name, const short &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_integer(const std::wstring &name, const long &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_integer(const std::wstring &name, const long long &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_integer(const std::wstring &name, const unsigned int &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_integer(const std::wstring &name, const unsigned char &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_integer(const std::wstring &name, const unsigned short &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_integer(const std::wstring &name, const unsigned long &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}

void Registry::write_integer(const std::wstring &name, const unsigned long long &value)
{
	try
	{
		this->write(name, std::to_wstring(value));
	}
	catch ( std::exception & )
	{
	}
}




std::wstring Registry::read(const std::wstring &name)
{
	bool success = true;
	std::vector<wchar_t> buffer;
	HKEY key = nullptr;
	DWORD type = 0, length = 0;
	success &= ::RegOpenKeyExW(HKEY_CURRENT_USER, _folder.c_str(), 0, KEY_READ, &key) == ERROR_SUCCESS;
	success &= ::RegQueryValueExW(key, name.c_str(), nullptr, &type, nullptr, &length) == ERROR_SUCCESS;
	success &= type == REG_EXPAND_SZ && length % sizeof(wchar_t) == 0;
	buffer.resize(length /= sizeof(wchar_t));
	success &= ::RegQueryValueExW(key, name.c_str(), nullptr, &type, reinterpret_cast<BYTE*>(buffer.data()), &(length *= sizeof(wchar_t))) == ERROR_SUCCESS;
	success &= type == REG_EXPAND_SZ && length % sizeof(wchar_t) == 0 && length/sizeof(wchar_t) == buffer.size();
	success &= ::RegCloseKey(key) == ERROR_SUCCESS;
	if ( !success ) throw std::runtime_error("Cannot read registry record.");
	return std::wstring(buffer.data());
}

void Registry::write(const std::wstring &name, const std::wstring &value)
{
	bool success = true;
	std::vector<wchar_t> buffer(value.begin(), value.end()); buffer.push_back(0);
	HKEY key = nullptr;
	success &= ::RegCreateKeyExW(HKEY_CURRENT_USER, _folder.c_str(), 0, nullptr, REG_OPTION_NON_VOLATILE, KEY_WRITE, nullptr, &key, nullptr) == ERROR_SUCCESS;
	success &= ::RegSetValueExW(key, name.c_str(), 0, REG_EXPAND_SZ, reinterpret_cast<const BYTE*>(buffer.data()), sizeof(wchar_t) * buffer.size()) == ERROR_SUCCESS;
	success &= ::RegCloseKey(key) == ERROR_SUCCESS;
	if ( !success ) throw std::runtime_error("Cannot write registry record.");
}



