#pragma once
#include <string>



class Registry
{
public:
	Registry(const std::wstring &folder);
	
	void read_bool(const std::wstring &name, bool &value);
	void read_integer(const std::wstring &name, int &value);
	void read_integer(const std::wstring &name, char &value);
	void read_integer(const std::wstring &name, long &value);
	void read_integer(const std::wstring &name, short &value);
	void read_integer(const std::wstring &name, long long &value);
	void read_integer(const std::wstring &name, unsigned int &value);
	void read_integer(const std::wstring &name, unsigned char &value);
	void read_integer(const std::wstring &name, unsigned long &value);
	void read_integer(const std::wstring &name, unsigned short &value);
	void read_integer(const std::wstring &name, unsigned long long &value);
	void read_float(const std::wstring &name, float &value);
	void read_float(const std::wstring &name, double &value);
	void read_text(const std::wstring &name, std::wstring &value);
	
	void write_bool(const std::wstring &name, const bool &value);
	void write_integer(const std::wstring &name, const int &value);
	void write_integer(const std::wstring &name, const char &value);
	void write_integer(const std::wstring &name, const long &value);
	void write_integer(const std::wstring &name, const short &value);
	void write_integer(const std::wstring &name, const long long &value);
	void write_integer(const std::wstring &name, const unsigned int &value);
	void write_integer(const std::wstring &name, const unsigned char &value);
	void write_integer(const std::wstring &name, const unsigned long &value);
	void write_integer(const std::wstring &name, const unsigned short &value);
	void write_integer(const std::wstring &name, const unsigned long long &value);
	void write_float(const std::wstring &name, const float &value);
	void write_float(const std::wstring &name, const double &value);
	void write_text(const std::wstring &name, const std::wstring &value);
	
private:
	std::wstring _folder;
	
	std::wstring read(const std::wstring &name);
	void write(const std::wstring &name, const std::wstring &value);
};
