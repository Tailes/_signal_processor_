#include "..\StdAfx.hpp"
#include "..\Utility\TypeArithmetics.hpp"



namespace utility
{
	std::uint16_t make_word(std::uint8_t low, std::uint8_t high)
	{
		return (static_cast<std::uint16_t>(low) & 0x00FF) | ((static_cast<std::uint16_t>(high) & 0x00FF) << 8);
	}

	unsigned char get_low_byte(std::uint16_t word)
	{
		return static_cast<std::uint8_t>(word & 0x00FF);
	}

	unsigned char get_high_byte(std::uint16_t word)
	{
		return static_cast<std::uint8_t>((word & 0xFF00) >> 8);
	}

	std::uint16_t get_low_word(std::uint32_t dword)
	{
		return static_cast<std::uint16_t>((dword & 0x0000FFFF));
	}

	std::uint16_t get_high_word(std::uint32_t dword)
	{
		return static_cast<std::uint16_t>((dword & 0xFFFF0000) >> 16);
	}

	std::uint32_t make_dword(std::uint16_t low, std::uint16_t high)
	{
		return (static_cast<std::uint32_t>(low) & 0x0000FFFF) | ((static_cast<std::uint32_t>(high) & 0x0000FFFF) << 16);
	}

	std::uint32_t make_dword(std::uint8_t low_low, std::uint8_t low_high, std::uint8_t high_low, std::uint8_t high_high)
	{
		return make_dword(make_word(low_low, low_high), make_word(high_low, high_high));
	}

	std::uint32_t reverse_dword(std::uint32_t dword)
	{
		return make_dword(get_high_byte(get_high_word(dword)), get_low_byte(get_high_word(dword)), get_high_byte(get_low_word(dword)), get_low_byte(get_low_word(dword)));
	}

	std::uintptr_t align(std::uintptr_t value, std::uintptr_t bitmask)
	{
		return (value + bitmask) & ~ bitmask;
	}
}