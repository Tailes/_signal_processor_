#include "..\StdAfx.hpp"
#include "..\View\Resources.hpp"
#include "..\View\GeneralPanel.hpp"
#include "..\View\Processing\SliceProcessorView.hpp"
#include "..\Controller\SignalProcessorController.hpp"

#include <cvt\wstring>
#include <cvt\utf8>
#include <sstream>
#include <iomanip>
#include <cwchar>
#include <limits>
#include <array>

#include <atlwin.h>

#include "..\WTL\atlapp.h"
#include "..\WTL\atldlgs.h"




static const int CommentsBufferSize = 512;
static const int RangeStepMax = 256;
static const int RangeZeroMax = 100;
static const int RangeZeroPage = 10;
static const int RangeAmplificationMax = 9;
static const int RangeAmplificationPage = 1;
static const int RangeMagnitudeMax = 2;
static const int RangeMagnitudePage = 1;




GeneralPanel::GeneralPanel(SliceProcessorView &viewport, SignalProcessorController &controller):
	_viewport(viewport), _controller(controller)
{
	_channel_handlers.at(0) = &GeneralPanel::OnBnClickedSliceChannel<0>;
	_channel_handlers.at(1) = &GeneralPanel::OnBnClickedSliceChannel<1>;
	_channel_handlers.at(2) = &GeneralPanel::OnBnClickedSliceChannel<2>;
	_channel_handlers.at(3) = &GeneralPanel::OnBnClickedSliceChannel<3>;
}

GeneralPanel::~GeneralPanel()
{
}

void GeneralPanel::load(const std::wstring &root)
{
}

void GeneralPanel::save(const std::wstring &root) const
{
}

void GeneralPanel::update()
{
	int slices = _controller.slice_processor().slice_recording_filter().trace().count();
	float distance = static_cast<float>(_controller.slice_processor().slice_recording_filter().trace().distance());
	float minutes = 0.0f, seconds = ::modf(static_cast<float>(_controller.slice_processor().slice_recording_filter().trace().survey_time() / 60.0), &minutes) * 0.6f;
	if ( this->IsWindow() )
	{
		this->SetDlgItemInt(IDC_EDT_SIGNALS_COUNT, slices);
		this->SetDlgItemTextW(IDC_EDT_DURATION, this->experiment_duration(minutes + seconds).c_str());
		this->SetDlgItemTextW(IDC_EDT_PROFILELENGTH, this->experiment_distance(distance).c_str());
	}
}




LRESULT GeneralPanel::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	this->CheckRadioButton(IDC_BTN_VIEWSIGNALS, IDC_BTN_VIEWTRACE, IDC_BTN_VIEWSIGNALS + static_cast<int>(_viewport.mode()));
	this->CheckDlgButton(IDC_BTN_CHANNEL1, this->slice_signal_enabled(0) || this->trace_signal_enabled(0) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL2, this->slice_signal_enabled(1) || this->trace_signal_enabled(1) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL3, this->slice_signal_enabled(2) || this->trace_signal_enabled(2) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL4, this->slice_signal_enabled(3) || this->trace_signal_enabled(3) ? BST_CHECKED : BST_UNCHECKED);
	this->SetDlgItemTextW(IDC_EDT_COMMENTS, resources::string<IDS_DEFAULT_COMMENTARY>().c_str());
	this->SetDlgItemTextW(IDC_EDT_ZEROLEVEL, _T("0.00"));
	this->SetDlgItemTextW(IDC_EDT_AMPLIFICATION, _T("1.0"));
	this->SetDlgItemTextW(IDC_EDT_PROFILELENGTH, _T("0.0"));
	this->SetDlgItemTextW(IDC_EDT_DURATION, _T("0.0"));
	this->SetDlgItemInt(IDC_EDT_SIGNALS_COUNT, 0);
	
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_ZERO)).SetRange(-RangeZeroMax, RangeZeroMax, TRUE);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_ZERO)).SetPageSize(RangeZeroPage);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_ZERO)).SetPos(0);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_MAGNITUDE)).SetRange(-RangeMagnitudeMax, RangeMagnitudeMax, TRUE);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_MAGNITUDE)).SetPageSize(RangeMagnitudePage);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_MAGNITUDE)).SetPos(0);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_AMPLIFICATION)).SetRange(-RangeAmplificationMax, RangeAmplificationMax, TRUE);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_AMPLIFICATION)).SetPageSize(RangeZeroPage);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_AMPLIFICATION)).SetPos(0);
	
	CComboBoxEx(this->GetDlgItem(IDC_CMB_CHANNEL)).AddItem(resources::string<IDS_CHANNEL1>().c_str(), 0, 0, 0, 0);
	CComboBoxEx(this->GetDlgItem(IDC_CMB_CHANNEL)).AddItem(resources::string<IDS_CHANNEL2>().c_str(), 0, 0, 0, 1);
	CComboBoxEx(this->GetDlgItem(IDC_CMB_CHANNEL)).AddItem(resources::string<IDS_CHANNEL3>().c_str(), 0, 0, 0, 2);
	CComboBoxEx(this->GetDlgItem(IDC_CMB_CHANNEL)).AddItem(resources::string<IDS_CHANNEL4>().c_str(), 0, 0, 0, 3);
	CComboBoxEx(this->GetDlgItem(IDC_CMB_CHANNEL)).SetCurSel(0);
	
	return 0;
}

LRESULT GeneralPanel::OnVScroll(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	switch ( CWindow(reinterpret_cast<HWND>(lParam)).GetDlgCtrlID() )
	{
	case IDC_SLD_ZERO: return this->OnVScrollSldZeroLevel(uMsg, wParam, lParam, bHandled);
	case IDC_SLD_MAGNITUDE: return this->OnVScrollSldMagnitude(uMsg, wParam, lParam, bHandled);
	case IDC_SLD_AMPLIFICATION: return this->OnVScrollSldAmplification(uMsg, wParam, lParam, bHandled);
	};
	return 0;
}

LRESULT GeneralPanel::OnVScrollSldZeroLevel(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	float value = this->zero_level_value(CTrackBarCtrl(this->GetDlgItem(IDC_SLD_ZERO)).GetPos());
	this->SetDlgItemTextW(IDC_EDT_ZEROLEVEL, this->zero_level_text(value).c_str());
	return 0;
}

LRESULT GeneralPanel::OnVScrollSldMagnitude(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	float value = this->magnitude_value(CTrackBarCtrl(this->GetDlgItem(IDC_SLD_MAGNITUDE)).GetPos());
	int channel = CComboBoxEx(this->GetDlgItem(IDC_CMB_CHANNEL)).GetCurSel();
	_controller.slice_processor().slice_adjustment_filter().magnitude(channel, value);
	this->SetDlgItemTextW(IDC_STATIC_MAGNITUDE, this->magnitude_text(value).c_str());
	return 0;
}

LRESULT GeneralPanel::OnVScrollSldAmplification(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	float value = this->amplification_value(CTrackBarCtrl(this->GetDlgItem(IDC_SLD_AMPLIFICATION)).GetPos());
	this->SetDlgItemTextW(IDC_EDT_AMPLIFICATION, this->amplification_text(value).c_str());
	return 0;
}

LRESULT GeneralPanel::OnBnClickedBtnSubtract(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_controller.slice_processor().slice_subtraction_filter().enable(this->IsDlgButtonChecked(IDC_BTN_SUBTRACTION) == BST_CHECKED);
	return 0;
}

LRESULT GeneralPanel::OnBnClickedBtnSubtractMark(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_controller.slice_processor().slice_subtraction_filter().accept();
	return 0;
}

template<short Index>
LRESULT GeneralPanel::OnBnClickedTraceChannel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_viewport.traces().activate(this->IsDlgButtonChecked(wID) == BST_CHECKED ? Index : -1);
	_viewport.redraw();
	this->CheckDlgButton(IDC_BTN_CHANNEL1, this->trace_signal_enabled(0) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL2, this->trace_signal_enabled(1) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL3, this->trace_signal_enabled(2) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL4, this->trace_signal_enabled(3) ? BST_CHECKED : BST_UNCHECKED);
	return 0;
}

template<short Index>
LRESULT GeneralPanel::OnBnClickedSliceChannel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_viewport.signals().show(Index, this->IsDlgButtonChecked(wID) == BST_CHECKED);
	_viewport.redraw();
	this->CheckDlgButton(IDC_BTN_CHANNEL1, this->slice_signal_enabled(0) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL2, this->slice_signal_enabled(1) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL3, this->slice_signal_enabled(2) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL4, this->slice_signal_enabled(3) ? BST_CHECKED : BST_UNCHECKED);
	return 0;
}

LRESULT GeneralPanel::OnBnClickedBtnViewSignals(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_viewport.mode(ViewMode::Signals);
	_viewport.redraw();
	this->CheckRadioButton(IDC_BTN_VIEWSIGNALS, IDC_BTN_VIEWTRACE, IDC_BTN_VIEWSIGNALS);
	this->CheckDlgButton(IDC_BTN_CHANNEL1, this->slice_signal_enabled(0) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL2, this->slice_signal_enabled(1) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL3, this->slice_signal_enabled(2) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL4, this->slice_signal_enabled(3) ? BST_CHECKED : BST_UNCHECKED);
	_channel_handlers.at(0) = &GeneralPanel::OnBnClickedSliceChannel<0>;
	_channel_handlers.at(1) = &GeneralPanel::OnBnClickedSliceChannel<1>;
	_channel_handlers.at(2) = &GeneralPanel::OnBnClickedSliceChannel<2>;
	_channel_handlers.at(3) = &GeneralPanel::OnBnClickedSliceChannel<3>;
	return 0;
}

LRESULT GeneralPanel::OnBnClickedBtnViewTrace(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_viewport.mode(ViewMode::Trace);
	_viewport.redraw();
	this->CheckRadioButton(IDC_BTN_VIEWSIGNALS, IDC_BTN_VIEWTRACE, IDC_BTN_VIEWTRACE);
	this->CheckDlgButton(IDC_BTN_CHANNEL1, this->trace_signal_enabled(0) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL2, this->trace_signal_enabled(1) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL3, this->trace_signal_enabled(2) ? BST_CHECKED : BST_UNCHECKED);
	this->CheckDlgButton(IDC_BTN_CHANNEL4, this->trace_signal_enabled(3) ? BST_CHECKED : BST_UNCHECKED);
	_channel_handlers.at(0) = &GeneralPanel::OnBnClickedTraceChannel<0>;
	_channel_handlers.at(1) = &GeneralPanel::OnBnClickedTraceChannel<1>;
	_channel_handlers.at(2) = &GeneralPanel::OnBnClickedTraceChannel<2>;
	_channel_handlers.at(3) = &GeneralPanel::OnBnClickedTraceChannel<3>;
	return 0;
}

LRESULT GeneralPanel::OnBnClickedBtnChannelColor(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CColorDialog dialog(_viewport.color(CComboBoxEx(this->GetDlgItem(IDC_CMB_CHANNEL)).GetCurSel()));
	if ( dialog.DoModal() == IDOK )
	{
		_viewport.color(CComboBoxEx(this->GetDlgItem(IDC_CMB_CHANNEL)).GetCurSel(), dialog.GetColor());
	}
	return 0;
}

LRESULT GeneralPanel::OnCbnSelChangeCmbChannel(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	int channel = CComboBoxEx(this->GetDlgItem(IDC_CMB_CHANNEL)).GetCurSel();
	float zero = _controller.slice_processor().slice_adjustment_filter().zero(channel);
	float magnitude = _controller.slice_processor().slice_adjustment_filter().magnitude(channel);
	float amplification = _controller.slice_processor().slice_adjustment_filter().amplification(channel);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_ZERO)).SetPos(this->zero_level_tick(zero));
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_MAGNITUDE)).SetPos(this->magnitude_tick(magnitude));
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_AMPLIFICATION)).SetPos(this->amplification_tick(amplification));
	this->SetDlgItemTextW(IDC_EDT_ZEROLEVEL, this->zero_level_text(zero).c_str());
	this->SetDlgItemTextW(IDC_STATIC_MAGNITUDE, this->magnitude_text(magnitude).c_str());
	this->SetDlgItemTextW(IDC_EDT_AMPLIFICATION, this->amplification_text(amplification).c_str());
	return 0;
}

LRESULT GeneralPanel::OnEnChangeEdtZeroLevel(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	float value = 0.0f;
	std::size_t channel = 0;
	std::array<wchar_t, std::numeric_limits<float>::digits10> buffer = {};
	if ( (channel = CComboBoxEx(this->GetDlgItem(IDC_CMB_CHANNEL)).GetCurSel()) == -1 ) return 0;
	this->GetDlgItemTextW(IDC_EDT_ZEROLEVEL, buffer.data(), buffer.size());
	_controller.slice_processor().slice_adjustment_filter().zero(channel, value = static_cast<float>(::_wtof(buffer.data())));
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_ZERO)).SetPos(this->zero_level_tick(value));
	return 0;
}

LRESULT GeneralPanel::OnEnChangeEdtAmplification(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	float value = 0.0f;
	std::size_t channel = 0;
	std::array<wchar_t, std::numeric_limits<float>::digits10> buffer = {};
	if ( (channel = CComboBoxEx(this->GetDlgItem(IDC_CMB_CHANNEL)).GetCurSel()) == -1 ) return 0;
	this->GetDlgItemTextW(IDC_EDT_AMPLIFICATION, buffer.data(), buffer.size());
	_controller.slice_processor().slice_adjustment_filter().amplification(channel, value = static_cast<float>(::_wtof(buffer.data())));
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_AMPLIFICATION)).SetPos(this->amplification_tick(value));
	return 0;
}

LRESULT GeneralPanel::OnEnChangeEdtComments(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	std::array<char, CommentsBufferSize> ascii = {};
	::GetDlgItemTextA(*this, IDC_EDT_COMMENTS, ascii.data(), ascii.size());
	_controller.slice_processor().slice_recording_filter().trace().comments(ascii.data());
	return 0;
}

LRESULT GeneralPanel::OnEnChangeEdtProfileLength(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	std::array<wchar_t, std::numeric_limits<float>::digits10> buffer = {};
	this->GetDlgItemTextW(IDC_EDT_PROFILELENGTH, buffer.data(), buffer.size());
	_controller.slice_processor().slice_recording_filter().trace().distance(static_cast<std::float_t>(::_wtof(buffer.data())));
	return 0;
}


#pragma region Convertors
std::wstring GeneralPanel::experiment_duration(float value) const
{
	return static_cast<std::wstringstream&>(std::wstringstream() << std::fixed << std::setprecision(2) << value).str();
}

std::wstring GeneralPanel::experiment_distance(float value) const
{
	return static_cast<std::wstringstream&>(std::wstringstream() << std::fixed << std::setprecision(2) << value).str();
}

std::wstring GeneralPanel::amplification_text(float value) const
{
	return static_cast<std::wstringstream&>(std::wstringstream() << std::fixed << std::setprecision(2) << value).str();
}

std::wstring GeneralPanel::zero_level_text(float value) const
{
	return static_cast<std::wstringstream&>(std::wstringstream() << std::fixed << std::setprecision(2) << value).str();
}

std::wstring GeneralPanel::magnitude_text(float value) const
{
	return static_cast<std::wstringstream&>(std::wstringstream() << std::fixed << std::setprecision(3) << value).str();
}

float GeneralPanel::zero_level_value(int tick) const
{
	return static_cast<float>(-tick) / static_cast<float>(RangeZeroMax);
}

float GeneralPanel::magnitude_value(int tick) const
{
	return static_cast<float>(std::pow(10.0, -static_cast<float>(tick)));
}

float GeneralPanel::amplification_value(int tick) const
{
	return static_cast<float>(std::pow(std::fabs(static_cast<float>(tick)) + 1.0, (tick < 0 ? 1.0 : -1.0)));
}

int GeneralPanel::zero_level_tick(float value) const
{
	return -static_cast<int>(value * RangeZeroMax);
}

int GeneralPanel::magnitude_tick(float value) const
{
	return -static_cast<int>(::log10(value));
}

int GeneralPanel::amplification_tick(float value) const
{
	return static_cast<int>(std::nearbyint(std::copysign(1.0, value - 1.0) * (1.0 - std::pow(static_cast<double>(value), std::copysign(1.0, value - 1.0)))));
}
#pragma endregion



bool GeneralPanel::slice_signal_enabled(std::size_t value) const
{
	return _viewport.mode() == ViewMode::Signals && _controller.slice_processor().slice_signal_monitoring_filter().visible(value);
}

bool GeneralPanel::trace_signal_enabled(std::size_t value) const
{
	return _viewport.mode() == ViewMode::Trace && _controller.slice_processor().slice_trace_monitoring_filter().active() == value;
}
