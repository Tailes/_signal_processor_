#pragma once
#include "..\Resource.hpp"
#include "..\Model\Data\Slice.hpp"
#include "..\Controller\Hardware\HardwareDataSourceController.hpp"

#include "..\View\GeneralPanel.hpp"
#include "..\View\Processing\SliceProcessorView.hpp"
#include "..\View\Processing\SliceDynamicGainFilterPanel.hpp"
#include "..\View\Processing\SliceRunningAverageFilterPanel.hpp"
#include "..\View\Hardware\Serial\SerialDataSourcePanel.hpp"
#include "..\View\Hardware\Ethernet\EthernetDataSourcePanel.hpp"
#include "..\View\Hardware\Ethernet\EthernetDataSourceDelayPanel.hpp"
#include "..\View\Hardware\Ethernet\EthernetDataSourceDelayerPanel.hpp"

#include <forward_list>
#include <atomic>
#include <vector>
#include <array>
#include <map>

#include <commdlg.h>
#include <ShellAPI.h>

#include <atlbase.h>
#include <atlwin.h>

#include "..\WTL\atlapp.h"
#include "..\WTL\atlframe.h"
#include "..\WTL\atlctrls.h"
#include "..\WTL\atlctrlx.h"



class SignalProcessorController;



enum
{
	WM_BASE = WM_APP,
	WM_UPDATEVIEW,
	WM_UPDATESTATS,
	WM_RECORDINTERRUPTED,
	WM_REPORT_CODE,
	WM_REPORT_TEXT,
	WM_REPORT_WTEXT
};



class SignalProcessorView :
	public CDialogResize<SignalProcessorView>,
	public CDialogImpl<SignalProcessorView>
{
public:
	enum : unsigned long { IDD = IDD_SIGNALPROCESSOREX_DIALOG };
	
public:
	SignalProcessorView(SignalProcessorController &controller);
	~SignalProcessorView();
	
	//void create(int mode);
	void experiment_begin_handler(const std::function<void()> &callback);
	void experiment_end_handler(const std::function<void()> &callback);
	void slice_tracking_callback(std::function<void(float)> &&callback);
	//void save_handler(const std::function<void()> &callback);
	void update();
	void reset();
	
	void load(const std::wstring &root);
	void save(const std::wstring &root) const;
	
	const Ethernet::DataSourcePanel& ethernet_panel() const;
	
	Ethernet::DataSourcePanel& ethernet_panel();
	
public:
	BEGIN_DLGRESIZE_MAP(SignalProcessorView)
		DLGRESIZE_CONTROL(IDC_PCT_TRACE_VIEWPORT, DLSZ_SIZE_X | DLSZ_SIZE_Y)
		DLGRESIZE_CONTROL(IDC_PCT_SLICE_VIEWPORT, DLSZ_SIZE_X | DLSZ_SIZE_Y)
		DLGRESIZE_CONTROL(IDC_PCT_SLICE_X_AXIS, DLSZ_SIZE_X | DLSZ_MOVE_Y)
		DLGRESIZE_CONTROL(IDC_GRPBOX_VIEWPORT, DLSZ_SIZE_X | DLSZ_SIZE_Y)
		//DLGRESIZE_CONTROL(IDC_GRPBOX_DATASOURCE, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_GRPBOX_EXPERIMENT, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_GRPBOX_BLOCKS, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_EDT_ACCUMULATION, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_EDT_AVERAGING, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_SPN_ACCUMULATION, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_SPN_AVERAGING, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDD_SERIAL_DATA_SOURCE_PANEL, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDD_ETHERNET_DATA_SOURCE_PANEL, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_STATIC_AVERAGING, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_STATIC_RECORDING, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_STATIC_EXPERIMENT, DLSZ_MOVE_X)
		//DLGRESIZE_CONTROL(IDC_CMB_DATA_SOURCE, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_BTN_EXPERIMENT_START, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_BTN_RECORD_START, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_CHK_ACCUMULATION, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(Ethernet::DataSourceDelayPanel::IDD, DLSZ_SIZE_X | DLSZ_MOVE_Y)
		DLGRESIZE_CONTROL(Ethernet::DataSourceDelayerPanel::IDD, DLSZ_MOVE_X | DLSZ_MOVE_Y)
	END_DLGRESIZE_MAP()
	
protected:
	BEGIN_MSG_MAP(SignalProcessorView)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_RECORDINTERRUPTED, OnRecordInterrupted)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		COMMAND_HANDLER(IDC_BTN_START, BN_CLICKED, (this->*_command_handlers.at(IDC_BTN_START)))
		COMMAND_HANDLER(IDC_BTN_RECORD, BN_CLICKED, (this->*_command_handlers.at(IDC_BTN_RECORD)))
		COMMAND_HANDLER(IDC_CHK_ACCUMULATION, BN_CLICKED, OnBnClickedChkAccumulation)
		//COMMAND_HANDLER(IDC_CMB_DATA_SOURCE, CBN_SELCHANGE, OnCbnSelChangeCmbDataSource)
		COMMAND_HANDLER(IDC_EDT_ACCUMULATION, EN_CHANGE, OnEnChangeEdtAccumulation)
		COMMAND_HANDLER(IDC_EDT_AVERAGING, EN_CHANGE, OnEnChangeEdtAveraging)
		CHAIN_MSG_MAP(CDialogResize<SignalProcessorView>)
	END_MSG_MAP()
	
private:
	typedef LRESULT (SignalProcessorView::* CommandHandlerType)(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	
	SignalProcessorController &_controller;
	std::map<unsigned int, CommandHandlerType> _data_source_handlers;
	std::map<int, CommandHandlerType> _command_handlers;
	std::function<void()> _experiment_begin_callback;
	std::function<void()> _experiment_end_callback;
	//std::function<void()> _save_callback;
	std::atomic_flag _interrupt_required;
	std::atomic_flag _redraw_required;
	Ethernet::DataSourceDelayerPanel _ethernet_delayer_panel;
	Ethernet::DataSourceDelayPanel _ethernet_delay_panel;
	Ethernet::DataSourcePanel _ethernet_panel;
	Serial::DataSourcePanel _serial_panel;
	SliceRunningAverageFilterPanel _running_average_filter_panel;
	SliceDynamicGainFilterPanel _dynamic_gain_filter_panel;
	SliceProcessorView _viewport;
	GeneralPanel _general;
	
	void destroy_overlay_panels();
	void create_overlay_panels(CWindow &general, CWindow &running_avg, CWindow &dyn_gain, CWindow &control, CWindow &delay, CWindow &auto_delay, CWindow &statusbar);
	void create_file_names(std::array<std::wstring, std::tuple_size<Batch>::value> &names, const std::wstring &dir, const std::wstring &pattern, const std::wstring &extension) const;

	LRESULT OnRecordInterrupted(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedChkAccumulation(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedBtnExperimentStart(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedBtnExperimentStop(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedBtnRecordStart(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedBtnRecordStop(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnCbnSelChangeCmbDataSourceEthernet(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnCbnSelChangeCmbDataSourceSerial(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/); 
	LRESULT OnCbnSelChangeCmbDataSource(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnEnChangeEdtAccumulation(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnEnChangeEdtAveraging(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	
	void on_slice_averaging_amount(std::size_t amount);
	void on_ethernet_trigger_mode(Ethernet::TriggerMode mode);
	void on_tick_length(std::float_t length);

	void on_experiment_begin();
	void on_experiment_end();
	void on_redraw();
	void on_update();
};
