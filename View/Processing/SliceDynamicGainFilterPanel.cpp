#include "..\..\StdAfx.hpp"
#include "..\..\View\Resources.hpp"
#include "..\..\View\GeneralPanel.hpp"
#include "..\..\View\Processing\SliceDynamicGainFilterPanel.hpp"
#include "..\..\Controller\Processing\SliceDynamicGainFilterController.hpp"

#include <cvt\wstring>
#include <cvt\utf8>
#include <sstream>
#include <iomanip>
#include <cwchar>
#include <limits>
#include <array>

#include <atlwin.h>

#include "..\..\WTL\atlapp.h"
#include "..\..\WTL\atldlgs.h"


static const int RangeGainMin = 0;
static const int RangeGainMax = 150;
static const int RangeGainStep = 100;
static const int RangeGainPage = 10;



SliceDynamicGainFilterPanel::SliceDynamicGainFilterPanel(SliceDynamicGainFilterController &controller):
	_controller(controller)
{
	_controller.amount_callback([this](float amount) { this->on_slice_dynamic_gain_amount(amount); });

}

SliceDynamicGainFilterPanel::~SliceDynamicGainFilterPanel()
{
}

void SliceDynamicGainFilterPanel::load(const std::wstring &root)
{
}

void SliceDynamicGainFilterPanel::save(const std::wstring &root) const
{
}

void SliceDynamicGainFilterPanel::update()
{
	if (this->IsWindow())
	{
		this->SetDlgItemTextW(IDC_DYNAMIC_GAIN_AMOUNT, this->dynamic_gain_text(_controller.amount()).c_str());
	}
}




LRESULT SliceDynamicGainFilterPanel::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	this->SetDlgItemTextW(IDC_DYNAMIC_GAIN_AMOUNT, this->dynamic_gain_text(_controller.amount()).c_str());
	
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_DYNAMIC_GAIN_AMOUNT)).SetRange(RangeGainMin, RangeGainMax, TRUE);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_DYNAMIC_GAIN_AMOUNT)).SetPageSize(RangeGainPage);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_DYNAMIC_GAIN_AMOUNT)).SetPos(RangeGainMin);
	
	return 0;
}

LRESULT SliceDynamicGainFilterPanel::OnHScroll(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	switch ( CWindow(reinterpret_cast<HWND>(lParam)).GetDlgCtrlID() )
	{
	case IDC_SLD_DYNAMIC_GAIN_AMOUNT: return this->OnHScrollSldAmount(uMsg, wParam, lParam, bHandled);
	};
	return 0;
}

LRESULT SliceDynamicGainFilterPanel::OnHScrollSldAmount(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	_controller.amount(this->dynamic_gain_value(CTrackBarCtrl(this->GetDlgItem(IDC_SLD_DYNAMIC_GAIN_AMOUNT)).GetPos()));
	this->SetDlgItemTextW(IDC_DYNAMIC_GAIN_AMOUNT, this->dynamic_gain_text(_controller.amount()).c_str());
	return 0;
}

std::wstring SliceDynamicGainFilterPanel::dynamic_gain_text(float value) const
{
	return static_cast<std::wstringstream&>(std::wstringstream() << std::fixed << std::setprecision(2) << value).str();
}

float SliceDynamicGainFilterPanel::dynamic_gain_value(int tick) const
{
	return static_cast<float>(tick) / static_cast<float>(RangeGainStep);
}

int SliceDynamicGainFilterPanel::dynamic_gain_tick(float value) const
{
	return static_cast<int>(value * RangeGainStep);
}

void SliceDynamicGainFilterPanel::on_slice_dynamic_gain_amount(float amount)
{
	if (this->dynamic_gain_value(CTrackBarCtrl(this->GetDlgItem(IDC_SLD_DYNAMIC_GAIN_AMOUNT)).GetPos()) != amount)
	{
		this->SetDlgItemTextW(IDC_DYNAMIC_GAIN_AMOUNT, this->dynamic_gain_text(amount).c_str());
	}
}




