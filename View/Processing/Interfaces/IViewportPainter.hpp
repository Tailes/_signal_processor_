#pragma once


namespace Color { class rgba; }



struct IViewportPainter
{
	virtual void color(std::size_t index, const Color::rgba &color) = 0;
	virtual void redraw() = 0;
	virtual void show(bool visible) = 0;
	virtual Color::rgba color(std::size_t index) const = 0;
};
