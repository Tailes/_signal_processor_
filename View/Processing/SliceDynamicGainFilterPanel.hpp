#pragma once
#include "..\..\Resource.hpp"

#include <algorithm>
#include <string>
#include <array>
using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\WTL\atlapp.h"
#include "..\..\WTL\atlframe.h"
#include "..\..\WTL\atlctrls.h"



class SliceDynamicGainFilterController;



class SliceDynamicGainFilterPanel :
	public CDialogImpl<SliceDynamicGainFilterPanel>
{
public:
	enum : unsigned long { IDD = IDD_DYNAMIC_GAIN_FILTER_PANEL };
	
public:
	SliceDynamicGainFilterPanel(SliceDynamicGainFilterController &controller);
	~SliceDynamicGainFilterPanel();
	
	void load(const std::wstring &root);
	void save(const std::wstring &root) const;
	
	void update();
	
protected:
	BEGIN_MSG_MAP(SliceDynamicGainFilterPanel)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_HSCROLL, OnHScroll)
	END_MSG_MAP()
	
private:
	SliceDynamicGainFilterController &_controller;
	
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnHScroll(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnHScrollSldAmount(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	
	void on_slice_dynamic_gain_amount(float amount);
	
	std::wstring dynamic_gain_text(float value) const;
	float dynamic_gain_value(int tick) const;
	int dynamic_gain_tick(float value) const;
};
