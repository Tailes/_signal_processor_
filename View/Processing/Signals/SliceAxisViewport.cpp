#include "..\..\..\StdAfx.hpp"
#include "..\..\..\View\Utilities.hpp"
#include "..\..\..\View\Processing\Signals\SliceAxisViewport.hpp"
#include "..\..\..\Controller\Processing\SliceSignalMonitoringFilterController.hpp"



#include "..\..\..\WTL\atlmisc.h"



SliceAxisViewport::SliceAxisViewport(SliceSignalMonitoringFilterController &controller):
	_controller(controller)
{
}

void SliceAxisViewport::show(bool visible)
{
	this->ShowWindow(visible ? SW_SHOW : SW_HIDE);
}

void SliceAxisViewport::redraw()
{
	this->RedrawWindow();
}


LRESULT SliceAxisViewport::OnWindowPosChanging(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT SliceAxisViewport::OnWindowPosChanged(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT SliceAxisViewport::OnNcPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT SliceAxisViewport::OnPaint(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	RECT rect = {0};
	this->GetClientRect(&rect);
	if ( wParam != NULL )
	{
		_controller.draw_axis(CDCHandle(CMemoryDC((HDC)wParam, rect)), rect);
	}
	else
	{
		_controller.draw_axis(CDCHandle(CMemoryDC(CPaintDC(*this), rect)), rect);
	}
	return 0;
}

LRESULT SliceAxisViewport::OnEraseBackground(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return TRUE;
}


