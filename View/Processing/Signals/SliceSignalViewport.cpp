#include "..\..\..\StdAfx.hpp"
#include "..\..\..\View\Utilities.hpp"
#include "..\..\..\View\Processing\Signals\SliceSignalViewport.hpp"
#include "..\..\..\Controller\Processing\SliceSignalMonitoringFilterController.hpp"



#include "..\..\..\WTL\atlmisc.h"



SliceSignalViewport::SliceSignalViewport(SliceSignalMonitoringFilterController &controller):
	_controller(controller)
{
}

void SliceSignalViewport::show(bool visible)
{
	this->ShowWindow(visible ? SW_SHOW : SW_HIDE);
}

void SliceSignalViewport::redraw()
{
	this->RedrawWindow();
}

void SliceSignalViewport::show(std::size_t index, bool visible)
{
	_controller.show(index, visible);
}

void SliceSignalViewport::color(std::size_t index, COLORREF color)
{
	_controller.color(index, color);
}

COLORREF SliceSignalViewport::color(std::size_t index) const
{
	return _controller.color(index);
}




LRESULT SliceSignalViewport::OnNcHitTest(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return HTCLIENT;
}

LRESULT SliceSignalViewport::OnMouseMove(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	RECT rect = {0};
	this->GetClientRect(&rect);
	_controller.track(CDCHandle(CClientDC(*this)), rect, CPoint(lParam));
	return 0;
}

LRESULT SliceSignalViewport::OnWindowPosChanging(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT SliceSignalViewport::OnWindowPosChanged(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT SliceSignalViewport::OnNcPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT SliceSignalViewport::OnPaint(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	RECT rect = {0};
	this->GetClientRect(&rect);
	if ( wParam != NULL )
	{
		_controller.draw_signals(CDCHandle(CMemoryDC((HDC)wParam, rect)), rect);
	}
	else
	{
		_controller.draw_signals(CDCHandle(CMemoryDC(CPaintDC(*this), rect)), rect);
	}
	return 0;
}

LRESULT SliceSignalViewport::OnEraseBackground(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return TRUE;
}


