#include "..\..\StdAfx.hpp"
#include "..\..\View\Resources.hpp"
#include "..\..\View\GeneralPanel.hpp"
#include "..\..\View\Processing\SliceRunningAverageFilterPanel.hpp"
#include "..\..\Controller\Processing\SliceRunningAverageFilterController.hpp"

#include <cvt\wstring>
#include <cvt\utf8>
#include <sstream>
#include <iomanip>
#include <cwchar>
#include <limits>
#include <array>

#include <atlwin.h>

#include "..\..\WTL\atlapp.h"
#include "..\..\WTL\atldlgs.h"


static const int AveragingAmountMin = 1;
static const int AveragingAmountMax = 15;
static const int AveragingAmountPage = 1;



SliceRunningAverageFilterPanel::SliceRunningAverageFilterPanel(SliceRunningAverageFilterController &controller):
	_controller(controller)
{
	_controller.amount_callback([this](std::size_t amount) { this->on_slice_running_average_amount(amount); });

}

SliceRunningAverageFilterPanel::~SliceRunningAverageFilterPanel()
{
}

void SliceRunningAverageFilterPanel::load(const std::wstring &root)
{
}

void SliceRunningAverageFilterPanel::save(const std::wstring &root) const
{
}

void SliceRunningAverageFilterPanel::update()
{
	if (this->IsWindow())
	{
		this->SetDlgItemInt(IDC_RUNNING_AVERAGE_AMOUNT, _controller.amount());
	}
}




LRESULT SliceRunningAverageFilterPanel::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	this->SetDlgItemInt(IDC_RUNNING_AVERAGE_AMOUNT, _controller.amount());
	
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_RUNNIG_AVERAGE_AMOUNT)).SetRange(AveragingAmountMin, AveragingAmountMax, TRUE);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_RUNNIG_AVERAGE_AMOUNT)).SetPageSize(AveragingAmountPage);
	CTrackBarCtrl(this->GetDlgItem(IDC_SLD_RUNNIG_AVERAGE_AMOUNT)).SetPos(AveragingAmountMin);
	
	return 0;
}

LRESULT SliceRunningAverageFilterPanel::OnHScroll(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	switch ( CWindow(reinterpret_cast<HWND>(lParam)).GetDlgCtrlID() )
	{
	case IDC_SLD_RUNNIG_AVERAGE_AMOUNT: return this->OnHScrollSldAmount(uMsg, wParam, lParam, bHandled);
	};
	return 0;
}

LRESULT SliceRunningAverageFilterPanel::OnHScrollSldAmount(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	std::size_t value = static_cast<std::size_t>(CTrackBarCtrl(this->GetDlgItem(IDC_SLD_RUNNIG_AVERAGE_AMOUNT)).GetPos());
	_controller.amount(value);
	return 0;
}

void SliceRunningAverageFilterPanel::on_slice_running_average_amount(std::size_t amount)
{
	if (this->GetDlgItemInt(IDC_RUNNING_AVERAGE_AMOUNT, nullptr, FALSE) != amount)
	{
		this->SetDlgItemInt(IDC_RUNNING_AVERAGE_AMOUNT, amount, FALSE);
	}
}




