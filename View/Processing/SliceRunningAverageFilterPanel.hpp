#pragma once
#include "..\..\Resource.hpp"

#include <algorithm>
#include <string>
#include <array>
using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\WTL\atlapp.h"
#include "..\..\WTL\atlframe.h"
#include "..\..\WTL\atlctrls.h"



class SliceRunningAverageFilterController;



class SliceRunningAverageFilterPanel :
	public CDialogImpl<SliceRunningAverageFilterPanel>
{
public:
	enum : unsigned long { IDD = IDD_RUNNING_AVERAGE_FILTER_PANEL };
	
public:
	SliceRunningAverageFilterPanel(SliceRunningAverageFilterController &controller);
	~SliceRunningAverageFilterPanel();
	
	void load(const std::wstring &root);
	void save(const std::wstring &root) const;
	
	void update();
	
protected:
	BEGIN_MSG_MAP(SliceRunningAverageFilterPanel)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_HSCROLL, OnHScroll)
	END_MSG_MAP()
	
private:
	SliceRunningAverageFilterController &_controller;
	
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnHScroll(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnHScrollSldAmount(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	
	void on_slice_running_average_amount(std::size_t amount);
};
