#pragma once
//#include "..\Engine\RawBitmap.hpp"
#include "..\..\View\Processing\Interfaces\IViewportPainter.hpp"


#include <algorithm>
#include <atomic>
#include <thread>
#include <mutex>
#include <map>

using std::min;
using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\WTL\atlapp.h"
#include "..\..\WTL\atlgdi.h"
#include "..\..\WTL\atlctrls.h"
#include "..\..\WTL\atlframe.h"



class SliceTraceMonitoringFilterController;



class SliceTraceView :
	public IViewportPainter,
	public CWindowImpl<SliceTraceView>,
	public CDoubleBufferImpl<SliceTraceView>
{
public:
	SliceTraceView(SliceTraceMonitoringFilterController &controller);
	
	virtual void color(std::size_t index, const Color::rgba &color);
	virtual void redraw();
	virtual void show(bool visible);
	virtual Color::rgba color(std::size_t index) const;
	
	void activate(std::size_t index);
	void attach(CWindow viewport);
	void detach();
	
private:
	BEGIN_MSG_MAP(SliceProcessorView)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_NCPAINT, OnNcPaint)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
		MESSAGE_HANDLER(WM_PRINTCLIENT, OnPaint)
		MESSAGE_HANDLER(WM_WINDOWPOSCHANGED, OnWindowPosChanged)
		MESSAGE_HANDLER(WM_WINDOWPOSCHANGING, OnWindowPosChanging)
	END_MSG_MAP()
	
private:
	SliceTraceMonitoringFilterController &_controller;
	
	LRESULT OnPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnNcPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnEraseBackground(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnWindowPosChanged(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnWindowPosChanging(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
};

