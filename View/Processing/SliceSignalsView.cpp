#include "..\..\StdAfx.hpp"
#include "..\..\View\Utilities.hpp"
#include "..\..\View\Processing\SliceSignalsView.hpp"
#include "..\..\Controller\Processing\SliceProcessorController.hpp"



#include "..\..\WTL\atlmisc.h"



SliceSignalsView::SliceSignalsView(SliceSignalMonitoringFilterController &controller):
	_controller(controller), _signals(controller), _axis(controller)
{
}

void SliceSignalsView::color(std::size_t index, const Color::rgba &color)
{
	_signals.color(index, color);
}

void SliceSignalsView::redraw()
{
	_signals.redraw();
	_axis.redraw();
}

Color::rgba SliceSignalsView::color(std::size_t index) const
{
	return _signals.color(index);
}



void SliceSignalsView::show(std::size_t index, bool visible)
{
	_signals.show(index, visible);
}


void SliceSignalsView::show(bool visible)
{
	_signals.show(visible);
	_axis.show(visible);
}

void SliceSignalsView::detach()
{
	_signals.UnsubclassWindow();
	_axis.UnsubclassWindow();
}

void SliceSignalsView::attach(CWindow signals, CWindow axis)
{
	_signals.SubclassWindow(signals);
	_axis.SubclassWindow(axis);
}

void SliceSignalsView::tracking_callback(std::function<void(float)> &&callback)
{
	_controller.tracking_callback(std::forward<std::function<void(float)>>(callback));
}





SliceAxisViewport& SliceSignalsView::axis()
{
	return _axis;
}

SliceSignalViewport& SliceSignalsView::signals()
{
	return _signals;
}

const SliceAxisViewport& SliceSignalsView::axis() const
{
	return _axis;
}
const SliceSignalViewport& SliceSignalsView::signals() const
{
	return _signals;
}
