#include "..\..\StdAfx.hpp"
#include "..\..\View\Utilities.hpp"
#include "..\..\View\Processing\SliceProcessorView.hpp"
#include "..\..\Controller\Processing\SliceProcessorController.hpp"



#include "..\..\WTL\atlmisc.h"



SliceProcessorView::SliceProcessorView(SliceProcessorController &controller):
	_mode(ViewMode::Signals), _traces(controller.slice_trace_monitoring_filter()), _signals(controller.slice_signal_monitoring_filter())
{
	_viewers[ViewMode::Signals] = &_signals;
	_viewers[ViewMode::Trace] = &_traces;
}



void SliceProcessorView::attach(CWindow signals, CWindow axis, CWindow traces)
{
	_signals.attach(signals, axis);
	_traces.attach(traces);
}

void SliceProcessorView::detach()
{
	_signals.detach();
	_traces.detach();
}

void SliceProcessorView::redraw()
{
	_viewers.at(_mode)->redraw();
}

void SliceProcessorView::color(std::size_t index, COLORREF color)
{
	_signals.color(index, color);
	_traces.color(index, color);
}

void SliceProcessorView::mode(ViewMode mode)
{
	_viewers.at(_mode)->show(false);
	_viewers.at(_mode = mode)->show(true);
}

ViewMode SliceProcessorView::mode() const
{
	return _mode;
}



void SliceProcessorView::slice_tracking_callback(std::function<void(float)> &&callback)
{
	_signals.tracking_callback(std::forward<std::function<void(float)>>(callback));
}

COLORREF SliceProcessorView::color(std::size_t index) const
{
	return _viewers.at(_mode)->color(index);
}





SliceTraceView& SliceProcessorView::traces()
{
	return _traces;
}

SliceSignalsView& SliceProcessorView::signals()
{
	return _signals;
}

const SliceTraceView& SliceProcessorView::traces() const
{
	return _traces;
}

const SliceSignalsView& SliceProcessorView::signals() const
{
	return _signals;
}