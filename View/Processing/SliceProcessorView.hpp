#pragma once
//#include "..\Engine\RawBitmap.hpp"
#include "..\..\View\Processing\Interfaces\IViewportPainter.hpp"
#include "..\..\View\Processing\SliceSignalsView.hpp"
#include "..\..\View\Processing\SliceTraceView.hpp"

#include <functional>
#include <algorithm>
#include <atomic>
#include <thread>
#include <mutex>
#include <map>

using std::min;
using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\WTL\atlapp.h"
#include "..\..\WTL\atlgdi.h"
#include "..\..\WTL\atlctrls.h"
#include "..\..\WTL\atlframe.h"



class SliceProcessorController;


enum class ViewMode : unsigned int
{
	Signals = 0,
	Trace = 1,
	Total,
};



class SliceProcessorView
{
public:
	SliceProcessorView(SliceProcessorController &controller);
	
	ViewMode mode() const;
	
	void attach(CWindow signals, CWindow axis, CWindow traces);
	void detach();
	void redraw();
	void color(std::size_t index, COLORREF color);
	void mode(ViewMode mode);
	
	void slice_tracking_callback(std::function<void(float)> &&callback);
	
	COLORREF color(std::size_t index) const;
	
	SliceTraceView& traces();
	SliceSignalsView& signals();
	
	const SliceTraceView& traces() const;
	const SliceSignalsView& signals() const;
	
private:
	std::map<ViewMode, IViewportPainter*> _viewers;
	SliceSignalsView _signals;
	SliceTraceView _traces;
	ViewMode _mode;
};

