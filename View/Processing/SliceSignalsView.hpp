#pragma once
#include "..\..\View\Processing\Signals\SliceAxisViewport.hpp"
#include "..\..\View\Processing\Signals\SliceSignalViewport.hpp"
#include "..\..\View\Processing\Interfaces\IViewportPainter.hpp"

#include <functional>
#include <algorithm>

using std::min;
using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\WTL\atlapp.h"
#include "..\..\WTL\atlgdi.h"
#include "..\..\WTL\atlctrls.h"
#include "..\..\WTL\atlframe.h"



class SliceSignalMonitoringFilterController;



class SliceSignalsView:
	public IViewportPainter
{
public:
	SliceSignalsView(SliceSignalMonitoringFilterController &controller);
	
	virtual void redraw();
	virtual void color(std::size_t index, const Color::rgba &color);
	virtual void show(bool visible);
	virtual Color::rgba color(std::size_t index) const;
	
	void tracking_callback(std::function<void(float)> &&callback);
	void attach(CWindow signals, CWindow axis);
	void detach();
	void show(std::size_t index, bool visible);
	
	SliceAxisViewport& axis();
	SliceSignalViewport& signals();
	
	const SliceAxisViewport& axis() const;
	const SliceSignalViewport& signals() const;
	
private:
	SliceSignalMonitoringFilterController &_controller;
	SliceSignalViewport _signals;
	SliceAxisViewport _axis;
};

