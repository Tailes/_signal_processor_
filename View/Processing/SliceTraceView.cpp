#include "..\..\StdAfx.hpp"
#include "..\..\View\Utilities.hpp"
#include "..\..\View\Processing\SliceTraceView.hpp"
#include "..\..\Controller\Processing\SliceProcessorController.hpp"



#include "..\..\WTL\atlmisc.h"




SliceTraceView::SliceTraceView(SliceTraceMonitoringFilterController &controller):
	_controller(controller)
{
}


void SliceTraceView::color(std::size_t index, const Color::rgba &color)
{
	_controller.color(index, color);
}

void SliceTraceView::redraw()
{
	this->RedrawWindow();
}

Color::rgba SliceTraceView::color(std::size_t index) const
{
	return _controller.color(index);
}



void SliceTraceView::activate(std::size_t index)
{
	_controller.activate(index);
}

void SliceTraceView::show(bool visible)
{
	this->ShowWindow(visible ? SW_SHOW : SW_HIDE);
}

void SliceTraceView::detach()
{
	this->UnsubclassWindow();
}

void SliceTraceView::attach(CWindow viewport)
{
	this->SubclassWindow(viewport);
}



LRESULT SliceTraceView::OnWindowPosChanging(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT SliceTraceView::OnWindowPosChanged(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT SliceTraceView::OnNcPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT SliceTraceView::OnPaint(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	RECT rect = { 0 };
	this->GetClientRect(&rect);
	if(wParam != NULL)
	{
		_controller.draw(CDCHandle(CMemoryDC((HDC)wParam, rect)), rect);
	}
	else
	{
		_controller.draw(CDCHandle(CMemoryDC(CPaintDC(*this), rect)), rect);
	}
	return 0;
}

LRESULT SliceTraceView::OnEraseBackground(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return TRUE;
}




