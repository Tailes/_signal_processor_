#pragma once
#include "..\Resource.hpp"

#include <algorithm>
#include <string>
#include <array>
using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\WTL\atlapp.h"
#include "..\WTL\atlframe.h"
#include "..\WTL\atlctrls.h"



class SignalProcessorController;
class SliceProcessorView;



static const int ChannelButtonsCount = 4;



class GeneralPanel:
	public CDialogImpl<GeneralPanel>
{
public:
	enum : unsigned long { IDD = IDD_GENERAL_PANEL };
	
public:
	GeneralPanel(SliceProcessorView &viewport, SignalProcessorController &controller);
	~GeneralPanel();
	
	void load(const std::wstring &root);
	void save(const std::wstring &root) const;
	
	void update();
	
protected:
	BEGIN_MSG_MAP(GeneralPanel)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_VSCROLL, OnVScroll)
		COMMAND_HANDLER(IDC_BTN_VIEWTRACE, BN_CLICKED, OnBnClickedBtnViewTrace)
		COMMAND_HANDLER(IDC_BTN_VIEWSIGNALS, BN_CLICKED, OnBnClickedBtnViewSignals)
		COMMAND_HANDLER(IDC_BTN_CHANNEL1, BN_CLICKED, (this->*_channel_handlers.at(0)))
		COMMAND_HANDLER(IDC_BTN_CHANNEL2, BN_CLICKED, (this->*_channel_handlers.at(1)))
		COMMAND_HANDLER(IDC_BTN_CHANNEL3, BN_CLICKED, (this->*_channel_handlers.at(2)))
		COMMAND_HANDLER(IDC_BTN_CHANNEL4, BN_CLICKED, (this->*_channel_handlers.at(3)))
		COMMAND_HANDLER(IDC_BTN_SUBTRACTION_MARK, BN_CLICKED, OnBnClickedBtnSubtractMark)
		COMMAND_HANDLER(IDC_BTN_SUBTRACTION, BN_CLICKED, OnBnClickedBtnSubtract)
		COMMAND_HANDLER(IDC_BTN_CHANNEL_COLOR, BN_CLICKED, OnBnClickedBtnChannelColor)
		COMMAND_HANDLER(IDC_CMB_CHANNEL, CBN_SELCHANGE, OnCbnSelChangeCmbChannel)
		COMMAND_HANDLER(IDC_EDT_AMPLIFICATION, EN_CHANGE, OnEnChangeEdtAmplification)
		COMMAND_HANDLER(IDC_EDT_ZEROLEVEL, EN_CHANGE, OnEnChangeEdtZeroLevel)
		COMMAND_HANDLER(IDC_EDT_COMMENTS, EN_CHANGE, OnEnChangeEdtComments)
	END_MSG_MAP()
	
private:
	SignalProcessorController &_controller;
	SliceProcessorView &_viewport;
	std::array<LRESULT(GeneralPanel::*)(WORD, WORD, HWND, BOOL&), ChannelButtonsCount> _channel_handlers;
	
	template<short Index> LRESULT OnBnClickedTraceChannel(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	template<short Index> LRESULT OnBnClickedSliceChannel(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnVScroll(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnVScrollSldZeroLevel(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnVScrollSldMagnitude(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnVScrollSldAmplification(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedBtnChannelColor(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedBtnSubtract(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedBtnSubtractMark(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedBtnViewSignals(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedBtnViewTrace(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnCbnSelChangeCmbChannel(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnEnChangeEdtZeroLevel(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnEnChangeEdtAmplification(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnEnChangeEdtComments(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnEnChangeEdtProfileLength(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	
	std::wstring experiment_duration(float value) const;
	std::wstring experiment_distance(float value) const;
	std::wstring amplification_text(float value) const;
	std::wstring zero_level_text(float value) const;
	std::wstring magnitude_text(float value) const;
	float amplification_value(int tick) const;
	float zero_level_value(int tick) const;
	float magnitude_value(int tick) const;
	int amplification_tick(float value) const;
	int zero_level_tick(float value) const;
	int magnitude_tick(float value) const;
	
	bool slice_signal_enabled(std::size_t value) const;
	bool trace_signal_enabled(std::size_t value) const;
};
