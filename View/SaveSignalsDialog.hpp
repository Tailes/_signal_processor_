#pragma once
#include "..\Model\Data\Slice.hpp"

#include <array>
#include <string>

#include <atlbase.h>

#include "..\WTL\atlapp.h"
#include "..\WTL\atldlgs.h"



class SignalsDialogBase
{
protected:
	SignalsDialogBase();
	
protected:
	const std::wstring _default_filter;
	const std::wstring _default_name;
	const std::wstring _default_ext;
	
private:
	static std::wstring default_filter();
	static std::wstring default_name();
	static std::wstring default_ext();
};


class SaveSignalsDialog:
	private SignalsDialogBase,
	public CFileDialogImpl<SaveSignalsDialog>
{
public:
	SaveSignalsDialog(HWND parent);
	
	BOOL OnFileOK(LPOFNOTIFY /*lpon*/);
	
	const std::array<std::wstring, std::tuple_size<Batch>::value>& names() const;
	const std::wstring& folder() const;
	
private:
	std::array<std::wstring, std::tuple_size<Batch>::value> _names;
	std::wstring _folder;
};
