#pragma once

#include <algorithm>
#include <array>
#include <map>

using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>


#include "..\WTL\atlapp.h"
#include "..\WTL\atlctrls.h"



class StatusBar:
	public CWindowImpl<StatusBar, CStatusBarCtrl>
{
private:
	enum class Severity: int { None, Error, Total };
	enum class Charge: int { Depleted, Quarter, Half, ThreeQuarters, Full, Unknown, Total };
	enum Sections: int { Status, Signals, Position, Viewer, Battery, Total };
	
	class Panel;
	
public:
	StatusBar();
	~StatusBar();
	
	HWND create(HWND parent);
	
	void report_signal_number(std::size_t number);
	void report_information(const std::wstring &message);
	void report_signal_time(const float time);
	void report_tracking(unsigned long tick_count, unsigned long queue_length, bool wrong_direction);
	void report_error(const std::wstring &message);
	void battery(float charge, float voltage, float current);
	
public:
	BEGIN_MSG_MAP(StatusBar)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
	END_MSG_MAP()
	
private:
	std::map<Severity, CIcon> _severities;
	std::map<Charge, CIcon> _charges;
	
	const int _gripper_width;
	const int _icon_height;
	const int _icon_width;
	const int _edge_width;
	
	LRESULT OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	
	std::wstring format_battery_status(float charge, float voltage, float current) const;
	std::wstring format_signal_number(std::size_t number) const;
	std::wstring format_signal_time(const float time) const;
	std::wstring format_position(long tick_count, unsigned long queue_length, bool wrong_direction) const;
	std::wstring format_status(const std::wstring &message) const;
};



class StatusBar::Panel
{
public:
	Panel(StatusBar::Sections section);
	
	int measure(StatusBar &owner) const;
	
	void text(StatusBar &owner, const std::wstring &text)  const;
	void icon(StatusBar &owner, HICON icon) const;
	
private:
	int _index;
	
	int measure(StatusBar &owner, CDC &dc, CFontHandle font) const;
};
