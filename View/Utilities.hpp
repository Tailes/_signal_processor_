#pragma once

#include <string>
#include <vector>

#include <atlbase.h>

#include "..\WTL\atlapp.h"
#include "..\WTL\atlgdi.h"
#include "..\WTL\atlctrls.h"



class rgba;
class Signal;
class bitmap;
class gradient;


void makeBitmapRectangle(CButton button, COLORREF color);

template <class T> T getComboBoxSelectionData(const CComboBox &source);
template <class T> T* getComboBoxSelectionDataPtr(const CComboBox &source);
template <class T> T limit(T source, T lower, T higher);

namespace utility
{
	void select_combo_box_item(CComboBox &control, const std::wstring &value);
	void select_combo_box_item(CComboBox &control, unsigned long long value);
	void select_combo_box_item(CComboBox &control, unsigned long long value, unsigned long index);
	
	int get_window_text_width(CWindow &wnd, const std::wstring &text);
	int get_window_text_width(CWindow &wnd, const std::wstring &text);
	int get_text_width(CDC &dc, CFontHandle font, const std::wstring &text);

	float get_dialog_item_float(CWindow &dialog, unsigned long id);
	void set_dialog_item_float(CWindow &dialog, unsigned long id, float value, std::streamsize precision);

	wchar_t* format_file_name(wchar_t *buffer, std::size_t size, const std::wstring &format);
	
	std::wstring get_hex(unsigned int value);
	std::wstring get_double(double value, std::streamsize precision = 2);
	std::wstring get_decimal(unsigned int value);
	std::wstring load_string(unsigned int id);

	std::wstring format_double(double value, std::streamsize precision = 2);
}

template < class T >
T getComboBoxSelectionData(const CComboBox &source)
{
	return static_cast<T>(source.GetItemData(source.GetCurSel()));
}


template < class T >
T* getComboBoxSelectionDataPtr(const CComboBox &source)
{
	return static_cast<T*>(source.GetItemDataPtr(source.GetCurSel()));
}


template < class T > T limit(T source, T lower, T higher)
{
	return std::min<T>(higher, std::max<T>(source, lower));
}