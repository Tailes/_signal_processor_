#include "..\StdAfx.hpp"
#include "..\Model\Data\Signal.hpp"
#include "..\View\Utilities.hpp"
#include "..\Utility\TypeArithmetics.hpp"

#include <iomanip>
#include <sstream>
#include <string>
#include <array>

#include <atltypes.h>



enum : unsigned long
{
	IconWidth = 20,
	IconHeight = 15
};

enum : unsigned long
{
	StringBufferSize = 64
};




void makeBitmapRectangle(CButton button, COLORREF color)
{
	CDC dc = ::CreateCompatibleDC(CWindowDC(button));
	CPen pen = dc.SelectPen(::CreatePen(PS_SOLID, 1, RGB(0,0,0)));
	CBrush brush = dc.SelectBrush(::CreateSolidBrush(color));
	CBitmap bitmap = dc.SelectBitmap(::CreateCompatibleBitmap(CWindowDC(button), IconWidth, IconHeight));
	dc.Rectangle(0, 0, IconWidth, IconHeight);
	bitmap.Attach(dc.SelectBitmap(bitmap.Detach()));
	brush.Attach(dc.SelectBrush(brush.Detach()));
	pen.Attach(dc.SelectPen(pen.Detach()));
	button.SetBitmap(bitmap);
}

namespace utility
{
	void select_combo_box_item(CComboBox &control, const std::wstring &value)
	{
		for (int i = 0; i < control.GetCount(); ++i)
		{
			if (value.compare(static_cast<wchar_t*>(control.GetItemDataPtr(i))) == 0)
			{
				control.SetCurSel(i);
				break;
			}
		}
	}


	void select_combo_box_item(CComboBox &control, unsigned long long value)
	{
		for (int i = 0; i < control.GetCount(); ++i)
		{
			if (control.GetItemData(i) == value)
			{
				control.SetCurSel(i);
				break;
			}
		}
	}


	void select_combo_box_item(CComboBox &control, unsigned long long value, unsigned long index)
	{
		for (int i = 0; i < control.GetCount(); ++i)
		{
			if (control.GetItemData(i) == value)
			{
				control.SetCurSel(i);
				return;
			}
		}
		if (index < static_cast<unsigned short>(control.GetCount()))
		{
			control.SetCurSel(index);
		}
	}

	int get_window_text_width(CWindow &wnd, const std::wstring &text)
	{
		CSize extent;
		CWindowDC dc(wnd);
		CFont font(dc.SelectFont(wnd.GetFont()));
		dc.GetTextExtent(text.c_str(), text.length(), &extent);
		dc.SelectFont(font.Detach());
		return extent.cx;
	}

	int get_text_width(CDC &dc, CFontHandle font, const std::wstring &text)
	{
		SIZE size = {};
		font.Attach(dc.SelectFont(font.Detach()));
		dc.GetTextExtent(text.c_str(), text.length(), &size);
		font.Attach(dc.SelectFont(font.Detach()));
		return size.cx;
	}

	float get_dialog_item_float(CWindow &dialog, unsigned long id)
	{
		std::array<wchar_t, std::numeric_limits<double>::digits10> buffer = {};
		dialog.GetDlgItemText(id, buffer.data(), buffer.size());
		return std::stof(std::wstring(buffer.data(), buffer.size()), nullptr);
	}

	void set_dialog_item_float(CWindow &dialog, unsigned long id, float value, std::streamsize precision)
	{
		dialog.SetDlgItemText(id, static_cast<std::wstringstream&>(std::wstringstream() << std::setw(5) << std::fixed << std::setprecision(precision) << value).str().c_str());
	}


	std::wstring get_hex(unsigned int value)
	{
		return static_cast<std::wstringstream&>(std::wstringstream() << std::setw(2) << std::setfill(L'0') << std::hex << std::uppercase << value).str();
	}

	std::wstring get_double(double value, std::streamsize precision)
	{
		return static_cast<std::wstringstream&>(std::wstringstream() << std::setw(5) << std::fixed << std::setprecision(precision) << value).str();
	}

	std::wstring get_decimal(unsigned int value)
	{
		return static_cast<std::wstringstream&>(std::wstringstream() << value).str();
	}

	std::wstring format_double(double value, std::streamsize precision)
	{
		return static_cast<std::wstringstream&>(std::wstringstream() << std::setw(5) << std::fixed << std::setprecision(precision) << value).str();
	}




	std::wstring load_string(unsigned int id)
	{
		std::vector<wchar_t> buffer(StringBufferSize, 0);
		for (std::size_t n = buffer.size(); n == buffer.size(); buffer.resize(n *= 2, 0))
			if ((n = ::LoadStringW(nullptr, id, buffer.data(), buffer.size())) != buffer.size())
				return std::wstring(buffer.data(), n);
		return std::wstring();
	}
}

