#pragma once

#include <string>

#include <atlbase.h>




namespace resources
{
	template<std::size_t ID> std::wstring string()
	{
		return std::wstring(ATL::AtlGetStringResourceImage(ID)->achString, ATL::AtlGetStringResourceImage(ID)->nLength);
	}
};