#include "..\stdafx.hpp"
#include "..\resource.hpp"
#include "..\View\StatusBar.hpp"
#include "..\View\Utilities.hpp"
#include "..\View\Resources.hpp"

#include <sstream>
#include <iomanip>
#include <array>




StatusBar::StatusBar():
	_icon_height(::GetSystemMetrics(SM_CYSMICON)), _icon_width(::GetSystemMetrics(SM_CXSMICON)), _edge_width(::GetSystemMetrics(SM_CXEDGE)),
	_gripper_width(::GetSystemMetrics(SM_CXVSCROLL) + ::GetSystemMetrics(SM_CXEDGE))
{
	_severities[Severity::None].LoadIconW(IDI_SEVERITY_NONE, _icon_width, _icon_height, LR_CREATEDIBSECTION);
	_severities[Severity::Error].LoadIconW(IDI_SEVERITY_ERROR, _icon_width, _icon_height, LR_CREATEDIBSECTION);
	_charges[Charge::ThreeQuarters].LoadIconW(IDI_SEVERITY_WARNING, _icon_width, _icon_height, LR_CREATEDIBSECTION);
	_charges[Charge::Depleted].LoadIconW(IDI_SEVERITY_WARNING, _icon_width, _icon_height, LR_CREATEDIBSECTION);
	_charges[Charge::Unknown].LoadIconW(IDI_SEVERITY_WARNING, _icon_width, _icon_height, LR_CREATEDIBSECTION);
	_charges[Charge::Quarter].LoadIconW(IDI_SEVERITY_WARNING, _icon_width, _icon_height, LR_CREATEDIBSECTION);
	_charges[Charge::Half].LoadIconW(IDI_SEVERITY_WARNING, _icon_width, _icon_height, LR_CREATEDIBSECTION);
	_charges[Charge::Full].LoadIconW(IDI_SEVERITY_WARNING, _icon_width, _icon_height, LR_CREATEDIBSECTION);
}

StatusBar::~StatusBar()
{
}

HWND StatusBar::create(HWND parent)
{
	RECT client = {};
	int h_border = 0, v_border = 0, spacing = 0;
	std::array<int, Sections::Total> delimiters;
	this->Create(parent);
	this->SetParts(delimiters.size(), delimiters.data());
	this->SetText(Sections::Status, resources::string<IDS_STATUS_PLACEHOLDER>().c_str());
	this->SetIcon(Sections::Status, _severities.at(Severity::None));
	this->SetText(Sections::Signals, this->format_signal_number(99999).c_str());
	this->SetText(Sections::Position, this->format_position(99999, 999, false).c_str());
	this->SetText(Sections::Viewer, resources::string<IDS_VIEWER_PLACEHOLDER>().c_str());
	this->SetText(Sections::Battery, resources::string<IDS_BATTERY_STATUS_PLACEHOLDER>().c_str());
	this->SetIcon(Sections::Battery, _charges.at(Charge::Unknown));
	this->GetClientRect(&client);
	this->GetBorders(h_border, v_border, spacing);
	delimiters.at(Sections::Battery) = client.right;
	delimiters.at(Sections::Viewer) = (client.right -= (spacing + StatusBar::Panel(Sections::Battery).measure(*this) + _gripper_width));
	delimiters.at(Sections::Position) = (client.right -= (spacing + StatusBar::Panel(Sections::Viewer).measure(*this) + _gripper_width));
	delimiters.at(Sections::Signals) = (client.right -= (spacing + StatusBar::Panel(Sections::Position).measure(*this) + _gripper_width));
	delimiters.at(Sections::Status) = (client.right -= (spacing + StatusBar::Panel(Sections::Signals).measure(*this)));
	this->SetParts(delimiters.size(), delimiters.data());
	return *this;
}

void StatusBar::report_information(const std::wstring &message)
{
	Panel(Sections::Status).text(*this, this->format_status(message.c_str()));
	Panel(Sections::Status).icon(*this, _severities.at(Severity::None));
}

void StatusBar::report_signal_time(const float time)
{
	Panel(Sections::Viewer).text(*this, this->format_signal_time(time));
}

void StatusBar::report_signal_number(std::size_t number)
{
	Panel(Sections::Signals).text(*this, this->format_signal_number(number));
}

void StatusBar::report_error(const std::wstring &message)
{
	Panel(Sections::Status).text(*this, this->format_status(message));
	Panel(Sections::Status).icon(*this, _severities.at(Severity::Error));
}

void StatusBar::report_tracking(unsigned long tick_count, unsigned long queue_length, bool wrong_direction)
{
	Panel(Sections::Position).text(*this, this->format_position(static_cast<long>(tick_count), queue_length, wrong_direction));
}

void StatusBar::battery(float charge, float voltage, float current)
{
	if ( 0.9f < charge ) Panel(Sections::Battery).icon(*this, _charges.at(Charge::Full));
	else if ( 0.75f < charge ) Panel(Sections::Battery).icon(*this, _charges.at(Charge::ThreeQuarters));
	else if ( 0.50f < charge ) Panel(Sections::Battery).icon(*this, _charges.at(Charge::Half));
	else if ( 0.25f < charge ) Panel(Sections::Battery).icon(*this, _charges.at(Charge::Quarter));
	else Panel(Sections::Battery).icon(*this, _charges.at(Charge::Depleted));
	Panel(Sections::Battery).text(*this, this->format_battery_status(charge, voltage, current));
}



LRESULT StatusBar::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE;
	RECT client = {};
	std::array<int, Sections::Total> delimiters;
	this->GetClientRect(&client);
	this->GetParts(delimiters.size(), delimiters.data());
	delimiters.at(Sections::Position) = std::max<long>(0, delimiters.at(Sections::Position) + client.right - delimiters.at(Sections::Battery));
	delimiters.at(Sections::Signals) = std::max<long>(0, delimiters.at(Sections::Signals) + client.right - delimiters.at(Sections::Battery));
	delimiters.at(Sections::Status) = std::max<long>(0, delimiters.at(Sections::Status) + client.right - delimiters.at(Sections::Battery));
	delimiters.at(Sections::Viewer) = std::max<long>(0, delimiters.at(Sections::Viewer) + client.right - delimiters.at(Sections::Battery));
	delimiters.at(Sections::Battery) = client.right;
	this->SetParts(delimiters.size(), delimiters.data());
	return TRUE;
}



std::wstring StatusBar::format_status(const std::wstring &message) const
{
	return message;
}

std::wstring StatusBar::format_signal_time(const float time) const
{
	std::wstringstream buffer;
	buffer << std::fixed << std::setw(5) << std::setprecision(2) << time << L"ns";
	return buffer.str();
}

std::wstring StatusBar::format_battery_status(float charge, float voltage, float current) const
{
	std::wstringstream buffer;
	buffer << std::fixed;
	buffer << std::setw(3) << std::setprecision(0) << charge << L"% ";
	buffer << std::setw(5) << std::setprecision(2) << current << L"A ";
	buffer << std::setw(5) << std::setprecision(2) << voltage << L"V";
	return buffer.str();
}

std::wstring StatusBar::format_signal_number(std::size_t number) const
{
	std::wstringstream buffer;
	buffer << resources::string<IDS_SIGNALS_TOTAL_COUNT>() << std::setw(6) << number;
	return buffer.str();
}

std::wstring StatusBar::format_position(long tick_count, unsigned long queue_length, bool wrong_direction) const
{
	std::wstringstream buffer;
	buffer << resources::string<IDS_POSITIONER_TICK_NUMBER>() << std::setw(6) << std::setprecision(0) << tick_count << L" ";
	buffer << resources::string<IDS_POSITIONER_TICK_QUEUE>() << std::setw(6) << std::setprecision(0) << queue_length << L" ";
	buffer << resources::string<IDS_POSITIONER_DIRECTION>() << std::setw(5) << std::setprecision(0) << (wrong_direction ? resources::string<IDS_POSITIONER_WRONG_DIRECTION>() : resources::string<IDS_POSITIONER_RIGHT_DIRECTION>());
	return buffer.str();
}








StatusBar::Panel::Panel(Sections section):
	_index(section)
{
}

int StatusBar::Panel::measure(StatusBar &owner) const
{
	return this->measure(owner, CClientDC(owner), CFontHandle(owner.GetFont()));
}

int StatusBar::Panel::measure(StatusBar &owner, CDC &dc, CFontHandle font) const
{
	SIZE size = {};
	int width = 0, length = 0;
	std::array<wchar_t, 128> buffer = {};
	if ( (length = LOWORD(owner.GetText(_index, buffer.data()))) != 0 )
	{
		font.Attach(dc.SelectFont(font.Detach()));
		dc.GetTextExtent(buffer.data(), length, &size);
		font.Attach(dc.SelectFont(font.Detach()));
		width += owner._edge_width + size.cx + owner._edge_width;
	}
	if ( owner.GetIcon(_index) != nullptr )
	{
		if ( width == 0 )
		{
			width += owner._edge_width;
		}
		width += owner._edge_width + owner._icon_width + owner._edge_width;
	}
	return width;
}


void StatusBar::Panel::text(StatusBar &owner, const std::wstring &text) const
{
	owner.SetText(_index, text.c_str());
}

void StatusBar::Panel::icon(StatusBar &owner, HICON icon) const
{
	owner.SetIcon(_index, icon);
}

