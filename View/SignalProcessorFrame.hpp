#pragma once
#include "..\View\StatusBar.hpp"
#include "..\View\SignalProcessorView.hpp"

#include <string>

#include "..\WTL\atlapp.h"
#include "..\WTL\atlframe.h"



class SignalProcessorController;


class SignalProcessorFrame:
	public CFrameWindowImpl<SignalProcessorFrame>
{
private:
	enum
	{
		WM_BASE = WM_USER,
		WM_RESET,
		WM_REPORT_TEXT,
		WM_REPORT_SCANNING_BEGIN,
		WM_REPORT_SCANNING_END
	};
	
public:
	SignalProcessorFrame(SignalProcessorController &controller);
	
	void create(int mode);
	void update();
	
	void load(const std::wstring &root);
	void save(const std::wstring &root);
	
	const SignalProcessorView& view() const;
	
	SignalProcessorView& view();
	
public:
	DECLARE_FRAME_WND_CLASS(L"SignalProcessorFrame", IDC_SIGNALPROCESSOREX)
	
	BEGIN_MSG_MAP(SignalProcessorFrame)
		MESSAGE_HANDLER(WM_CLOSE, OnClose)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
		MESSAGE_HANDLER(WM_RESET, OnReset)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_REPORT_TEXT, OnReportText)
		MESSAGE_HANDLER(WM_REPORT_SCANNING_END, OnReportScanningEnd)
		MESSAGE_HANDLER(WM_REPORT_SCANNING_BEGIN, OnReportScanningBegin)
		MESSAGE_HANDLER(WM_GETMINMAXINFO, OnGetMinMaxInfo)
		COMMAND_ID_HANDLER(ID_FILE_SAVE, OnFileSave)
		COMMAND_ID_HANDLER(ID_FILE_SAVEANDOPEN, OnFileSaveAndOpen)
		COMMAND_ID_HANDLER(ID_FILE_EXIT, OnFileExit)
		COMMAND_ID_HANDLER(ID_EXPERIMENT_START, OnExperimentStart)
		COMMAND_ID_HANDLER(ID_EXPERIMENT_RECORD, OnExperimentRecord)
		COMMAND_ID_HANDLER(ID_OPTIONS_SERIALPORT, OnOptionsSerialPort)
		COMMAND_ID_HANDLER(ID_OPTIONS_SERIALDATASOURCE, OnOptionsSerialDataSource)
		COMMAND_ID_HANDLER(ID_OPTIONS_ETHERNETPORT, OnOptionsEthernetPort)
		COMMAND_ID_HANDLER(ID_OPTIONS_ETHERNETDELAYS, OnOptionsEthernetDelays)
		COMMAND_ID_HANDLER(ID_OPTIONS_ETHERNETDATASOURCE, OnOptionsEthernetDataSource)
		CHAIN_MSG_MAP(CFrameWindowImpl<SignalProcessorFrame>)
	END_MSG_MAP()
	
private:
	SignalProcessorController &_controller;
	SignalProcessorView _view;
	StatusBar _status;
	
	void save_project(bool reopen = false);
	
	void on_experiment_begin();
	void on_experiment_end();
	void on_report();
	void on_reset();
	
	LRESULT OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnTimer(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnReset(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnReportText(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnGetMinMaxInfo(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnReportScanningEnd(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnReportScanningBegin(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	
	LRESULT OnFileSave(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnFileSaveAndOpen(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnFileExit(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnExperimentStart(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnExperimentRecord(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnOptionsSerialPort(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnOptionsSerialDataSource(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnOptionsEthernetPort(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnOptionsEthernetDelays(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnOptionsEthernetDataSource(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
};
