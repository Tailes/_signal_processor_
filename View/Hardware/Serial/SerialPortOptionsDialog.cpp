#include "..\..\..\StdAfx.hpp"
#include "..\..\..\resource.hpp"
#include "..\..\..\Controller\Hardware\Serial\SerialDataSourceController.hpp"
#include "..\..\..\View\Hardware\Serial\SerialPortOptionsDialog.hpp"
#include "..\..\..\View\Utilities.hpp"


#include <string.h>
#include <string>
#include <iomanip>
#include <sstream>
#include <array>



namespace Serial
{
	PortOptionsDialog::PortOptionsDialog(DataSourceController &source):
		_source(source)
	{
	}


	LRESULT PortOptionsDialog::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
	{
		_port_name = this->GetDlgItem(IDC_COMBO_PORT_NAME);
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM1")), _T("COM1"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM2")), _T("COM2"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM3")), _T("COM3"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM4")), _T("COM4"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM5")), _T("COM5"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM6")), _T("COM6"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM7")), _T("COM7"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM8")), _T("COM8"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM9")), _T("COM9"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM10")), _T("COM10"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM11")), _T("COM11"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM12")), _T("COM12"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM13")), _T("COM13"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM14")), _T("COM14"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM15")), _T("COM15"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM16")), _T("COM16"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM17")), _T("COM17"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM18")), _T("COM18"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM19")), _T("COM19"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM20")), _T("COM20"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM21")), _T("COM21"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM22")), _T("COM22"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM23")), _T("COM23"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM24")), _T("COM24"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM25")), _T("COM25"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM26")), _T("COM26"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM27")), _T("COM27"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM28")), _T("COM28"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM29")), _T("COM29"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM30")), _T("COM30"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM31")), _T("COM31"));
		_port_name.SetItemDataPtr(_port_name.AddString(_T("COM32")), _T("COM32"));

		_baud_rate = this->GetDlgItem(IDC_COMBO_BAUD_RATE);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("110")), CBR_110);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("300")), CBR_300);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("600")), CBR_600);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("1200")), CBR_1200);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("2400")), CBR_2400);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("4800")), CBR_4800);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("9600")), CBR_9600);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("14400")), CBR_14400);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("19200")), CBR_19200);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("38400")), CBR_38400);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("56000")), CBR_56000);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("57600")), CBR_57600);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("115200")), CBR_115200);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("128000")), CBR_128000);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("230400 (N)")), 230400);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("256000")), CBR_256000);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("460800 (N)")), 460800);
		_baud_rate.SetItemData(_baud_rate.AddString(_T("921600 (N)")), 921600);

		_byte_length = this->GetDlgItem(IDC_COMBO_BYTE_LENGTH);
		_byte_length.SetItemData(_byte_length.AddString(_T("5")), 5);
		_byte_length.SetItemData(_byte_length.AddString(_T("6")), 6);
		_byte_length.SetItemData(_byte_length.AddString(_T("7")), 7);
		_byte_length.SetItemData(_byte_length.AddString(_T("8")), 8);

		_stop_bits = this->GetDlgItem(IDC_COMBO_STOP_BITS);
		_stop_bits.SetItemData(_stop_bits.AddString(_T("1")), ONESTOPBIT);
		_stop_bits.SetItemData(_stop_bits.AddString(_T("1.5")), ONE5STOPBITS);
		_stop_bits.SetItemData(_stop_bits.AddString(_T("2")), TWOSTOPBITS);

		_parity_mode = this->GetDlgItem(IDC_COMBO_PARITY_MODE);
		_parity_mode.SetItemData(_parity_mode.AddString(utility::load_string(IDS_PARITY_NONE).c_str()), NOPARITY);
		_parity_mode.SetItemData(_parity_mode.AddString(utility::load_string(IDS_PARITY_ODD).c_str()), ODDPARITY);
		_parity_mode.SetItemData(_parity_mode.AddString(utility::load_string(IDS_PARITY_EVEN).c_str()), EVENPARITY);
		_parity_mode.SetItemData(_parity_mode.AddString(utility::load_string(IDS_PARITY_MARK).c_str()), MARKPARITY);
		_parity_mode.SetItemData(_parity_mode.AddString(utility::load_string(IDS_PARITY_SPACE).c_str()), SPACEPARITY);

		_dtr_mode = this->GetDlgItem(IDC_COMBO_DTR_CONTROL_MODE);
		_dtr_mode.SetItemData(_dtr_mode.AddString(utility::load_string(IDS_MODE_DISABLE).c_str()), DTR_CONTROL_DISABLE);
		_dtr_mode.SetItemData(_dtr_mode.AddString(utility::load_string(IDS_MODE_ENABLE).c_str()), DTR_CONTROL_ENABLE);
		_dtr_mode.SetItemData(_dtr_mode.AddString(utility::load_string(IDS_MODE_HANDSHAKE).c_str()), DTR_CONTROL_HANDSHAKE);

		_rts_mode = this->GetDlgItem(IDC_COMBO_RTS_CONTROL_MODE);
		_rts_mode.SetItemData(_rts_mode.AddString(utility::load_string(IDS_MODE_DISABLE).c_str()), RTS_CONTROL_DISABLE);
		_rts_mode.SetItemData(_rts_mode.AddString(utility::load_string(IDS_MODE_ENABLE).c_str()), RTS_CONTROL_ENABLE);
		_rts_mode.SetItemData(_rts_mode.AddString(utility::load_string(IDS_MODE_HANDSHAKE).c_str()), RTS_CONTROL_HANDSHAKE);
		_rts_mode.SetItemData(_rts_mode.AddString(utility::load_string(IDS_MODE_TOGGLE).c_str()), RTS_CONTROL_TOGGLE);

		_eof_byte = this->GetDlgItem(IDC_COMBO_EOF_BYTE);
		_x_on_byte = this->GetDlgItem(IDC_COMBO_XON_BYTE);
		_x_off_byte = this->GetDlgItem(IDC_COMBO_XOFF_BYTE);
		_error_byte = this->GetDlgItem(IDC_COMBO_ERROR_BYTE);
		_event_byte = this->GetDlgItem(IDC_COMBO_EVENT_BYTE);
		for ( int i = 0; i <= std::numeric_limits<unsigned char>::max(); ++i )
		{
			_eof_byte.SetItemData(_eof_byte.AddString(utility::get_hex(i).c_str()), i);
			_x_on_byte.SetItemData(_x_on_byte.AddString(utility::get_hex(i).c_str()), i);
			_x_off_byte.SetItemData(_x_off_byte.AddString(utility::get_hex(i).c_str()), i);
			_error_byte.SetItemData(_error_byte.AddString(utility::get_hex(i).c_str()), i);
			_event_byte.SetItemData(_event_byte.AddString(utility::get_hex(i).c_str()), i);
		}

		utility::select_combo_box_item(_port_name, _source.port().name().c_str());
		utility::select_combo_box_item(_baud_rate, static_cast<int>(_source.device_control_block().baud_rate()));
		utility::select_combo_box_item(_byte_length, _source.device_control_block().byte_size());
		utility::select_combo_box_item(_stop_bits, static_cast<int>(_source.device_control_block().stop_bits()));
		utility::select_combo_box_item(_dtr_mode, static_cast<int>(_source.device_control_block().dtr_control_mode()));
		utility::select_combo_box_item(_rts_mode, static_cast<int>(_source.device_control_block().rts_control_mode()));
		utility::select_combo_box_item(_parity_mode, static_cast<int>(_source.device_control_block().parity_mode()));
		utility::select_combo_box_item(_x_on_byte, _source.device_control_block().x_on_char());
		utility::select_combo_box_item(_x_off_byte, _source.device_control_block().x_off_char());
		utility::select_combo_box_item(_eof_byte, _source.device_control_block().eof_char());
		utility::select_combo_box_item(_error_byte, _source.device_control_block().error_char());
		utility::select_combo_box_item(_event_byte, _source.device_control_block().evt_char());

		this->CheckDlgButton(IDC_CHECK_DSR_SENSITIVITY, _source.device_control_block().dsr_sensitivity_enabled());
		this->CheckDlgButton(IDC_CHECK_OUTPUT_DSR_CONTROL, _source.device_control_block().output_dsr_flow_enabled());
		this->CheckDlgButton(IDC_CHECK_OUTPUT_CTS_CONTROL, _source.device_control_block().output_cts_flow_enabled());
		this->CheckDlgButton(IDC_CHECK_PARITY, _source.device_control_block().parity_enabled());
		this->CheckDlgButton(IDC_CHECK_DISCARD_NULL, _source.device_control_block().null_enabled());
		this->CheckDlgButton(IDC_CHECK_ABORT_ON_ERROR, _source.device_control_block().abort_on_error_enabled());
		this->CheckDlgButton(IDC_CHECK_OUTGOING_XON_XOFF, _source.device_control_block().out_x_enabled());
		this->CheckDlgButton(IDC_CHECK_INCOMING_XON_XOFF, _source.device_control_block().in_x_enabled());
		this->CheckDlgButton(IDC_CHECK_CONTINUE_ON_XOFF, _source.device_control_block().transmission_continue_on_x_off_enabled());

		this->SetDlgItemInt(IDC_EDIT_XON_LIMIT, _source.device_control_block().x_on_limit(), FALSE);
		this->SetDlgItemInt(IDC_EDIT_XOFF_LIMIT, _source.device_control_block().x_off_limit(), FALSE);
		this->SetDlgItemInt(IDC_EDIT_READ_INTERVAL, _source.comm_timeouts().read_interval_timeout(), FALSE);
		this->SetDlgItemInt(IDC_EDIT_READ_MULTIPLIER, _source.comm_timeouts().read_total_timeout_multiplier(), FALSE);
		this->SetDlgItemInt(IDC_EDIT_READ_TOTAL_CONSTANT, _source.comm_timeouts().read_total_timeout_constant(), FALSE);
		this->SetDlgItemInt(IDC_EDIT_WRITE_MULTIPLIER, _source.comm_timeouts().write_total_timeout_multiplier(), FALSE);
		this->SetDlgItemInt(IDC_EDIT_WRITE_TOTAL_CONSTANT, _source.comm_timeouts().write_total_timeout_constant(), FALSE);
		this->SetDlgItemInt(IDC_EDIT_INPUT_BUFFER_SIZE, _source.port().input_buffer_size(), FALSE);
		this->SetDlgItemInt(IDC_EDIT_OUTPUT_BUFFER_SIZE, _source.port().output_buffer_size(), FALSE);

		return 0;
	}

	LRESULT PortOptionsDialog::OnOkClicked(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		bHandled = FALSE;

		_source.port().name(std::wstring(reinterpret_cast<wchar_t*>(_port_name.GetItemDataPtr(_port_name.GetCurSel()))));

		_source.device_control_block().baud_rate(static_cast<BaudRate>(_baud_rate.GetItemData(_baud_rate.GetCurSel())));
		_source.device_control_block().byte_size(static_cast<unsigned char>(_byte_length.GetItemData(_byte_length.GetCurSel())));
		_source.device_control_block().stop_bits(static_cast<StopBits>(_stop_bits.GetItemData(_stop_bits.GetCurSel())));
		_source.device_control_block().dtr_control_mode(static_cast<DtrMode>(_dtr_mode.GetItemData(_dtr_mode.GetCurSel())));
		_source.device_control_block().parity_mode(static_cast<ParityMode>(_parity_mode.GetItemData(_parity_mode.GetCurSel())));
		_source.device_control_block().rts_control_mode(static_cast<RtsMode>(_rts_mode.GetItemData(_rts_mode.GetCurSel())));
		_source.device_control_block().x_on_char(static_cast<char>(_x_on_byte.GetItemData(_x_on_byte.GetCurSel())));
		_source.device_control_block().x_off_char(static_cast<char>(_x_off_byte.GetItemData(_x_off_byte.GetCurSel())));
		_source.device_control_block().eof_char(static_cast<char>(_eof_byte.GetItemData(_eof_byte.GetCurSel())));
		_source.device_control_block().error_char(static_cast<char>(_error_byte.GetItemData(_error_byte.GetCurSel())));
		_source.device_control_block().evt_char(static_cast<char>(_event_byte.GetItemData(_event_byte.GetCurSel())));
		_source.device_control_block().enable_dsr_sensitivity(this->IsDlgButtonChecked(IDC_CHECK_DSR_SENSITIVITY) == BST_CHECKED);
		_source.device_control_block().enable_output_dsr_flow(this->IsDlgButtonChecked(IDC_CHECK_OUTPUT_DSR_CONTROL) == BST_CHECKED);
		_source.device_control_block().enable_output_cts_flow(this->IsDlgButtonChecked(IDC_CHECK_OUTPUT_CTS_CONTROL) == BST_CHECKED);
		_source.device_control_block().enable_parity(this->IsDlgButtonChecked(IDC_CHECK_PARITY) == BST_CHECKED);
		_source.device_control_block().enable_null(this->IsDlgButtonChecked(IDC_CHECK_DISCARD_NULL) == BST_CHECKED);
		_source.device_control_block().enable_abort_on_error(this->IsDlgButtonChecked(IDC_CHECK_ABORT_ON_ERROR) == BST_CHECKED);
		_source.device_control_block().enable_out_x(this->IsDlgButtonChecked(IDC_CHECK_OUTGOING_XON_XOFF) == BST_CHECKED);
		_source.device_control_block().enable_in_x(this->IsDlgButtonChecked(IDC_CHECK_INCOMING_XON_XOFF) == BST_CHECKED);
		_source.device_control_block().enable_transmission_continue_on_x_off(this->IsDlgButtonChecked(IDC_CHECK_CONTINUE_ON_XOFF) == BST_CHECKED);
		_source.device_control_block().x_on_limit(this->GetDlgItemInt(IDC_EDIT_XON_LIMIT, NULL, FALSE));
		_source.device_control_block().x_off_limit(this->GetDlgItemInt(IDC_EDIT_XOFF_LIMIT, NULL, FALSE));

		_source.comm_timeouts().read_interval_timeout(this->GetDlgItemInt(IDC_EDIT_READ_INTERVAL, NULL, FALSE));
		_source.comm_timeouts().read_total_timeout_multiplier(this->GetDlgItemInt(IDC_EDIT_READ_MULTIPLIER, NULL, FALSE));
		_source.comm_timeouts().read_total_timeout_constant(this->GetDlgItemInt(IDC_EDIT_READ_TOTAL_CONSTANT, NULL, FALSE));
		_source.comm_timeouts().write_total_timeout_multiplier(this->GetDlgItemInt(IDC_EDIT_WRITE_MULTIPLIER, NULL, FALSE));
		_source.comm_timeouts().write_total_timeout_constant(this->GetDlgItemInt(IDC_EDIT_WRITE_TOTAL_CONSTANT, NULL, FALSE));

		_source.port().input_buffer_size(this->GetDlgItemInt(IDC_EDIT_INPUT_BUFFER_SIZE, nullptr, FALSE));
		_source.port().output_buffer_size(this->GetDlgItemInt(IDC_EDIT_OUTPUT_BUFFER_SIZE, nullptr, FALSE));

		return 0;
	}
}