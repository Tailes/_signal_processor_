#include "..\..\..\StdAfx.hpp"
#include "..\..\..\resource.hpp"
#include "..\..\..\Controller\Hardware\Serial\SerialDataSourceController.hpp"
#include "..\..\..\View\Hardware\Serial\SerialDataSourceOptionsDialog.hpp"
#include "..\..\..\View\Utilities.hpp"

#include <string>


namespace Serial
{

	DataSourceOptionsDialog::DataSourceOptionsDialog(DataSourceController &source):
		_source(source)
	{
	}


	LRESULT DataSourceOptionsDialog::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
	{
		_packet_size = this->GetDlgItem(IDC_CMB_PACKET_SIZE);
		_packet_size.SetItemData(_packet_size.AddString(_T("512")), static_cast<int>(PacketLength::_512));
		_packet_size.SetItemData(_packet_size.AddString(_T("1024")), static_cast<int>(PacketLength::_1024));
		
		_point_size = this->GetDlgItem(IDC_CMB_POINT_SIZE);
		_point_size.SetItemData(_point_size.AddString(_T("8")), static_cast<int>(DataSize::Byte));
		_point_size.SetItemData(_point_size.AddString(_T("12")), static_cast<int>(DataSize::Dozen));
		_point_size.SetItemData(_point_size.AddString(_T("16")), static_cast<int>(DataSize::Word));
		
		_packet_mark = this->GetDlgItem(IDC_CMB_PACKET_MARKER);
		for ( int i = 0; i <= std::numeric_limits<unsigned char>::max(); ++i )
			_packet_mark.SetItemData(_packet_mark.AddString(utility::get_hex(i).c_str()), i);
		
		utility::select_combo_box_item(_packet_size, static_cast<int>(_source.packet_length()));
		utility::select_combo_box_item(_point_size, static_cast<int>(_source.point_size()));
		utility::select_combo_box_item(_packet_mark, static_cast<int>(_source.packet().marker()));
		
		return 0;
	}

	LRESULT DataSourceOptionsDialog::OnOkClicked(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		bHandled = FALSE;
		_source.packet_length(static_cast<PacketLength>(_packet_size.GetItemData(_packet_size.GetCurSel())));
		_source.point_size(static_cast<DataSize>(_point_size.GetItemData(_point_size.GetCurSel())));
		_source.packet().marker(static_cast<unsigned char>(_packet_mark.GetItemData(_packet_mark.GetCurSel())));
		return 0;
	}
}