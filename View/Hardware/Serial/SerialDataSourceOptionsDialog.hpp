#pragma once
#include "..\..\..\resource.hpp"

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\WTL\atlapp.h"
#include "..\..\..\WTL\atlctrls.h"



namespace Serial
{
	class DataSourceController;
	
	
	
	class DataSourceOptionsDialog:
		public CSimpleDialog< IDD_SERIAL_DATA_SOURCE_OPTIONS >
	{
	public:
		DataSourceOptionsDialog(DataSourceController &source);
		
	private:
		BEGIN_MSG_MAP(DataSourceOptionsDialog)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			COMMAND_HANDLER(IDOK, BN_CLICKED, OnOkClicked)
			CHAIN_MSG_MAP(CSimpleDialog< IDD_SERIAL_DATA_SOURCE_OPTIONS >)
		END_MSG_MAP()
		
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnOkClicked(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		
	private:
		DataSourceController &_source;
		
		CComboBox _packet_size;
		CComboBox _packet_mark;
		CComboBox _point_size;
	};
}