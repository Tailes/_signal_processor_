#pragma once
#include "..\..\..\resource.hpp"
#include "..\..\..\Model\Hardware\Serial\SerialCommunicationEnums.hpp"


#include <algorithm>
using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\WTL\atlapp.h"
#include "..\..\..\WTL\atlframe.h"
#include "..\..\..\WTL\atlctrls.h"



namespace Serial
{
	class DataSourceController;
	
	
	
	class DataSourcePanel:
		//public IHardwarePanel,
		public CDialogImpl<DataSourcePanel>
	{
	public:
		enum: unsigned long { IDD = IDD_SERIAL_DATA_SOURCE_PANEL };
		
		DataSourcePanel(DataSourceController &source);
		
		void reset();
		void start_recording(bool scanning);
		void start_scanning(bool recording);
		void stop_recording(bool scanning);
		void stop_scanning(bool recording);
		
	private:
		BEGIN_MSG_MAP(DataSourcePanel)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			COMMAND_HANDLER(IDC_BTN_SERIAL_ACTIVE, BN_CLICKED, OnBnClickedAlgorithm<ScanningAlgorithm::ActiveAntenna>);
			COMMAND_HANDLER(IDC_BTN_SERIAL_ACTIVECROSS, BN_CLICKED, OnBnClickedAlgorithm<ScanningAlgorithm::ActiveAntennaCrossInput>);
			COMMAND_HANDLER(IDC_BTN_SERIAL_TRANSMITRECIEVE, BN_CLICKED, OnBnClickedAlgorithm<ScanningAlgorithm::TransmitRecieve>);
			COMMAND_HANDLER(IDC_BTN_SERIAL_QUADRO, BN_CLICKED, OnBnClickedAlgorithm<ScanningAlgorithm::QuadroAntenna>);
			COMMAND_HANDLER(IDC_EDT_SERIAL_ACCUMULATING, EN_CHANGE, OnEnChangeEdtAccumulation);
			COMMAND_HANDLER(IDC_EDT_SERIAL_DELAY, EN_CHANGE, OnEnChangeEdtDelay);
			COMMAND_HANDLER(IDC_EDT_SERIAL_PROBING_PERIOD, EN_CHANGE, OnEnChangeEdtProbingPeriod);
			COMMAND_HANDLER(IDC_CMB_TIMEFRAME, CBN_SELCHANGE, OnCbnSelChangeCmbTimeFrame);
		END_MSG_MAP()
		
		template<ScanningAlgorithm Algorithm>
		LRESULT OnBnClickedAlgorithm(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnEnChangeEdtAccumulation(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnEnChangeEdtDelay(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnEnChangeEdtProbingPeriod(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbTimeFrame(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		
	private:
		DataSourceController &_source;
	};
}