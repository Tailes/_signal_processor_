#pragma once
#include "..\..\..\resource.hpp"

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\WTL\atlapp.h"
#include "..\..\..\WTL\atlctrls.h"



namespace Serial
{
	class DataSourceController;
	
	
	
	class PortOptionsDialog:
		public CSimpleDialog<IDD_SERIAL_PORT_OPTIONS>
	{
	public:
		PortOptionsDialog(DataSourceController &source);
		
	private:
		BEGIN_MSG_MAP(PortOptionsDialog)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			COMMAND_HANDLER(IDOK, BN_CLICKED, OnOkClicked)
			CHAIN_MSG_MAP(CSimpleDialog< IDD_SERIAL_PORT_OPTIONS >)
		END_MSG_MAP()
		
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnOkClicked(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		
	private:
		DataSourceController &_source;
		
		CComboBox _port_name;
		CComboBox _baud_rate;
		CComboBox _byte_length;
		CComboBox _stop_bits;
		CComboBox _dtr_mode;
		CComboBox _rts_mode;
		CComboBox _x_on_byte;
		CComboBox _x_off_byte;
		CComboBox _eof_byte;
		CComboBox _error_byte;
		CComboBox _event_byte;
		CComboBox _parity_mode;
	};
}