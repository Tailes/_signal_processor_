#include "..\..\..\StdAfx.hpp"
#include "..\..\..\resource.hpp"
#include "..\..\..\Controller\Hardware\Serial\SerialDataSourceController.hpp"
#include "..\..\..\View\Hardware\Serial\SerialDataSourcePanel.hpp"
#include "..\..\..\View\Utilities.hpp"

#include <string>



namespace Serial
{
	DataSourcePanel::DataSourcePanel(DataSourceController &source):
		_source(source)
	{
	}


	void DataSourcePanel::reset()
	{
		this->GetDlgItem(IDC_EDT_SERIAL_ACCUMULATING).EnableWindow(TRUE);
		this->GetDlgItem(IDC_BTN_SERIAL_ACTIVE).EnableWindow(TRUE);
		this->GetDlgItem(IDC_BTN_SERIAL_ACTIVECROSS).EnableWindow(TRUE);
		this->GetDlgItem(IDC_BTN_SERIAL_TRANSMITRECIEVE).EnableWindow(TRUE);
		this->GetDlgItem(IDC_BTN_SERIAL_QUADRO).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_SERIAL_TIMEFRAME).EnableWindow(TRUE);
		this->GetDlgItem(IDC_EDT_SERIAL_DELAY).EnableWindow(TRUE);
		this->GetDlgItem(IDC_EDT_SERIAL_PROBING_PERIOD).EnableWindow(TRUE);
	}


	void DataSourcePanel::start_scanning(bool recording)
	{
		this->GetDlgItem(IDC_EDT_SERIAL_ACCUMULATING).EnableWindow(FALSE);
		this->GetDlgItem(IDC_BTN_SERIAL_ACTIVE).EnableWindow(FALSE);
		this->GetDlgItem(IDC_BTN_SERIAL_ACTIVECROSS).EnableWindow(FALSE);
		this->GetDlgItem(IDC_BTN_SERIAL_TRANSMITRECIEVE).EnableWindow(FALSE);
		this->GetDlgItem(IDC_BTN_SERIAL_QUADRO).EnableWindow(FALSE);
		this->GetDlgItem(IDC_CMB_SERIAL_TIMEFRAME).EnableWindow(FALSE);
		this->GetDlgItem(IDC_EDT_SERIAL_DELAY).EnableWindow(FALSE);
		this->GetDlgItem(IDC_EDT_SERIAL_PROBING_PERIOD).EnableWindow(FALSE);
	}



	void DataSourcePanel::stop_scanning(bool recording)
	{
		this->GetDlgItem(IDC_EDT_SERIAL_ACCUMULATING).EnableWindow(TRUE);
		this->GetDlgItem(IDC_BTN_SERIAL_ACTIVE).EnableWindow(TRUE);
		this->GetDlgItem(IDC_BTN_SERIAL_ACTIVECROSS).EnableWindow(TRUE);
		this->GetDlgItem(IDC_BTN_SERIAL_TRANSMITRECIEVE).EnableWindow(TRUE);
		this->GetDlgItem(IDC_BTN_SERIAL_QUADRO).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_SERIAL_TIMEFRAME).EnableWindow(TRUE);
		this->GetDlgItem(IDC_EDT_SERIAL_DELAY).EnableWindow(TRUE);
		this->GetDlgItem(IDC_EDT_SERIAL_PROBING_PERIOD).EnableWindow(TRUE);
	}


	void DataSourcePanel::start_recording(bool scanning)
	{
	}


	void DataSourcePanel::stop_recording(bool scanning)
	{
	}


	LRESULT DataSourcePanel::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
	{
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_ACCUMULATING)).SetRange(static_cast<int>(ChunkSize::Min), static_cast<int>(ChunkSize::Max));
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_DELAY)).SetRange32(static_cast<int>(ProbingDelay::Min), static_cast<int>(ProbingDelay::Max));
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_PROBING_PERIOD)).SetRange32(static_cast<int>(ProbingPeriod::Min), static_cast<int>(ProbingPeriod::Max));
		
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_ACCUMULATING)).SetPos(_source.packet().accumulation());
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_DELAY)).SetPos32(_source.packet().start_delay());
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_PROBING_PERIOD)).SetPos(_source.packet().probing_period());
		
		this->CheckDlgButton(IDC_BTN_SERIAL_ACTIVE, _source.scanning_algorithm() == ScanningAlgorithm::ActiveAntenna ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_BTN_SERIAL_ACTIVECROSS, _source.scanning_algorithm() == ScanningAlgorithm::ActiveAntennaCrossInput ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_BTN_SERIAL_TRANSMITRECIEVE, _source.scanning_algorithm() == ScanningAlgorithm::TransmitRecieve ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_BTN_SERIAL_QUADRO, _source.scanning_algorithm() == ScanningAlgorithm::QuadroAntenna ? BST_CHECKED : BST_UNCHECKED);
		
		CComboBoxEx(this->GetDlgItem(IDC_CMB_SERIAL_TIMEFRAME)).AddItem(utility::load_string(IDS_TIME_FRAME_2_5).c_str(), 0, 0, 0, static_cast<int>(HardwareDataSourceTimeFrame::_01));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_SERIAL_TIMEFRAME)).AddItem(utility::load_string(IDS_TIME_FRAME_5).c_str(), 0, 0, 0, static_cast<int>(HardwareDataSourceTimeFrame::_02));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_SERIAL_TIMEFRAME)).AddItem(utility::load_string(IDS_TIME_FRAME_10).c_str(), 0, 0, 0, static_cast<int>(HardwareDataSourceTimeFrame::_04));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_SERIAL_TIMEFRAME)).AddItem(utility::load_string(IDS_TIME_FRAME_25).c_str(), 0, 0, 0, static_cast<int>(HardwareDataSourceTimeFrame::_10));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_SERIAL_TIMEFRAME)).AddItem(utility::load_string(IDS_TIME_FRAME_50).c_str(), 0, 0, 0, static_cast<int>(HardwareDataSourceTimeFrame::_20));
		
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_SERIAL_TIMEFRAME)), static_cast<int>(_source.packet().scanning_time_frame()));
		
		return 0;
	}


	template<ScanningAlgorithm Algorithm>
	LRESULT DataSourcePanel::OnBnClickedAlgorithm(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_source.scanning_algorithm(Algorithm);
		this->CheckDlgButton(IDC_BTN_SERIAL_ACTIVE, Algorithm == ScanningAlgorithm::ActiveAntenna ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_BTN_SERIAL_ACTIVECROSS, Algorithm == ScanningAlgorithm::ActiveAntennaCrossInput ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_BTN_SERIAL_TRANSMITRECIEVE, Algorithm == ScanningAlgorithm::TransmitRecieve ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_BTN_SERIAL_QUADRO, Algorithm == ScanningAlgorithm::QuadroAntenna ? BST_CHECKED : BST_UNCHECKED);
		return 0;
	}


	LRESULT DataSourcePanel::OnEnChangeEdtAccumulation(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_source.packet().accumulation(static_cast<unsigned char>(this->GetDlgItemInt(IDC_EDT_SERIAL_ACCUMULATING)));
		//	if (this->GetDlgItemInt(IDC_EDT_SERIAL_ACCUMULATING) != CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_ACCUMULATING)).GetPos())
		//	CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_ACCUMULATING)).SetPos(_source.getControlPacket().getAccumulation());
		return 0;
	}


	LRESULT DataSourcePanel::OnEnChangeEdtDelay(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_source.packet().start_delay(static_cast<unsigned short>(this->GetDlgItemInt(IDC_EDT_SERIAL_DELAY)));
		//if (this->GetDlgItemInt(IDC_EDT_SERIAL_DELAY) != CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_DELAY)).GetPos32())
		//CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_DELAY)).SetPos32(_source.getControlPacket().getStartDelay());
		return 0;
	}


	LRESULT DataSourcePanel::OnEnChangeEdtProbingPeriod(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_source.packet().probing_period(static_cast<unsigned short>(this->GetDlgItemInt(IDC_EDT_SERIAL_PROBING_PERIOD)));
		//if (this->GetDlgItemInt(IDC_EDT_SERIAL_PROBING_PERIOD) != CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_PROBING_PERIOD)).GetPos32())
		//CUpDownCtrl(this->GetDlgItem(IDC_SPN_SERIAL_PROBING_PERIOD)).SetPos32(_source.getControlPacket().getStartDelay());
		return 0;
	}


	LRESULT DataSourcePanel::OnCbnSelChangeCmbTimeFrame(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_source.packet().scanning_time_frame(::getComboBoxSelectionData<HardwareDataSourceTimeFrame>(CComboBoxEx(this->GetDlgItem(IDC_CMB_SERIAL_TIMEFRAME))));
		return 0;
	}
}