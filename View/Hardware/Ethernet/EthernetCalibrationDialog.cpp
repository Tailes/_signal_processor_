#include "..\..\..\stdafx.hpp"
#include "..\..\..\resource.hpp"
#include "..\..\..\View\Resources.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetCalibrationDialog.hpp"
#include "..\..\..\View\Utilities.hpp"

#include <sstream>
#include <iomanip>
#include <array>



namespace Ethernet
{
	static const std::float_t MinTickLength = 0.001f;


	CalibrationDialog::CalibrationDialog(std::uint32_t ticks_count):
		_ticks_count(ticks_count), _tick_length(1.0)
	{
	}

	LRESULT CalibrationDialog::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
	{
		bHandled = FALSE;
		this->SetDlgItemText(IDC_EDT_TICK_LENGTH, utility::get_double(_tick_length).c_str());
		this->SetDlgItemText(IDC_EDT_PATH_LENGTH, utility::get_decimal(0).c_str());
		this->SetDlgItemInt(IDC_EDT_TICKS_COUNT, _ticks_count, FALSE);
		return 0;
	}

	std::float_t CalibrationDialog::tick_length() const
	{
		return _tick_length;
	}

	LRESULT CalibrationDialog::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
	{
		bHandled = FALSE;
		return 0;
	}

	LRESULT CalibrationDialog::OnOkClicked(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		std::array<wchar_t, std::numeric_limits<std::float_t>::digits> buffer;
		this->GetDlgItemText(IDC_EDT_TICK_LENGTH, buffer.data(), buffer.size());
		if (!(std::wstringstream(std::wstring(buffer.data(), buffer.size())) >> _tick_length)) return 0;
		_tick_length /= 100.0f;
		this->EndDialog(IDOK);
		return 0;
	}

	LRESULT CalibrationDialog::OnCancelClicked(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		this->EndDialog(IDCANCEL);
		return 0;
	}

	LRESULT CalibrationDialog::OnEnChangeEdtPathLength(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		std::float_t path_length = 1.0f;
		std::array<wchar_t, std::numeric_limits<std::float_t>::digits> buffer;
		this->GetDlgItemText(IDC_EDT_PATH_LENGTH, buffer.data(), buffer.size());
		if (!(std::wstringstream(std::wstring(buffer.data(), buffer.size())) >> path_length)) return 0;
		this->SetDlgItemText(IDC_EDT_TICK_LENGTH, utility::get_double(_tick_length = std::max<std::float_t>(path_length / _ticks_count, MinTickLength)).c_str());
		return 0;
	}
}
