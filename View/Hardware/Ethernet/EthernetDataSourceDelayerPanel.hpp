#pragma once
#include "..\..\..\resource.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetDelayerModes.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetDataSourceDelayerModeTimerPanel.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetDataSourceDelayerModeCounterPanel.hpp"

#include <functional>
#include <algorithm>
#include <map>

using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\WTL\atlapp.h"
#include "..\..\..\WTL\atlgdi.h"
#include "..\..\..\WTL\atlframe.h"
#include "..\..\..\WTL\atlctrls.h"



namespace Ethernet
{
	class DelayerController;



	class DataSourceDelayerPanel:
		public CDialogImpl<DataSourceDelayerPanel>
	{
	public:
		enum: unsigned long { IDD = IDD_ETHERNET_DATA_SOURCE_DELAYER_PANEL };
		
	public:
		DataSourceDelayerPanel(DelayerController &controller);
		
		void enabled_handler(std::function<void(bool)> callback);
		
		void start_recording(bool scanning);
		void start_scanning(bool recording);
		void stop_recording(bool scanning);
		void stop_scanning(bool recording);
		void destroy();
		void create(CWindow parent, CWindow placeholder, bool visible);
		void reset();
		void show(bool visible);
		
		const DataSourceDelayerModeCounterPanel& counter() const;
		const DataSourceDelayerModeTimerPanel& timer() const;
		
		DataSourceDelayerModeCounterPanel& counter();
		DataSourceDelayerModeTimerPanel& timer();
		
	public:
		BEGIN_MSG_MAP(DataSourceDelayerPanel)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
			COMMAND_HANDLER(IDC_EDT_AUTO_DELAY_STEP, EN_CHANGE, OnEnChangeEdtStep);
			COMMAND_HANDLER(IDC_CHK_AUTO_DELAY_RIGHT, BN_CLICKED, OnBnClickedRightAutoDelay);
			COMMAND_HANDLER(IDC_CHK_AUTO_DELAY_LEFT, BN_CLICKED, OnBnClickedLeftAutoDelay);
			COMMAND_HANDLER(IDC_BTN_STOP_AUTO_DELAY, BN_CLICKED, OnBnClickedStopAutoDelay);
			COMMAND_HANDLER(IDC_CMB_AUTO_DELAY_MODE, CBN_SELCHANGE, OnCbnSelChangeCmbMode);
		END_MSG_MAP()
		
	private:
		typedef LRESULT(DataSourceDelayerPanel::* CommandHandlerType)(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		
		DelayerController& _controller;
		std::map<DelayerMode, CommandHandlerType> _delayer_mode_handlers;
		std::function<void(bool)> _enabled_handler;
		DataSourceDelayerModeCounterPanel _counter;
		DataSourceDelayerModeTimerPanel _timer;
		CFont _button_font;
		
		LRESULT OnCbnSelChangeCmbModeCounter(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbModeTimer(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnBnClickedRightAutoDelay(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnBnClickedLeftAutoDelay(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnBnClickedStopAutoDelay(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbMode(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnEnChangeEdtStep(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		
		Delay delay_value(std::float_t nanoseconds) const;
		std::float_t delay_nanoseconds(Delay delay) const;
	};
}