#include "..\..\..\stdafx.hpp"
#include "..\..\..\Resource.hpp"
#include "..\..\..\View\Utilities.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetDataSourceDelayerModeTimerPanel.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\DelayerModes\EthernetDelayerModeTimerController.hpp"


#include <string>
#include <array>



namespace Ethernet
{
	DataSourceDelayerModeTimerPanel::DataSourceDelayerModeTimerPanel(DelayerModeTimerController &controller):
		_controller(controller)
	{
	}

	void DataSourceDelayerModeTimerPanel::destroy()
	{
		this->DestroyWindow();
	}

	void DataSourceDelayerModeTimerPanel::create(CWindow owner, CWindow placeholder, bool visible)
	{
		WINDOWPLACEMENT placement = {sizeof(placement)};
		placeholder.GetWindowPlacement(&placement);
		placeholder.ShowWindow(SW_HIDE);
		this->Create(owner);
		this->SetWindowPlacement(&placement);
		this->SetWindowPos(placeholder, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
		this->SetDlgCtrlID(DataSourceDelayerModeTimerPanel::IDD);
		this->ShowWindow(visible ? SW_SHOW : SW_HIDE);
	}

	void DataSourceDelayerModeTimerPanel::show(bool visible)
	{
		this->ShowWindow(visible ? SW_SHOW : SW_HIDE);
	}

	void DataSourceDelayerModeTimerPanel::reset()
	{
	}

	void DataSourceDelayerModeTimerPanel::start_recording(bool scanning)
	{
	}

	void DataSourceDelayerModeTimerPanel::stop_recording(bool scanning)
	{
	}

	void DataSourceDelayerModeTimerPanel::start_scanning(bool recording)
	{
	}

	void DataSourceDelayerModeTimerPanel::stop_scanning(bool recording)
	{
	}




	LRESULT DataSourceDelayerModeTimerPanel::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		utility::set_dialog_item_float(*this, IDC_EDT_DELAYER_TIMER_FREQUENCY, this->duration_to_value(_controller.interval()), 3);
		return TRUE;
	}

	LRESULT DataSourceDelayerModeTimerPanel::OnEnChangeEdtInterval(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_controller.interval(this->value_to_duration(static_cast<std::float_t>(utility::get_dialog_item_float(*this, IDC_EDT_DELAYER_TIMER_FREQUENCY))));
		return LRESULT();
	}

	LRESULT DataSourceDelayerModeTimerPanel::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		return LRESULT();
	}

	std::chrono::milliseconds DataSourceDelayerModeTimerPanel::value_to_duration(std::float_t value) const
	{
		return std::chrono::milliseconds(static_cast<std::chrono::milliseconds::rep>(std::round(value * static_cast<std::float_t>(1000.))));
	}

	std::float_t DataSourceDelayerModeTimerPanel::duration_to_value(std::chrono::milliseconds value) const
	{
		return static_cast<std::float_t>(value.count())/static_cast<std::float_t>(1000.);
	}
}