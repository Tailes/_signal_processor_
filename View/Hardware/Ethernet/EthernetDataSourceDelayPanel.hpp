#pragma once
#include "..\..\..\resource.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <functional>
#include <algorithm>

using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\WTL\atlapp.h"
#include "..\..\..\WTL\atlframe.h"
#include "..\..\..\WTL\atlctrls.h"



namespace Ethernet
{
	class TimerController;
	class OutputController;
	class DataSourceController;
	
	
	
	class DataSourceDelayPanel:
		public CDialogImpl<DataSourceDelayPanel>,
		public CDialogResize<DataSourceDelayPanel>
	{
	public:
		enum: unsigned long { IDD = IDD_ETHERNET_DATA_SOURCE_DELAY_PANEL };
		
	public:
		DataSourceDelayPanel(DataSourceController &datasource);
		
		void start_recording(bool scanning);
		void start_scanning(bool recording);
		void stop_recording(bool scanning);
		void stop_scanning(bool recording);
		void destroy();
		void update();
		void create(CWindow parent, CWindow placeholder, bool visible);
		void reset();
		void show(bool visible);
		
		void delay_change_handler(const std::function<void(Delay)> &callback);
		
		void on_change_delay(Delay value);
		void on_auto_delay(bool enabled);
		
	public:
		BEGIN_DLGRESIZE_MAP(DataSourceDelayPanel)
			DLGRESIZE_CONTROL(IDC_GRPBOX_DELAY, DLSZ_SIZE_X)
			DLGRESIZE_CONTROL(IDC_SLD_DELAY, DLSZ_SIZE_X)
		END_DLGRESIZE_MAP()
		
		BEGIN_MSG_MAP(DataSourceDelayPanel)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			MESSAGE_HANDLER(WM_HSCROLL, OnHScroll)
			MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
			CHAIN_MSG_MAP(CDialogResize<DataSourceDelayPanel>)
		END_MSG_MAP()
		
	private:
		DataSourceController& _datasource;
		std::function<void(Delay)> _delay_change_handler;
		CTrackBarCtrl _trackbar;
		bool _delay_locked;
		
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnHScroll(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		
		Delay delay_value(std::uint32_t tick) const;
		
		std::uint32_t delay_tick(Delay value) const;
	};

}