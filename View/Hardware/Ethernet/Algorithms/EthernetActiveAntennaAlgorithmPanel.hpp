#pragma once
#include "..\..\..\..\resource.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <functional>
#include <algorithm>
#include <string>

using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\..\WTL\atlapp.h"
#include "..\..\..\..\WTL\atlframe.h"
#include "..\..\..\..\WTL\atlctrls.h"



namespace Ethernet
{
	class ChannelerController;




	class ScanningAlgorithmActiveAntennaPanel :
		public CDialogImpl<ScanningAlgorithmActiveAntennaPanel>
	{
	public:
		enum : unsigned long { IDD = IDD_ETHERNET_DATA_SOURCE_ACTIVE_ANTENNA_PANEL };
		
	public:
		ScanningAlgorithmActiveAntennaPanel(ChannelerController &controller);
		
	public:
		BEGIN_MSG_MAP(ScanningAlgorithmActiveAntennaPanel)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
			COMMAND_HANDLER(IDC_CMB_ETHERNET_CHANNEL, CBN_SELCHANGE, OnCbnSelChangeCmbChannel);
		END_MSG_MAP()
		
	private:
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbChannel(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);

		std::wstring format_channel_name(Channel channel) const;
		
	private:
		ChannelerController & _controller;
	};
}