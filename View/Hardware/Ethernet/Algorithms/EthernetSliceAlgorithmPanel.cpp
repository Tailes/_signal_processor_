#include "..\..\..\..\View\Hardware\Ethernet\Algorithms\EthernetSliceAlgorithmPanel.hpp"
#include "..\..\..\..\Controller\Hardware\Ethernet\Algorithms\EthernetScanningAlgorithmSliceController.hpp"

#include <stdexcept>


namespace Ethernet
{
	ScanningAlgorithmSlicePanel::ScanningAlgorithmSlicePanel(ScanningAlgorithmSliceController & controller):
		_controller(controller)
	{
	}

	LRESULT ScanningAlgorithmSlicePanel::OnInitDialog(UINT, WPARAM, LPARAM, BOOL &)
	{
		this->CheckDlgButton(IDC_BTN_CHANNEL1, _controller.enabled(Channel::_1) ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_BTN_CHANNEL2, _controller.enabled(Channel::_2) ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_BTN_CHANNEL3, _controller.enabled(Channel::_3) ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_BTN_CHANNEL4, _controller.enabled(Channel::_4) ? BST_CHECKED : BST_UNCHECKED);
		return LRESULT();
	}

	LRESULT ScanningAlgorithmSlicePanel::OnDestroy(UINT, WPARAM, LPARAM, BOOL &)
	{
		return LRESULT();
	}

	template<unsigned int ID, Channel Selection>
	inline LRESULT ScanningAlgorithmSlicePanel::OnBnClickedBtnChannel(WORD, WORD, HWND, BOOL &)
	{
		if (this->IsDlgButtonChecked(ID) == BST_CHECKED)
		{
			_controller.enable(Selection);
		}
		else if (this->IsDlgButtonChecked(ID) == BST_UNCHECKED)
		{
			try
			{
				_controller.disable(Selection);
			}
			catch (std::runtime_error &)
			{
				::AtlMessageBox(nullptr, IDS_ETHERNET_SCANNING_MODE_SLICE_AT_LEAST_ONE);
				this->CheckDlgButton(ID, BST_CHECKED);
			}
		}
		return 0;
	}

	LRESULT ScanningAlgorithmSlicePanel::OnBnClickedBtnChannel1(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		return OnBnClickedBtnChannel<IDC_BTN_CHANNEL1, Channel::_1>(wNotifyCode, wID, hWndCtl, bHandled);
	}
	
	LRESULT ScanningAlgorithmSlicePanel::OnBnClickedBtnChannel2(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		return OnBnClickedBtnChannel<IDC_BTN_CHANNEL2, Channel::_2>(wNotifyCode, wID, hWndCtl, bHandled);
	}
	
	LRESULT ScanningAlgorithmSlicePanel::OnBnClickedBtnChannel3(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		return OnBnClickedBtnChannel<IDC_BTN_CHANNEL3, Channel::_3>(wNotifyCode, wID, hWndCtl, bHandled);
	}
	
	LRESULT ScanningAlgorithmSlicePanel::OnBnClickedBtnChannel4(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		return OnBnClickedBtnChannel<IDC_BTN_CHANNEL4, Channel::_4>(wNotifyCode, wID, hWndCtl, bHandled);
	}
}