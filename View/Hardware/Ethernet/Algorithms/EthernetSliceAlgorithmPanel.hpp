#pragma once
#include "..\..\..\..\resource.hpp"
#include "..\..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <functional>
#include <algorithm>

using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\..\WTL\atlapp.h"
#include "..\..\..\..\WTL\atlframe.h"
#include "..\..\..\..\WTL\atlctrls.h"



namespace Ethernet
{
	class ScanningAlgorithmSliceController;




	class ScanningAlgorithmSlicePanel :
		public CDialogImpl<ScanningAlgorithmSlicePanel>
	{
	public:
		enum : unsigned long { IDD = IDD_ETHERNET_DATA_SOURCE_SLICE_PANEL };
		
	public:
		ScanningAlgorithmSlicePanel(ScanningAlgorithmSliceController &controller);
		
	public:
		BEGIN_MSG_MAP(ScanningAlgorithmSlicePanel)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
			COMMAND_HANDLER(IDC_BTN_CHANNEL1, BN_CLICKED, OnBnClickedBtnChannel1)
			COMMAND_HANDLER(IDC_BTN_CHANNEL2, BN_CLICKED, OnBnClickedBtnChannel2)
			COMMAND_HANDLER(IDC_BTN_CHANNEL3, BN_CLICKED, OnBnClickedBtnChannel3)
			COMMAND_HANDLER(IDC_BTN_CHANNEL4, BN_CLICKED, OnBnClickedBtnChannel4)
		END_MSG_MAP()

	private:
		ScanningAlgorithmSliceController & _controller;
		
		template<unsigned int ID, Channel Selection>
		LRESULT OnBnClickedBtnChannel(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnBnClickedBtnChannel1(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
		LRESULT OnBnClickedBtnChannel2(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
		LRESULT OnBnClickedBtnChannel3(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
		LRESULT OnBnClickedBtnChannel4(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	};

}