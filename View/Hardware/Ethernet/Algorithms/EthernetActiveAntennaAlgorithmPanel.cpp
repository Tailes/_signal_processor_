#include "..\..\..\..\View\Utilities.hpp"
#include "..\..\..\..\View\Resources.hpp"
#include "..\..\..\..\Controller\Hardware\Ethernet\EthernetChannelerController.hpp"
#include "..\..\..\..\View\Hardware\Ethernet\Algorithms\EthernetActiveAntennaAlgorithmPanel.hpp"

#include <sstream>
#include <iomanip>


namespace Ethernet
{
	ScanningAlgorithmActiveAntennaPanel::ScanningAlgorithmActiveAntennaPanel(ChannelerController & controller) :
		_controller(controller)
	{
	}

	LRESULT ScanningAlgorithmActiveAntennaPanel::OnInitDialog(UINT, WPARAM, LPARAM, BOOL &)
	{
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_CHANNEL)).AddItem(this->format_channel_name(Channel::_1).c_str(), 0, 0, 0, static_cast<int>(Channel::_1));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_CHANNEL)).AddItem(this->format_channel_name(Channel::_2).c_str(), 0, 0, 0, static_cast<int>(Channel::_2));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_CHANNEL)).AddItem(this->format_channel_name(Channel::_3).c_str(), 0, 0, 0, static_cast<int>(Channel::_3));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_CHANNEL)).AddItem(this->format_channel_name(Channel::_4).c_str(), 0, 0, 0, static_cast<int>(Channel::_4));
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_CHANNEL)), static_cast<int>(_controller.channel()));
		return LRESULT();
	}

	LRESULT ScanningAlgorithmActiveAntennaPanel::OnDestroy(UINT, WPARAM, LPARAM, BOOL &)
	{
		return LRESULT();
	}

	LRESULT ScanningAlgorithmActiveAntennaPanel::OnCbnSelChangeCmbChannel(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_controller.channel(::getComboBoxSelectionData<Channel>(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_CHANNEL))));
		return 0;
	}

	std::wstring ScanningAlgorithmActiveAntennaPanel::format_channel_name(Channel channel) const
	{
		std::wstringstream buffer;
		buffer << resources::string<IDS_CHANNEL_FORMAT>() << static_cast<unsigned int>(channel) + 1;
		return buffer.str();
	}
}