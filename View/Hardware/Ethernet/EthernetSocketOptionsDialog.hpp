#pragma once
#include "..\..\..\resource.hpp"

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\WTL\atlapp.h"
#include "..\..\..\WTL\atlctrls.h"



namespace Ethernet
{
	class SocketSettingsController;
	class DataSourceController;
	
	
	
	class SocketOptionsDialog:
		public CSimpleDialog<IDD_ETHERNET_SOCKET_OPTIONS>
	{
	public:
		SocketOptionsDialog(DataSourceController &source);
		
		BEGIN_MSG_MAP(SocketOptionsDialog)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			COMMAND_HANDLER(IDOK, BN_CLICKED, OnOkClicked)
			CHAIN_MSG_MAP(CSimpleDialog< IDD_ETHERNET_SOCKET_OPTIONS >)
		END_MSG_MAP()
		
	private:
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnOkClicked(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		
	private:
		SocketSettingsController &_source;
		
		unsigned long address(CIPAddressCtrl &source) const;
	};
}