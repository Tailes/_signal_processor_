#include "..\..\..\StdAfx.hpp"
#include "..\..\..\resource.hpp"
#include "..\..\..\Controller\Processing\SliceRecordingFilterController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTypeRanges.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTimerController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetInterpolatorController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetDataSourceController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetOutputController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetSettingsController.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetDataSourceOptionsDialog.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetDataSourcePanel.hpp"
#include "..\..\..\View\SaveSignalsDialog.hpp"
#include "..\..\..\View\Utilities.hpp"
#include "..\..\..\View\Resources.hpp"

#include <strstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <array>
#include <regex>



namespace Ethernet
{
	DataSourceOptionsDialog::DataSourceOptionsDialog(SliceRecordingFilterController &recording, DataSourceController &controler, DataSourcePanel &panel):
		_panel(panel),
		_interpolator(controler.interpolator()),
		_datasource(controler),
		_settings(controler.settings()),
		_output(controler.channeler().signal_provider().signal_source().output()),
		_timer(controler.timer()),
		_recording(recording),
		_temperature_hint(),
		_tick_length_hint(),
		_limit_hint()
	{
		_temperature_hint.cbStruct = sizeof(_temperature_hint);
		_temperature_hint.pszText = MAKEINTRESOURCEW(IDS_TEMPERATURE_VALIDATION);
		_tick_length_hint.cbStruct = sizeof(_tick_length_hint);
		_tick_length_hint.pszText = MAKEINTRESOURCEW(IDS_TICK_LENGTH_VALIDATION);
		_limit_hint.cbStruct = sizeof(_limit_hint);
		_limit_hint.pszText = MAKEINTRESOURCEW(IDS_LIMIT_VALIDATION);
	}



	LRESULT DataSourceOptionsDialog::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
	{
		bHandled = FALSE;
		
		_packet_size.Attach(this->GetDlgItem(IDC_CMB_PACKET_SIZE));
		_packet_size.SetItemData(_packet_size.AddString(_T("512")), static_cast<int>(SignalLength::Single));
		_packet_size.SetItemData(_packet_size.AddString(_T("1024")), static_cast<int>(SignalLength::Double));
		_packet_size.SetItemData(_packet_size.AddString(_T("2048")), static_cast<int>(SignalLength::Quadruple));
		_packet_size.SetItemData(_packet_size.AddString(_T("4096")), static_cast<int>(SignalLength::Octuple));
		_packet_size.SetItemData(_packet_size.AddString(_T("8192")), static_cast<int>(SignalLength::Sedecuple));
		_probing_period.Attach(this->GetDlgItem(IDC_SPN_ETHERNET_PROBING_PERIOD));
		_probing_period.SetRange32(ProbingPeriodMin, ProbingPeriodMax);
		_probing_period.SetPos32(_output.probing_period());
		//_temperature.Attach(this->GetDlgItem(IDC_EDT_TEMPERATURE));
		//_temperature.SetWindowTextW(this->temperature_text().c_str());
		_tick_length.Attach(this->GetDlgItem(IDC_EDT_TICK_LENGTH));
		_tick_length.SetWindowTextW(this->format_tick_length(_settings.tick_length()*100.f).c_str());
		_advanced_settings.Attach(this->GetDlgItem(IDC_CHK_ETHERNET_ADVANCED_SETTINGS));
		_advanced_settings.SetCheck(_datasource.advanced_settings() ? BST_CHECKED : BST_UNCHECKED);
		//_time_correction.Attach(this->GetDlgItem(IDC_CHCK_TIME_CORRECTION));
		//_time_correction.SetCheck(_interpolator.enabled() ? BST_CHECKED : BST_UNCHECKED);
		_limit_upper.Attach(this->GetDlgItem(IDC_EDT_ETHERNET_PROBING_LIMIT_UPPER));
		_limit_lower.Attach(this->GetDlgItem(IDC_EDT_ETHERNET_PROBING_LIMIT_LOWER));
		
		utility::set_dialog_item_float(*this, IDC_EDT_ETHERNET_PROBING_LIMIT_LOWER, this->delay_to_ns(_datasource.delayer().lower_limit()), 2);
		utility::set_dialog_item_float(*this, IDC_EDT_ETHERNET_PROBING_LIMIT_UPPER, this->delay_to_ns(_datasource.delayer().upper_limit()), 2);
		utility::select_combo_box_item(_packet_size, static_cast<int>(_datasource.signal_length()));
		
		return 0;
	}

	LRESULT DataSourceOptionsDialog::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		//_time_correction.Detach();
		_probing_period.Detach();
		//_temperature.Detach();
		_packet_size.Detach();
		_tick_length.Detach();
		return 0;
	}

	LRESULT DataSourceOptionsDialog::OnOkClicked(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		std::array<wchar_t, std::numeric_limits<float>::digits10> buffer = {};
		//std::wstring temperature_text(buffer.data(), _temperature.GetWindowTextW(buffer.data(), buffer.size()));
		std::wstring tick_length_text(buffer.data(), _tick_length.GetWindowTextW(buffer.data(), buffer.size()));
		std::float_t tick_length = _settings.tick_length();
		Delay lower_limit = _datasource.delayer().lower_limit();
		Delay upper_limit = _datasource.delayer().upper_limit();
		SignalLength signal_length = _datasource.signal_length();
		SaveSignalsDialog dialog(this->GetTopLevelParent());
		
		try
		{
			tick_length = std::stof(tick_length_text, nullptr)/100.f;
		}
		catch ( std::exception & )
		{
			_tick_length.ShowBalloonTip(&_tick_length_hint);
			return LRESULT();
		}
		try
		{
			lower_limit = this->ns_to_delay(utility::get_dialog_item_float(*this, IDC_EDT_ETHERNET_PROBING_LIMIT_LOWER));
			lower_limit = std::max<Delay>(std::min<Delay>(DelayMax, lower_limit), DelayMin);
		}
		catch (std::exception &)
		{
			_limit_lower.ShowBalloonTip(&_limit_hint);
			return LRESULT();
		}
		try
		{
			upper_limit = this->ns_to_delay(utility::get_dialog_item_float(*this, IDC_EDT_ETHERNET_PROBING_LIMIT_UPPER));
			upper_limit = std::max<Delay>(std::min<Delay>(DelayMax, upper_limit), DelayMin);
		}
		catch (std::exception &)
		{
			_limit_upper.ShowBalloonTip(&_limit_hint);
			return LRESULT();
		}
		try
		{
			if (upper_limit < lower_limit) throw std::invalid_argument(nullptr);
		}
		catch (std::exception &)
		{
			::AtlMessageBox(nullptr, IDS_LIMIT_ORDERING_VALIDATION, nullptr, MB_OK);
			return LRESULT();
		}
		
		signal_length = static_cast<SignalLength>(_packet_size.GetItemData(_packet_size.GetCurSel()));
		if ( signal_length != _datasource.signal_length() && _recording.trace().is_dirty() )
		{
			switch ( ::AtlMessageBox(nullptr, IDS_DATA_UNSAVED, nullptr, MB_YESNOCANCEL) )
			{
			case IDYES:
				switch ( dialog.DoModal() )
				{
				case IDOK: _recording.trace().save(dialog.names()); break;
				case IDCANCEL: return LRESULT();
				}
				break;
			case IDNO: _recording.trace().clear(); break;
			case IDCANCEL: return LRESULT();
			}
		}
		//_interpolator.enable(FALSE != _scaling_mode.GetItemData(_scaling_mode.GetCurSel()));
		_datasource.delayer().limit(lower_limit, upper_limit);
		_datasource.signal_length(signal_length);
		_datasource.advanced_settings(_advanced_settings.GetCheck() == BST_CHECKED);
		//_output.generator_sync_frequency(std::max<unsigned short>(1, _probing_period.GetPos32()));
		_output.probing_period(std::max<std::uint16_t>(std::min<std::uint16_t>(ProbingPeriodMax, 0xFFFF & static_cast<uint16_t>(_probing_period.GetPos32())), ProbingPeriodMin));
		_settings.tick_length(tick_length);
		//_interpolator.enable(_time_correction.GetCheck() == BST_CHECKED);
		_panel.change_packet_length();
		_panel.change_tick_length();
		this->EndDialog(IDOK);
		return LRESULT();
	}

	LRESULT DataSourceOptionsDialog::OnCancelClicked(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		this->EndDialog(IDCANCEL);
		return LRESULT();
	}

	//LRESULT DataSourceOptionsDialog::OnSpnDeltaPosGeneratorFrequency(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
	//{
	//	bHandled = FALSE;
	//	reinterpret_cast<NMUPDOWN*>(pnmh)->iDelta = reinterpret_cast<NMUPDOWN*>(pnmh)->iDelta * GeneratorSyncFrequencyStep;
	//	return 0;
	//}



	//std::wstring DataSourceOptionsDialog::temperature_text() const
	//{
	//	return static_cast<std::wstringstream&>(std::wstringstream() << std::fixed << std::setprecision(2) << _timer.temperature()).str();
	//}

	std::wstring DataSourceOptionsDialog::format_tick_length(std::float_t length) const
	{
		return static_cast<std::wstringstream&>(std::wstringstream() << std::fixed << std::setprecision(0) << length).str();
	}
	
	std::float_t DataSourceOptionsDialog::delay_to_ns(Delay delay) const
	{
		return static_cast<std::float_t>(delay)/static_cast<std::float_t>(100.);
	}
	
	Delay DataSourceOptionsDialog::ns_to_delay(std::float_t ns) const
	{
		return static_cast<Delay>(std::round(ns * static_cast<std::float_t>(100.)));
	}
}