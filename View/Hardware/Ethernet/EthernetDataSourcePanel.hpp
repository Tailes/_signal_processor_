#pragma once
#include "..\..\..\resource.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"
#include "..\..\..\View\Hardware\Ethernet\Algorithms\EthernetSliceAlgorithmPanel.hpp"
#include "..\..\..\View\Hardware\Ethernet\Algorithms\EthernetActiveAntennaAlgorithmPanel.hpp"

#include <functional>
#include <algorithm>
#include <cstdint>
#include <vector>
#include <cmath>
#include <map>

using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\WTL\atlapp.h"
#include "..\..\..\WTL\atlframe.h"
#include "..\..\..\WTL\atlctrls.h"


namespace Ethernet
{
	class TimerController;
	class OutputController;
	class SettingsController;
	class ChannelerController;
	class DataSourceController;
	class PositionerController;




	class DataSourcePanel:
		public CDialogImpl<DataSourcePanel>
	{
	public:
		enum: unsigned long { IDD = IDD_ETHERNET_DATA_SOURCE_PANEL };
		
	public:
		DataSourcePanel(DataSourceController &datasource);
		
		void delay_change_handler(const std::function<void(Delay)> &callback);
		
		void change_packet_length();
		void change_tick_length();
		void update();
		void reset();
		void update_settings();
		void start_recording(bool recording);
		void stop_recording(bool recording);
		void start_scanning(bool recording);
		void stop_scanning(bool recording);
		
		void on_advanced_settings(bool enabled);
		void on_change_delay(Delay delay);
		void on_auto_delay(bool enabled);
		
	private:
		BEGIN_MSG_MAP(DataSourcePanel)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			COMMAND_HANDLER(IDC_CMB_ETHERNET_SCANNING_MODE, CBN_SELCHANGE, OnCbnSelChangeCmbScanningMode);
			COMMAND_HANDLER(IDC_EDT_ETHERNET_DELAY, EN_CHANGE, OnEnChangeEdtDelay);
			COMMAND_HANDLER(IDC_EDT_ETHERNET_CHARGES_COUNT, EN_CHANGE, OnEnChangeChargesCount);
			COMMAND_HANDLER(IDC_EDT_ETHERNET_STROBE_PULSE_LENGTH, EN_CHANGE, OnEnChangeStrobePulseLength);
			//COMMAND_HANDLER(IDC_EDT_ETHERNET_PROBING_PERIOD, EN_CHANGE, OnEnChangeEdtProbingPeriod);
			COMMAND_HANDLER(IDC_CHK_CALIBRATION, BN_CLICKED, OnBnCheckedCalibration);
			COMMAND_HANDLER(IDC_CMB_ETHERNET_TRIGGER_MODE, CBN_SELCHANGE, OnCbnSelChangeCmbTriggerMode);
			COMMAND_HANDLER(IDC_CMB_ETHERNET_TRIGGER_SCANS, CBN_SELCHANGE, OnCbnSelChangeCmbScansPerTrigger);
			COMMAND_HANDLER(IDC_CMB_ETHERNET_TRIGGER_STEPS, CBN_SELCHANGE, OnCbnSelChangeCmbStepsPerTrigger);
			COMMAND_HANDLER(IDC_CMB_ETHERNET_TIMEFRAME_512, CBN_SELCHANGE, OnCbnSelChangeCmbTimeFrame512);
			COMMAND_HANDLER(IDC_CMB_ETHERNET_TIMEFRAME_1024, CBN_SELCHANGE, OnCbnSelChangeCmbTimeFrame1024);
			COMMAND_HANDLER(IDC_CMB_ETHERNET_TIMEFRAME_2048, CBN_SELCHANGE, OnCbnSelChangeCmbTimeFrame2048);
			COMMAND_HANDLER(IDC_CMB_ETHERNET_TIMEFRAME_4096, CBN_SELCHANGE, OnCbnSelChangeCmbTimeFrame4096);
			COMMAND_HANDLER(IDC_CMB_ETHERNET_TIMEFRAME_8192, CBN_SELCHANGE, OnCbnSelChangeCmbTimeFrame8192);
		END_MSG_MAP()
		
		//template<Mode Algorithm> LRESULT OnBnClickedAlgorithm(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnEnChangeEdtDelay(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnEnChangeChargesCount(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnEnChangeStrobePulseLength(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		//LRESULT OnEnChangeEdtProbingPeriod(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnBnCheckedCalibration(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbTriggerMode(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbScanningMode(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbTimeFrame512(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbTimeFrame1024(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbTimeFrame2048(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbTimeFrame4096(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbTimeFrame8192(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbScansPerTrigger(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnCbnSelChangeCmbStepsPerTrigger(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		
	private:
		Delay delay_value(std::uint32_t index) const;
		std::uint32_t delay_index(Delay value) const;
		std::wstring format_step_length(float length) const;
		
		void create_overlay_panels(CWindow &placeholder);
		
		void on_trigger_steps_count(std::uint8_t steps);
		void on_trigger_scans_count(std::uint8_t scans);
		void on_trigger_mode(TriggerMode mode);
		void on_tick_length(std::float_t length);
		
	private:
		std::map<SignalLength, unsigned long> _frames;
		std::function<void(Delay)> _delay_change_handler;
		ScanningAlgorithmActiveAntennaPanel _active_antenna_panel;
		ScanningAlgorithmSlicePanel _slice_panel;
		DataSourceController &_datasource;
		PositionerController &_positioner;
		ChannelerController &_channeler;
		SettingsController &_settings;
		OutputController &_output;
		TimerController &_timer;
		bool _delay_locked;
	};
}