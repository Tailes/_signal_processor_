#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Resource.hpp"
#include "..\..\..\utility\TypeArithmetics.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetDataSourceController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetSocketSettingsController.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetSocketOptionsDialog.hpp"
#include "..\..\..\View\Utilities.hpp"

#include <string>
#include <vector>
#include <cvt\wstring>
#include <cvt\utf8>

//#include <ws2bth.h>



namespace Ethernet
{
	SocketOptionsDialog::SocketOptionsDialog(DataSourceController &source):
		_source(source.channeler().signal_provider().signal_source().settings())
	{
	}


	LRESULT SocketOptionsDialog::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
	{
		CComboBox address_family = this->GetDlgItem(IDC_CMB_ADDRESS_FAMILY);
		address_family.SetItemData(address_family.AddString(_T("Inet(IPv4)")), AF_INET);
		//address_family.SetItemData(address_family.AddString(_T("Inet(IPv6)")), AF_INET6);		//Not Needed
		//address_family.SetItemData(address_family.AddString(_T("IRDA")), AF_IRDA);			//Not Needed
		//address_family.SetItemData(address_family.AddString(_T("BTH")), AF_BTH);				//Not Needed
		utility::select_combo_box_item(address_family, _source.address_family());
		
		CComboBox type = this->GetDlgItem(IDC_CMB_TYPE);
		type.SetItemData(type.AddString(_T("Stream")), SOCK_STREAM);
		type.SetItemData(type.AddString(_T("Datagram")), SOCK_DGRAM);
		//type.SetItemData(type.AddString(_T("Raw")), SOCK_RAW);								//Not Needed
		//type.SetItemData(type.AddString(_T("Reliable Datagram")), SOCK_RDM);					//Not Needed
		//type.SetItemData(type.AddString(_T("Pseudo Stream")), SOCK_SEQPACKET);				//Not Needed
		utility::select_combo_box_item(type, _source.type());
		
		CComboBox protocol = this->GetDlgItem(IDC_CMB_PROTOCOL);
		//protocol.SetItemData(protocol.AddString(_T("ICMP")), IPPROTO_ICMP);					//Not Needed
		//protocol.SetItemData(protocol.AddString(_T("IGMP")), IPPROTO_IGMP);					//Not Needed
		//protocol.SetItemData(protocol.AddString(_T("Bluetooth RFCOMM")), BTHPROTO_RFCOMM);	//Not Needed
		protocol.SetItemData(protocol.AddString(_T("TCP")), IPPROTO_TCP);
		protocol.SetItemData(protocol.AddString(_T("UDP")), IPPROTO_UDP);
		//protocol.SetItemData(protocol.AddString(_T("ICMPv6")), IPPROTO_ICMPV6);				//Not Needed
		//protocol.SetItemData(protocol.AddString(_T("Reliable Multicast")), IPPROTO_RM);		//Not Installed
		utility::select_combo_box_item(protocol, _source.protocol());
		
		CUpDownCtrl source_port = this->GetDlgItem(IDC_SPN_SOURCE_PORT);
		source_port.SetRange32(std::numeric_limits<unsigned short>::min(), std::numeric_limits<unsigned short>::max());
		source_port.SetPos32(_source.source_port());
		
		CUpDownCtrl target_port = this->GetDlgItem(IDC_SPN_TARGET_PORT);
		target_port.SetRange32(std::numeric_limits<unsigned short>::min(), std::numeric_limits<unsigned short>::max());
		target_port.SetPos32(_source.target_port());
		
		CUpDownCtrl input_timeout = this->GetDlgItem(IDC_SPN_INPUT_TIMEOUT);
		input_timeout.SetRange32(0, std::numeric_limits<int>::max());
		input_timeout.SetPos32(_source.receive_timeout());
		
		CUpDownCtrl output_timeout = this->GetDlgItem(IDC_SPN_OUTPUT_TIMEOUT);
		output_timeout.SetRange32(0, std::numeric_limits<int>::max());
		output_timeout.SetPos32(_source.send_timeout());
		
		CUpDownCtrl linger_timeout = this->GetDlgItem(IDC_SPN_LINGER_TIMEOUT);
		linger_timeout.SetRange32(0, std::numeric_limits<int>::max());
		linger_timeout.SetPos32(_source.linger_delay());
		
		CUpDownCtrl input_buffer_size = this->GetDlgItem(IDC_SPN_INPUT_BUFFER_SIZE);
		input_buffer_size.SetRange32(0, std::numeric_limits<int>::max());
		input_buffer_size.SetPos32(_source.input_buffer_size());
		
		CUpDownCtrl output_buffer_size = this->GetDlgItem(IDC_SPN_OUTPUT_BUFFER_SIZE);
		output_buffer_size.SetRange32(0, std::numeric_limits<int>::max());
		output_buffer_size.SetPos32(_source.output_buffer_size());
		
		CIPAddressCtrl source_ip = this->GetDlgItem(IDC_IPADDRESS_SOURCE);
		source_ip.SetAddress(utility::reverse_dword(_source.source_ip()));
		
		CIPAddressCtrl target_ip = this->GetDlgItem(IDC_IPADDRESS_TARGET);
		target_ip.SetAddress(utility::reverse_dword(_source.target_ip()));
		
		CEdit linger_timeout_edit = this->GetDlgItem(IDC_EDT_LINGER_TIMEOUT);
		linger_timeout_edit.EnableWindow(_source.lingering() ? TRUE : FALSE);
		
		this->CheckDlgButton(IDC_CHECK_LINGER, _source.lingering() ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_CHECK_ADDRESS_REUSE, _source.address_reuse() ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_CHECK_EXCLUSIVE_ADDRESS_USE, _source.exclusive_address_use() ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_CHECK_KEEP_ALIVE, _source.keep_alive() ? BST_CHECKED : BST_UNCHECKED);
		this->CheckDlgButton(IDC_CHECK_NAGLE, _source.nagle_algorithm() ? BST_CHECKED : BST_UNCHECKED);
		
		this->SetDlgItemInt(IDC_EDT_LINGER_TIMEOUT, _source.lingering() ? _source.linger_delay() : 0);
		
		return 0;
	}

	LRESULT SocketOptionsDialog::OnOkClicked(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		bHandled = FALSE;
		
		_source.address_family(CComboBox(this->GetDlgItem(IDC_CMB_ADDRESS_FAMILY)).GetItemData(CComboBox(this->GetDlgItem(IDC_CMB_ADDRESS_FAMILY)).GetCurSel()));
		_source.type(CComboBox(this->GetDlgItem(IDC_CMB_TYPE)).GetItemData(CComboBox(this->GetDlgItem(IDC_CMB_TYPE)).GetCurSel()));
		_source.protocol(CComboBox(this->GetDlgItem(IDC_CMB_PROTOCOL)).GetItemData(CComboBox(this->GetDlgItem(IDC_CMB_PROTOCOL)).GetCurSel()));
		_source.source_ip(this->address(CIPAddressCtrl(this->GetDlgItem(IDC_IPADDRESS_SOURCE))));
		_source.target_ip(this->address(CIPAddressCtrl(this->GetDlgItem(IDC_IPADDRESS_TARGET))));
		_source.source_port(CUpDownCtrl(this->GetDlgItem(IDC_SPN_SOURCE_PORT)).GetPos32());
		_source.target_port(CUpDownCtrl(this->GetDlgItem(IDC_SPN_TARGET_PORT)).GetPos32());
		_source.send_timeout(CUpDownCtrl(this->GetDlgItem(IDC_SPN_OUTPUT_TIMEOUT)).GetPos32());
		_source.receive_timeout(CUpDownCtrl(this->GetDlgItem(IDC_SPN_INPUT_TIMEOUT)).GetPos32());
		_source.input_buffer_size(CUpDownCtrl(this->GetDlgItem(IDC_SPN_INPUT_BUFFER_SIZE)).GetPos32());
		_source.output_buffer_size(CUpDownCtrl(this->GetDlgItem(IDC_SPN_OUTPUT_BUFFER_SIZE)).GetPos32());
		_source.linger_delay(CUpDownCtrl(this->GetDlgItem(IDC_SPN_LINGER_TIMEOUT)).GetPos32());
		_source.linger(this->IsDlgButtonChecked(IDC_CHECK_LINGER) == BST_CHECKED);
		_source.keep_alive(this->IsDlgButtonChecked(IDC_CHECK_KEEP_ALIVE) == BST_CHECKED);
		_source.address_reuse(this->IsDlgButtonChecked(IDC_CHECK_ADDRESS_REUSE) == BST_CHECKED);
		_source.nagle_algorithm(this->IsDlgButtonChecked(IDC_CHECK_NAGLE) == BST_CHECKED);
		_source.exclusive_address_use(this->IsDlgButtonChecked(IDC_CHECK_EXCLUSIVE_ADDRESS_USE) == BST_CHECKED);
		return 0;
	}

	unsigned long SocketOptionsDialog::address(CIPAddressCtrl &source) const
	{
		std::vector<wchar_t> buffer(source.GetWindowTextLength() + 1);
		source.GetWindowText(buffer.data(), buffer.size());
		return ::inet_addr(std::string(std::wstring_convert<stdext::cvt::codecvt_utf8<wchar_t>>().to_bytes(std::wstring(buffer.data(), buffer.size()))).c_str());
	}
}