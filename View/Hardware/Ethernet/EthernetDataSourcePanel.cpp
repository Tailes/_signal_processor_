#include "..\..\..\StdAfx.hpp"
#include "..\..\..\Resource.hpp"
#include "..\..\..\View\Utilities.hpp"
#include "..\..\..\View\Resources.hpp"
#include "..\..\..\Utility\TypeArithmetics.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetDataSourcePanel.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetCalibrationDialog.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTypeRanges.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTimerController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetChannelerController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetDataSourceController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetInputController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetOutputController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetSettingsController.hpp"

#include <sstream>
#include <iomanip>
#include <string>
#include <map>





namespace Ethernet
{
	DataSourcePanel::DataSourcePanel(DataSourceController &datasource):
		_active_antenna_panel(datasource.channeler()),
		_slice_panel(datasource.channeler().scanning_algorithm_slice()),
		_positioner(datasource.channeler().signal_provider().signal_source().positioner()),
		_datasource(datasource),
		_channeler(datasource.channeler()),
		_settings(datasource.settings()),
		_output(datasource.channeler().signal_provider().signal_source().output()),
		_timer(datasource.timer()),
		_delay_locked(false)
	{
		_frames[SignalLength::Single] = IDC_CMB_ETHERNET_TIMEFRAME_512;
		_frames[SignalLength::Double] = IDC_CMB_ETHERNET_TIMEFRAME_1024;
		_frames[SignalLength::Quadruple] = IDC_CMB_ETHERNET_TIMEFRAME_2048;
		_frames[SignalLength::Octuple] = IDC_CMB_ETHERNET_TIMEFRAME_4096;
		_frames[SignalLength::Sedecuple] = IDC_CMB_ETHERNET_TIMEFRAME_8192;
		
		_output.trigger_scans_callback([this](std::uint8_t scans) { this->on_trigger_scans_count(scans); });
		_output.trigger_steps_callback([this](std::uint8_t steps) { this->on_trigger_steps_count(steps); });
		_output.trigger_mode_callback([this](Ethernet::TriggerMode mode) { this->on_trigger_mode(mode); });
		
		_settings.tick_length_callback([this](std::float_t length) { this->on_tick_length(length); });
		
		_datasource.advanced_settings_change_handler([this](bool enabled) {this->on_advanced_settings(enabled); });
	}


	void DataSourcePanel::delay_change_handler(const std::function<void(Delay)> &callback)
	{
		_delay_change_handler = callback;
	}

	void DataSourcePanel::update_settings()
	{
		//CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_PROBING_PERIOD)).SetRange32(_output.generator_sync_frequency(), GeneratorSyncFrequencyMax);
		//CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_PROBING_PERIOD)).SetPos32(CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_PROBING_PERIOD)).GetPos32());
	}


	void DataSourcePanel::start_scanning(bool recording)
	{
		this->GetDlgItem(IDC_CMB_ETHERNET_SCANNING_MODE).EnableWindow(FALSE);
		this->GetDlgItem(IDD_ETHERNET_DATA_SOURCE_SLICE_PANEL).EnableWindow(FALSE);
		this->GetDlgItem(IDD_ETHERNET_DATA_SOURCE_ACTIVE_ANTENNA_PANEL).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_CHK_CALIBRATION).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_EDT_ETHERNET_DELAY).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_SPN_ETHERNET_DELAY).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192).EnableWindow(FALSE);
	}


	void DataSourcePanel::stop_scanning(bool recording)
	{
		this->GetDlgItem(IDC_CHK_CALIBRATION).EnableWindow(TRUE);
		this->GetDlgItem(IDC_EDT_ETHERNET_DELAY).EnableWindow(_delay_locked ? FALSE : TRUE);
		this->GetDlgItem(IDC_SPN_ETHERNET_DELAY).EnableWindow(_delay_locked ? FALSE : TRUE);
		this->GetDlgItem(IDC_CMB_ETHERNET_SCANNING_MODE).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192).EnableWindow(TRUE);
		this->GetDlgItem(IDD_ETHERNET_DATA_SOURCE_SLICE_PANEL).EnableWindow(TRUE);
		this->GetDlgItem(IDD_ETHERNET_DATA_SOURCE_ACTIVE_ANTENNA_PANEL).EnableWindow(TRUE);
	}


	void DataSourcePanel::start_recording(bool recording)
	{
		if (recording) this->GetDlgItem(IDC_CHK_CALIBRATION).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_EDT_ETHERNET_DELAY).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_SPN_ETHERNET_DELAY).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096).EnableWindow(FALSE);
		if (recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192).EnableWindow(FALSE);
		//if (recording) this->GetDlgItem(IDC_EDT_ETHERNET_PROBING_PERIOD).EnableWindow(FALSE);
	}


	void DataSourcePanel::stop_recording(bool recording)
	{
		if (!recording) this->GetDlgItem(IDC_CHK_CALIBRATION).EnableWindow(TRUE);
		if (!recording) this->GetDlgItem(IDC_EDT_ETHERNET_DELAY).EnableWindow(_delay_locked ? FALSE : TRUE);
		if (!recording) this->GetDlgItem(IDC_SPN_ETHERNET_DELAY).EnableWindow(_delay_locked ? FALSE : TRUE);
		if (!recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512).EnableWindow(TRUE);
		if (!recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024).EnableWindow(TRUE);
		if (!recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048).EnableWindow(TRUE);
		if (!recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096).EnableWindow(TRUE);
		if (!recording) this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192).EnableWindow(TRUE);
		//if (!recording) this->GetDlgItem(IDC_EDT_ETHERNET_PROBING_PERIOD).EnableWindow(TRUE);
	}


	void DataSourcePanel::reset()
	{
		this->GetDlgItem(IDC_SPN_ETHERNET_DELAY).EnableWindow(TRUE);
		this->GetDlgItem(IDC_EDT_ETHERNET_DELAY).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096).EnableWindow(TRUE);
		this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192).EnableWindow(TRUE);
		this->GetDlgItem(IDD_ETHERNET_DATA_SOURCE_SLICE_PANEL).EnableWindow(TRUE);
		this->GetDlgItem(IDD_ETHERNET_DATA_SOURCE_ACTIVE_ANTENNA_PANEL).EnableWindow(TRUE);
	}

	void DataSourcePanel::change_packet_length()
	{
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512)).ShowWindow(_datasource.signal_length() == SignalLength::Single ? SW_SHOW : SW_HIDE);
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024)).ShowWindow(_datasource.signal_length() == SignalLength::Double ? SW_SHOW : SW_HIDE);
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048)).ShowWindow(_datasource.signal_length() == SignalLength::Quadruple ? SW_SHOW : SW_HIDE);
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096)).ShowWindow(_datasource.signal_length() == SignalLength::Octuple ? SW_SHOW : SW_HIDE);
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192)).ShowWindow(_datasource.signal_length() == SignalLength::Sedecuple ? SW_SHOW : SW_HIDE);
	}

	void DataSourcePanel::change_tick_length()
	{
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS)).ResetContent();
		for ( std::size_t i = StepsPerTriggerMin; i <= StepsPerTriggerMax; i += StepsPerTriggerStep )
			CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS)).AddItem(utility::get_decimal(static_cast<unsigned int>(i * _settings.tick_length()*100.f)).c_str(), 0, 0, 0, i);
		if (_output.trigger_mode() != TriggerMode::None)
			utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS)), _output.steps_per_trigger());
	}

	void DataSourcePanel::update()
	{
		if (_delay_locked)
		{
			CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_DELAY)).SetPos32(this->delay_index(_datasource.scanning_delay()));
		}
	}



	void DataSourcePanel::on_advanced_settings(bool enabled)
	{
		this->GetDlgItem(IDC_SPN_ETHERNET_CHARGES_COUNT).ShowWindow(enabled ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_EDT_ETHERNET_CHARGES_COUNT).ShowWindow(enabled ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_TXT_ETHERNET_CHARGES_COUNT).ShowWindow(enabled ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_TXT_ETHERNET_CHARGES_COUNT_EXT).ShowWindow(enabled ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_SPN_ETHERNET_STROBE_PULSE_LENGTH).ShowWindow(enabled ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_EDT_ETHERNET_STROBE_PULSE_LENGTH).ShowWindow(enabled ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_TXT_ETHERNET_STROBE_PULSE_LENGTH).ShowWindow(enabled ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_TXT_ETHERNET_STROBE_PULSE_LENGTH_EXT).ShowWindow(enabled ? SW_SHOW : SW_HIDE);
	}

	void DataSourcePanel::on_change_delay(Delay value)
	{
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_DELAY)).SetPos32(this->delay_index(value));
	}

	void DataSourcePanel::on_auto_delay(bool enabled)
	{
		_delay_locked = enabled;
		this->GetDlgItem(IDC_SPN_ETHERNET_DELAY).EnableWindow(enabled ? FALSE : TRUE);
		this->GetDlgItem(IDC_EDT_ETHERNET_DELAY).EnableWindow(enabled ? FALSE : TRUE);
	}



	std::wstring DataSourcePanel::format_step_length(float length) const
	{
		return static_cast<std::wstringstream&>(std::wstringstream() << std::fixed << std::setprecision(1) << length).str();
	}

	void DataSourcePanel::create_overlay_panels(CWindow &placeholder)
	{
		WINDOWPLACEMENT placement = { sizeof(placement) };
		placeholder.GetWindowPlacement(&placement);
		_slice_panel.Create(m_hWnd);
		_slice_panel.SetWindowPlacement(&placement);
		_slice_panel.SetDlgCtrlID(IDD_ETHERNET_DATA_SOURCE_SLICE_PANEL);
		_slice_panel.ShowWindow(_channeler.mode() == Mode::Slice ? SW_SHOW : SW_HIDE);
		_active_antenna_panel.Create(m_hWnd);
		_active_antenna_panel.SetWindowPlacement(&placement);
		_active_antenna_panel.SetDlgCtrlID(IDD_ETHERNET_DATA_SOURCE_ACTIVE_ANTENNA_PANEL);
		_active_antenna_panel.ShowWindow(_channeler.mode() == Mode::ActiveAntenna ? SW_SHOW : SW_HIDE);
	}

	Delay DataSourcePanel::delay_value(std::uint32_t index) const
	{
		return static_cast<Delay>(index);
	}

	std::uint32_t DataSourcePanel::delay_index(Delay value) const
	{
		return static_cast<std::uint32_t>(value);
	}





	LRESULT DataSourcePanel::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
	{
		this->create_overlay_panels(this->GetDlgItem(IDC_SCANNING_MODE_PLACEHOLDER));
		
		//CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_PROBING_PERIOD)).SetBuddy(this->GetDlgItem(IDC_EDT_ETHERNET_PROBING_PERIOD));
		//CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_PROBING_PERIOD)).SetRange32(Ethernet::ProbingPeriodMin, Ethernet::ProbingPeriodMax);
		//CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_PROBING_PERIOD)).SetPos32(_output.probing_period());
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_DELAY)).SetBuddy(this->GetDlgItem(IDC_EDT_ETHERNET_DELAY));
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_DELAY)).SetRange32(DelayMin, DelayMax);
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_DELAY)).SetPos32(this->delay_index(_datasource.scanning_delay()));
		
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_CHARGES_COUNT)).SetBuddy(this->GetDlgItem(IDC_EDT_ETHERNET_CHARGES_COUNT));
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_CHARGES_COUNT)).SetRange32(HomogenousAccumulationMin, HomogenousAccumulationMax);
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_CHARGES_COUNT)).SetPos32(_output.charges_count());
		
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_STROBE_PULSE_LENGTH)).SetBuddy(this->GetDlgItem(IDC_EDT_ETHERNET_STROBE_PULSE_LENGTH));
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_STROBE_PULSE_LENGTH)).SetRange32(StrobePulseLengthMin, StrobePulseLengthMax);
		CUpDownCtrl(this->GetDlgItem(IDC_SPN_ETHERNET_STROBE_PULSE_LENGTH)).SetPos32(_output.strobe_pulse_length());
		
		//for ( std::size_t i = DelayMin; i <= DelayMax; i += DelayStep )
		//	CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_DELAY)).AddItem(utility::get_decimal(i * DelayCoeff).c_str(), 0, 0, 0, i);
		//for ( std::size_t i = StrobePulseLengthMin; i <= StrobePulseLengthMax; i += StrobePulseLengthStep )
		//	CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_STROBEPULSE)).AddItem(utility::get_decimal(i).c_str(), 0, 0, 0, i);
		for ( std::size_t i = ScansPerTriggerMin; i <= ScansPerTriggerMax; i += ScansPerTriggerStep )
			CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_SCANS)).AddItem(utility::get_decimal(i).c_str(), 0, 0, 0, i);
		for ( std::size_t i = StepsPerTriggerMin; i <= StepsPerTriggerMax; i += StepsPerTriggerStep )
			CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS)).AddItem(this->format_step_length(i*100*_settings.tick_length()).c_str(), 0, 0, 0, i);
		//for ( std::size_t i = HomogenousAccumulationMin; i <= HomogenousAccumulationMax; i += HomogenousAccumulationStep )
		//	CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_HOMOGENOUS_ACCUMULATION)).AddItem(utility::get_decimal(i).c_str(), 0, 0, 0, i);
		
		//this->CheckDlgButton(IDC_CHK_LIMITERS, _output.scans_per_trigger() != 0 ? BST_CHECKED : BST_UNCHECKED);
		
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_SCANNING_MODE)).AddItem(resources::string<IDS_ETHERNET_SCANNING_MODE_SLICE>().c_str(), 0, 0, 0, static_cast<int>(Mode::Slice));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_SCANNING_MODE)).AddItem(resources::string<IDS_ETHERNET_SCANNING_MODE_ACTIVE_ANTENNA>().c_str(), 0, 0, 0, static_cast<int>(Mode::ActiveAntenna));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_SCANNING_MODE)).AddItem(resources::string<IDS_ETHERNET_SCANNING_MODE_CROSS_POLARIZATION>().c_str(), 0, 0, 0, static_cast<int>(Mode::ActiveAntennaCrossInput));
		
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512)).AddItem(L"5", 0, 0, 0, static_cast<int>(TimeFrameSingle::_05_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512)).AddItem(L"10", 0, 0, 0, static_cast<int>(TimeFrameSingle::_10_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512)).AddItem(L"20", 0, 0, 0, static_cast<int>(TimeFrameSingle::_20_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512)).AddItem(L"25", 0, 0, 0, static_cast<int>(TimeFrameSingle::_25_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512)).AddItem(L"40", 0, 0, 0, static_cast<int>(TimeFrameSingle::_40_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512)).AddItem(L"80", 0, 0, 0, static_cast<int>(TimeFrameSingle::_80_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512)).AddItem(L"160", 0, 0, 0, static_cast<int>(TimeFrameSingle::_160_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512)).ShowWindow(_datasource.signal_length() == SignalLength::Single ? SW_SHOW : SW_HIDE);
		
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024)).AddItem(L"10", 0, 0, 0, static_cast<int>(TimeFrameDouble::_10_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024)).AddItem(L"20", 0, 0, 0, static_cast<int>(TimeFrameDouble::_20_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024)).AddItem(L"40", 0, 0, 0, static_cast<int>(TimeFrameDouble::_40_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024)).AddItem(L"80", 0, 0, 0, static_cast<int>(TimeFrameDouble::_80_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024)).AddItem(L"160", 0, 0, 0, static_cast<int>(TimeFrameDouble::_160_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024)).ShowWindow(_datasource.signal_length() == SignalLength::Double ? SW_SHOW : SW_HIDE);
		
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048)).AddItem(L"20", 0, 0, 0, static_cast<int>(TimeFrameQuadruple::_20_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048)).AddItem(L"40", 0, 0, 0, static_cast<int>(TimeFrameQuadruple::_40_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048)).AddItem(L"80", 0, 0, 0, static_cast<int>(TimeFrameQuadruple::_80_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048)).AddItem(L"160", 0, 0, 0, static_cast<int>(TimeFrameQuadruple::_160_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048)).AddItem(L"320", 0, 0, 0, static_cast<int>(TimeFrameQuadruple::_320_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048)).ShowWindow(_datasource.signal_length() == SignalLength::Quadruple ? SW_SHOW : SW_HIDE);
		
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096)).AddItem(L"40", 0, 0, 0, static_cast<int>(TimeFrameOctuple::_40_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096)).AddItem(L"80", 0, 0, 0, static_cast<int>(TimeFrameOctuple::_80_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096)).AddItem(L"160", 0, 0, 0, static_cast<int>(TimeFrameOctuple::_160_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096)).AddItem(L"320", 0, 0, 0, static_cast<int>(TimeFrameOctuple::_320_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096)).AddItem(L"640", 0, 0, 0, static_cast<int>(TimeFrameOctuple::_640_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096)).ShowWindow(_datasource.signal_length() == SignalLength::Octuple ? SW_SHOW : SW_HIDE);
		
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192)).AddItem(L"80", 0, 0, 0, static_cast<int>(TimeFrameSedecuple::_80_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192)).AddItem(L"160", 0, 0, 0, static_cast<int>(TimeFrameSedecuple::_160_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192)).AddItem(L"320", 0, 0, 0, static_cast<int>(TimeFrameSedecuple::_320_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192)).AddItem(L"640", 0, 0, 0, static_cast<int>(TimeFrameSedecuple::_640_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192)).AddItem(L"1280", 0, 0, 0, static_cast<int>(TimeFrameSedecuple::_1280_ns));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192)).ShowWindow(_datasource.signal_length() == SignalLength::Sedecuple ? SW_SHOW : SW_HIDE);
		
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_SCANS)).EnableWindow(_output.trigger_mode() != TriggerMode::None ? TRUE : FALSE);
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS)).EnableWindow(_output.trigger_mode() != TriggerMode::None ? TRUE : FALSE);
		
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_MODE)).AddItem(resources::string<IDS_POSITIONER_MODE_NONE>().c_str(), 0, 0, 0, static_cast<int>(TriggerMode::None));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_MODE)).AddItem(resources::string<IDS_POSITIONER_MODE_WHEEL>().c_str(), 0, 0, 0, static_cast<int>(TriggerMode::WheelDirect));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_MODE)).AddItem(resources::string<IDS_POSITIONER_MODE_WHEEL_INV>().c_str(), 0, 0, 0, static_cast<int>(TriggerMode::WheelReversed));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_MODE)).AddItem(resources::string<IDS_POSITIONER_MODE_VELOCITY_COUNTER>().c_str(), 0, 0, 0, static_cast<int>(TriggerMode::Velocity));
		
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_SCANNING_MODE)), static_cast<int>(_channeler.mode()));
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_MODE)), static_cast<int>(_output.trigger_mode()));
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_SCANS)), _output.trigger_mode() == TriggerMode::None ? -1 : _output.scans_per_trigger());
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS)), _output.trigger_mode() == TriggerMode::None ? -1 : _output.steps_per_trigger());
		//utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_STROBEPULSE)), _output.strobe_pulse_length());
		//utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_HOMOGENOUS_ACCUMULATION)), HomogenousAccumulationMin);
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512)), static_cast<int>(_datasource.time_frame_single()));
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024)), static_cast<int>(_datasource.time_frame_double()));
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096)), static_cast<int>(_datasource.time_frame_octuple()));
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048)), static_cast<int>(_datasource.time_frame_quadruple()));
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192)), static_cast<int>(_datasource.time_frame_sedecuple()));
		
		this->GetDlgItem(IDC_SPN_ETHERNET_CHARGES_COUNT).ShowWindow(_datasource.advanced_settings() ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_EDT_ETHERNET_CHARGES_COUNT).ShowWindow(_datasource.advanced_settings() ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_TXT_ETHERNET_CHARGES_COUNT).ShowWindow(_datasource.advanced_settings() ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_TXT_ETHERNET_CHARGES_COUNT_EXT).ShowWindow(_datasource.advanced_settings() ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_SPN_ETHERNET_STROBE_PULSE_LENGTH).ShowWindow(_datasource.advanced_settings() ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_EDT_ETHERNET_STROBE_PULSE_LENGTH).ShowWindow(_datasource.advanced_settings() ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_TXT_ETHERNET_STROBE_PULSE_LENGTH).ShowWindow(_datasource.advanced_settings() ? SW_SHOW : SW_HIDE);
		this->GetDlgItem(IDC_TXT_ETHERNET_STROBE_PULSE_LENGTH_EXT).ShowWindow(_datasource.advanced_settings() ? SW_SHOW : SW_HIDE);
		
		return TRUE;
	}



	LRESULT DataSourcePanel::OnCbnSelChangeCmbScanningMode(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_channeler.mode(::getComboBoxSelectionData<Mode>(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_SCANNING_MODE))));
		_active_antenna_panel.ShowWindow(_channeler.mode() == Mode::ActiveAntenna ? SW_SHOW : SW_HIDE);
		_slice_panel.ShowWindow(_channeler.mode() == Mode::Slice ? SW_SHOW : SW_HIDE);
		//utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_CHANNEL)), static_cast<int>(_channeler.channel()));
		return 0;
	}

	LRESULT DataSourcePanel::OnEnChangeEdtDelay(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		if (this->GetDlgItem(IDC_EDT_ETHERNET_DELAY).IsWindowEnabled() != FALSE)
		{
			_datasource.scanning_delay(this->delay_value(static_cast<std::uint32_t>(this->GetDlgItemInt(IDC_EDT_ETHERNET_DELAY, nullptr, FALSE))));
			_delay_change_handler(this->delay_value(static_cast<std::uint32_t>(this->GetDlgItemInt(IDC_EDT_ETHERNET_DELAY, nullptr, FALSE))));
		}
		return 0;
	}

	LRESULT DataSourcePanel::OnEnChangeChargesCount(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_output.charges_count(static_cast<std::uint8_t>(this->GetDlgItemInt(IDC_EDT_ETHERNET_CHARGES_COUNT, nullptr, FALSE)));
		return 0;
	}

	LRESULT DataSourcePanel::OnEnChangeStrobePulseLength(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_output.strobe_pulse_length(static_cast<std::uint8_t>(this->GetDlgItemInt(IDC_EDT_ETHERNET_STROBE_PULSE_LENGTH, nullptr, FALSE)));
		return 0;
	}

	///LRESULT DataSourcePanel::OnEnChangeEdtProbingPeriod(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	///{
	///	_output.probing_period(static_cast<std::uint16_t>(0xFFFF & this->GetDlgItemInt(IDC_EDT_ETHERNET_PROBING_PERIOD, nullptr, FALSE)));
	///	return 0;
	///}

	LRESULT DataSourcePanel::OnCbnSelChangeCmbTimeFrame512(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_datasource.time_frame_single(::getComboBoxSelectionData<TimeFrameSingle>(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_512))));
		return 0;
	}

	LRESULT DataSourcePanel::OnCbnSelChangeCmbTimeFrame1024(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_datasource.time_frame_double(::getComboBoxSelectionData<TimeFrameDouble>(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_1024))));
		return 0;
	}

	LRESULT DataSourcePanel::OnCbnSelChangeCmbTimeFrame2048(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_datasource.time_frame_quadruple(::getComboBoxSelectionData<TimeFrameQuadruple>(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_2048))));
		return 0;
	}

	LRESULT DataSourcePanel::OnCbnSelChangeCmbTimeFrame4096(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_datasource.time_frame_octuple(::getComboBoxSelectionData<TimeFrameOctuple>(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_4096))));
		return 0;
	}

	LRESULT DataSourcePanel::OnCbnSelChangeCmbTimeFrame8192(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_datasource.time_frame_sedecuple(::getComboBoxSelectionData<TimeFrameSedecuple>(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TIMEFRAME_8192))));
		return 0;
	}




	LRESULT DataSourcePanel::OnBnCheckedCalibration(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		if (this->IsDlgButtonChecked(IDC_CHK_CALIBRATION) == BST_CHECKED)
		{
			_positioner.anchor_tick_count();
		}
		else
		{
			CalibrationDialog calibration(std::max<std::uint32_t>(_positioner.tick_count_difference(), 1));
			if (calibration.DoModal() == IDOK)
			{
				_settings.tick_length(calibration.tick_length());
			}
		}
		return 0;
	}

	LRESULT DataSourcePanel::OnCbnSelChangeCmbScansPerTrigger(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_output.scans_per_trigger(::getComboBoxSelectionData<std::uint8_t>(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_SCANS))));
		return 0;
	}

	LRESULT DataSourcePanel::OnCbnSelChangeCmbStepsPerTrigger(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_output.steps_per_trigger(::getComboBoxSelectionData<std::uint8_t>(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS))));
		return 0;
	}

	LRESULT DataSourcePanel::OnCbnSelChangeCmbTriggerMode(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_output.trigger_mode(::getComboBoxSelectionData<TriggerMode>(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_MODE))));
		return 0;
	}




	void DataSourcePanel::on_trigger_steps_count(std::uint8_t steps)
	{
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS)), steps);
	}

	void DataSourcePanel::on_trigger_scans_count(std::uint8_t scans)
	{
		if (_output.trigger_mode() == TriggerMode::None) CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_SCANS)).SetCurSel(-1);
		else utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_SCANS)), scans);
	}

	void DataSourcePanel::on_trigger_mode(TriggerMode mode)
	{
		if (mode == TriggerMode::None) CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS)).SetCurSel(-1);
		if (mode == TriggerMode::None) CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_SCANS)).SetCurSel(-1);
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS)).EnableWindow(mode == TriggerMode::None ? FALSE : TRUE);
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_SCANS)).EnableWindow(mode == TriggerMode::None ? FALSE : TRUE);
	}

	void DataSourcePanel::on_tick_length(std::float_t length)
	{
		for (std::size_t i = StepsPerTriggerMin, j = 0; i <= StepsPerTriggerMax; i += StepsPerTriggerStep, ++j)
			CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS)).SetItemText(j, this->format_step_length(i * 100 * _settings.tick_length()).c_str());
		CComboBoxEx(this->GetDlgItem(IDC_CMB_ETHERNET_TRIGGER_STEPS)).RedrawWindow();
	}
}
