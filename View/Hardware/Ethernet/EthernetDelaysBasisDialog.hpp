#pragma once
#include "..\..\..\resource.hpp"

#include <algorithm>
using std::min; using std::max;
#include <vector>


#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\WTL\atlapp.h"
#include "..\..\..\WTL\atlctrls.h"



class SliceRecordingFilterController;

namespace Ethernet
{
	class TimerController;
	class OutputController;
	class DataSourceController;
	
	
	
	class DelaysBasisDialog:
		public CDialogImpl<DelaysBasisDialog>
	{
	public:
		enum { IDD = IDD_ETHERNET_DATA_SOURCE_TIMER };
		
	public:
		DelaysBasisDialog(SliceRecordingFilterController &recording, DataSourceController &controller);
		
		BEGIN_MSG_MAP(DelaysBasisDialog)
			MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			COMMAND_HANDLER(IDOK, BN_CLICKED, OnOkClicked)
			COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnCancelClicked)
		END_MSG_MAP()
		
	private:
		SliceRecordingFilterController &_recording;
		DataSourceController &_controller;
		
		LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnOkClicked(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnCancelClicked(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	};
}