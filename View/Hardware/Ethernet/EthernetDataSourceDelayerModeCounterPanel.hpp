#pragma once
#include "..\..\..\resource.hpp"
#include "..\..\..\Model\Hardware\Ethernet\EthernetTypes.hpp"

#include <functional>
#include <algorithm>

using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\WTL\atlapp.h"
#include "..\..\..\WTL\atlframe.h"
#include "..\..\..\WTL\atlctrls.h"



namespace Ethernet
{
	class DelayerModeCounterController;



	class DataSourceDelayerModeCounterPanel:
		public CDialogImpl<DataSourceDelayerModeCounterPanel>
	{
	public:
		enum: unsigned long { IDD = IDD_ETHERNET_DELAYER_COUNTER };
		
	public:
		DataSourceDelayerModeCounterPanel(DelayerModeCounterController &controller);
		
		void start_recording(bool scanning);
		void start_scanning(bool recording);
		void stop_recording(bool scanning);
		void stop_scanning(bool recording);
		void destroy();
		void create(CWindow parent, CWindow placeholder, bool visible);
		void reset();
		void show(bool visible);
		
	public:
		
		BEGIN_MSG_MAP(DataSourceDelayerModeCounterPanel)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
			COMMAND_HANDLER(IDC_EDT_DELAYER_COUNTER_FREQUENCY, EN_CHANGE, OnEnChangeEdtInterval);
		END_MSG_MAP()
		
	private:
		DelayerModeCounterController& _controller;
		
		LRESULT OnEnChangeEdtInterval(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	};
}