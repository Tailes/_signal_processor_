#pragma once
#include "..\..\..\resource.hpp"

#include <algorithm>
using std::min; using std::max;

#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\WTL\atlapp.h"
#include "..\..\..\WTL\atlctrls.h"


class SliceRecordingFilterController;

namespace Ethernet
{
	class DataSourcePanel;
	class TimerController;
	class OutputController;
	class SettingsController;
	class DataSourceController;
	class InterpolatorController;
	class SignalThermometerController;
	class SignalInterpolatorController;
	
	
	
	
	class DataSourceOptionsDialog:
		public CDialogImpl<DataSourceOptionsDialog>
	{
	public:
		enum { IDD = IDD_ETHERNET_DATA_SOURCE_OPTIONS };
		
	public:
		DataSourceOptionsDialog(SliceRecordingFilterController &recording, DataSourceController &controler, DataSourcePanel &panel);
		
		BEGIN_MSG_MAP(DataSourceOptionsDialog)
			MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			COMMAND_HANDLER(IDOK, BN_CLICKED, OnOkClicked)
			COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnCancelClicked)
			//NOTIFY_HANDLER(IDC_SPN_GENERATOR_SYNC_FREQUENCY, UDN_DELTAPOS, OnSpnDeltaPosGeneratorFrequency);
		END_MSG_MAP()
		
	private:
		//EthernetSignalInterpolatorController &_interpolator;
		SliceRecordingFilterController &_recording;
		InterpolatorController &_interpolator;
		DataSourceController &_datasource;
		SettingsController &_settings;
		OutputController &_output;
		TimerController &_timer;
		DataSourcePanel &_panel;
		EDITBALLOONTIP _temperature_hint;
		EDITBALLOONTIP _tick_length_hint;
		EDITBALLOONTIP _limit_hint;
		CUpDownCtrl _strobe_pulse_length;
		CUpDownCtrl _probing_period;
		CUpDownCtrl _charges_count;
		CComboBox _packet_size;
		CButton _advanced_settings;
		CButton _time_correction;
		//CEdit _temperature;
		CEdit _tick_length;
		CEdit _limit_upper;
		CEdit _limit_lower;
		
	private:
		LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnOkClicked(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnCancelClicked(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnAdvancedSettings(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		//LRESULT OnSpnDeltaPosGeneratorFrequency(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/);
		
		//std::wstring temperature_text() const;
		std::wstring format_tick_length(std::float_t length) const;
		std::float_t delay_to_ns(Delay delay) const;
		Delay ns_to_delay(std::float_t ns) const;
	};
}