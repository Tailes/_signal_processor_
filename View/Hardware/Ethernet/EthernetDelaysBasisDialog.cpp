#include "..\..\..\stdafx.hpp"
#include "..\..\..\resource.hpp"
#include "..\..\..\View\Resources.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetDelaysBasisDialog.hpp"
#include "..\..\..\Controller\Processing\SliceRecordingFilterController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTypeRanges.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTimerController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetDataSourceController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetOutputController.hpp"
#include "..\..\..\View\SaveSignalsDialog.hpp"
#include "..\..\..\View\Utilities.hpp"




namespace Ethernet
{
	static const int EditBoxes[] =
	{
		IDC_EDT_D0_DELAY,
		IDC_EDT_D1_DELAY,
		IDC_EDT_D2_DELAY,
		IDC_EDT_D3_DELAY,
		IDC_EDT_D4_DELAY,
		IDC_EDT_D5_DELAY,
		IDC_EDT_D6_DELAY,
		IDC_EDT_D7_DELAY,
		IDC_EDT_D8_DELAY,
		IDC_EDT_D9_DELAY,
		IDC_EDT_D10_DELAY,
		IDC_EDT_D11_DELAY,
		IDC_EDT_D12_DELAY,
	};



	DelaysBasisDialog::DelaysBasisDialog(SliceRecordingFilterController &recording, DataSourceController &controller):
		_controller(controller),
		_recording(recording)
	{
	}

	LRESULT DelaysBasisDialog::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
	{
		bHandled = FALSE;
		auto cit = std::cbegin(_controller.delays_basis());
		for ( auto id : EditBoxes ) this->SetDlgItemInt(id, *cit++, FALSE);
		return 0;
	}

	LRESULT DelaysBasisDialog::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
	{
		bHandled = FALSE;
		return 0;
	}

	LRESULT DelaysBasisDialog::OnOkClicked(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		std::array<std::uint16_t, DelayTotalLinesCount> target;
		std::array<std::uint16_t, DelayTotalLinesCount>::iterator it = target.begin();
		SaveSignalsDialog dialog(this->GetTopLevelParent());
		
		for ( auto id : EditBoxes ) *it++ = static_cast<std::uint16_t>(0x0000FFFF & this->GetDlgItemInt(id, nullptr, FALSE));
		
		if ( target != _controller.delays_basis() && _recording.trace().is_dirty() )
		{
			switch ( ::AtlMessageBox(nullptr, IDS_DATA_UNSAVED, nullptr, MB_YESNOCANCEL) )
			{
			case IDYES:
				switch ( dialog.DoModal() )
				{
				case IDOK: _recording.trace().save(dialog.names()); break;
				case IDCANCEL: return 0;
				}
				break;
			case IDNO: _recording.trace().clear(); break;
			case IDCANCEL: return 0;
			}
		}
		
		_controller.delays_basis(target);
		this->EndDialog(IDOK);
		return 0;
	}

	LRESULT DelaysBasisDialog::OnCancelClicked(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		this->EndDialog(IDCANCEL);
		return 0;
	}
}
