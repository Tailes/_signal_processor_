#include "..\..\..\stdafx.hpp"
#include "..\..\..\Resource.hpp"
#include "..\..\..\Utility\TypeArithmetics.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetDataSourceDelayPanel.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTypeRanges.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTimerController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetDataSourceController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetInputController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetOutputController.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\InputOutput\EthernetSettingsController.hpp"






namespace Ethernet
{
	DataSourceDelayPanel::DataSourceDelayPanel(DataSourceController &datasource):
		_datasource(datasource), _delay_locked(false)
	{
	}

	void DataSourceDelayPanel::destroy()
	{
		this->DestroyWindow();
	}

	void DataSourceDelayPanel::update()
	{
		if (_trackbar.IsWindowEnabled() == FALSE)
		{
			_trackbar.SetPos(this->delay_tick(_datasource.scanning_delay()));
		}
	}

	void DataSourceDelayPanel::create(CWindow owner, CWindow placeholder, bool visible)
	{
		WINDOWPLACEMENT placement = {sizeof(placement)};
		placeholder.GetWindowPlacement(&placement);
		placeholder.ShowWindow(SW_HIDE);
		this->Create(owner);
		this->SetWindowPlacement(&placement);
		this->SetWindowPos(placeholder, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
		this->SetDlgCtrlID(DataSourceDelayPanel::IDD);
		this->ShowWindow(visible ? SW_SHOW : SW_HIDE);
	}

	void DataSourceDelayPanel::show(bool visible)
	{
		this->ShowWindow(visible ? SW_SHOW : SW_HIDE);
	}

	void DataSourceDelayPanel::reset()
	{
		_trackbar.EnableWindow(TRUE);
	}

	void DataSourceDelayPanel::start_recording(bool scanning)
	{
		if ( scanning ) _trackbar.EnableWindow(FALSE);
	}

	void DataSourceDelayPanel::stop_recording(bool scanning)
	{
		if ( !scanning  ) _trackbar.EnableWindow(_delay_locked ? FALSE : TRUE);
	}

	void DataSourceDelayPanel::start_scanning(bool recording)
	{
		if ( recording ) _trackbar.EnableWindow(FALSE);
	}

	void DataSourceDelayPanel::stop_scanning(bool recording)
	{
		_trackbar.EnableWindow(_delay_locked ? FALSE : TRUE);
	}



	void DataSourceDelayPanel::delay_change_handler(const std::function<void(Delay)> &callback)
	{
		_delay_change_handler = callback;
	}




	void DataSourceDelayPanel::on_change_delay(Delay value)
	{
		_trackbar.SetPos(this->delay_tick(value));
	}

	void DataSourceDelayPanel::on_auto_delay(bool enabled)
	{
		_trackbar.EnableWindow((_delay_locked = enabled) ? FALSE : TRUE);
	}





	LRESULT DataSourceDelayPanel::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		_trackbar.Attach(this->GetDlgItem(IDC_SLD_DELAY));
		_trackbar.SetRangeMin(this->delay_tick(DelayMin), FALSE);
		_trackbar.SetRangeMax(this->delay_tick(DelayMax), TRUE);
		_trackbar.SetPageSize(std::labs(this->delay_tick(DelayMax - DelayMin)) / 200);
		_trackbar.SetPos(this->delay_tick(_datasource.scanning_delay()));

		this->DlgResize_Init(false);

		return TRUE;
	}

	LRESULT DataSourceDelayPanel::OnHScroll(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		_datasource.scanning_delay(this->delay_value(_trackbar.GetPos()));
		_delay_change_handler(this->delay_value(_trackbar.GetPos()));
		return 0;
	}

	LRESULT DataSourceDelayPanel::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		_trackbar.Detach();
		return 0;
	}

	Delay DataSourceDelayPanel::delay_value(std::uint32_t tick) const
	{
		return static_cast<Delay>(tick);
	}

	std::uint32_t DataSourceDelayPanel::delay_tick(Delay value) const
	{
		return static_cast<std::uint32_t>(value);
	}
}