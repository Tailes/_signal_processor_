#include "..\..\..\stdafx.hpp"
#include "..\..\..\Resource.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetDataSourceDelayerModeCounterPanel.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\DelayerModes\EthernetDelayerModeCounterController.hpp"






namespace Ethernet
{
	DataSourceDelayerModeCounterPanel::DataSourceDelayerModeCounterPanel(DelayerModeCounterController &controller):
		_controller(controller)
	{
	}

	void DataSourceDelayerModeCounterPanel::destroy()
	{
		this->DestroyWindow();
	}

	void DataSourceDelayerModeCounterPanel::create(CWindow owner, CWindow placeholder, bool visible)
	{
		WINDOWPLACEMENT placement = {sizeof(placement)};
		placeholder.GetWindowPlacement(&placement);
		placeholder.ShowWindow(SW_HIDE);
		this->Create(owner);
		this->SetWindowPlacement(&placement);
		this->SetWindowPos(placeholder, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
		this->SetDlgCtrlID(DataSourceDelayerModeCounterPanel::IDD);
		this->ShowWindow(visible ? SW_SHOW : SW_HIDE);
	}

	void DataSourceDelayerModeCounterPanel::show(bool visible)
	{
		this->ShowWindow(visible ? SW_SHOW : SW_HIDE);
	}

	void DataSourceDelayerModeCounterPanel::reset()
	{
	}

	void DataSourceDelayerModeCounterPanel::start_recording(bool scanning)
	{
	}

	void DataSourceDelayerModeCounterPanel::stop_recording(bool scanning)
	{
	}

	void DataSourceDelayerModeCounterPanel::start_scanning(bool recording)
	{
	}

	void DataSourceDelayerModeCounterPanel::stop_scanning(bool recording)
	{
	}




	LRESULT DataSourceDelayerModeCounterPanel::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		this->SetDlgItemInt(IDC_EDT_DELAYER_COUNTER_FREQUENCY, _controller.interval(), FALSE);
		return TRUE;
	}

	LRESULT DataSourceDelayerModeCounterPanel::OnEnChangeEdtInterval(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		_controller.interval(this->GetDlgItemInt(IDC_EDT_DELAYER_COUNTER_FREQUENCY, nullptr, FALSE));
		return LRESULT();
	}

	LRESULT DataSourceDelayerModeCounterPanel::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		return 0;
	}
}