#include "..\..\..\stdafx.hpp"
#include "..\..\..\Resource.hpp"
#include "..\..\..\View\Resources.hpp"
#include "..\..\..\View\Utilities.hpp"
#include "..\..\..\Utility\TypeArithmetics.hpp"
#include "..\..\..\View\Hardware\Ethernet\EthernetDataSourceDelayerPanel.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetTypeRanges.hpp"
#include "..\..\..\Controller\Hardware\Ethernet\EthernetDelayerController.hpp"



#include <array>



namespace Ethernet
{
	static const TCHAR WebdingsFontName[] = _T("Webdings");


	DataSourceDelayerPanel::DataSourceDelayerPanel(DelayerController &controller):
		_controller(controller), _timer(controller.timer()), _counter(controller.counter())
	{
		_enabled_handler = [](bool) {};
		_delayer_mode_handlers.emplace(DelayerMode::Counter, &DataSourceDelayerPanel::OnCbnSelChangeCmbModeCounter);
		_delayer_mode_handlers.emplace(DelayerMode::Timer, &DataSourceDelayerPanel::OnCbnSelChangeCmbModeTimer);
	}

	void DataSourceDelayerPanel::enabled_handler(std::function<void(bool)> callback)
	{
		_enabled_handler = std::move(callback);
	}

	void DataSourceDelayerPanel::destroy()
	{
		this->DestroyWindow();
	}

	void DataSourceDelayerPanel::create(CWindow owner, CWindow placeholder, bool visible)
	{
		WINDOWPLACEMENT placement = {sizeof(placement)};
		placeholder.GetWindowPlacement(&placement);
		placeholder.ShowWindow(SW_HIDE);
		this->Create(owner);
		this->SetWindowPlacement(&placement);
		this->SetWindowPos(placeholder, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
		this->SetDlgCtrlID(DataSourceDelayerPanel::IDD);
		this->ShowWindow(visible ? SW_SHOW : SW_HIDE);
	}

	void DataSourceDelayerPanel::show(bool visible)
	{
		this->ShowWindow(visible ? SW_SHOW : SW_HIDE);
	}

	void DataSourceDelayerPanel::reset()
	{
	}

	void DataSourceDelayerPanel::start_recording(bool scanning)
	{
	}

	void DataSourceDelayerPanel::stop_recording(bool scanning)
	{
	}

	void DataSourceDelayerPanel::start_scanning(bool recording)
	{
	}

	void DataSourceDelayerPanel::stop_scanning(bool recording)
	{
	}


	LRESULT DataSourceDelayerPanel::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		LOGFONT logfont = {};
		TCHAR font_name[LF_FACESIZE] = _T("Webdings");
		CFontHandle(this->GetFont()).GetLogFont(logfont);
		std::copy(std::begin(font_name), std::end(font_name), logfont.lfFaceName);
		logfont.lfHeight = ::MulDiv(logfont.lfHeight, 4, 3);
		logfont.lfCharSet = SYMBOL_CHARSET;
		_button_font.CreateFontIndirect(&logfont);
		
		_counter.create(*this, this->GetDlgItem(IDC_DELAYER_MODE_PLACEHOLDER), _controller.mode() == DelayerMode::Counter);
		_timer.create(*this, this->GetDlgItem(IDC_DELAYER_MODE_PLACEHOLDER), _controller.mode() == DelayerMode::Timer);
		
		CComboBoxEx(this->GetDlgItem(IDC_CMB_AUTO_DELAY_MODE)).AddItem(resources::string<IDS_ETHERNET_DELAYER_MODE_COUNTER>().c_str(), 0, 0, 0, static_cast<int>(DelayerMode::Counter));
		CComboBoxEx(this->GetDlgItem(IDC_CMB_AUTO_DELAY_MODE)).AddItem(resources::string<IDS_ETHERNET_DELAYER_MODE_TIMER>().c_str(), 0, 0, 0, static_cast<int>(DelayerMode::Timer));
		
		utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_AUTO_DELAY_MODE)), static_cast<int>(_controller.mode()));
		
		utility::set_dialog_item_float(*this, IDC_EDT_AUTO_DELAY_STEP, this->delay_nanoseconds(_controller.delta()), 3);
		
		this->GetDlgItem(IDC_CHK_AUTO_DELAY_RIGHT).SetFont(_button_font);
		this->GetDlgItem(IDC_CHK_AUTO_DELAY_LEFT).SetFont(_button_font);
		this->GetDlgItem(IDC_BTN_STOP_AUTO_DELAY).SetFont(_button_font);
		
		return TRUE;
	}

	LRESULT DataSourceDelayerPanel::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		this->GetDlgItem(IDC_CHK_AUTO_DELAY_RIGHT).SetFont(this->GetFont());
		this->GetDlgItem(IDC_CHK_AUTO_DELAY_LEFT).SetFont(this->GetFont());
		this->GetDlgItem(IDC_BTN_STOP_AUTO_DELAY).SetFont(this->GetFont());
		_button_font.DeleteObject();
		return LRESULT();
	}

	LRESULT DataSourceDelayerPanel::OnBnClickedRightAutoDelay(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		this->CheckDlgButton(IDC_CHK_AUTO_DELAY_RIGHT, BST_CHECKED);
		this->CheckDlgButton(IDC_CHK_AUTO_DELAY_LEFT, BST_UNCHECKED);
		_controller.direction(true);
		_controller.enable(true);
		_enabled_handler(true);
		return LRESULT();
	}

	LRESULT DataSourceDelayerPanel::OnBnClickedLeftAutoDelay(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		this->CheckDlgButton(IDC_CHK_AUTO_DELAY_RIGHT, BST_UNCHECKED);
		this->CheckDlgButton(IDC_CHK_AUTO_DELAY_LEFT, BST_CHECKED);
		_controller.direction(false);
		_controller.enable(true);
		_enabled_handler(true);
		return LRESULT();
	}

	LRESULT DataSourceDelayerPanel::OnBnClickedStopAutoDelay(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		this->CheckDlgButton(IDC_CHK_AUTO_DELAY_RIGHT, BST_UNCHECKED);
		this->CheckDlgButton(IDC_CHK_AUTO_DELAY_LEFT, BST_UNCHECKED);
		_controller.enable(false);
		_enabled_handler(false);
		return LRESULT();
	}


	LRESULT DataSourceDelayerPanel::OnCbnSelChangeCmbMode(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		return (this->*_delayer_mode_handlers.at(::getComboBoxSelectionData<DelayerMode>(CComboBoxEx(this->GetDlgItem(IDC_CMB_AUTO_DELAY_MODE)))))(wNotifyCode, wID, hWndCtl, bHandled);
	}

	LRESULT DataSourceDelayerPanel::OnEnChangeEdtStep(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		_controller.delta(this->delay_value(static_cast<std::float_t>(utility::get_dialog_item_float(*this, IDC_EDT_AUTO_DELAY_STEP))));
		return LRESULT();
	}

	LRESULT DataSourceDelayerPanel::OnCbnSelChangeCmbModeCounter(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		_controller.mode(DelayerMode::Counter);
		_counter.show(true);
		_timer.show(false);
		return LRESULT();
	}

	LRESULT DataSourceDelayerPanel::OnCbnSelChangeCmbModeTimer(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL &bHandled)
	{
		_controller.mode(DelayerMode::Timer);
		_counter.show(false);
		_timer.show(true);
		return LRESULT();
	}




	const DataSourceDelayerModeCounterPanel & DataSourceDelayerPanel::counter() const
	{
		return _counter;
	}

	const DataSourceDelayerModeTimerPanel & DataSourceDelayerPanel::timer() const
	{
		return _timer;
	}

	DataSourceDelayerModeCounterPanel & DataSourceDelayerPanel::counter()
	{
		return _counter;
	}

	DataSourceDelayerModeTimerPanel & DataSourceDelayerPanel::timer()
	{
		return _timer;
	}



	Delay DataSourceDelayerPanel::delay_value(std::float_t nanoseconds) const
	{
		return static_cast<Delay>(std::round(nanoseconds * 100.));
	}

	std::float_t DataSourceDelayerPanel::delay_nanoseconds(Delay delay) const
	{
		return static_cast<std::float_t>(delay) / static_cast<std::float_t>(100.);
	}
}