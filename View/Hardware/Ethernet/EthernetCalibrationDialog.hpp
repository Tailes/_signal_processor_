#pragma once
#include "..\..\..\resource.hpp"

#include <algorithm>
using std::min; using std::max;
#include <vector>
#include <cmath>


#include <atlbase.h>
#include <atlwin.h>

#include "..\..\..\WTL\atlapp.h"
#include "..\..\..\WTL\atlctrls.h"


namespace Ethernet
{
	class CalibrationDialog:
		public CDialogImpl<CalibrationDialog>
	{
	public:
		enum { IDD = IDD_ETHERNET_DATA_SOURCE_CALIBRATION };
		
	public:
		CalibrationDialog(std::uint32_t ticks_count);
		
		BEGIN_MSG_MAP(CalibrationDialog)
			MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
			MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
			COMMAND_HANDLER(IDOK, BN_CLICKED, OnOkClicked)
			COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnCancelClicked)
			COMMAND_HANDLER(IDC_EDT_PATH_LENGTH, EN_CHANGE, OnEnChangeEdtPathLength);
		END_MSG_MAP()
		
	public:
		std::float_t tick_length() const;
		
	private:
		std::uint32_t _ticks_count;
		std::float_t _tick_length;
		
	private:
		LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnOkClicked(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnCancelClicked(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
		LRESULT OnEnChangeEdtPathLength(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	};
}