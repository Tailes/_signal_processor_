#include "..\stdafx.hpp"
#include "..\resource.hpp"
#include "..\View\SignalProcessorFrame.hpp"
#include "..\View\Resources.hpp"
#include "..\View\Utilities.hpp"
#include "..\View\SaveSignalsDialog.hpp"
#include "..\View\Hardware\Serial\SerialPortOptionsDialog.hpp"
#include "..\View\Hardware\Serial\SerialDataSourceOptionsDialog.hpp"
#include "..\View\Hardware\Ethernet\EthernetDelaysBasisDialog.hpp"
#include "..\View\Hardware\Ethernet\EthernetSocketOptionsDialog.hpp"
#include "..\View\Hardware\Ethernet\EthernetDataSourceOptionsDialog.hpp"
#include "..\Model\Hardware\Ethernet\EthernetSignalProvider.hpp"
#include "..\Model\Hardware\Ethernet\EthernetSignalCounter.hpp"
#include "..\Controller\SignalProcessorController.hpp"

#include <sstream>
#include <iomanip>
#include <chrono>
#include <string>
#include <ctime>

#include "..\WTL\atldlgs.h"


static const float MaxVoltage = 14.0f;
static const float MaxCurrent = 5.0f;

static const int TimerID = 1;




SignalProcessorFrame::SignalProcessorFrame(SignalProcessorController &controller):
	_controller(controller), _view(controller)
{
	_controller.report_handler([this]() { this->on_report(); });
	_controller.reset_handler([this]() { this->on_reset(); });
	_view.experiment_begin_handler([this]() { this->on_experiment_begin(); });
	_view.experiment_end_handler([this]() { this->on_experiment_end(); });
	_view.slice_tracking_callback([this](float time) { _status.report_signal_time(time); });
}

void SignalProcessorFrame::create(int mode)
{
	this->CreateEx();
	this->ShowWindow(mode);
}

void SignalProcessorFrame::load(const std::wstring &root)
{
	_view.load(root);
}

void SignalProcessorFrame::save(const std::wstring &root)
{
	_view.save(root);
}

void SignalProcessorFrame::update()
{
	unsigned long trigger_state = _controller.slice_processor().hardware_data_source().ethernet_data_source().channeler().signal_provider().signal_source().positioner().trigger_state();
	unsigned long queue_length = _controller.slice_processor().hardware_data_source().ethernet_data_source().channeler().signal_provider().signal_source().positioner().queue_length();
	unsigned long ticks_count = _controller.slice_processor().hardware_data_source().ethernet_data_source().channeler().signal_provider().signal_source().positioner().tick_count();
	std::size_t number = _controller.slice_processor().slice_counting_filter().amount();
	float voltage = _controller.slice_processor().hardware_data_source().ethernet_data_source().channeler().signal_provider().signal_source().tracker().voltage();
	float current = _controller.slice_processor().hardware_data_source().ethernet_data_source().channeler().signal_provider().signal_source().tracker().current();
	//_controller.slice_processor().slice_signal_monitoring_filter().speeding(queue_length != 0);
	//_controller.slice_processor().slice_trace_monitoring_filter().speeding(queue_length != 0);
	_status.report_signal_number(number);
	_status.report_tracking(ticks_count, queue_length, (trigger_state & 0x0001) != 0);
	_status.battery(std::sqrt(voltage * current), MaxVoltage * voltage, MaxCurrent * current);
	_view.update();
}

void SignalProcessorFrame::save_project(bool reopen)
{
	SaveSignalsDialog dialog(*this);
	if ( dialog.DoModal() == IDOK )
	{
		_controller.slice_processor().slice_recording_filter().trace().save(dialog.names());
		if ( reopen )
		{
			for ( const std::wstring &name : dialog.names() )
			{
				::ShellExecuteW(*this, L"open", name.c_str(), nullptr, dialog.folder().c_str(), SW_SHOWDEFAULT);
			}
		}
	}
}



#pragma region Event Handlers
void SignalProcessorFrame::on_experiment_begin()
{
	this->PostMessageW(WM_REPORT_SCANNING_BEGIN);
	this->SetTimer(TimerID, USER_TIMER_MINIMUM);
}

void SignalProcessorFrame::on_experiment_end()
{
	this->KillTimer(TimerID);
	this->PostMessageW(WM_REPORT_SCANNING_END);
}

void SignalProcessorFrame::on_report()
{
	this->PostMessageW(WM_REPORT_TEXT);
}

void SignalProcessorFrame::on_reset()
{
	this->KillTimer(TimerID);
	this->PostMessageW(WM_RESET);
}
#pragma endregion


#pragma region Accessors
const SignalProcessorView& SignalProcessorFrame::view() const
{
	return _view;
}

SignalProcessorView& SignalProcessorFrame::view()
{
	return _view;
}
#pragma endregion


#pragma region Message Handlers
LRESULT SignalProcessorFrame::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	bHandled = FALSE;
	RECT client = {}, view = {}, status = {};
	//_view.Create(nullptr);
	m_hWndClient = _view.Create(m_hWnd);
	m_hWndStatusBar = _status.create(m_hWnd);
	_view.GetWindowRect(&view);
	_status.GetWindowRect(&status);
	this->ResizeClient(std::max(view.right - view.left, status.right - status.left), view.bottom - view.top + status.bottom - status.top);
	return TRUE;
}

LRESULT SignalProcessorFrame::OnReset(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	_view.reset();
	return 0;
}

LRESULT SignalProcessorFrame::OnTimer(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	this->update();
	return 0;
}

LRESULT SignalProcessorFrame::OnGetMinMaxInfo(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE;
	RECT client = {}, status = {};
	MINMAXINFO client_mmi = {}, *mmi = reinterpret_cast<LPMINMAXINFO>(lParam);
	::SendMessageW(m_hWndClient, WM_GETMINMAXINFO, 0, reinterpret_cast<LPARAM>(&client_mmi));
	::GetWindowRect(m_hWndStatusBar, &status);
	::SetRect(&client, 0, 0, client_mmi.ptMinTrackSize.x, client_mmi.ptMinTrackSize.y + status.bottom - status.top);
	::AdjustWindowRect(&client, ::GetWindowLongW(m_hWnd, GWL_STYLE), TRUE);
	mmi->ptMinTrackSize.x = client.right - client.left;
	mmi->ptMinTrackSize.y = client.bottom - client.top;
	return TRUE;
}

LRESULT SignalProcessorFrame::OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	bHandled = FALSE;
	if ( _controller.slice_processor().slice_recording_filter().trace().is_dirty() )
	{
		switch ( ::AtlMessageBox(nullptr, IDS_DATA_UNSAVED, nullptr, MB_YESNOCANCEL) )
		{
		case IDCANCEL:
			return bHandled = TRUE;
		case IDYES:
			_controller.stop_experiment();
			this->save_project();
			this->DestroyWindow();
			break;
		case IDNO:
			_controller.stop_experiment();
			this->DestroyWindow();
			break;
		}
	}
	else
	{
		_controller.stop_experiment();
		this->DestroyWindow();
	}
	return TRUE;
}

LRESULT SignalProcessorFrame::OnReportText(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	std::forward_list<std::wstring> messages = _controller.messages();
	if ( !messages.empty() ) _status.report_error(messages.front());
	return TRUE;
}

LRESULT SignalProcessorFrame::OnReportScanningEnd(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	_status.report_information(resources::string<IDS_SESSION_END>());
	return TRUE;
}

LRESULT SignalProcessorFrame::OnReportScanningBegin(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	_status.report_information(resources::string<IDS_SESSION_BEGIN>());
	return TRUE;
}
#pragma endregion


#pragma region Menu Command Handlers
LRESULT SignalProcessorFrame::OnFileSave(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	try
	{
		this->save_project(false);
	}
	catch ( std::exception &ex )
	{
		_controller.report(ex.what());
	}
	return 0;
}

LRESULT SignalProcessorFrame::OnFileSaveAndOpen(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	try
	{
		this->save_project(true);
	}
	catch ( std::exception &ex )
	{
		_controller.report(ex.what());
	}
	return 0;
}

LRESULT SignalProcessorFrame::OnFileExit(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	this->SendMessageW(WM_CLOSE);
	return 0;
}

LRESULT SignalProcessorFrame::OnExperimentStart(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_view.SendMessage(WM_COMMAND, IDC_BTN_EXPERIMENT_START, 0);
	return 0;
}

LRESULT SignalProcessorFrame::OnExperimentRecord(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_view.SendMessage(WM_COMMAND, IDC_BTN_RECORD, 0);
	return 0;
}

LRESULT SignalProcessorFrame::OnOptionsSerialPort(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	Serial::PortOptionsDialog(_controller.slice_processor().hardware_data_source().serial_data_source()).DoModal();
	return 0;
}

LRESULT SignalProcessorFrame::OnOptionsSerialDataSource(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	Serial::DataSourceOptionsDialog(_controller.slice_processor().hardware_data_source().serial_data_source()).DoModal();
	return 0;
}

LRESULT SignalProcessorFrame::OnOptionsEthernetPort(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	Ethernet::SocketOptionsDialog(_controller.slice_processor().hardware_data_source().ethernet_data_source()).DoModal();
	return 0;
}


LRESULT SignalProcessorFrame::OnOptionsEthernetDelays(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	Ethernet::DelaysBasisDialog(_controller.slice_processor().slice_recording_filter(), _controller.slice_processor().hardware_data_source().ethernet_data_source()).DoModal();
	return 0;
}

LRESULT SignalProcessorFrame::OnOptionsEthernetDataSource(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if ( Ethernet::DataSourceOptionsDialog(_controller.slice_processor().slice_recording_filter(), _controller.slice_processor().hardware_data_source().ethernet_data_source(), _view.ethernet_panel()).DoModal() == IDOK )
	{
		_view.ethernet_panel().update_settings();
	}
	return 0;
}
#pragma endregion
