#include "..\stdafx.hpp"
#include "..\resource.hpp"
#include "..\View\Resources.hpp"
#include "..\View\SaveSignalsDialog.hpp"

#include <cstddef>
#include <regex>
#include <ctime>


SignalsDialogBase::SignalsDialogBase():
	_default_ext(this->default_ext()), _default_filter(this->default_filter()), _default_name(this->default_name())
{
}

std::wstring SignalsDialogBase::default_filter()
{
	return resources::string<IDS_SAVE_FILE_FILTER>().append(L"(*.").append(SignalsDialogBase::default_ext()).append(L")\0*.", 4).append(SignalsDialogBase::default_ext()).append(2, L'\0');
}

std::wstring SignalsDialogBase::default_name()
{
	std::tm container = {};
	std::time_t now = std::time(nullptr);
	std::array<wchar_t, _MAX_PATH> buffer;
	::localtime_s(&container, &now);
	::wcsftime(buffer.data(), buffer.size(), resources::string<IDS_FILE_PATTERN>().c_str(), &container);
	return std::wstring(buffer.data());
}

std::wstring SignalsDialogBase::default_ext()
{
	return resources::string<IDS_FILE_EXTENSION>();
}




SaveSignalsDialog::SaveSignalsDialog(HWND parent):
	CFileDialogImpl<SaveSignalsDialog>(FALSE, _default_ext.c_str(), _default_name.c_str(), OFN_HIDEREADONLY, _default_filter.c_str(), parent)
{
}

BOOL SaveSignalsDialog::OnFileOK(LPOFNOTIFY lpon)
{
	std::array<wchar_t, _MAX_PATH> buffer = {};
	std::wstring ext(::PathFindExtensionW(m_ofn.lpstrFile));
	std::wstring name(std::regex_replace(std::wstring(::PathFindFileNameW(m_ofn.lpstrFile), ::PathFindExtensionW(m_ofn.lpstrFile)), std::wregex(L"_channel_[1-9]$"), L""));
	_folder = std::wstring(m_ofn.lpstrFile, ::PathFindFileNameW(m_ofn.lpstrFile));
	for ( std::size_t i = 0; i < _names.size(); ++i )
	{
		_names.at(i) = std::wstring(::PathCombineW(buffer.data(), _folder.c_str(), (name + resources::string<IDS_CHANNEL_FILENAME_PART>() + std::to_wstring(i + 1) + ext).c_str()));
		if ( ::PathFileExistsW(_names.at(i).c_str()) == TRUE )
		{
			if ( ::AtlMessageBox(nullptr, IDS_FILES_EXIST, nullptr, MB_YESNO) != IDYES )
			{
				return FALSE;
			}
		}
	}
	return TRUE;
}

const std::array<std::wstring, std::tuple_size<Batch>::value>& SaveSignalsDialog::names() const
{
	return _names;
}

const std::wstring& SaveSignalsDialog::folder() const
{
	return _folder;
}

