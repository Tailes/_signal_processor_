#include "..\StdAfx.hpp"
#include "..\View\Resources.hpp"
#include "..\View\Utilities.hpp"
#include "..\View\SaveSignalsDialog.hpp"
#include "..\View\SignalProcessorView.hpp"
#include "..\Controller\SignalProcessorController.hpp"
#include "..\Utility\TypeArithmetics.hpp"
#include "..\Model\SignalProcessorModel.hpp"

#include <iomanip>
#include <sstream>
#include <cstdio>
#include <string>
#include <limits>
#include <vector>
#include <array>




SignalProcessorView::SignalProcessorView(SignalProcessorController &controller) :
	_general(_viewport, controller),
	_controller(controller),
	_viewport(controller.slice_processor()),
	_serial_panel(controller.slice_processor().hardware_data_source().serial_data_source()),
	_ethernet_panel(controller.slice_processor().hardware_data_source().ethernet_data_source()),
	_ethernet_delay_panel(controller.slice_processor().hardware_data_source().ethernet_data_source()),
	_ethernet_delayer_panel(controller.slice_processor().hardware_data_source().ethernet_data_source().delayer()),
	_dynamic_gain_filter_panel(controller.slice_processor().slice_dynamic_gain_filter()),
	_running_average_filter_panel(controller.slice_processor().slice_running_average_filter())
{
	_data_source_handlers[static_cast<unsigned int>(HardwareDataSourceMode::Ethernet)] = &SignalProcessorView::OnCbnSelChangeCmbDataSourceEthernet;
	_data_source_handlers[static_cast<unsigned int>(HardwareDataSourceMode::SerialPort)] = &SignalProcessorView::OnCbnSelChangeCmbDataSourceSerial;

	_command_handlers[IDC_BTN_START] = &SignalProcessorView::OnBnClickedBtnExperimentStart;
	_command_handlers[IDC_BTN_RECORD] = &SignalProcessorView::OnBnClickedBtnRecordStart;

	_controller.experiment_begin_handler([this]() { this->on_experiment_begin(); });
	_controller.experiment_end_handler([this]() { this->on_experiment_end(); });
	_controller.redraw_handler([this]() { this->on_redraw(); });
	_controller.update_handler([this]() { this->on_update(); });
	
	_controller.slice_processor().slice_averaging_filter().amount_callback([this](std::size_t amount) { this->on_slice_averaging_amount(amount); });
	_controller.slice_processor().hardware_data_source().ethernet_data_source().channeler().signal_provider().signal_source().output().trigger_mode_callback([this](Ethernet::TriggerMode mode) { this->on_ethernet_trigger_mode(mode); });
	_controller.slice_processor().hardware_data_source().ethernet_data_source().settings().tick_length_callback([this](float length) { this->on_tick_length(length); });
	
	_ethernet_delay_panel.delay_change_handler([this](Ethernet::Delay delay) { _ethernet_panel.on_change_delay(delay); });
	_ethernet_panel.delay_change_handler([this](Ethernet::Delay delay) { _ethernet_delay_panel.on_change_delay(delay); });
	_ethernet_delayer_panel.enabled_handler([this](bool enabled) { _ethernet_delay_panel.on_auto_delay(enabled); _ethernet_panel.on_auto_delay(enabled); });
	
	_interrupt_required.clear();
	_redraw_required.clear();
}

SignalProcessorView::~SignalProcessorView()
{
}

void SignalProcessorView::reset()
{
	//this->GetDlgItem(IDC_CMB_DATA_SOURCE).EnableWindow(TRUE);
	this->GetDlgItem(IDC_BTN_START).SetWindowTextW(utility::load_string(IDS_START).c_str());
	this->CheckDlgButton(IDC_BTN_START, BST_UNCHECKED);
	_command_handlers.at(IDC_BTN_START) = &SignalProcessorView::OnBnClickedBtnExperimentStart;
	_ethernet_delay_panel.reset();
	_ethernet_panel.reset();
	_serial_panel.reset();
}

void SignalProcessorView::update()
{
	if ( _redraw_required.test_and_set() )
	{
		if ( this->IsWindow() )
		{
			_running_average_filter_panel.update();
			_dynamic_gain_filter_panel.update();
			_ethernet_delay_panel.update();
			_ethernet_panel.update();
			_viewport.redraw();
			_general.update();
		}
	}
	_redraw_required.clear();
}

void SignalProcessorView::load(const std::wstring &root)
{
	_general.load(root);
}

void SignalProcessorView::save(const std::wstring &root) const
{
	_general.save(root);
}

void SignalProcessorView::destroy_overlay_panels()
{
	_interrupt_required.clear();
	_redraw_required.clear();
	_viewport.detach();
	_running_average_filter_panel.DestroyWindow();
	_dynamic_gain_filter_panel.DestroyWindow();
	_ethernet_delayer_panel.destroy();
	_ethernet_delay_panel.destroy();
	_ethernet_panel.DestroyWindow();
	_serial_panel.DestroyWindow();
	_general.DestroyWindow();
}

void SignalProcessorView::create_overlay_panels(CWindow &general, CWindow &running_avg, CWindow &dyn_gain, CWindow &control, CWindow &delay, CWindow &auto_delay, CWindow &statusbar)
{
	WINDOWPLACEMENT placement = {sizeof(placement)};
	general.GetWindowPlacement(&placement);
	_general.Create(m_hWnd);
	_general.SetWindowPlacement(&placement);
	_general.SetDlgCtrlID(IDD_GENERAL_PANEL);
	_general.SetWindowPos(control, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	control.GetWindowPlacement(&placement);
	_serial_panel.Create(m_hWnd);
	_serial_panel.SetWindowPlacement(&placement);
	_serial_panel.SetDlgCtrlID(IDD_SERIAL_DATA_SOURCE_PANEL);
	_serial_panel.SetWindowPos(control, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	_serial_panel.ShowWindow(_controller.slice_processor().hardware_data_source().mode() == HardwareDataSourceMode::SerialPort ? SW_SHOW : SW_HIDE);
	_ethernet_delay_panel.create(*this, delay, _controller.slice_processor().hardware_data_source().mode() == HardwareDataSourceMode::Ethernet);
	_ethernet_delayer_panel.create(*this, auto_delay, _controller.slice_processor().hardware_data_source().mode() == HardwareDataSourceMode::Ethernet);
	_ethernet_panel.Create(m_hWnd);
	_ethernet_panel.SetWindowPlacement(&placement);
	_ethernet_panel.SetDlgCtrlID(IDD_ETHERNET_DATA_SOURCE_PANEL);
	_ethernet_panel.SetWindowPos(control, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	_ethernet_panel.ShowWindow(_controller.slice_processor().hardware_data_source().mode() == HardwareDataSourceMode::Ethernet ? SW_SHOW : SW_HIDE);
	running_avg.GetWindowPlacement(&placement);
	_running_average_filter_panel.Create(m_hWnd);
	_running_average_filter_panel.SetWindowPlacement(&placement);
	_running_average_filter_panel.SetDlgCtrlID(IDD_RUNNING_AVERAGE_FILTER_PANEL);
	_running_average_filter_panel.SetWindowPos(control, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	dyn_gain.GetWindowPlacement(&placement);
	_dynamic_gain_filter_panel.Create(m_hWnd);
	_dynamic_gain_filter_panel.SetWindowPlacement(&placement);
	_dynamic_gain_filter_panel.SetDlgCtrlID(IDD_DYNAMIC_GAIN_FILTER_PANEL);
	_dynamic_gain_filter_panel.SetWindowPos(control, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	_viewport.attach(this->GetDlgItem(IDC_PCT_SLICE_VIEWPORT), this->GetDlgItem(IDC_PCT_SLICE_X_AXIS), this->GetDlgItem(IDC_PCT_TRACE_VIEWPORT));
}








LRESULT SignalProcessorView::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	this->create_overlay_panels(this->GetDlgItem(IDC_GRPBOX_GENERAL_OPTIONS), this->GetDlgItem(IDC_GRPBOX_DYNAMIC_GAIN_FILTER_OPTIONS), this->GetDlgItem(IDC_GRPBOX_RUNNING_AVERAGE_FILTER_OPTIONS), this->GetDlgItem(IDC_GRPBOX_DATA_SOURCE_OPTIONS), this->GetDlgItem(IDC_GRPBOX_DATA_SOURCE_DELAY), this->GetDlgItem(IDC_GRPBOX_DATA_SOURCE_AUTO_DELAY), this->GetDlgItem(IDC_GRPBOX_STATUSBAR));
	this->SetDlgItemTextW(IDC_BTN_RECORD_START, utility::load_string(IDS_START).c_str());
	this->SetDlgItemTextW(IDC_BTN_START, utility::load_string(IDS_START).c_str());
	this->CheckDlgButton(IDC_CHK_ACCUMULATION, _controller.slice_processor().slice_recording_filter().limited() ? BST_CHECKED : BST_UNCHECKED);
	this->GetDlgItem(IDC_EDT_ACCUMULATION).EnableWindow(_controller.slice_processor().slice_recording_filter().limited() ? TRUE : FALSE);
	this->GetDlgItem(IDC_EDT_AVERAGING).EnableWindow(_controller.slice_processor().hardware_data_source().ethernet_data_source().channeler().signal_provider().model().signal_source().output().trigger_mode() == Ethernet::TriggerMode::None ? TRUE : FALSE);
	
	CUpDownCtrl(this->GetDlgItem(IDC_SPN_ACCUMULATION)).SetBuddy(this->GetDlgItem(IDC_EDT_ACCUMULATION));
	CUpDownCtrl(this->GetDlgItem(IDC_SPN_ACCUMULATION)).SetRange32(1, 1000);
	CUpDownCtrl(this->GetDlgItem(IDC_SPN_ACCUMULATION)).SetPos32(_controller.slice_processor().slice_recording_filter().limit());
	CUpDownCtrl(this->GetDlgItem(IDC_SPN_AVERAGING)).SetBuddy(this->GetDlgItem(IDC_EDT_AVERAGING));
	CUpDownCtrl(this->GetDlgItem(IDC_SPN_AVERAGING)).SetRange32(1, 128);
	CUpDownCtrl(this->GetDlgItem(IDC_SPN_AVERAGING)).SetPos32(_controller.slice_processor().slice_averaging_filter().amount());
	
	//CComboBoxEx(this->GetDlgItem(IDC_CMB_DATA_SOURCE)).AddItem(utility::load_string(IDS_DATA_SOURCE_ETHERNET).c_str(), 0, 0, 0, static_cast<std::uint32_t>(HardwareDataSourceMode::Ethernet));
	//CComboBoxEx(this->GetDlgItem(IDC_CMB_DATA_SOURCE)).AddItem(utility::load_string(IDS_DATA_SOURCE_SERIAL).c_str(), 0, 0, 0, static_cast<std::uint32_t>(HardwareDataSourceMode::SerialPort));
	//CComboBoxEx(this->GetDlgItem(IDC_CMB_DATA_SOURCE)).SetCurSel(_controller.slice_processor().hardware_data_source().mode() == HardwareDataSourceMode::Ethernet ? 0 : 1);
	
	this->DlgResize_Init(false, true, 0);
	
	return 0;
}

LRESULT SignalProcessorView::OnRecordInterrupted(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	_serial_panel.stop_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_ethernet_panel.stop_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_ethernet_delay_panel.stop_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_ethernet_delayer_panel.stop_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_command_handlers.at(IDC_BTN_RECORD) = &SignalProcessorView::OnBnClickedBtnRecordStart;
	this->CheckDlgButton(IDC_BTN_RECORD, BST_UNCHECKED);
	this->SetDlgItemTextW(IDC_BTN_RECORD, utility::load_string(IDS_START).c_str());
	return 0;
}

LRESULT SignalProcessorView::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	this->destroy_overlay_panels();
	return 0;
}

LRESULT SignalProcessorView::OnBnClickedBtnExperimentStart(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	SaveSignalsDialog dialog(*this);
	if (_controller.slice_processor().slice_recording_filter().trace().is_dirty())
	{
		switch (::AtlMessageBox(nullptr, IDS_DATA_UNSAVED, nullptr, MB_YESNOCANCEL))
		{
		case IDYES:
			switch (dialog.DoModal())
			{
			case IDOK:
				_controller.slice_processor().slice_recording_filter().trace().save(dialog.names());
				break;
			case IDCANCEL:
				return 0;
			}
		case IDNO:
			break;
		case IDCANCEL:
			return 0;
		}
	}
	_controller.start_experiment();
	return 0;
}

LRESULT SignalProcessorView::OnBnClickedBtnExperimentStop(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_controller.stop_experiment();
	return 0;
}

LRESULT SignalProcessorView::OnBnClickedBtnRecordStart(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	this->SetDlgItemTextW(IDC_BTN_RECORD, utility::load_string(IDS_STOP).c_str());
	_interrupt_required.clear();
	_controller.slice_processor().slice_recording_filter().reset_counter();
	_controller.slice_processor().slice_recording_filter().enable(true);
	_ethernet_delayer_panel.start_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_ethernet_delay_panel.start_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_ethernet_panel.start_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_serial_panel.start_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_command_handlers.at(IDC_BTN_RECORD) = &SignalProcessorView::OnBnClickedBtnRecordStop;
	return 0;
}

LRESULT SignalProcessorView::OnBnClickedBtnRecordStop(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_controller.slice_processor().slice_recording_filter().enable(false);
	_serial_panel.stop_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_ethernet_panel.stop_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_ethernet_delayer_panel.stop_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_ethernet_delay_panel.stop_recording(this->IsDlgButtonChecked(IDC_BTN_START) == BST_CHECKED);
	_command_handlers.at(IDC_BTN_RECORD) = &SignalProcessorView::OnBnClickedBtnRecordStart;
	this->GetDlgItem(IDC_BTN_RECORD).SetWindowTextW(utility::load_string(IDS_START).c_str());
	return 0;
}

LRESULT SignalProcessorView::OnCbnSelChangeCmbDataSourceEthernet(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	SaveSignalsDialog dialog(*this);
	if ( _controller.slice_processor().hardware_data_source().mode() == HardwareDataSourceMode::Ethernet ) return 0;
	if ( _controller.slice_processor().slice_recording_filter().trace().is_dirty() )
	{
		switch ( ::AtlMessageBox(nullptr, IDS_DATA_UNSAVED, nullptr, MB_YESNOCANCEL) )
		{
		case IDYES:
			switch ( dialog.DoModal() )
			{
			case IDOK:
				_controller.slice_processor().slice_recording_filter().trace().save(dialog.names());
				break;
			case IDCANCEL:
				utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_DATA_SOURCE)), static_cast<unsigned int>(_controller.slice_processor().hardware_data_source().mode()), -1);
				return 0;
			}
		case IDNO:
			_controller.reset();
			break;
		case IDCANCEL:
			utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_DATA_SOURCE)), static_cast<unsigned int>(_controller.slice_processor().hardware_data_source().mode()), -1);
			return 0;
		}
	}
	_serial_panel.ShowWindow(SW_HIDE);
	_ethernet_panel.ShowWindow(SW_SHOW);
	_ethernet_delayer_panel.ShowWindow(SW_SHOW);
	_ethernet_delay_panel.show(true);
	_controller.slice_processor().hardware_data_source().mode(HardwareDataSourceMode::Ethernet);
	return 0;
}

LRESULT SignalProcessorView::OnCbnSelChangeCmbDataSourceSerial(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	SaveSignalsDialog dialog(*this);
	if ( _controller.slice_processor().hardware_data_source().mode() == HardwareDataSourceMode::SerialPort ) return 0;
	if ( _controller.slice_processor().slice_recording_filter().trace().is_dirty() )
	{
		switch ( ::AtlMessageBox(nullptr, IDS_DATA_UNSAVED, nullptr, MB_YESNOCANCEL) )
		{
		case IDYES:
			switch ( dialog.DoModal() )
			{
			case IDOK:
				_controller.slice_processor().slice_recording_filter().trace().save(dialog.names());
				break;
			case IDCANCEL:
				utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_DATA_SOURCE)), static_cast<unsigned int>(_controller.slice_processor().hardware_data_source().mode()), -1);
				return 0;
			}
		case IDNO:
			_controller.reset();
			break;
		case IDCANCEL:
			utility::select_combo_box_item(CComboBoxEx(this->GetDlgItem(IDC_CMB_DATA_SOURCE)), static_cast<unsigned int>(_controller.slice_processor().hardware_data_source().mode()), -1);
			return 0;
		}
	}
	_serial_panel.ShowWindow(SW_SHOW);
	_ethernet_panel.ShowWindow(SW_HIDE);
	_ethernet_delayer_panel.ShowWindow(SW_HIDE);
	_ethernet_delay_panel.show(false);
	_controller.slice_processor().hardware_data_source().mode(HardwareDataSourceMode::SerialPort);
	return 0;
}

LRESULT SignalProcessorView::OnCbnSelChangeCmbDataSource(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	int selection = CComboBoxEx(this->GetDlgItem(IDC_CMB_DATA_SOURCE)).GetCurSel();
	unsigned int mode = static_cast<std::uint32_t>(CComboBoxEx(this->GetDlgItem(IDC_CMB_DATA_SOURCE)).GetItemData(selection));
	return (this->*_data_source_handlers.at(mode))(wNotifyCode, wID, hWndCtl, bHandled);
}

LRESULT SignalProcessorView::OnEnChangeEdtAveraging(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_controller.slice_processor().slice_averaging_filter().amount(std::max<std::size_t>(this->GetDlgItemInt(IDC_EDT_AVERAGING, nullptr, FALSE), 1));
	return 0;
}

LRESULT SignalProcessorView::OnEnChangeEdtAccumulation(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	_controller.slice_processor().slice_recording_filter().limit(this->GetDlgItemInt(IDC_EDT_ACCUMULATION));
	return 0;
}

LRESULT SignalProcessorView::OnBnClickedChkAccumulation(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	this->GetDlgItem(IDC_EDT_ACCUMULATION).EnableWindow(this->IsDlgButtonChecked(IDC_CHK_ACCUMULATION) == BST_CHECKED ? TRUE : FALSE);
	_controller.slice_processor().slice_recording_filter().limited(this->IsDlgButtonChecked(IDC_CHK_ACCUMULATION) == BST_CHECKED);
	return 0;
}






void SignalProcessorView::on_experiment_begin()
{
	_experiment_begin_callback();
	_serial_panel.start_scanning(this->IsDlgButtonChecked(IDC_BTN_RECORD) == BST_CHECKED);
	_ethernet_panel.start_scanning(this->IsDlgButtonChecked(IDC_BTN_RECORD) == BST_CHECKED);
	_ethernet_delayer_panel.start_scanning(this->IsDlgButtonChecked(IDC_BTN_RECORD) == BST_CHECKED);
	_ethernet_delay_panel.start_scanning(this->IsDlgButtonChecked(IDC_BTN_RECORD) == BST_CHECKED);
	_command_handlers.at(IDC_BTN_START) = &SignalProcessorView::OnBnClickedBtnExperimentStop;
	//this->GetDlgItem(IDC_CMB_DATA_SOURCE).EnableWindow(FALSE);
	this->GetDlgItem(IDC_BTN_START).SetWindowTextW(resources::string<IDS_STOP>().c_str());
}

void SignalProcessorView::on_experiment_end()
{
	//this->GetDlgItem(IDC_CMB_DATA_SOURCE).EnableWindow(TRUE);
	this->SetDlgItemTextW(IDC_BTN_START, resources::string<IDS_START>().c_str());
	_command_handlers.at(IDC_BTN_START) = &SignalProcessorView::OnBnClickedBtnExperimentStart;
	_ethernet_delayer_panel.stop_scanning(this->IsDlgButtonChecked(IDC_BTN_RECORD) == BST_CHECKED);
	_ethernet_delay_panel.stop_scanning(this->IsDlgButtonChecked(IDC_BTN_RECORD) == BST_CHECKED);
	_ethernet_panel.stop_scanning(this->IsDlgButtonChecked(IDC_BTN_RECORD) == BST_CHECKED);
	_serial_panel.stop_scanning(this->IsDlgButtonChecked(IDC_BTN_RECORD) == BST_CHECKED);
	_experiment_end_callback();
}

void SignalProcessorView::on_redraw()
{
	_redraw_required.test_and_set();
}

void SignalProcessorView::on_update()
{
	if ( _controller.slice_processor().slice_recording_filter().suspended() && !_interrupt_required.test_and_set() && this->PostMessageW(WM_RECORDINTERRUPTED) == FALSE )
		throw std::runtime_error("Cannot update window.");
}




const Ethernet::DataSourcePanel& SignalProcessorView::ethernet_panel() const
{
	return _ethernet_panel;
}

Ethernet::DataSourcePanel& SignalProcessorView::ethernet_panel()
{
	return _ethernet_panel;
}




void SignalProcessorView::experiment_begin_handler(const std::function<void()> &callback)
{
	_experiment_begin_callback = callback;
}

void SignalProcessorView::experiment_end_handler(const std::function<void()> &callback)
{
	_experiment_end_callback = callback;
}

void SignalProcessorView::slice_tracking_callback(std::function<void(float)> &&callback)
{
	_viewport.slice_tracking_callback(std::forward<std::function<void(float)>>(callback));
}

void SignalProcessorView::on_slice_averaging_amount(std::size_t amount)
{
	if (this->GetDlgItemInt(IDC_EDT_AVERAGING, nullptr, FALSE) != amount)
	{
		this->SetDlgItemInt(IDC_EDT_AVERAGING, amount, FALSE);
	}
}

void SignalProcessorView::on_ethernet_trigger_mode(Ethernet::TriggerMode mode)
{
	this->GetDlgItem(IDC_EDT_AVERAGING).EnableWindow(mode == Ethernet::TriggerMode::None ? TRUE : FALSE);
}

void SignalProcessorView::on_tick_length(std::float_t length)
{
//	_general.
}


//void SignalProcessorView::save_handler(const std::function<void()> &callback)
//{
//	_save_callback = callback;
//}

