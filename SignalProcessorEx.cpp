#include ".\StdAfx.hpp"
#include ".\SignalProcessorEx.hpp"
#include ".\Application\SignalProcessorApplication.hpp"

//"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'"

#include <tchar.h>
#include <vector>
#include <cassert>



int APIENTRY _tWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow )
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	
	{
		SignalProcessorApplication().run(nCmdShow);
	}
	_CrtDumpMemoryLeaks();
	return 0;
}

