#pragma once

#include "targetver.hpp"

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN

//#include <Windows.h>

#define _ATL_NO_COM_SUPPORT
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS
#define _WTL_NO_CSTRING
#define _SECURE_ATL TRUE
#define STRSAFE_LIB


#if defined (DEBUG)
	//#define USE_RANDOM_GENERATOR
#endif //defined (DEBUG)



// C RunTime Header Files

//#include <atlbase.h>
//#include <atlstr.h>


// TODO: reference additional headers your program requires here

