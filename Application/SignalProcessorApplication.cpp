#include "..\StdAfx.hpp"
#include "..\Application\SignalProcessorApplication.hpp"

#include <array>
#include <limits>

#include "..\WTL\atluser.h"



static const std::wstring _root = L"Software\\IRE NASU\\Oscilloscope Quadro Net";



SignalProcessorApplication::SignalProcessorApplication():
	_model(), _controller(_model), _frame(_controller) //_view(_controller)
{
}

SignalProcessorApplication::~SignalProcessorApplication()
{
}

void SignalProcessorApplication::run(int mode)
{
	this->startup(mode);
	this->execute();
	this->shutdown();
}


//const SignalProcessorView& SignalProcessorApplication::view() const
//{
//	return _view;
//}

const SignalProcessorFrame& SignalProcessorApplication::frame() const
{
	return _frame;
}

const SignalProcessorModel& SignalProcessorApplication::model() const
{
	return _model;
}

const SignalProcessorController& SignalProcessorApplication::controller() const
{
	return _controller;
}

//SignalProcessorView& SignalProcessorApplication::view()
//{
//	return _view;
//}

SignalProcessorFrame& SignalProcessorApplication::frame()
{
	return _frame;
}

SignalProcessorModel& SignalProcessorApplication::model()
{
	return _model;
}

SignalProcessorController& SignalProcessorApplication::controller()
{
	return _controller;
}

void SignalProcessorApplication::startup(int mode)
{
	_model.load(_root);
	_controller.load(_root);
	//_view.load(_root);
	_frame.load(_root);
	_frame.create(mode);
	//_view.create(mode);
}

void SignalProcessorApplication::execute()
{
	CAccelerator accelerators(::LoadAcceleratorsW(nullptr, MAKEINTRESOURCE(IDC_SIGNALPROCESSOREX)));
	for ( MSG msg = MSG(); ::GetMessageW(&msg, nullptr, 0, 0) != FALSE && msg.message != WM_QUIT; )
	{
		//if ( !accelerators.TranslateAcceleratorW(_view, &msg) && !_view.IsDialogMessageW(&msg) )
		if (_frame.IsWindow() && !accelerators.TranslateAcceleratorW(_frame, &msg) && !_frame.view().IsDialogMessageW(&msg) )
		{
			::TranslateMessage(&msg);
			::DispatchMessageW(&msg);
		}
	}
}

void SignalProcessorApplication::shutdown()
{
	//_view.save(_root);
	_frame.save(_root);
	_controller.save(_root);
	_model.save(_root);
}
