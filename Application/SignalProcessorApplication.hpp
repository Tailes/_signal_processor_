#pragma once
//#include "..\View\SignalProcessorView.hpp"
#include "..\View\SignalProcessorFrame.hpp"
#include "..\Model\SignalProcessorModel.hpp"
#include "..\Controller\SignalProcessorController.hpp"

#include <atlbase.h>

#include <mutex>



class SignalProcessorApplication: public CAtlExeModuleT<SignalProcessorApplication>
{
public:
	SignalProcessorApplication();
	~SignalProcessorApplication();
	
	void run(int mode);
	
	//const SignalProcessorView& view() const;
	const SignalProcessorFrame& frame() const;
	const SignalProcessorModel& model() const;
	const SignalProcessorController& controller() const;
	//SignalProcessorView& view();
	SignalProcessorFrame& frame();
	SignalProcessorModel& model();
	SignalProcessorController& controller();
	
private:
	SignalProcessorModel _model;
	SignalProcessorController _controller;
	SignalProcessorFrame _frame;
	//SignalProcessorView _view;
	
	void startup(int mode);
	void execute();
	void shutdown();
};
