#pragma once

#include <cstddef>

namespace OS
{
	class Handle
	{
	public:
		typedef void * system_handle_type;
		
	public:
		Handle();
		Handle(nullptr_t);
		Handle(Handle &source);
		Handle(Handle &&source);
		Handle(system_handle_type source);
		~Handle();
		
		operator bool() const;
		operator system_handle_type() const;
		Handle& operator=(nullptr_t);
		Handle& operator=(Handle &source);
		Handle& operator=(Handle &&source);
		
		bool operator == (system_handle_type handle) const;
		bool operator != (system_handle_type handle) const;
		
	protected:
		system_handle_type _handle;
	};
}