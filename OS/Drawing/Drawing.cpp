#include "..\..\StdAfx.hpp"
#include "..\..\Model\Data\Slice.hpp"
#include "..\..\Model\Data\Signal.hpp"
#include "..\..\Controller\Data\GridController.hpp"
#include "..\..\OS\Drawing\Color.hpp"
#include "..\..\OS\Drawing\Bitmap.hpp"
#include "..\..\OS\Drawing\Gradient.hpp"
#include "..\..\OS\Drawing\BitmapInfo.hpp"
#include "..\..\OS\Drawing\Drawing.hpp"


#include <iomanip>
#include <sstream>


#include <atlbase.h>

#include "..\..\WTL\atlapp.h"
#include "..\..\WTL\atlgdi.h"
#include "..\..\WTL\atlmisc.h"



static const unsigned char GridLinesHorizontal = 10;
static const unsigned char GridLinesVertical = 5;
static const unsigned char GridLinesWidth = 1;

static const unsigned char SignalLinesWidth = 1;

static const Color::rgba ColorBackground = Color::rgba(0);
static const Color::rgba ColorGrid = Color::rgba(0x0000ff00);
static const Color::rgba ColorText = Color::rgba(0x00ffffff);



namespace utility
{
	void measure(WTL::CDCT<false> &dc, const std::wstring &text, WTL::CSize &size)
	{
		if ( dc.GetTextExtent(text.c_str(), text.length(), &size) == FALSE )
			throw std::runtime_error("Cannot measure text.");
	}


	void draw_background(WTL::CDCT<false> &dc, const WTL::CRect &bounds)
	{
		CBrush brush;
		if ( brush.CreateSolidBrush(ColorBackground) == nullptr )
			throw std::runtime_error("Cannot create grid background brush.");
		if ( dc.FillRect(bounds, brush) == FALSE )
			throw std::runtime_error("Cannot fill grid background rectangle.");
	}


	void draw_grid(WTL::CDCT<false> &dc, const WTL::CRect &bounds)
	{
		int backgroundMode = dc.SetBkMode(TRANSPARENT);
		
		CPen solid, dash;
		if ( solid.CreatePen(PS_SOLID, GridLinesWidth, ColorGrid) == nullptr )
			throw std::runtime_error("Cannot create grid solid pen.");
		if ( dash.CreatePen(PS_DOT, GridLinesWidth, ColorGrid) == nullptr )
			throw std::runtime_error("Cannot create grid dash pen.");
		
		dash.Attach(dc.SelectPen(dash.Detach()));
		for ( unsigned int i = 0; i <= GridLinesHorizontal; ++i )
		{
			if ( dc.MoveTo(bounds.left, bounds.top + ::MulDiv(bounds.Height(), i, GridLinesHorizontal)) == FALSE )
				throw std::runtime_error("Cannot move drawing cursor while painting grid.");
			if ( dc.LineTo(bounds.right, bounds.top + ::MulDiv(bounds.Height(), i, GridLinesHorizontal)) == FALSE )
				throw std::runtime_error("Cannot draw line while painting grid.");
		}
		for ( unsigned int i = 0; i <= GridLinesVertical; ++i )
		{
			if ( dc.MoveTo(bounds.left + ::MulDiv(bounds.Width(), i, GridLinesVertical), bounds.top) == FALSE )
				throw std::runtime_error("Cannot move drawing cursor while painting grid.");
			if ( dc.LineTo(bounds.left + ::MulDiv(bounds.Width(), i, GridLinesVertical), bounds.bottom) == FALSE )
				throw std::runtime_error("Cannot draw line while painting grid.");
		}
		dash.Attach(dc.SelectPen(dash.Detach()));
		
		solid.Attach(dc.SelectPen(solid.Detach()));
		if ( dc.MoveTo(bounds.left, bounds.top + ::MulDiv(bounds.Height(), 1, 2)) == FALSE )
			throw std::runtime_error("Cannot move drawing cursor while painting grid.");
		if ( dc.LineTo(bounds.right, bounds.top + ::MulDiv(bounds.Height(), 1, 2)) == FALSE )
			throw std::runtime_error("Cannot draw line while painting grid.");
		
		CBrush brush;
		if ( brush.CreateSolidBrush(ColorGrid) == nullptr )
			throw std::runtime_error("Cannot create grid colored solid brush.");
		if ( dc.FrameRect(bounds, brush) == FALSE )
			throw std::runtime_error("Cannot draw grid frame rectangle.");
		solid.Attach(dc.SelectPen(solid.Detach()));
		
		dc.SetBkMode(backgroundMode);
	}


	void draw_uniform_grid(WTL::CDCT<false> &dc, const WTL::CRect &bounds, const GridController &grid)
	{
		int backgroundMode = dc.SetBkMode(TRANSPARENT);
		
		CPen solid, dash;
		if ( solid.CreatePen(PS_SOLID, GridLinesWidth, ColorGrid) == nullptr )
			throw std::runtime_error("Cannot create grid solid pen.");
		if ( dash.CreatePen(PS_DOT, GridLinesWidth, ColorGrid) == nullptr )
			throw std::runtime_error("Cannot create grid dash pen.");
		
		dash.Attach(dc.SelectPen(dash.Detach()));
		for ( unsigned int i = 0; i <= GridLinesHorizontal; ++i )
		{
			if ( dc.MoveTo(bounds.left, bounds.top + ::MulDiv(bounds.Height(), i, GridLinesHorizontal)) == FALSE )
				throw std::runtime_error("Cannot move drawing cursor while painting grid.");
			if ( dc.LineTo(bounds.right, bounds.top + ::MulDiv(bounds.Height(), i, GridLinesHorizontal)) == FALSE )
				throw std::runtime_error("Cannot draw line while painting grid.");
		}
		
		COLORREF text_color = dc.SetTextColor(ColorText);
		UINT text_alignment = dc.SetTextAlign(TA_BOTTOM | TA_CENTER);
		for ( float line = grid.first_line(), x = grid.part(line) * bounds.Width(); line < grid.last_line(); x = grid.part(++line) * bounds.Width() )
		{
			if ( dc.MoveTo(bounds.left + static_cast<int>(x), bounds.top) == FALSE )
				throw std::runtime_error("Cannot move drawing cursor while painting grid.");
			if ( dc.LineTo(bounds.left + static_cast<int>(x), bounds.bottom) == FALSE )
				throw std::runtime_error("Cannot draw line while painting grid.");
		}
		dc.SetTextAlign(text_alignment);
		dc.SetTextColor(text_color);
		dash.Attach(dc.SelectPen(dash.Detach()));
		
		solid.Attach(dc.SelectPen(solid.Detach()));
		if ( dc.MoveTo(bounds.left, bounds.top + ::MulDiv(bounds.Height(), 1, 2)) == FALSE )
			throw std::runtime_error("Cannot move drawing cursor while painting grid.");
		if ( dc.LineTo(bounds.right, bounds.top + ::MulDiv(bounds.Height(), 1, 2)) == FALSE )
			throw std::runtime_error("Cannot draw line while painting grid.");
		
		CBrush brush;
		if ( brush.CreateSolidBrush(ColorGrid) == nullptr )
			throw std::runtime_error("Cannot create grid colored solid brush.");
		if ( dc.FrameRect(bounds, brush) == FALSE )
			throw std::runtime_error("Cannot draw grid frame rectangle.");
		solid.Attach(dc.SelectPen(solid.Detach()));
		
		dc.SetBkMode(backgroundMode);
	}


	void draw_signal(WTL::CDCT<false> &dc, const WTL::CRect &bounds, const Signal &signal, const Color::rgba &color)
	{
		if ( signal.empty() ) return;
		
		CPen solid;
		if ( solid.CreatePen(PS_SOLID, SignalLinesWidth, color) == nullptr )
			throw std::runtime_error("Cannot create signal solid pen.");
		
		solid.Attach(dc.SelectPen(solid.Detach()));
		if ( dc.MoveTo(bounds.left, bounds.bottom - static_cast<unsigned long>(bounds.Height() * (signal.at(0) + 1.0)*0.5)) == FALSE )
			throw std::runtime_error("Cannot move drawing cursor while drawing signal.");
		for ( std::size_t i = 0; i < signal.size(); ++i )
			if ( dc.LineTo(bounds.left + ::MulDiv(bounds.Width(), i, signal.size() - 1), bounds.bottom - static_cast<unsigned long>(bounds.Height() * (signal.at(i) + 1.0)*0.5)) == FALSE )
				throw std::runtime_error("Cannot draw line while drawing signal.");
		solid.Attach(dc.SelectPen(solid.Detach()));
	}


	void draw_axis_labels(WTL::CDCT<false> &dc, const WTL::CRect &bounds, const GridController &grid)
	{
		std::wstringstream buffer;
		INT background = dc.SetBkMode(TRANSPARENT);
		COLORREF text_color = dc.SetTextColor(ColorText);
		UINT text_alignment = dc.SetTextAlign(TA_TOP | TA_CENTER);
		for ( float line = grid.first_line(), x = grid.part(line) * bounds.Width(); line < grid.last_line(); x = grid.part(++line) * bounds.Width() )
		{
			buffer << std::fixed << std::setw(5) << std::setprecision(2) << grid.label(line) << L"ns";
			if ( dc.TextOutW(bounds.left + static_cast<int>(x), bounds.top, buffer.str().c_str()) == FALSE )
				throw std::runtime_error("Cannot output axis label.");
			buffer.str(std::wstring());
		}
		dc.SetTextAlign(text_alignment);
		dc.SetTextColor(text_color);
		dc.SetBkMode(background);
	}


	void draw_split_bitmap(WTL::CDCT<false> &dc, const WTL::CRect &bounds, const Bitmap &image, const Gradient &palette, unsigned long splitter)
	{
		BitmapInfo info(image, palette);
		int sourcex, sourcey, sourcew, sourceh, targetx, targety, targeth, targetw;
		sourcex = std::max<int>(0, splitter - bounds.Width());
		sourcey = 0;
		sourcew = std::min<int>(bounds.Width(), splitter);
		sourceh = image.height();
		targetx = bounds.right - sourcew;
		targety = bounds.top;
		targetw = sourcew;
		targeth = bounds.Height();
		dc.StretchDIBits(targetx, targety, targetw, targeth, sourcex, sourcey, sourcew, sourceh, image.bytes(), info.info(), DIB_RGB_COLORS, SRCCOPY);
		if ( sourcew == bounds.Width() ) return;
		sourcex = image.width() - std::min<int>( bounds.Width(), image.width() ) + splitter;
		sourcey = 0;
		sourcew = std::min<int>(bounds.Width(), image.width() ) - splitter;
		sourceh = image.height();
		targetx = std::max<int>(0, bounds.right - std::min<int>(bounds.Width(), splitter) - sourcew);
		targety = bounds.top;
		targetw = std::min<int>(sourcew, bounds.Width() - std::min<int>(bounds.Width(), splitter));
		targeth = bounds.Height();
		dc.StretchDIBits(targetx, targety, targetw, targeth, sourcex, sourcey, sourcew, sourceh, image.bytes(), info.info(), DIB_RGB_COLORS, SRCCOPY);
	}

	void draw_icon(WTL::CDCT<false> &dc, const WTL::CRect &bounds, const WTL::CIconT<true> &icon)
	{
		int radius = static_cast<int>(0.45 * std::min<int>(bounds.Width(), bounds.Height()));
		dc.DrawIconEx(-CPoint(radius, radius) + bounds.CenterPoint(), icon, CSize(2*radius, 2*radius), 0, nullptr, DI_NORMAL);
	}
}