#pragma once

#include <cstdint>


namespace Color
{
#pragma pack(push, 1)
class rgb
{
public:
	rgb();
	rgb(std::uint32_t value);
	
	std::uint8_t red() const;
	std::uint8_t blue() const;
	std::uint8_t green() const;
	
	void red(std::uint8_t value);
	void blue(std::uint8_t value);
	void green(std::uint8_t value);
	
private:
	std::uint8_t _red;
	std::uint8_t _green;
	std::uint8_t _blue;
};
#pragma pack(pop)



#pragma pack(push, 1)
class rgba: public rgb
{
public:
	rgba();
	rgba(std::uint32_t value);
	
	operator std::uint32_t() const;
	operator std::uint32_t&();
	
	std::uint8_t alpha() const;
	
	void alpha(std::uint8_t value);
	
private:
	std::uint8_t _alpha;
};
#pragma pack(pop)


#pragma pack(push, 1)
class bitmap_rgba
{
public:
	bitmap_rgba();
	bitmap_rgba(std::uint32_t value);
	
	operator std::uint32_t() const;
	operator std::uint32_t&();
	
	std::uint8_t red() const;
	std::uint8_t blue() const;
	std::uint8_t green() const;
	std::uint8_t alpha() const;
	
	void red(std::uint8_t value);
	void blue(std::uint8_t value);
	void green(std::uint8_t value);
	void alpha(std::uint8_t value);
	
private:
	std::uint8_t _blue;
	std::uint8_t _green;
	std::uint8_t _red;
	std::uint8_t _alpha;
};
#pragma pack(pop)
}