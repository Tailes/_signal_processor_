#include "..\..\StdAfx.hpp"
#include "..\..\OS\Drawing\Color.hpp"
#include "..\..\OS\Drawing\Bitmap.hpp"
#include "..\..\OS\Drawing\Gradient.hpp"
#include "..\..\OS\Drawing\BitmapInfo.hpp"


#include <algorithm>



static const RGBQUAD& convert(const Color::bitmap_rgba &value);



BitmapInfo::BitmapInfo(const Bitmap &picture, const Gradient &palette)
{
	_header.biSize = sizeof(BITMAPINFOHEADER);
	_header.biWidth = picture.width();
	_header.biHeight = picture.height();
	_header.biPlanes = 1;
	_header.biBitCount = 8;
	_header.biCompression = BI_RGB;
	_header.biSizeImage = 0;
	_header.biXPelsPerMeter = 0;
	_header.biYPelsPerMeter = 0;
	_header.biClrUsed = 0;
	_header.biClrImportant = 0;
	std::transform(palette.begin(), palette.end(), _palette, convert);
}

const BITMAPINFO* BitmapInfo::info() const
{
	return reinterpret_cast<const BITMAPINFO*>(this);
}

const BITMAPINFOHEADER* BitmapInfo::header() const
{
	return &_header;
}



const RGBQUAD& convert(const Color::bitmap_rgba &value)
{
	return reinterpret_cast<const RGBQUAD &>(value);
}
