#pragma once

class Slice;
class Signal;
class Bitmap;
class Gradient;
class GridController;

namespace Color{class rgba;}

namespace WTL { class CRect; class CSize; }
namespace WTL { template<bool> class CDCT; }
namespace WTL { template<bool> class CIconT; }


namespace utility
{
	void draw_grid(WTL::CDCT<false> &dc, const WTL::CRect &bounds);
	void draw_icon(WTL::CDCT<false> &dc, const WTL::CRect &bounds, const WTL::CIconT<true> &icon);
	void draw_signal(WTL::CDCT<false> &dc, const WTL::CRect &bounds, const Signal &signal, const Color::rgba &color);
	void draw_background(WTL::CDCT<false> &dc, const WTL::CRect &bounds);
	void draw_axis_labels(WTL::CDCT<false> &dc, const WTL::CRect &bounds, const GridController &grid);
	void draw_split_bitmap(WTL::CDCT<false> &dc, const WTL::CRect &bounds, const Bitmap &image, const Gradient &palette, unsigned long splitter);
	void draw_uniform_grid(WTL::CDCT<false> &dc, const WTL::CRect &bounds, const GridController &grid);
}

//
//class SignalPlotter;
//class BitmapPlotter;
//class GenericPlotter;
//
//
//
//class GenericPlotter
//{
//protected:
//	GenericPlotter(WTL::CDCT<false> &dc, const WTL::CRect &bounds);
//	
//	void background();
//	void grid();
//	
//protected:
//	const WTL::CRect &bounds;
//	WTL::CDCT<false> &_dc;
//};
//
//
//class SignalPlotter:
//	private GenericPlotter
//{
//public:
//	SignalPlotter(WTL::CDCT<false> &dc, const WTL::CRect &bounds, const std::vector<scan_t> &signal, const Color::rgba &color);
//
//};