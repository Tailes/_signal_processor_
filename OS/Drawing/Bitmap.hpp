#pragma once
#include "..\..\OS\Drawing\Color.hpp"

#include <array>
#include <vector>
#include <cstdint>




class Bitmap
{
public:
	Bitmap();
	
	void clear();
	
	void size(unsigned long width, unsigned long height);
	void width(unsigned long width);
	void pixel(unsigned long column, unsigned long row, std::uint8_t value);
	void height(unsigned long height);
	
	unsigned long width() const;
	unsigned long height() const;
	const std::uint8_t * bytes() const;
	
	bool empty() const;
	
private:
	std::vector<std::uint8_t> _pixels;
	unsigned long _height;
	unsigned long _width;
};

