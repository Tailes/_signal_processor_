#include "..\..\StdAfx.hpp"
#include "..\..\Utility\TypeArithmetics.hpp"
#include "..\..\OS\Drawing\Color.hpp"



namespace Color
{
rgb::rgb():
	_red(0), _green(0), _blue(0)
{
}

rgb::rgb(std::uint32_t value)
{
	_red = utility::get_low_byte(utility::get_low_word(value));
	_green = utility::get_high_byte(utility::get_low_word(value));
	_blue = utility::get_low_byte(utility::get_high_word(value));
}

std::uint8_t rgb::red() const
{
	return _red;
}


std::uint8_t rgb::blue() const
{
	return _blue;
}

std::uint8_t rgb::green() const
{
	return _green;
}


void rgb::red(std::uint8_t value)
{
	_red = value;
}

void rgb::blue(std::uint8_t value)
{
	_blue = value;
}

void rgb::green(std::uint8_t value)
{
	_green = value;
}




rgba::rgba():
	_alpha(0)
{
}

rgba::rgba(std::uint32_t value): rgb(value)
{
	_alpha = utility::get_high_byte(utility::get_high_word(value));
}

rgba::operator std::uint32_t() const
{
	return *reinterpret_cast<const std::uint32_t*>(this);
}

rgba::operator std::uint32_t&()
{
	return *reinterpret_cast<std::uint32_t*>(this);
}

std::uint8_t rgba::alpha() const
{
	return _alpha;
}

void rgba::alpha(std::uint8_t value)
{
	_alpha = value;
}




bitmap_rgba::bitmap_rgba():
	_red(0), _green(0), _blue(0), _alpha(0)
{
}

bitmap_rgba::bitmap_rgba(std::uint32_t value)
{
	_blue = utility::get_low_byte(utility::get_low_word(value));
	_green = utility::get_high_byte(utility::get_low_word(value));
	_red = utility::get_low_byte(utility::get_high_word(value));
	_alpha = utility::get_high_byte(utility::get_high_word(value));
}

bitmap_rgba::operator std::uint32_t() const
{
	return *reinterpret_cast<const std::uint32_t*>(this);
}

bitmap_rgba::operator std::uint32_t&()
{
	return *reinterpret_cast<std::uint32_t*>(this);
}


std::uint8_t bitmap_rgba::red() const
{
	return _red;
}

std::uint8_t bitmap_rgba::blue() const
{
	return _blue;
}

std::uint8_t bitmap_rgba::green() const
{
	return _green;
}

std::uint8_t bitmap_rgba::alpha() const
{
	return _alpha;
}

void bitmap_rgba::red(std::uint8_t value)
{
	_red = value;
}

void bitmap_rgba::blue(std::uint8_t value)
{
	_blue = value;
}

void bitmap_rgba::green(std::uint8_t value)
{
	_green = value;
}

void bitmap_rgba::alpha(std::uint8_t value)
{
	_alpha = value;
}
}
