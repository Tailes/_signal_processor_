#include "..\..\StdAfx.hpp"
#include "..\..\OS\Drawing\Gradient.hpp"



void Gradient::fill(Color::rgba begin, Color::rgba end)
{
	for ( std::size_t i = 0; i < this->size(); ++i )
	{
		this->at(i).red(static_cast<uint8_t>(begin.red() + i*(end.red() - begin.red())/this->size()));
		this->at(i).blue(static_cast<uint8_t>(begin.blue() + i*(end.blue() - begin.blue())/this->size()));
		this->at(i).green(static_cast<uint8_t>(begin.green() + i*(end.green() - begin.green())/this->size()));
		this->at(i).alpha(static_cast<uint8_t>(begin.alpha() + i*(end.alpha() - begin.alpha())/this->size()));
	}
}