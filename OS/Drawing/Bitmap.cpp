#include "..\..\StdAfx.hpp"
#include "..\..\OS\Drawing\Bitmap.hpp"
#include "..\..\Utility\TypeArithmetics.hpp"



Bitmap::Bitmap():
	_width(0), _height(0)
{
}

void Bitmap::clear()
{
	_pixels.assign(_pixels.size(), 0);
}

void Bitmap::size(unsigned long width, unsigned long height)
{
	_pixels.resize(height * utility::align(width, (1 << sizeof(std::uint32_t)) - 1));
	_pixels.assign(_pixels.size(), 0);
	_height = height;
	_width = width;
}

void Bitmap::width(unsigned long width)
{
	_pixels.resize(_height * utility::align(width, (1 << sizeof(std::uint32_t)) - 1));
	_pixels.assign(_pixels.size(), 0);
	_width = width;
}

void Bitmap::height(unsigned long height)
{
	_pixels.resize(height * utility::align(_width, (1 << sizeof(std::uint32_t)) - 1));
	_pixels.assign(_pixels.size(), 0);
	_height = height;
}

void Bitmap::pixel(unsigned long column, unsigned long row, std::uint8_t value)
{
	_pixels.at(utility::align(_width, (1 << sizeof(std::uint32_t)) - 1) * row + column) = value;
}

const unsigned char * Bitmap::bytes() const
{
	return _pixels.data();
}

unsigned long Bitmap::width() const
{
	return _width;
}

unsigned long Bitmap::height() const
{
	return _height;
}

bool Bitmap::empty() const
{
	return _pixels.empty();
}
