#pragma once

#include "..\..\OS\Drawing\Color.hpp"

#include <array>


static const std::size_t PaletteSize = 256;



class Gradient: public std::array<Color::bitmap_rgba, PaletteSize>
{
public:
	void fill(Color::rgba begin, Color::rgba end);
};

