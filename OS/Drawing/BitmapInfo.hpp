#pragma once

#include <Windows.h>



class Bitmap;
class Gradient;



class BitmapInfo
{
public:
	BitmapInfo(const Bitmap &picture, const Gradient &palette);
	
	const BITMAPINFO* info() const;
	const BITMAPINFOHEADER* header() const;
	
private:
	BITMAPINFOHEADER _header;
	RGBQUAD _palette[256];
};

