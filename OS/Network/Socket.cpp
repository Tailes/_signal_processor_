#include "..\..\StdAfx.hpp"
#include "..\..\OS\Network\Socket.hpp"

#include <type_traits>
#include <stdexcept>
#include <cassert>

#include <WinSock2.h>



static_assert(std::is_same<std::uintptr_t, SOCKET>::value, "SOCKET type is incompatible with std::uintptr_t.");



namespace OS
{
	Socket::Socket():
		_handle(INVALID_SOCKET)
	{
	}

	Socket::Socket(int family, int type, int protocol) :
		_handle(::socket(family, type, protocol))
	{
		assert(_handle != INVALID_SOCKET);
	}

	Socket::~Socket()
	{
		if ( _handle != INVALID_SOCKET )
		{
			::shutdown(_handle, SD_BOTH);
			::closesocket(_handle);
		}
	}

	Socket& Socket::operator = (OS::Socket &source)
	{
		_handle = source._handle;
		source._handle = INVALID_SOCKET;
		return *this;
	}

	void Socket::create(int family, int type, int protocol)
	{
		assert(_handle == INVALID_SOCKET);
		_handle = ::socket(family, type, protocol);
		if ( _handle == INVALID_SOCKET )
			throw std::runtime_error("Cannot open socket.");
	}

	void Socket::configure(int level, int name, void* data, int length)
	{
		assert(_handle != INVALID_SOCKET);
		if ( ::setsockopt(_handle, level, name, reinterpret_cast<const char *>(data), length) == SOCKET_ERROR )
			throw std::runtime_error("Cannot configure socket.");
	}

	void Socket::connect(const ::sockaddr *address, int length)
	{
		assert(_handle != INVALID_SOCKET);
		if ( ::connect(_handle, address, length) == SOCKET_ERROR )
			throw std::runtime_error("Cannot connect socket.");
	}

	void Socket::bind(const ::sockaddr *address, int length)
	{
		assert(_handle != INVALID_SOCKET);
		if ( ::bind(_handle, address, length) == SOCKET_ERROR )
			throw std::runtime_error("Cannot bind socket.");
	}

	void Socket::receive(void *data, long count)
	{
		assert(_handle != INVALID_SOCKET);
		for ( int received = 0, piece = 0; received < count; received += piece )
			if ( ((piece = ::recv(_handle, reinterpret_cast<char*>(data)+received, count - received, 0)) == SOCKET_ERROR) || !piece )
				throw std::runtime_error("Cannot receive data from socket.");
	}

	void Socket::send(const void *data, long count)
	{
		assert(_handle != INVALID_SOCKET);
		for ( int sent = 0, piece = 0; sent < count; sent += piece )
			if ( ((piece = ::send(_handle, reinterpret_cast<const char*>(data)+sent, count - sent, 0)) == SOCKET_ERROR) || !piece )
				throw std::runtime_error("Cannot send data into socket.");
	}

	void Socket::shutdown(int direction)
	{
		assert(_handle != INVALID_SOCKET);
		if ( ::shutdown(_handle, direction) == SOCKET_ERROR )
			throw std::runtime_error("Cannot shutdown socket.");
	}

	void Socket::close()
	{
		assert(_handle != INVALID_SOCKET);
		if ( ::closesocket(_handle) == SOCKET_ERROR )
			throw std::runtime_error("Cannot close socket.");
		_handle = INVALID_SOCKET;
	}

	void Socket::swap(OS::Socket &socket)
	{
		std::swap(_handle, socket._handle);
	}
}