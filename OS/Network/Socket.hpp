#pragma once
#include <cstdint>


struct sockaddr;


namespace OS
{
	class Socket
	{
	public:
		Socket();
		Socket(int family, int type, int protocol);
		~Socket();
		
		OS::Socket& operator = (OS::Socket &source);
		
		void create(int family, int type, int protocol);
		void configure(int level, int name, void* data, int length);
		void connect(const ::sockaddr *address, int length);
		void bind(const ::sockaddr *address, int length);
		void receive(void *data, long count);
		void send(const void *data, long count);
		void shutdown(int direction);
		void close();
		void swap(OS::Socket &socket);
		
	private:
		std::uintptr_t _handle;
	};
}