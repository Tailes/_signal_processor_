#include "..\..\StdAfx.hpp"
#include "..\..\OS\Network\SocketLibrary.hpp"
#include "..\..\Utility\TypeArithmetics.hpp"

#include <stdexcept>

#include <WinSock2.h>



namespace OS
{
	SocketLibrary::SocketLibrary(int major, int minor):
		_data(new ::WSAData())
	{
		if ( 0 != ::WSAStartup(utility::make_word(minor, major), _data.get()) )
			throw std::runtime_error("Cannot initialize Windows Socket Library.");
	}

	SocketLibrary::~SocketLibrary()
	{
		::WSACleanup();
	}
}