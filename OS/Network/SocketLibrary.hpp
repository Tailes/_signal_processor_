#pragma once

#include <memory>


struct WSAData;


namespace OS
{
	class SocketLibrary
	{
	public:
		SocketLibrary(int major = 1, int minor = 1);
		~SocketLibrary();
		
	private:
		std::unique_ptr<WSAData> _data;
	};
}