#include "..\stdafx.hpp"
#include "..\OS\Handle.hpp"

#include <utility>

#include <Windows.h>




namespace OS
{
	Handle::Handle(): _handle()
	{
	}

	Handle::Handle(nullptr_t) : _handle()
	{
	}

	Handle::Handle(Handle &source) : _handle(system_handle_type())
	{
		std::swap(_handle, source._handle);
	}

	Handle::Handle(Handle &&source) : _handle(std::move(source._handle))
	{
		source._handle = system_handle_type();
	}

	Handle::Handle(system_handle_type source) : _handle(source)
	{
	}

	Handle::~Handle()
	{
		if ( _handle != system_handle_type() && _handle != INVALID_HANDLE_VALUE )
			::CloseHandle(_handle);
	}

	Handle::operator bool() const
	{
		return _handle != system_handle_type() && _handle != INVALID_HANDLE_VALUE;
	}

	Handle::operator system_handle_type() const
	{
		return _handle;
	}

	Handle& Handle::operator=(nullptr_t)
	{
		if ( _handle != system_handle_type() )
			::CloseHandle(_handle);
		_handle = system_handle_type();
		return *this;
	}

	Handle& Handle::operator=(Handle &source)
	{
		std::swap(_handle, source._handle);
		return *this;
	}

	Handle& Handle::operator=(Handle &&source)
	{
		std::swap(_handle, source._handle);
		return *this;
	}

	bool Handle::operator == (system_handle_type handle) const
	{
		return _handle == handle;
	}

	bool Handle::operator != (system_handle_type handle) const
	{
		return _handle != handle;
	}
}